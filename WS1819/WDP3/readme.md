# WDP Project von Schaubmayr Lukas (S1710307032)
* Project-Name: Infowebsite 1. Weltkrieg
* Project-Typ: Website
* Gruppenproject: Nein
* Externe JS/CSS Bibliotheken: -
* Zeitaufwand (h): 45
* Beschreibung: Infosite über den ersten Weltkrieg, Mittelmächte, Entente, + Quiz, Gästebuch und Kontaktseite
* ZusatzInformation: Die Website ist auf http://ambientc.at/tmpWeb/ zum testen verfügbar