<?php
   include('../session.php');
   include('../config.php');

 if($_SERVER["REQUEST_METHOD"] == "POST") {
     $m_dateTime = gmdate("U"); 
     if(isset($_POST['bt_set']) && $login_eier > 0)
     {

        $date1 = $_POST['date'];
        $m_white = mysqli_real_escape_string($db,$_POST['white']); 
        $m_green = mysqli_real_escape_string($db,$_POST['green']);
        $m_brown = mysqli_real_escape_string($db,$_POST['brown']);
        $sql = "INSERT INTO eier(fk_id_user, id_inputTime,color_white,color_brown,color_green,`timestamp`) VALUES($login_userID,$m_dateTime,$m_white,$m_green,$m_brown,'$date1')";
        $result = mysqli_query($db,$sql);

        if($result) {
            echo '<script language="javascript">';
            echo 'alert("Einfügen erfolgreich")';
			echo '</script>';
			$m_white = "0"; 
			$m_green = "0";
			$m_brown = "0";
        }else {
			echo '<script language="javascript">';
            echo 'alert("Einfügen fehlgeschlagen")';
            echo '</script>';
        }
     }
   }
?>
<html>
<head>
        <title>Eier</title>
          <link rel="stylesheet" type="text/css" href="style.css">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    </head>
    <body>
        <h1 class="headLine">Eier einfügen</h1>
        <div class="mainList">
            <form class="container" action = "" method = "post" >
                <label class="slimWideButton loginInput">Datum
                    <input class="loginInput" type="date" name="date" <?php echo('value="'.date('Y-m-d').'"')?>>
                </label>
                <label class="slimWideButton loginInput">Weiß
                    <input class="loginInput" type="number" placeholder="0" name="white" <?php echo('value="'.(isset($m_white)?$m_white:0).'"')?>>
                </label>
                <label class="slimWideButton loginInput">Grün
                    <input class="loginInput" type="number" placeholder="0" name="green" <?php echo('value="'.(isset($m_green)?$m_green:0).'"')?>>
                </label>
                <label class="slimWideButton loginInput">Braun
                    <input class="loginInput" type="number" placeholder="0" name="brown" <?php echo('value="'.(isset($m_brown)?$m_brown:0).'"')?>>
                </label>
                <button  <?php if($login_eier > 0) {?> name="bt_set" <?php } ?> class="mainButton" type="submit">Einfügen</button>
            </form>
			<br>
			<button class="backbutton" onclick="window.location.href='../eier.php'">Zurück</button>
        </div>    
    </body>
</html>

