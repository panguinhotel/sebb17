(*Stack of Inteer values implemented as abstractdata structure *)
(*By default, the Stack is empty*)
unit StackADS2;
	
interface

	(*initializes the stack*)
	Procedure Init;

	(* resets the stack. Stack can be reused afterwards *)
	Procedure Done;

	(* Pushes a value onto the stack*)
	(* In value : value that will be pushed on the stack*)
	(* Out ok : False in case of any errors; TRUE otherwise
		IF false, the value will has not been pushed onto the stack *)
	PROCEDURE Push(value:Integer; Var ok : Boolean);

	(* Pops the most recently pushed value from the stack *)
	(* Out value : value popped from the stack. *)
	(* Out ok : False in case of any errors (e.g. stack is empty)
		In this case, the returned value mustn't be used *)
	PROCEDURE Pop(Var value:Integer; Var ok : Boolean);

	(* Checks if stack is currently empty*)
	(* Returns : True if stack is empty; False otherwise*)
	Function IsEmpty: Boolean;

implementation
	TYPE
		Node = ^NodeRec;
		NodeRec = Record
			value: Integer;
			next : Node;
		End;

	Var
		top : Node;
	
	PROCEDURE Init;
	BEGIN
		top:= NIL;
	END; (* Init *)

	PROCEDURE Done;
	Var
		next : Node;
	BEGIN
		While top <> NIL Do Begin
			next := top^.next;
			Dispose(top);
			top := next;
		End;
		top := NIL;
	END; (* Done *)

	PROCEDURE Push(value:Integer; Var ok : Boolean);
	Var
		n : Node;
	BEGIN
		New(n);
		ok := n <> NIL;
		IF ok Then Begin
			n^.value := value;
			n^.next := top;
			top := n;
		END;
	END; (* Push *)

	PROCEDURE Pop(Var value:Integer; Var ok : Boolean);
	Var
		n: Node;
	BEGIN
		IF not IsEmpty Then Begin
			value := top^.value;
			n := top^.next;
			Dispose(top);
			top := n;
			ok := true;
		End ELSE BEGIN
			ok := False;
		END;
	END; (* Pop *)

	Function IsEmpty: Boolean;
	Begin
		IsEmpty := top = NIL;
	END;
end.