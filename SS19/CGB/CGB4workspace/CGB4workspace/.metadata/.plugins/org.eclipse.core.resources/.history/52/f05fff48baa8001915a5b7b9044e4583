import ij.*;
import ij.plugin.Concatenator;
import ij.plugin.filter.PlugInFilter;
import ij.process.*;

import java.awt.*;
import java.util.Arrays;
import java.util.Collections;

import ij.gui.GenericDialog;
import ij.gui.Roi;



public class S1710307032_SCHAUBMAYR_UE8_ implements PlugInFilter {

	public static int BG_VAL = 255;
	public static int FG_VAL = 0;

	public int setup(String arg, ImagePlus imp) {
		if (arg.equals("about"))
		{showAbout(); return DONE;}
		return DOES_8G+DOES_STACKS+SUPPORTS_MASKING;
	} //setup



	/**
	 * 
	 * @param inImg - input image
	 * @param width 
	 * @param height
	 * @param transX - translation in x-direction
	 * @param transY - translation in y-direction
	 * @param rotAngle - rotation angle in degrees
	 * @return transformed image
	 */
	
	public int[][] transformImg(int[][] inImg, int width, int height, 
			double transX, double transY, double rotAngle, int destWidth, int destHeight) 
	{
		return transformImg(inImg,width,height,transX,transY,rotAngle,destWidth,destHeight,new Roi(new Rectangle(0,0,destWidth,destHeight)));
	}
	
	public int[][] transformImg(int[][] inImg, int width, int height, 
			double transX, double transY, double rotAngle) 
	{
		return transformImg(inImg,width,height,transX,transY,rotAngle,width,height);
	}

	/**
	 * 
	 * @param inImg - input image
	 * @param width 
	 * @param height
	 * @param transX - translation in x-direction
	 * @param transY - translation in y-direction
	 * @param rotAngle - rotation angle in degrees
	 * @return transformed image
	 */
	public int[][] transformImg(int[][] inImg, int width, int height, 
			double transX, double transY, double rotAngle, 
			int destWidth, int destHeight, Roi region) {

		int[][] tmpImg = new int[destWidth][destHeight];
		for(int i = 0; i < destWidth; ++i)
		{
			for(int k = 0; k < destHeight ; ++k)
			{
				if(i < width && k < height)
					tmpImg[i][k] = inImg[i][k];
				else
					tmpImg[i][k] = FG_VAL;
			}
		}

		//apply backward mapping
		int[][] resultImg = new int[region.getBounds().width][region.getBounds().height];

		//convert from degree to rad
		double rotAngleRad = -rotAngle * Math.PI / 180.0;
		double cosTheta = Math.cos(rotAngleRad);
		double sinTheta = Math.sin(rotAngleRad);

		double halfWidth = destWidth / 2.0;
		double halfHeight = destHeight / 2.0;


		//apply backward mapping from result imgae coordinates in input image are calculated
		for(int x = 0; x < region.getBounds().width; x++) {
			for(int y = 0; y < region.getBounds().height; y++) {
				double posX = region.getBounds().x+x;
				double posY = region.getBounds().y+y;

				//step1 move to center
				posX -= halfWidth;
				posY -= halfHeight;


				//step2 rotate
				double rotatedX = posX * cosTheta + posY * sinTheta;
				double rotatedY = -posX * sinTheta + posY * cosTheta;
				posX = rotatedX;
				posY = rotatedY;

				//step3 move back from center
				posX += halfWidth;
				posY += halfHeight;


				//step 4 translate
				posX -= transX;
				posY -= transY;

				//step5 apply value using NN interpoaltion
				//int finalVal = getNNinterpolatedValue(posX, posY, destWidth, destHeight, tmpImg);
				int finalVal = getBilinearInterpolatedValue(posX,posY,destWidth,destHeight, tmpImg);

				resultImg[x][y] = finalVal;
			}
		}


		return resultImg;
	}

	/**
	 * 
	 * @param refImg - static reference image
	 * @param testImg - moving image getting transformed
	 * @param width
	 * @param height
	 * @return error metric
	 */
	public double getImgDiffSSE(int[][] refImg, int[][] testImg, int width, int height) {
		return getImgDiffSSE(refImg,testImg,new Roi(0,0,width,height));
	}
	
	public double getImgDiffSSE(int[][] refImg, int[][] testImg, Roi region) {
		double totalError = 0.0;

		for(int x = 0; x < region.getBounds().width; x++) {
			for(int y = 0; y < region.getBounds().height; y++) {
				int diff = refImg[x+region.getBounds().x][y+region.getBounds().y] - testImg[x][y];
				totalError += diff * diff;
			} 
		}

		return totalError;
	}


	/**
	 * 
	 * @param xIdx - double x-image coordinate
	 * @param yIdx - double y-image coordinate
	 * @param width
	 * @param height
	 * @param img
	 * @return
	 */
	public static int getNNinterpolatedValue(double xIdx, double yIdx, int width, int height, int[][] img) {
		//just round the coordinates
		int xCoord = (int) (xIdx + 0.5);
		int yCoord = (int) (yIdx + 0.5);

		if(xCoord >= 0 && xCoord < width && yCoord >= 0 && yCoord < height) {
			return img[xCoord][yCoord];
		}


		return FG_VAL; // BLack BG, ATTENTION: return the matching outside value.return the value 
		//best matching the background of your image!!!
	}

	public static int getBilinearInterpolatedValue(double xIdx, double yIdx, int width, int height, int[][] img)
	{
		int lowX = (int) Math.floor(xIdx);
		int highX = (int) Math.ceil(xIdx);
		int lowY = (int) Math.floor(yIdx);
		int highY = (int) Math.ceil(yIdx);

		if(lowX < 0 && highX >=0)
			lowX = 0;
		
		if(highX >= width && lowX < width)
			highX = width-1;
		
		if(lowY < 0 && highY >=0)
			lowY = 0;
		
		if(highY >= height && lowY < height)
			highY = height-1;
		
		
		
		double leftValue = 0;
		double rightValue = 0;
		
		if(lowY == highY)
		{
			leftValue = img[lowX][lowY];
			rightValue = img[highX][lowY];
		}
		
		
	
		

		if(lowX >= 0 && lowX < width && highX < width && highX >= 0 && lowY >= 0 && lowY < height && highY < height && highY >= 0 )
		{
			double val1 = (highY-yIdx)/(highY-lowY)*img[lowX][lowY] + (yIdx-lowY)/(highY-lowY)*img[lowX][highY];
			double val2 = (highY-yIdx)/(highY-lowY)*img[highX][lowY] + (yIdx-lowY)/(highY-lowY)*img[highX][highY];

			return (int) ((highX-xIdx)/(highX-lowX)*val1 + (xIdx-lowX)/(highX-lowX)*val2);
		}

		return FG_VAL;

	}

	/**
	 * method for automated registration utilizing 11x11x11 permutations for the transformation parameters
	 * 
	 * @param refImg
	 * @param testImg
	 * @param width
	 * @param height
	 * @return
	 */
	public int[][] getRegisteredImage(int[][] refImg, int[][] testImg, int width,int height) {
		return getRegisteredImage(refImg, testImg, width, height, new Roi(0,0,width,height));
	}
	
	public int[][] getRegisteredImage(int[][] refImg, int[][] testImg, int width, int height,Roi region) {
		//global best solution
		double bestTx = 0.0;
		double bestTy = 0.0;
		double bestRot = 0.0;
		double bestError = getImgDiffSSE(refImg, testImg, region);

		double stepSize = 2.0;
		int stepsPerSide= 10; //i.e. radius ==> 11 iterations per dimension, 11^3 == 1331 in total 

		//6.2. c) use one for loop with 5 iterations there reducing the stepSize incrementally
		for(int i = 0; i < 10; ++i)
		{
			double LocalbestTx = 0.0;
			double LocalbestTy = 0.0;
			double LocalbestRot = 0.0;
			double LocalbestError = getImgDiffSSE(refImg, testImg,region);
			
			//first check all 1331 permutations
			for(int x = -stepsPerSide; x <= stepsPerSide; x++) { //11 steps, r=5
				for(int y = -stepsPerSide; y <= stepsPerSide; y++) { //11 steps, r=5
					for(int r = -stepsPerSide; r <= stepsPerSide; r++) { //11 steps, r=5 for ROT
						double tXtoUse = x * stepSize; 
						double tYtoUse = y * stepSize; 
						double rotToUse = r * stepSize; //better search at lower granularity for rotation
						//for exercise: starting from second iteration ==> place around global best solution of last run

						int[][] transformedImg = transformImg(testImg, region.getBounds().width, region.getBounds().height, tXtoUse, tYtoUse,rotToUse);
						double currError = getImgDiffSSE(refImg, transformedImg,region);
						//better solution found??? if yes - then adjust the global best

						if(currError < LocalbestError) {
							LocalbestError = currError;
							LocalbestTx = tXtoUse;
							LocalbestTy = tYtoUse;
							LocalbestRot = rotToUse;
							IJ.log("Local better solution at Ty=" + LocalbestTx + " Ty=" + LocalbestTy + " rot= " + LocalbestRot + ", ERR= " + LocalbestError);
						}


					}
				} 
			}
			if(LocalbestError < bestError) {
				bestError = LocalbestError;
				bestTx = LocalbestTx;
				bestTy = LocalbestTy;
				bestRot = LocalbestRot;
				
				IJ.log("Global better solution at Ty=" + bestTx + " Ty=" + bestTy + " rot= " + bestRot + ", ERR= " + bestError);
			}
			
			stepSize = stepSize*0.9;
		}
	
		//finally calc the best solution
		int[][] resultTransImg = transformImg(testImg, region.getBounds().width,  region.getBounds().height,  bestTx, bestTy, bestRot); //Use BILINEAR once here!!

		//for exercise only: use BILINEAR for final transform!!!!
		return resultTransImg;
	}


	public void run(ImageProcessor ip) {
		byte[] pixels = (byte[])ip.getPixels();
		int width = ip.getWidth();
		int height = ip.getHeight();
		int[][] inDataArrInt = ImageJUtility.convertFrom1DByteArr(pixels, width, height);

		
		Rectangle roiRectR = new Rectangle(1710,270,100,400);
		int[][] transformedImageR;
		int[][] transformedImageR_ROI;
		int[][] autoRegResultImgR;
		ImagePlus ipRight = new ImagePlus("C:\\Users\\schau\\Documents\\FH-GIT\\SS19\\CGB\\CGB4workspace\\CGB4workspace\\ImageJDevProjectCGB4\\right_part.tif");
		{

			ByteProcessor bpR = new ByteProcessor(ipRight.getBufferedImage());
			int widthB = bpR.getWidth(); int heightB = bpR.getHeight();
			byte[] pixelsB = (byte[])bpR.getPixels();
			int[][] in_B = ImageJUtility.convertFrom1DByteArr(pixelsB, widthB, heightB);

			//initially transform input image to get a registration task
			double transX = 1700;
			double transY = 160;
			double rot = 0.0;


			transformedImageR = transformImg(in_B, widthB, heightB, transX, transY, rot,width,height);
			ImageJUtility.showNewImage(transformedImageR, width, height, "1_3_right");
			
			//ROI
			transformedImageR_ROI = transformImg(in_B, widthB, heightB, transX, transY, rot,width,height, new Roi(roiRectR));
			double initError = getImgDiffSSE(inDataArrInt, transformedImageR_ROI, new Roi(roiRectR));
			IJ.log("R_ROI init error = " + initError);
			
			ImageJUtility.showNewImage(transformedImageR_ROI,roiRectR.width, roiRectR.height, "R_ROI init moving image");
			
			autoRegResultImgR = getRegisteredImage(inDataArrInt, transformedImageR_ROI, width, height,new Roi(roiRectR));
			ImageJUtility.showNewImage(autoRegResultImgR, roiRectR.width, roiRectR.height, "R_ROI final reg result");
		}

		Rectangle roiRectL = new Rectangle(780,404,180,400);
		int[][] transformedImageL;
		int[][] transformedImageL_ROI;
		int[][] autoRegResultImgL;
		ImagePlus ipLeft = new ImagePlus("C:\\Users\\schau\\Documents\\FH-GIT\\SS19\\CGB\\CGB4workspace\\CGB4workspace\\ImageJDevProjectCGB4\\left_part_inverted.tif");
		{

			ByteProcessor bpR = new ByteProcessor(ipLeft.getBufferedImage());
			int widthB = bpR.getWidth(); int heightB = bpR.getHeight();
			byte[] pixelsB = (byte[])bpR.getPixels();
			int[][] in_B = ImageJUtility.convertFrom1DByteArr(pixelsB, widthB, heightB);

			//initially transform input image to get a registration task
			double transX = 110;
			double transY = 350;
			double rot = 0.0;

			transformedImageL = transformImg(in_B, widthB, heightB, transX, transY, rot,width,height);	
			ImageJUtility.showNewImage(transformedImageL, width, height, "1_3_left");
			
			//ROI
			transformedImageL_ROI = transformImg(in_B, widthB, heightB, transX, transY, rot,width,height, new Roi(roiRectL));
			double initError = getImgDiffSSE(inDataArrInt, transformedImageL_ROI, new Roi(roiRectL));
			
			IJ.log("");
			IJ.log("");
			IJ.log("L_ROI init error = " + initError);
			
			ImageJUtility.showNewImage(transformedImageL_ROI,roiRectL.width, roiRectL.height, "L_ROI init moving image");
			
			autoRegResultImgL = getRegisteredImage(inDataArrInt, transformedImageL_ROI, width, height,new Roi(roiRectL));
			ImageJUtility.showNewImage(autoRegResultImgL, roiRectL.width, roiRectL.height, "L_ROI final reg result");
		}

//		ImagePlus[] mergeStack = {
//				getImage(ImageJUtility.convertFrom2DIntArr(inDataArrInt, width, height), width, height),
//				getImage(ImageJUtility.convertFrom2DIntArr(transformedImageR, width, height), width, height),
//				getImage(ImageJUtility.convertFrom2DIntArr(transformedImageL, width, height), width, height),
//				getImage(ImageJUtility.convertFrom2DIntArr(autoRegResultImgR, roiRectR.width, roiRectR.height), roiRectR.width, roiRectR.height),
//				getImage(ImageJUtility.convertFrom2DIntArr(autoRegResultImgL, roiRectL.width, roiRectL.height), roiRectL.width, roiRectL.height)
//		};
//		
//		ImagePlus res =  Concatenator(mergeStack[0],mergeStack[1],mergeStack[2],mergeStack[3],mergeStack[4]);
//		res.show();

		int[][] Arr1_4a = new int[width][height];
		int[][] Arr1_4b = new int[width][height];
		
		for(int i = 0; i < width; ++i)
		{
			for(int k = 0; k < height; ++k)
			{
				int sum = 0;
				int valueA = 0;
				int valueB = 0;
				if(transformedImageL[i][k] != BG_VAL && transformedImageL[i][k] != FG_VAL)
				{
					valueA += transformedImageL[i][k];
					valueB = transformedImageL[i][k];
					sum++;
				}
				if(transformedImageR[i][k] != BG_VAL && transformedImageR[i][k] != FG_VAL)
				{
					valueA += transformedImageR[i][k];
					valueB = transformedImageR[i][k];
					sum++;
				}
				if(inDataArrInt[i][k] != BG_VAL && inDataArrInt[i][k] != FG_VAL)
				{
					valueA += inDataArrInt[i][k];
					sum++;
				}



				if(sum > 0)
					Arr1_4a[i][k] = valueA / sum;
				else
					Arr1_4a[i][k] = FG_VAL;

				if(sum == 2)
					Arr1_4b[i][k] = valueB;
				else
					Arr1_4b[i][k] = Arr1_4a[i][k];
			}
		}


		ImageJUtility.showNewImage(Arr1_4a, width, height, "1_4a");
		ImageJUtility.showNewImage(Arr1_4b, width, height, "1_4b");

		
		//Put final image together
		int[][] finalData_A = new int[width][height];
		int[][] finalData_B = new int[width][height];
		for(int i = 0; i < width; ++i)
		{
			for(int k = 0; k < height; ++k)
			{
				int sum = 0;
				int valueA = 0;
				int valueB = 0;
				
				if(i >= roiRectL.x && i < roiRectL.x+roiRectL.width && k >= roiRectL.y && k < roiRectL.y+roiRectL.height)
				{
					valueA += autoRegResultImgL[i-roiRectL.x][k-roiRectL.y];
					valueB = autoRegResultImgL[i-roiRectL.x][k-roiRectL.y];
					sum++;
					
				}
				else if(transformedImageL[i][k] != BG_VAL && transformedImageL[i][k] != FG_VAL)
				{
					valueA += transformedImageL[i][k];
					valueB = transformedImageL[i][k];
					sum++;
				}
				
				
				if(i >= roiRectR.x && i < roiRectR.x+roiRectR.width && k >= roiRectR.y && k < roiRectR.y+roiRectR.height)
				{
					
					valueA += autoRegResultImgR[i-roiRectR.x][k-roiRectR.y];
					valueB = autoRegResultImgR[i-roiRectR.x][k-roiRectR.y];
					sum++;
				}
				else if(transformedImageR[i][k] != BG_VAL && transformedImageR[i][k] != FG_VAL)
				{
					valueA += transformedImageR[i][k];
					valueB = transformedImageR[i][k];
					sum++;
				}
				
				if(inDataArrInt[i][k] != BG_VAL && inDataArrInt[i][k] != FG_VAL)
				{
					valueA += inDataArrInt[i][k];
					sum++;
				}



				if(sum > 0)
					finalData_A[i][k] = valueA / sum;
				else
					finalData_A[i][k] = FG_VAL;

				if(sum == 2)
					finalData_B[i][k] = valueB;
				else
					finalData_B[i][k] = finalData_A[i][k];
			}
		}
		
		ImageJUtility.showNewImage(finalData_A, width, height, "3_a");
		ImageJUtility.showNewImage(finalData_B, width, height, "3_b");




	} //run

	/**
	 * 
	 * @param inByteArr
	 * @param width
	 * @param height
	 * @param title
	 */
	public ImagePlus getImage(byte[] inByteArr, int width, int height) {
		  ImageProcessor outImgProc = new ByteProcessor(width, height);
		  outImgProc.setPixels(inByteArr);
		  
		  return new ImagePlus("No-Title", outImgProc);		 
	}
	
	void showAbout() {
		IJ.showMessage("About Template_...",
				"this is a PluginFilter template\n");
	} //showAbout

}