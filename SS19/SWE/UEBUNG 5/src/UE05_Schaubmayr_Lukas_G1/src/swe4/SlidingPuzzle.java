package swe4;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.PriorityQueue;

public class SlidingPuzzle {
	// Berechnet die Zugfolge, welche die gegebene Board-Konfiguration in die
	// Ziel-Konfiguration �berf�hrt.
	// Wirft NoSolutionException (Checked Exception), falls es eine keine derartige
	// Zugfolge gibt.

	List<Board> m_nodes;
	PriorityQueue<SearchNode> m_queue;

	public List<Move> solve(Board board) throws NoSolutionException
	{
		m_nodes = new ArrayList<>();
		m_queue = new PriorityQueue<>();

		search(board);

		System.out.println("FINISHED \n\n\n\n");
		return m_queue.poll().toMoves();

		//throw new NoSolutionException();
	}
	// Gibt die Folge von Board-Konfigurationen auf der Konsole aus, die sich durch
	// Anwenden der Zugfolge moves auf die Ausgangskonfiguration board ergibt.
	public void printMoves(Board board, List<Move> moves)
	{
		try {
			board.Debug();
			for(int i = 0; i < moves.size(); ++i)
			{

				board.move(moves.get(i).row, moves.get(i).column);
				board.Debug();
			}
		} catch (IllegalMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void search(Board board)
	{	
				try {
					SearchNode pred;
					if(m_queue.isEmpty())
					{
						pred = new SearchNode(board);
						pred.setCostsFromStart(0);
						pred.setPredecessor(null);
						m_queue.add(pred);
					}
					else
						pred = m_queue.peek();
		
					
					pred.getBoard().Debug();
					if(pred.estimatedCostsToTarget() != 0)
					{
						m_nodes.add(pred.getBoard());
						System.out.println("COSTS " + pred.estimatedTotalCosts());
						Board myBoard = pred.getBoard();
						int row = myBoard.getEmptyTileRow();
						int col = myBoard.getEmptyTileColumn();
		
						if(row -1 > 0)
						{			
							Board useBoard = myBoard.copy();
							useBoard.moveUp();
							SearchNode node = new SearchNode(useBoard);
							node.setPredecessor(pred);
							node.setCostsFromStart(pred == null ? 0 : pred.costsFromStart()+1);
							if(!m_nodes.contains(useBoard))
								m_queue.add(node);
						}
		
						if(row +1 <= myBoard.size())
						{
							Board useBoard = myBoard.copy();
							useBoard.moveDown();
							SearchNode node = new SearchNode(useBoard);
							node.setPredecessor(pred);
							node.setCostsFromStart(pred == null ? 0 : pred.costsFromStart()+1);
							if(!m_nodes.contains(useBoard))
								m_queue.add(node);
						}
		
						if(col -1 > 0)
						{
							Board useBoard = myBoard.copy();
							useBoard.moveLeft();
							SearchNode node = new SearchNode(useBoard);
							node.setPredecessor(pred);
							node.setCostsFromStart(pred == null ? 0 : pred.costsFromStart()+1);
							if(!m_nodes.contains(useBoard))
								m_queue.add(node);
						}
		
						if(col +1 <=  myBoard.size())
						{
							Board useBoard = myBoard.copy();
							useBoard.moveRight();
							SearchNode node = new SearchNode(useBoard);
							node.setPredecessor(pred);
							node.setCostsFromStart(pred == null ? 0 : pred.costsFromStart()+1);
							if(!m_nodes.contains(useBoard))
								m_queue.add(node);
						}
		
						
						m_queue.remove(pred);
						search(board);
					}			
				} catch (IllegalMoveException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
}