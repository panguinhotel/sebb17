
import ij.*;
import ij.plugin.filter.PlugInFilter;
import ij.process.*;

import java.awt.*;

import ij.gui.GenericDialog;


public class Registration_ implements PlugInFilter {

	public int setup(String arg, ImagePlus imp) {
		if (arg.equals("about"))
		{showAbout(); return DONE;}
		return DOES_8G+DOES_STACKS+SUPPORTS_MASKING;
	} //setup

	/**
	 * 
	 * @param inImg - input image
	 * @param width 
	 * @param height
	 * @param transX - translation in x-direction
	 * @param transY - translation in y-direction
	 * @param rotAngle - rotation angle in degrees
	 * @return transformed image
	 */
	public int[][] transformImg(int[][] inImg, int width, int height, 
			double transX, double transY, double rotAngle) {

		//apply backward mapping
		int[][] resultImg = new int[width][height];

		//convert from degree to rad
		double rotAngleRad = rotAngle * Math.PI/180.0;
		double cosTheta =  Math.cos(rotAngleRad);
		double sinTheta = Math.sin(rotAngleRad);
		
		double halfWidth = width / 2.0;
	    double halfHeight = height/2.0;

		//apply backward mapping
		for(int i = 0; i < width; i++)
		{
			for(int k = 0; k < height; k ++)
			{

				double posX = i;
				double posY = k;
				
				//step 1 move to center
				posX -= halfWidth;
				posY -= halfHeight;
				

				//step 2 rotate
				double rotatedX = posX*cosTheta + posY*sinTheta;
				double rotatedY = -posX * sinTheta + posY *cosTheta;
				
				//step 3 move back from center			
				posX = rotatedX;
				posY = rotatedY;
				
				posX += halfWidth;
				posY += halfHeight;
				
				//step 4 translate
				posX -= transX;
				posY -= transY;
				
				//step5 apply value using NM interpolted
				int finalVal = getNNinterpolatedValue(posX, posY, width, height, inImg);
				resultImg[i][k] = finalVal;
			}
		}




		//TODO implementation required

		return resultImg;
	}

	/**
	 * 
	 * @param refImg - static reference image
	 * @param testImg - moving image getting transformed
	 * @param width
	 * @param height
	 * @return error metric
	 */
	public double getImgDiffSSE(int[][] refImg, int[][] testImg, int width, int height) {
		double totalError = 0.0;

		for(int i = 0; i < width; i++)
		{
			for(int k = 0; k < height; k++)
			{
				int diff = refImg[i][k] - testImg[i][k];
				totalError += diff*diff;
			}
		}

		return totalError;
	}


	/**
	 * 
	 * @param xIdx - double x-image coordinate
	 * @param yIdx - double y-image coordinate
	 * @param width
	 * @param height
	 * @param img
	 * @return
	 */
	public static int getNNinterpolatedValue(double xIdx, double yIdx, int width, int height, int[][] img) {
		//just round the coordinates

		int xCoord = (int) (xIdx +0.5);
		int yCoord = (int) (yIdx +0.5);

		if(xCoord >= 0 && xCoord < width && yCoord >= 0 && yCoord < height)
		{
			return img[xCoord][yCoord];
		}

		return 0; // BLACK GB; Attention return the matching outside value, return the value best matching the background of your image!!!
	}

	/**
	 * method for automated registration utilizing 11x11x11 permutations for the transformation parameters
	 * 
	 * @param refImg
	 * @param testImg
	 * @param width
	 * @param height
	 * @return
	 */
	public int[][] getRegisteredImage(int[][] refImg, int[][] testImg, int width, int height) {

		//global best Solution
		double bestTx = 0.0;
		double bestTy = 0.0;
		double bestRot = 0.0;
		double bestError = getImgDiffSSE(refImg, testImg, width, height);
		
		
		double stepSize = 2.0;
		int stepsPerside = 5; //i.e Radius ==> 11 iterations per dimension, 11^3 = 1331 in total
		
		//TODO for 6.2 c) use one for loop with 10 iteratorn there reducing the stepSize incrementally;
		
		//first check all 1331 permutations
		for(int i = -stepsPerside; i <= stepsPerside; ++i)
		{
			for(int k = -stepsPerside; k <= stepsPerside; ++k)
			{
				for(int r = -stepsPerside; r <= stepsPerside; ++r)
				{
					double tXtoUse = i*stepSize; // for exervice: starting from second iteration ==> place around global best solution of last run
					double tYtoUse = k*stepSize;
					double rotToUse = r*stepSize; //better search at lower granularity for rotation
					
					int[][] transformedImg = transformImg(testImg,width, height, tXtoUse,tYtoUse, rotToUse);
					double currError = getImgDiffSSE(refImg, transformedImg, width, height);
					//better solution found == if yes then adjust the global best
					
					if(currError < bestError)
						{
							bestError = currError;
							bestTx = tXtoUse;
							bestTy = tYtoUse;
							bestRot = rotToUse;
							IJ.log("better solution at Tx=" + bestTx + " Ty="+bestTy + " rot=" + bestRot + ", Err ="+bestError);
						
						}
				}
			}
		}
		
		//finally calc the best solution
		int[][] resultTransImg = transformImg(testImg, width, height, bestTx, bestTy, bestRot); //USe Bilinear once here
		
		//for exercie only: use BILINEAR for final transform!!!

		return null;
	}


	public void run(ImageProcessor ip) {
		byte[] pixels = (byte[])ip.getPixels();
		int width = ip.getWidth();
		int height = ip.getHeight();
		int[][] inDataArrInt = ImageJUtility.convertFrom1DByteArr(pixels, width, height);

		//initially transform input image to get a registration task
		double transX = 9.78;
		double transY = -1.99;
		double rot = 2.14;

		int[][] transformedImage = transformImg(inDataArrInt, width, height, transX, transY, rot);
		double initError = getImgDiffSSE(inDataArrInt, transformedImage, width, height);
		IJ.log("init error = " + initError);		
		
		ImageJUtility.showNewImage(transformedImage, width, height, "init moving image");
		
		// now do the auto registration with 1 optimization run only ==> use 10 for exercie
		int[][] autoRegResultImg = getRegisteredImage(inDataArrInt, transformedImage, width, height);
		ImageJUtility.showNewImage(autoRegResultImg, width, height, "final reg result");


	} //run

	void showAbout() {
		IJ.showMessage("About Template_...",
				"this is a PluginFilter template\n");
	} //showAbout

} //class RegistrationTemplate_
