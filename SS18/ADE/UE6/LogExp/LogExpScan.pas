UNIT LogExpScan;

INTERFACE
    TYPE
        Symbol = (noSym,
            biCondSym, condSym, disSym, konjSym, negSym,
            leftParSym, rightParSym,            
            boolSym);

    (* ---------------------------------------------------------------------- *)
    (* Description: Initializes the scanner.                                  *)
    (* Input: Text containing the logical expression.                         *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE InitScanner(inputText: STRING);

    (* ---------------------------------------------------------------------- *)
    (* Description: Gets the next symbol.                                     *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE GetNextSymbol;

    (* ---------------------------------------------------------------------- *)
    (* Description: Gets the current symbol.                                  *)
    (* Output: The current symbol.                                            *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION CurrentSymbol : Symbol;

    (* ---------------------------------------------------------------------- *)
    (* Description: Gets the position of the current symbol.                  *)
    (* Output: Position of the current symbol in the text.                     *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION GetCurrentSymbolPosition : INTEGER;

    (* ---------------------------------------------------------------------- *)
    (* Description: Gets the latest read boolean value as a string.           *)
    (* Output: Latest read boolean value.                                     *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION CurrentBooleanValue : STRING;

IMPLEMENTATION
   CONST
        TAB = Chr(9);
        BLANK = ' ';
        CR = Chr(13);

    VAR
        textToScan, currBoolVal : STRING;
        curChar: CHAR;
        charColumn: BYTE;
        curSymbol: Symbol;
        symbolLine, symbolColumn: INTEGER;

    (* ---------------------------------------------------------------------- *)
    (* ---------------------- Gets the next character ----------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE GetNextChar;
    BEGIN
        IF(charColumn <= Length(textToScan)) THEN BEGIN
            charColumn := charColumn + 1;
            curChar := textToScan[charColumn];
        END
        ELSE BEGIN
            (* Set an invalid character *)
            curChar := CR;
        END;
    END; (* GetNextChar *)
    
    (* ---------------------------------------------------------------------- *)
    (* ----------------------- Gets the next symbol ------------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE GetNextSymbol;
    VAR currentTextValue : STRING;
    BEGIN
        WHILE (curChar = BLANK) OR (curChar = Tab) DO
            GetNextChar;

        symbolColumn := charColumn;

        CASE curChar OF
            '(': BEGIN curSymbol := leftParSym; GetNextChar; END;
            ')': BEGIN curSymbol := rightParSym; GetNextChar; END;
            '+': BEGIN curSymbol := disSym; GetNextChar; END;
            '*': BEGIN curSymbol := konjSym; GetNextChar; END;
            '-': BEGIN curSymbol := negSym; GetNextChar; END;
            '<' : BEGIN
                GetNextChar;
                
                IF(curChar = '=') THEN BEGIN
                    GetNextChar;

                    IF(curChar = '>') THEN BEGIN
                        curSymbol := biCondSym;
                    END
                    ELSE BEGIN
                        curSymbol := noSym;
                    END;
                END
                ELSE BEGIN
                    curSymbol := noSym;
                END;
                    
                GetNextChar;
            END;
            '=': BEGIN
                GetNextChar;

                IF(curChar = '>') THEN BEGIN
                    curSymbol := condSym;
                END
                ELSE
                    curSymbol := noSym;

                GetNextChar;
            END;
            'a'..'z', 'A'..'Z': BEGIN
                currentTextValue := '';
                curSymbol := noSym;

                WHILE curChar IN ['a'..'z', 'A'..'Z'] DO BEGIN
                    currentTextValue := currentTextValue + UpCase(curChar);
                    GetNextChar;
                END;

                IF(currentTextValue = 'EQ') THEN BEGIN
                    curSymbol := biCondSym;
                END
                ELSE IF(currentTextValue = 'IMP') THEN BEGIN
                    curSymbol := condSym;
                END
                ELSE IF(currentTextValue = 'OR') THEN BEGIN
                    curSymbol := disSym;
                END
                ELSE IF(currentTextValue = 'AND') THEN BEGIN
                    curSymbol := konjSym;
                END
                ELSE IF(currentTextValue = 'NOT') THEN BEGIN
                    curSymbol := negSym;
                END
                ELSE IF(currentTextValue = 'TRUE') OR (currentTextValue = 'T') THEN BEGIN
                    curSymbol := boolSym;
                    currBoolVal := 'true';
                END
                ELSE IF (currentTextValue = 'FALSE') OR (currentTextValue = 'F') THEN BEGIN
                    curSymbol := boolSym;
                    currBoolVal := 'false';
                END
                ELSE BEGIN
                    curSymbol := noSym;
                END;
            END;
            ELSE BEGIN WriteLn(curChar); curSymbol := noSym; GetNextChar; END;
        END;
    END; (* GetNextSymbol *)

    (* ---------------------------------------------------------------------- *)
    (* ---------------------- Initializes the scanner ----------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE InitScanner(inputText: STRING);
    BEGIN
        textToScan := inputText;
        charColumn := 0;       
        GetNextChar;
        GetNextSymbol;
    END; (* InitScanner *)

    (* ---------------------------------------------------------------------- *)
    (* ------------- Gets the position of the current symbol ---------------- *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION GetCurrentSymbolPosition : INTEGER;
    BEGIN
        GetCurrentSymbolPosition := symbolColumn;
    END; (* GetCurrentSymbolPosition *)    
    
    (* ---------------------------------------------------------------------- *)
    (* ---------- Gets the latest read boolean value as a string ------------ *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION CurrentBooleanValue : STRING;
    BEGIN
        CurrentBooleanValue := currBoolVal;
    END;

    (* ---------------------------------------------------------------------- *)
    (* -------------------- Gets the current symbol ------------------------- *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION CurrentSymbol : Symbol;
    BEGIN
        CurrentSymbol := curSymbol;
    END; (* CurrentSymbol *)
END. (* JMinScan *)