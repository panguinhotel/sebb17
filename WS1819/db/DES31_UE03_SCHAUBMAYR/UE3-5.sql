--UE3-5.1
Select * 
from(
Select c.name as NAME, r.staff_id as STAFFID
from rental r
join inventory i on r.inventory_id = i.inventory_id
join film_category fc on fc.film_id = i.film_id
join category c on c.category_id = fc.category_id
order by r.staff_id,c.name
)
PIVOT
(Count(STAFFID)
FOR STAFFID
in (1 AS VK1,
    2 AS VK2))
order by NAME;

--UE3-5.2

SELECT f.Title,(CASE WHEN f.language_id = l.language_id THEN 'L' ELSE 'OL' END) as ART, l.name
from film f
join language l on (f.language_id = l.language_id or f.original_language_id = l.language_id)
where f.release_year = 1983
order by f.Title;