#include <stdio.h>

int main()
{
    char s[6] = "Hello";

    for (int i = 0; i < 8; i++)
    {
        printf("element %d = '%c' (%x)\n", i, s[i], s[i]);
    }

    return 0;
}