<?php
   include('../session.php');
   include('../config.php');

   if($_SERVER["REQUEST_METHOD"] == "POST") {
	$katname = mysqli_real_escape_string($db,$_POST['katname']);
	$date = date('Y-m-d');

	$sql = "Insert into kategorie (fk_id_user,name,timestamp) VALUES ($login_userID,'$katname','$date')";
	$result = mysqli_query($db,$sql);

	if($result) {
		echo '<script language="javascript">';
		echo 'alert("Einfügen erfolgreich")';
		echo '</script>';
	}else {
	  	echo '<script language="javascript">';
	  	echo 'alert("Einfügen fehlgeschlagen")';
	  	echo '</script>';
	}
 }
?>
<html>  
   <head>
    <title>Kategorie einfügen</title>
          <link rel="stylesheet" type="text/css" href="../style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
   </head>
   
   <body>
   		<h1 class="headLine">Kategorie einfügen</h1>
   		<div class="mainList">
		   	<form action = "" method = "post" >
			<input class="loginInput"  type="text" placeholder="Kategoriename" name="katname" required><br>
			<input class="mainButton" type="submit" value="Einfügen"><br>
			</form>
			<button class="backbutton" onclick="window.location.href='../books.php'">Zurück</button>
		</div>
   </body> 
</html>