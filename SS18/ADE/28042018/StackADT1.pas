unit StackADT1;
	
interface
	
	CONST
		MaxSize = 10;
	TYPE
		Stack = Record
			top: Integer;
			data: Array[1..MaxSize] Of Integer;
		END;

	(*Initializes the stack *)
	(*In/Out s: the Stack that should be initialized*)
	Procedure Init(VAR s: Stack);

	(* resets the stack. Stack can be reused afterwards *)
	Procedure Done(VAR s: Stack);

	(* Pushes a value onto the stack*)
	(* In value : value that will be pushed on the stack*)
	(* In/Out s: the Stack that should be used*)
	(* Out ok : False in case of any errors; TRUE otherwise
		IF false, the value will has not been pushed onto the stack *)
	PROCEDURE Push(value:Integer; Var ok : Boolean;VAR s: Stack);

	(* Pops the most recently pushed value from the stack *)
	(* Out value : value popped from the stack. *)
	(* In/Out s: the Stack that should be used*)
	(* Out ok : False in case of any errors (e.g. stack is empty)
		In this case, the returned value mustn't be used *)
	PROCEDURE Pop(Var value:Integer; Var ok : Boolean;VAR s: Stack);

	(* Checks if stack is currently empty*)
	(* In/Out s: the Stack that should be used*)
	(* Returns : True if stack is empty; False otherwise*)
	Function IsEmpty(Var s: Stack): Boolean;

implementation
	Procedure Init(VAR s: Stack);
	begin
		s.top := 0;
	end;

	Procedure Done(VAR s: Stack);
	begin
		(*Nothing to do or maybe Init*)
	end;

	PROCEDURE Push(value:Integer; Var ok : Boolean;VAR s: Stack);
	begin
		IF s.top = MaxSize  THEN BEGIN
			ok := false;
		END ELSE BEGIN
			Inc(s.top);
			s.data[s.top] := value;
			ok := true;
		END;
	end;

	PROCEDURE Pop(Var value:Integer; Var ok : Boolean;VAR s: Stack);
	begin
		IF IsEmpty(s)  THEN BEGIN
			ok := False;
		END ELSE BEGIN
			value := s.data[s.top];
			Dec(s.top);
			ok := TRUE;
		END;
	end;

	Function IsEmpty(Var s: Stack): Boolean;
	begin
		IsEmpty := s.top = 0;
	end;

	
end.