#pragma once

#include <string>
#include <iostream>
#include "person.h"

class student : public person {

public:
  student();
  student(std::string first_name,
          std::string last_name,
          std::string registration_nr);
  student(const student &s);
  student(student &&s);
  student& operator = (student s);
  virtual ~student();

  std::string get_registration_nr() const;

protected:
  virtual void print(std::ostream &os) const override;
  virtual void read(std::istream &is) override;

  std::string registration_nr;

  friend void swap(student &a, student &b);

};
