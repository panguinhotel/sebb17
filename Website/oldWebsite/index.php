<!DOCTYPE html>
<?php
    mb_internal_encoding('UTF-8');
    mb_http_output('UTF-8');
    mb_http_input('UTF-8');
    mb_language('uni');
    mb_regex_encoding('UTF-8');
    ob_start('mb_output_handler');
    include("config.php");
    session_start();
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      $myusername = mysqli_real_escape_string($db,$_POST['username']);
      $mypassword = mysqli_real_escape_string($db,$_POST['password']); 
      
      $sql = "SELECT id_user FROM user WHERE uname = '$myusername' and password = '$mypassword'";
      $result = mysqli_query($db,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $active = $row['active'];
      
      $count = mysqli_num_rows($result);
		
      if($count == 1) {
         $_SESSION['login_user'] = $myusername;
         header("location:welcome.php");
      }else {
        echo '<script language="javascript">';
        echo 'alert("Your Login Name or Password is invalid")';
        echo '</script>';
      }
   }
?>
<html>
    <head>
        <title>Ambient-C</title>
          <link rel="stylesheet" type="text/css" href="style.css">
          <meta http-equiv="content-type" content="text/html; charset=utf-8"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    </head>
    <body>
    <h1 class="headLine">Login</h2>
    <div class="mainList">
        <form action = "" method = "post" >
        <label for="username">Username</label>
        <input class="loginInput"  type="text" placeholder="Enter Username" name="username" required><br>
        <label for="password">Passwort</label>
        <input class="loginInput" type="password" placeholder="Enter Password" name="password" required><br>  
        <input class="mainButton" type="submit" value="Login"><br>
        </form>
    </div>
    </body>
</html>
