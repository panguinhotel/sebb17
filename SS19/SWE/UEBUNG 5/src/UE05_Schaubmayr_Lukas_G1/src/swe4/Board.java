package swe4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Board implements Comparable<Board>{

	private ArrayList<Integer> m_data;
	private int m_size;
	
	// Board mit Zielkonfiguration initialisieren.
	public Board(int size)
	{
		m_data = new ArrayList<>(size*size);
		m_size = size;
		for(int i = 1; i < size*size;++i)
		{
			m_data.add(i);
		}
		
		m_data.add(0);
	}
	
	// �berpr�fen, ob dieses Board und das Board other dieselbe Konfiguration
	// aufweisen.
	public boolean equals(Object other)
	{
		Board b = (Board)other;
		if(size() == b.size())
		{
			for(int i = 0; i < m_data.size(); ++i)
			{
				try {
					
					if(getTile(i/m_size+1,i%m_size+1) != b.getTile(i/m_size+1,i%m_size+1))
						return false;
				} catch (InvalidBoardIndexException e) {
					return false;
				}
			}
		}
		else
			return false;
		
		return true;
	}
	// <1, wenn dieses Board kleiner als other ist.
	// 0, wenn beide Boards gleich sind
	// >1, wenn dieses Board gr��er als other ist.
	public int compareTo(Board other)
	{
		int sa = size();
		int so = other.size();
		
		return sa == so ? 0 : (sa < so ? -1 : 1);

	}
	// Gibt die Nummer der Kachel an der Stelle (i,j) zur�ck,
	// Indizes beginnen bei 1. (1,1) ist somit die linke obere Ecke.
	// Wirft die Laufzeitausnahme InvalidBoardIndexException.
	public int getTile(int i, int j) throws InvalidBoardIndexException
	{
		if(i > m_size || j > m_size || i < 1 || j < 1)
			throw new InvalidBoardIndexException();
		
		return m_data.get((i-1)*m_size+j-1);
	}
	// Setzt die Kachelnummer an der Stelle (i,j) zur�ck. Wirft die
	// Laufzeitausnahmen
	// InvalidBoardIndexException und InvalidTileNumberException
	public void setTile(int i, int j, int number) throws InvalidBoardIndexException, InvalidTileNumberException
	{
		if(i > m_size || j > m_size || i < 1 || j < 1)
			throw new InvalidBoardIndexException();
		
		if(number >= Math.pow(m_size, 2))
			throw new InvalidTileNumberException();
		
		m_data.set((i-1)*m_size+j-1, number);
	}
	// Setzt die Position der leeren Kachel auf (i,j)
	// Entsprechende Kachel wird auf 0 gesetzt.
	// Wirft InvalidBoardIndexException.
	public void setEmptyTile(int i, int j) throws InvalidBoardIndexException
	{
		if(i > m_size || j > m_size || i < 1 || j < 1)
			throw new InvalidBoardIndexException();
		
		m_data.set((i-1)*m_size+j-1, 0);
	}
	// Zeilenindex der leeren Kachel
	public int getEmptyTileRow()
	{
		for(int i = 0; i < m_data.size(); ++i)
		{
			if(m_data.get(i) == 0)
				return (i / m_size)+1;
		}
		
		return -1;
	}
	// Gibt Spaltenindex der leeren Kachel zur�ck.
	public int getEmptyTileColumn()
	{
		for(int i = 0; i < m_data.size(); ++i)
		{
			if(m_data.get(i) == 0)
				return (i % m_size)+1;
		}
		
		return -1;
	}
	// Gibt Anzahl der Zeilen (= Anzahl der Spalten) des Boards zur�ck.
	public int size()
	{
		return m_size;
	}
	// �berpr�ft, ob Position der Kacheln konsistent ist.
	public boolean isValid()
	{
		Set<Integer> foundValues = new HashSet<Integer>();
		
		for(int i = 0; i < m_data.size(); ++i)
		{
			try {
				int val = getTile(i/m_size+1,i%m_size+1);
				if(foundValues.contains(val))
					return false;
				else
					foundValues.add(val);
			} catch (InvalidBoardIndexException e) {
				return false;
			}
		}
		
		return true;
	}
	// Macht eine tiefe Kopie des Boards.
	// Vorsicht: Referenztypen m�ssen neu allokiert und anschlie�end deren Inhalt
	// kopiert werden.
	public Board copy()
	{
		Board B = new Board(m_size);
		for(int i = 0; i < m_data.size(); ++i)
		{
			try {
				B.setTile((i/m_size)+1, (i%m_size)+1, getTile((i/m_size)+1, (i%m_size)+1));
			} catch (BoardException e) {
				return null;
			}
		}
		
		return B;
	}
	// Erzeugt eine zuf�llige l�sbare Konfiguration des Boards, indem auf die
	// bestehende
	// Konfiguration eine Reihe zuf�lliger Verschiebeoperationen angewandt wird.
	public void shuffle()
	{
		Random rand = new Random();
		for(int i = 0; i < 20; ++i)
		{
			try {
				move(rand.nextInt(m_size)+1, rand.nextInt(m_size)+1);
			} catch (IllegalMoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	// Verschiebt leere Kachel auf neue Position (row, col).
	// throws IllegalMoveException
	public void move(int row, int col) throws IllegalMoveException
	{
		int curRow = getEmptyTileRow();
		int curCol = getEmptyTileColumn();
		
		while(curCol > col)
		{
			moveLeft();
			curCol--;
		}
		
		while(curCol < col)
		{
			moveRight();
			curCol++;
		}
		
		while(curRow > row)
		{
			moveUp();
			curRow--;
		}
		
		while(curRow < row)
		{
			moveDown();
			curRow++;
		}
	}
	// Verschiebt leere Kachel nach links. Wirft Laufzeitausnahme
	// IllegalMoveException.
	public void moveLeft() throws IllegalMoveException
	{
		int col = getEmptyTileColumn();
		int row = getEmptyTileRow();
		swapTiles(row, col, row, col-1);
	}
	// Verschiebt leere Kachel nach rechts. Wirft IllegalMoveException.
	public void moveRight() throws IllegalMoveException
	{
		int col = getEmptyTileColumn();
		int row = getEmptyTileRow();
		swapTiles(row, col, row, col+1);
	}
	// Verschiebt leere Kachel nach oben. Wirft IllegalMoveException.
	public void moveUp() throws IllegalMoveException
	{
		int col = getEmptyTileColumn();
		int row = getEmptyTileRow();
		swapTiles(row, col, row-1, col);
	}
	// Verschiebt leere Kachel nach unten. Wirft IllegalMoveException.
	public void moveDown() throws IllegalMoveException
	{
		int col = getEmptyTileColumn();
		int row = getEmptyTileRow();
		swapTiles(row, col, row+1, col);
	}
	// F�hrt eine Sequenz an Verschiebeoperationen durch. Wirft
	// IllegalMoveException.
	public void makeMoves(List<Move> moves) throws IllegalMoveException
	{
		for(int i = 0; i < moves.size(); ++i)
		{
			move(moves.get(i).row,moves.get(i).column);
		}
	}
	
	private void swapTiles(int oldRow,int oldCol, int newRow, int newCol)
	{
		try {
			int val = getTile(newRow, newCol);
			setTile(newRow, newCol,  getTile(oldRow,oldCol));
			setTile(oldRow,oldCol,val);
		} catch (BoardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void Debug()
	{
		System.out.println("DEBUG BOARD");
		for(int i = 1; i <= m_size; ++i)
		{
			for(int k = 1; k <= m_size; ++k)
			{
				try {
					System.out.print(getTile(i, k)+"\t");
				} catch (InvalidBoardIndexException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("\n================\n");
		}
		System.out.println("\nEnd Debug Board\n");
//		System.out.println(m_data);
	}
	
}
