package tests;

import java.util.Random;

import queues.BinaryHeapQueue;
import queues.Heap;
import queues.DHeapQueue;

public class PQueueTest {

	public static void main(String[] args) {

		{
			System.out.println("Test BinaryHeapQueue\n");
			int[] insArr = new int[] {33,76,3,18,8};

			BinaryHeapQueue h = new BinaryHeapQueue();
			System.out.println("Test insertUnordered(int elem)");
			System.out.println(h);
			h.insertUnordered(1);
			h.insertUnordered(111);
			h.insertUnordered(11);
			System.out.println(h);

			System.out.println("\n\nTest insertUnordered(int[] elemArray)");
			System.out.println(h);
			System.out.println("Insert Items ");h.insertUnordered(insArr);
			System.out.println(h);

			System.out.println("\n\nTest isHeap() & heapify()");
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());
			System.out.println("heapify()");h.heapify();
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());

			System.out.println("\n\nTest max()");
			System.out.print("Max = ");System.out.println(h.max());
			System.out.println(h);

			System.out.println("\n\nTest removeMax()");
			System.out.println(h);
			System.out.println("Remove element");h.removeMax();
			System.out.println(h);

			System.out.println("\n\nTest nLargest(int 5)");
			int[] tmp = h.nLargest(5);
			String x = "";
			for(int i : tmp)
				x += i+" ";

			System.out.println(x);
			System.out.println(h);

			System.out.println("\n\nTest removeNLargest(int n)");
			System.out.println(h);
			System.out.println("Remove nLargest 3");h.removeNLargest(3);
			System.out.println(h);


			System.out.println("\n\nTest insert(int elem)");
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());
			h.insert(35);
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());

			System.out.println("\n\nTest size()");
			System.out.println(h);
			System.out.print("size = ");System.out.println(h.size());

			System.out.println("\n\nTest merge(BinaryHeapQueue queue)");
			BinaryHeapQueue second = new BinaryHeapQueue();
			second.insertUnordered(new int[] {1,4,5,6});
			second.heapify();
			System.out.println(h);
			System.out.println("Merge with {1,4,5,6}");h.merge(second);
			System.out.println(h);
		}

		{
			int[] insArr = new int[] {33,76,3,18,8};
			System.out.println("\n\n\n=============================================");
			System.out.println("Test DHeapQueue(2)\n");
			DHeapQueue h = new DHeapQueue(2);
			System.out.println("Test insertUnordered(int elem)");
			System.out.println(h);
			h.insertUnordered(1);
			h.insertUnordered(111);
			h.insertUnordered(11);
			System.out.println(h);

			System.out.println("\n\nTest insertUnordered(int[] elemArray)");
			System.out.println(h);
			System.out.println("Insert Items ");h.insertUnordered(insArr);
			System.out.println(h);

			System.out.println("\n\nTest isHeap() & heapify()");
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());
			System.out.println("heapify()");h.heapify();
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());

			System.out.println("\n\nTest max()");
			System.out.print("Max = ");System.out.println(h.max());
			System.out.println(h);

			System.out.println("\n\nTest removeMax()");
			System.out.println(h);
			System.out.println("Remove element");h.removeMax();
			System.out.println(h);

			System.out.println("\n\nTest nLargest(int 5)");
			int[] tmp = h.nLargest(5);
			String x = "";
			for(int i : tmp)
				x += i+" ";

			System.out.println(x);
			System.out.println(h);

			System.out.println("\n\nTest removeNLargest(int n)");
			System.out.println(h);
			System.out.println("Remove nLargest 3");h.removeNLargest(3);
			System.out.println(h);


			System.out.println("\n\nTest insert(int elem)");
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());
			h.insert(35);
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());

			System.out.println("\n\nTest size()");
			System.out.println(h);
			System.out.print("size = ");System.out.println(h.size());

			System.out.println("\n\nTest merge(DHeapQueue queue)");
			DHeapQueue second = new DHeapQueue(2);
			second.insertUnordered(new int[] {1,4,5,6});
			second.heapify();
			System.out.println(h);
			System.out.println("Merge with {1,4,5,6}");h.merge(second);
			System.out.println(h);
		}

		{
			int[] insArr = new int[] {33,76,3,18,8};
			System.out.println("\n\n\n=============================================");
			System.out.println("Test DHeapQueue(3)\n");
			DHeapQueue h = new DHeapQueue(3);
			System.out.println("Test insertUnordered(int elem)");
			System.out.println(h);
			h.insertUnordered(1);
			h.insertUnordered(111);
			h.insertUnordered(11);
			System.out.println(h);

			System.out.println("\n\nTest insertUnordered(int[] elemArray)");
			System.out.println(h);
			System.out.println("Insert Items ");h.insertUnordered(insArr);
			System.out.println(h);

			System.out.println("\n\nTest isHeap() & heapify()");
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());
			System.out.println("heapify()");h.heapify();
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());

			System.out.println("\n\nTest max()");
			System.out.print("Max = ");System.out.println(h.max());
			System.out.println(h);

			System.out.println("\n\nTest removeMax()");
			System.out.println(h);
			System.out.println("Remove element");h.removeMax();
			System.out.println(h);

			System.out.println("\n\nTest nLargest(int 5)");
			int[] tmp = h.nLargest(5);
			String x = "";
			for(int i : tmp)
				x += i+" ";

			System.out.println(x);
			System.out.println(h);

			System.out.println("\n\nTest removeNLargest(int n)");
			System.out.println(h);
			System.out.println("Remove nLargest 3");h.removeNLargest(3);
			System.out.println(h);


			System.out.println("\n\nTest insert(int elem)");
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());
			h.insert(35);
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());

			System.out.println("\n\nTest size()");
			System.out.println(h);
			System.out.print("size = ");System.out.println(h.size());

			System.out.println("\n\nTest merge(DHeapQueue queue)");
			DHeapQueue second = new DHeapQueue(3);
			second.insertUnordered(new int[] {1,4,5,6});
			second.heapify();
			System.out.println(h);
			System.out.println("Merge with {1,4,5,6}");h.merge(second);
			System.out.println(h);
		}

		{
			int[] insArr = new int[] {33,76,3,18,8};
			System.out.println("\n\n\n=============================================");
			System.out.println("Test DHeapQueue(4)\n");
			DHeapQueue h = new DHeapQueue(4);
			System.out.println("Test insertUnordered(int elem)");
			System.out.println(h);
			h.insertUnordered(1);
			h.insertUnordered(111);
			h.insertUnordered(11);
			System.out.println(h);

			System.out.println("\n\nTest insertUnordered(int[] elemArray)");
			System.out.println(h);
			System.out.println("Insert Items ");h.insertUnordered(insArr);
			System.out.println(h);

			System.out.println("\n\nTest isHeap() & heapify()");
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());
			System.out.println("heapify()");h.heapify();
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());

			System.out.println("\n\nTest max()");
			System.out.print("Max = ");System.out.println(h.max());
			System.out.println(h);

			System.out.println("\n\nTest removeMax()");
			System.out.println(h);
			System.out.println("Remove element");h.removeMax();
			System.out.println(h);

			System.out.println("\n\nTest nLargest(int 5)");
			int[] tmp = h.nLargest(5);
			String x = "";
			for(int i : tmp)
				x += i+" ";

			System.out.println(x);
			System.out.println(h);

			System.out.println("\n\nTest removeNLargest(int n)");
			System.out.println(h);
			System.out.println("Remove nLargest 3");h.removeNLargest(3);
			System.out.println(h);


			System.out.println("\n\nTest insert(int elem)");
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());
			h.insert(35);
			System.out.println(h);
			System.out.print("isHeap = ");System.out.println(h.isHeap());

			System.out.println("\n\nTest size()");
			System.out.println(h);
			System.out.print("size = ");System.out.println(h.size());

			System.out.println("\n\nTest merge(DHeapQueue queue)");
			DHeapQueue second = new DHeapQueue(4);
			second.insertUnordered(new int[] {1,4,5,6});
			second.heapify();
			System.out.println(h);
			System.out.println("Merge with {1,4,5,6}");h.merge(second);
			System.out.println(h);
		}

		int[] elem0 = new int[1];
		int[] elem1 = new int[10];
		int[] elem2 = new int[100];
		int[] elem3 = new int[1000];

		Random r = new Random();
		for (int i = 0; i < 1000; i++) {

			int rand  = r.nextInt(1000);

			if(i < elem0.length)
				elem0[i] = rand;

			if(i < elem1.length)
				elem1[i] = rand;

			if(i < elem2.length)
				elem2[i] = rand;

			if(i < elem3.length)
				elem3[i] = rand;
		}

		{
			for(int i = 1; i <= 10; i++)
			{
				DHeapQueue a = new DHeapQueue(i);
				DHeapQueue b = new DHeapQueue(i);
				b.insertUnordered(elem0);
				b.heapify();

				DHeapQueue c = new DHeapQueue(i);
				c.insertUnordered(elem1);
				c.heapify();

				DHeapQueue d = new DHeapQueue(i);
				d.insertUnordered(elem2);
				d.heapify();

				DHeapQueue e = new DHeapQueue(i);
				e.insertUnordered(elem3);
				e.heapify();

				System.out.printf("\n");
				System.out.print(i);
				{
					long start= System.nanoTime();
					a.insert(35);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					b.insert(35);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					c.insert(35);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					d.insert(35);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					e.insert(35);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}

			}	
		}
		
		{
			System.out.printf("\n");
			System.out.printf("\n Heapify");
			System.out.printf("\n");
		
			for(int i = 1; i <= 10; i++)
			{
				DHeapQueue a = new DHeapQueue(i);
				DHeapQueue b = new DHeapQueue(i);
				b.insertUnordered(elem0);

				DHeapQueue c = new DHeapQueue(i);
				c.insertUnordered(elem1);

				DHeapQueue d = new DHeapQueue(i);
				d.insertUnordered(elem2);

				DHeapQueue e = new DHeapQueue(i);
				e.insertUnordered(elem3);

				System.out.printf("\n");
				System.out.print(i);
				{
					long start= System.nanoTime();
					a.heapify();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					b.heapify();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					c.heapify();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					d.heapify();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					e.heapify();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}

			}	
		}
		{
			System.out.printf("\n");
			System.out.printf("\n max");
			System.out.printf("\n");
		
			for(int i = 1; i <= 10; i++)
			{
				DHeapQueue a = new DHeapQueue(i);
				DHeapQueue b = new DHeapQueue(i);
				b.insertUnordered(elem0);
				b.heapify();

				DHeapQueue c = new DHeapQueue(i);
				c.insertUnordered(elem1);
				c.heapify();

				DHeapQueue d = new DHeapQueue(i);
				d.insertUnordered(elem2);
				d.heapify();

				DHeapQueue e = new DHeapQueue(i);
				e.insertUnordered(elem3);
				e.heapify();


				System.out.printf("\n");
				System.out.print(i);
				{
					long start= System.nanoTime();
					//int tmp = a.max();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",-1/1.0);
				}
				{
					long start= System.nanoTime();
					b.max();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					c.max();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					d.max();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					e.max();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}

			}	
		}
		
		{
			System.out.printf("\n");
			System.out.printf("\n nLargest(6)");
			System.out.printf("\n");
		
			for(int i = 1; i <= 10; i++)
			{
				DHeapQueue a = new DHeapQueue(i);
				DHeapQueue b = new DHeapQueue(i);
				b.insertUnordered(elem0);
				b.heapify();

				DHeapQueue c = new DHeapQueue(i);
				c.insertUnordered(elem1);
				c.heapify();

				DHeapQueue d = new DHeapQueue(i);
				d.insertUnordered(elem2);
				d.heapify();

				DHeapQueue e = new DHeapQueue(i);
				e.insertUnordered(elem3);
				e.heapify();


				System.out.printf("\n");
				System.out.print(i);
				{
					long start= System.nanoTime();
					//int tmp = a.max();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",-1/1.0);
				}
				{
					long start= System.nanoTime();
					//b.nLargest(6);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",-1/1.0);
				}
				{
					long start= System.nanoTime();
					c.nLargest(6);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					d.nLargest(6);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					e.nLargest(6);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}

			}	
		}
		
		{
			System.out.printf("\n");
			System.out.printf("\n removeMax");
			System.out.printf("\n");
		
			for(int i = 1; i <= 10; i++)
			{
				DHeapQueue a = new DHeapQueue(i);
				DHeapQueue b = new DHeapQueue(i);
				b.insertUnordered(elem0);
				b.heapify();

				DHeapQueue c = new DHeapQueue(i);
				c.insertUnordered(elem1);
				c.heapify();

				DHeapQueue d = new DHeapQueue(i);
				d.insertUnordered(elem2);
				d.heapify();

				DHeapQueue e = new DHeapQueue(i);
				e.insertUnordered(elem3);
				e.heapify();


				System.out.printf("\n");
				System.out.print(i);
				{
					long start= System.nanoTime();
					//int tmp = a.max();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",-1/1.0);
				}
				{
					long start= System.nanoTime();
					b.removeMax();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					c.removeMax();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					d.removeMax();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					e.removeMax();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}

			}	
		}
		
		{
			System.out.printf("\n");
			System.out.printf("\n removeNLargest(6)");
			System.out.printf("\n");
		
			for(int i = 1; i <= 10; i++)
			{
				DHeapQueue a = new DHeapQueue(i);
				DHeapQueue b = new DHeapQueue(i);
				b.insertUnordered(elem0);
				b.heapify();

				DHeapQueue c = new DHeapQueue(i);
				c.insertUnordered(elem1);
				c.heapify();

				DHeapQueue d = new DHeapQueue(i);
				d.insertUnordered(elem2);
				d.heapify();

				DHeapQueue e = new DHeapQueue(i);
				e.insertUnordered(elem3);
				e.heapify();


				System.out.printf("\n");
				System.out.print(i);
				{
					long start= System.nanoTime();
					//int tmp = a.max();
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",-1/1.0);
				}
				{
					long start= System.nanoTime();
					//b.nLargest(6);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",-1/1.0);
				}
				{
					long start= System.nanoTime();
					c.removeNLargest(6);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					d.removeNLargest(6);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}
				{
					long start= System.nanoTime();
					e.removeNLargest(6);
					long time= System.nanoTime()-start;
					System.out.printf("\t%f",time/1000.0);
				}

			}	
		}
	}
}