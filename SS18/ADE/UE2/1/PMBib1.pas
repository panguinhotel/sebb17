UNIT PMBib1;

INTERFACE 

TYPE 
	TPmStatistics = RECORD
		comps: LONGINT;
	END;

PROCEDURE BruteSearchLR(s, p: STRING; VAR pos: INTEGER; VAR ismatch : Boolean);
PROCEDURE GetPMStatistics(VAR stat: TPmStatistics);
PROCEDURE InitPMStatistics;
PROCEDURE RabinKarp(s,p : String; VAR pos : Integer; VAR ismatch : Boolean);

IMPLEMENTATION
CONST 
	MAX_STR_LENGTH = 255;
	ASCII_TABLE_SIZE = 128;
VAR 
	ps: TPmStatistics;

PROCEDURE InitPMStatistics;
BEGIN
	WITH ps DO BEGIN
		comps := 0;
	END;
END;

PROCEDURE GetPMStatistics(VAR stat: TPmStatistics);
BEGIN
	stat := ps;
END;

FUNCTION EQ(ch1, ch2: CHAR): BOOLEAN;
BEGIN
	EQ := (ch1 = ch2);
	Inc(ps.comps);
END;
FUNCTION EQArr(ch1: CHAR;Var valArr : Array of Integer): BOOLEAN;
BEGIN
	IF valArr[Ord(ch1)] > 0  THEN BEGIN
		Dec(valArr[Ord(ch1)]);
		EQArr := true;
	END
	ELSE
		EQArr :=false;
	Inc(ps.comps);
END;
PROCEDURE BruteSearchLR(s, p: STRING; VAR pos: INTEGER; VAR ismatch : Boolean);
VAR 
	sLen, pLen, i, j,l: INTEGER;
	tmpArr : Array[0..ASCII_TABLE_SIZE] of Integer;
	valArr : Array[0..ASCII_TABLE_SIZE] of Integer;
BEGIN
	sLen := Length(s);
	pLen := Length(p);
	
	pos := 0;
	i := 1;
	ismatch := false;

		FOR l:=Low(valArr) TO High(valArr) DO BEGIN
			valArr[l] := 0;
			tmpArr[l] := 0;
		END;
		FOR l:=0 TO pLen DO BEGIN
			valArr[Ord(p[l+1])] += 1;
			tmpArr[Ord(p[l+1])] += 1;
		END;

	WHILE (pos = 0) AND (i + pLen-1 <= sLen) DO BEGIN
		j := 1;
		FOR l:=Low(valArr) TO High(valArr) DO BEGIN
			valArr[l] := tmpArr[l];
		END;
		WHILE (j <= pLen) AND (EQArr(s[i+j-1], valArr)) DO BEGIN
			Inc(j);
		END;
		IF j > pLen THEN BEGIN
			pos := i;
			ismatch := true;
		END
		ELSE BEGIN
			Inc(i);
		END;
	END;
END;

PROCEDURE RabinKarp(s,p : String; VAR pos : Integer; VAR ismatch : Boolean);
CONST
	q = 8355967;
	r = 256;
VAR
	hp : LongInt;
	hs : LongInt;
	rm : LongInt;
	sumP,sumS : LongInt;

	sLen,pLen, i,j,k,l : Integer;
	iMax : Integer;
	sPos : Integer;
	tmpArr : Array[0..ASCII_TABLE_SIZE] of Integer;
	valArr : Array[0..ASCII_TABLE_SIZE] of Integer;
BEGIN
	sLen := Length(s);
	pLen := Length(p);
	rm := 1;
	pos := 0;
	ismatch:=false;
	sumP := 0;
	sumS := 0;

		FOR l:=Low(valArr) TO High(valArr) DO BEGIN
			valArr[l] := 0;
			tmpArr[l] := 0;
		END;
		FOR l:=0 TO pLen DO BEGIN
			valArr[Ord(p[l+1])] += 1;
			tmpArr[Ord(p[l+1])] += 1;
		END;	


	FOR i := 1 TO pLen -1 DO BEGIN
		rm:=(r*rm) MOD q;
	END;
	 hp := 0;
	 hs := 0;

	 FOR i := 1 TO pLen DO BEGIN
	 	sumP := sumP + Ord(p[i]);
		sumS := sumS + Ord(s[i]);
	 END;
	 i := 1;
	 j := 1;
	 iMax := sLen - pLen + 1;

	 WHILE (i <= iMax) AND (j <= pLen)  DO BEGIN
	 	IF sumP = sumS  THEN BEGIN
			j := 1;
			sPos := i;
			WHILE (j <= pLen) and (EQArr(s[sPos], valArr))  DO BEGIN
				Inc(j);
				Inc(sPos);
			END;
		END;
		sumS := 0;
		FOR k := 1 TO pLen DO BEGIN
			sumS := sumS + Ord(s[k+i]);
	 	END;
		FOR l:=Low(valArr) TO High(valArr) DO BEGIN
			valArr[l] := tmpArr[l];
		END;
		Inc(i);
	 END;
	 IF (i > iMax) AND (j <= pLen)  THEN BEGIN
	 	pos := 0;
	 END
	 ELSE BEGIN
	 	pos := i-1;
		ismatch:=true;
	 END;
END;
BEGIN
END.