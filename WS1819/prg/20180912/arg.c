#include <stdio.h>

int main(int argc, char const *argv[])
{
	if(argc != 2) 
	{
		printf("wrong number of arguments!\n");
		printf("usage: %s <arg>\n", argv[0]);
		return 1;
	}
	else
	{
		printf("arg = \"%s\"\n",argv[1]);
	}
	
	return 0;
}
