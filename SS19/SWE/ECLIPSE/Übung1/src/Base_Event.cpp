/*
 * Base_Event.cpp
 *
 *  Created on: 06.03.2019
 *      Author: Lukas Schaubmayr
 */

#include "Base_Event.h"

#include <iostream>

Base_Event::Base_Event() {

}

Base_Event::~Base_Event() {
	while(m_child_events.size() > 0)
	{
		auto* event = m_child_events.top();
		delete event;
		m_child_events.pop();
	}
}

void Base_Event::register_child_events(std::priority_queue<Base_Event*,std::vector<Base_Event*>,Event_compare>  *qu)
{
	while(m_child_events.size() > 0)
	{
		auto* child = m_child_events.top();
		std::cout<<"--Register ChildEvent" << std::endl;
		child->set_start_delay(m_start_delay+child->get_start_delay());
		qu->push(child);
		m_child_events.pop();
	}


}
void Base_Event::add_child_event(Base_Event* event)
{
	m_child_events.push(event);
}
