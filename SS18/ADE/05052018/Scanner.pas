unit Scanner;
	
interface
	TYPE Symbol = 
		(noSym,
		endSym,
		plusSym,
		minusSym,
		multSym,
		divSym,
		leftParSym,
		rightParSym,
		numberSym);
	PROCEDURE InitScanner(inp: String);
	PROCEDURE GetNextSymbol;
	FUNCTION CurrentSymbol : Symbol;
	FUNCTION CurrentSymbolPos : BYTE;
	FUNCTION CurrentNumberValue : LONGINT;

implementation
	CONST
		ENDOFINPUT = CHR(0);
	VAR
		input : String;
		curPos : BYTE;
		curChar : Char;
		curNumberValue : LONGINT;
		curSymbol : Symbol;

	PROCEDURE GetNextChar;
	BEGIN
		IF curPos < Length(input) Then begin
			Inc(curPos);
			curChar := input[curPos];
		END ELSE BEGIN
			curChar := ENDOFINPUT;			
		END;
	END;
	PROCEDURE InitScanner(inp: String);
	begin
		input := inp;
		curPos := 0;
		curNumberValue := 0;
		GetNextChar;
		GetNextSymbol;
	end;
	PROCEDURE GetNextSymbol;
	VAR
		numberStr : String;
		code : LongInt;
	begin
		WHILE curChar = ' '  DO BEGIN
			GetNextChar;
		END;

		CASE curChar of
			ENDOFINPUT : BEGIN curSymbol := endSym; END;
			'+' : BEGIN curSymbol := plusSym; GetNextChar; End;
			'-' : BEGIN curSymbol := minusSym; GetNextChar; END;
			'*' : BEGIN curSymbol := multSym; GetNextChar; END;
			'/' : BEGIN curSymbol := divSym; GetNextChar; END;
			'(' : BEGIN curSymbol := leftParSym; GetNextChar; END;
			')' : BEGIN curSymbol := rightParSym; GetNextChar; END;
			'0'..'9' : BEGIN 
				numberStr := '';
				WHILE (curChar >= '0') AND (curChar <= '9') DO begin
					numberStr := numberStr + curChar;
					GetNextChar;
				end;
				curSymbol := numberSym;
				Val(numberStr, curNumberValue,code);
			END
			ELSE begin
				curSymbol := noSym; END;
			end;
	end;
	FUNCTION CurrentSymbol : Symbol;
	begin
		CurrentSymbol := curSymbol;
	end;
	FUNCTION CurrentSymbolPos : BYTE;
	begin
		CurrentSymbolPos := curPos;
	end;
	FUNCTION CurrentNumberValue : LONGINT;
	begin
		CurrentNumberValue := curNumberValue
	end;

BEGIN
	InitScanner('');
end.