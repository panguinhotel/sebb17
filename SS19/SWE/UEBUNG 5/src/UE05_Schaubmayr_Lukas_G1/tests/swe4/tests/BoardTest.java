package swe4.tests;

import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

import swe4.Board;
import swe4.BoardException;

public class BoardTest {

	@Test
	public void sizeTest()
	{
		Board board= new Board(5);
		System.out.println(board.size());
		assertEquals(5, board.size());
	}

	@Test
	public void compareToTest()
	{
		Board board= new Board(5);
		Board other= new Board(5);		
		assertEquals(0, board.compareTo(other));
		other= new Board(3);
		assertEquals(1,board.compareTo(other));
		other= new Board(7);
		assertEquals(-1, board.compareTo(other));
	}
	
	@Test
	public void setTileandGetTileTest()
	{
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 4);
			board.setTile(1, 2, 5);

			assertEquals(4, board.getTile(1, 1));
			assertEquals(5, board.getTile(1, 2));
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}
	
	@Test
	public void getEmptyTileColumnTest()
	{
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 6);
			board.setTile(3, 3, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 1, 0);

			assertNotEquals(-1, board.getEmptyTileColumn());
			assertEquals(1, board.getEmptyTileColumn());
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}
	
	@Test
	public void getEmptyTileRowTest()
	{
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 6);
			board.setTile(3, 3, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 1, 0);

			assertNotEquals(-1, board.getEmptyTileRow());
			assertEquals(3, board.getEmptyTileRow());
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}

	@Test
	public void equalsTest() {
		Board board;
		Board board1;
		Board board2;
		Board board3;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 0);
			
			board1 = new Board(3);      
			board1.setTile(1, 1, 3);
			board1.setTile(1, 2, 2);
			board1.setTile(1, 3, 1);
			board1.setTile(2, 1, 4);
			board1.setTile(2, 2, 5);
			board1.setTile(2, 3, 6);
			board1.setTile(3, 1, 7);
			board1.setTile(3, 2, 8);
			board1.setTile(3, 3, 0);
			
			board2 = new Board(3);      
			board2.setTile(1, 1, 1);
			board2.setTile(1, 2, 2);
			board2.setTile(1, 3, 3);
			board2.setTile(2, 1, 4);
			board2.setTile(2, 2, 5);
			board2.setTile(2, 3, 6);
			board2.setTile(3, 1, 7);
			board2.setTile(3, 2, 8);
			board2.setTile(3, 3, 0);
			
			board3 = new Board(4);      
			board3.setTile(1, 1, 1);
			board3.setTile(1, 2, 2);
			board3.setTile(1, 3, 3);
			board3.setTile(2, 1, 4);
			board3.setTile(2, 2, 5);
			board3.setTile(2, 3, 6);
			board3.setTile(3, 1, 7);
			board3.setTile(3, 2, 8);
			board3.setTile(3, 3, 0);
			
			assertTrue(board.equals(board2));
			assertTrue(board2.equals(board));
			
			assertFalse(board.equals(board1));
			assertFalse(board1.equals(board));
			
			assertFalse(board.equals(board3));
			assertFalse(board3.equals(board));
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}

	@Test
	public void simpleIsValidTest() {
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 0);

			assertTrue(board.isValid());
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}

	@Test
	public void simpleIsNotValidTest() {
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 1);
			board.setTile(3, 3, 0);

			assertTrue(! board.isValid());
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}

	@Test
	public void simpleIsNotValidTest2() {
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 8);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 0);
			board.setTile(2, 1, 7);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 4);
			board.setTile(3, 1, 3);
			board.setTile(3, 2, 1);
			board.setTile(3, 3, 6);

			assertTrue(board.isValid());
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}
	
	@Test
	public void copyTest() {
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 0);

			assertTrue(board.equals(board.copy()));
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}
	
	@Test
	public void moveLeftTest() {
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 0);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 5);

			assertEquals(2, board.getEmptyTileColumn());
			assertEquals(2, board.getEmptyTileRow());

			board.moveLeft();
			
			assertEquals(1, board.getEmptyTileColumn());
			assertEquals(2, board.getEmptyTileRow());
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}
	
	@Test
	public void moveRightTest() {
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 0);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 5);

			assertEquals(2, board.getEmptyTileColumn());
			assertEquals(2, board.getEmptyTileRow());

			board.moveRight();
			
			assertEquals(3, board.getEmptyTileColumn());
			assertEquals(2, board.getEmptyTileRow());
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}
	
	@Test
	public void moveUpTest() {
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 0);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 5);

			assertEquals(2, board.getEmptyTileColumn());
			assertEquals(2, board.getEmptyTileRow());

			board.moveUp();
			
			assertEquals(2, board.getEmptyTileColumn());
			assertEquals(1, board.getEmptyTileRow());
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}
	
	@Test
	public void moveDownTest() {
		Board board;
		try {
			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 0);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 5);

			assertEquals(2, board.getEmptyTileColumn());
			assertEquals(2, board.getEmptyTileRow());

			board.moveDown();
			
			assertEquals(2, board.getEmptyTileColumn());
			assertEquals(3, board.getEmptyTileRow());
		}
		catch (BoardException e) {
			fail("BoardException not expected.");
		} 
	}

}
