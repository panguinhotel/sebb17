unit Parser;

interface
	Uses Scanner,PlotDat;
	PROCEDURE Parse(Var inputFile : Text; Var ok : Boolean; 
					{Param}VAR dataList : ActionList{EndParam});
IMPlEMENTATION
	TYPE TPoint = RECORD
		x,y: Integer;
	END;
	Var
		success : Boolean;

PROCEDURE RECTANGLE({Param}VAR dataList : ActionList{endParam}); FORWARD;
PROCEDURE POLYGON({Param}VAR dataList : ActionList{endParam}); FORWARD;
PROCEDURE LINE({Param}VAR dataList : ActionList{endParam}); FORWARD;
PROCEDURE SHAPE({Param}VAR dataList : ActionList{endParam}); FORWARD;

PROCEDURE POINT(VAR coords : TPoint); FORWARD;

PROCEDURE RECTANGLE({Param}VAR dataList : ActionList{endParam});
VAR
{local}
	topLeft, bottomRight : TPoint;
{endLocal}
BEGIN
	GetNextSymbol;
	IF CurrentSymbol = atSym THEN BEGIN
		GetNextSymbol;
		IF CurrentSymbol = leftParSym THEN BEGIN
			POINT({Param}topLeft{endParam});
			GetNextSymbol;
			IF CurrentSymbol = toSym THEN BEGIN
				GetNextSymbol;
				POINT({Param}bottomRight{endParam});				
				GetNextSymbol;
			END ELSE IF CurrentSymbol = widthSym THEN BEGIN
				GetNextSymbol;
				IF CurrentSymbol = numberSym THEN BEGIN
					{sem}
					bottomRight.x := topLeft.x + CurrentNumberValue;
					{endSem}
					GetNextSymbol;
					IF CurrentSymbol = heightSym THEN BEGIN
						GetNextSymbol;
						IF CurrentSymbol = numberSym THEN BEGIN
							{sem}
							bottomRight.y := topLeft.y + CurrentNumberValue;
							{endSem}
							GetNextSymbol;
						END ELSE BEGIN success := FALSE; EXIT; END;
					END ELSE BEGIN success := FALSE; EXIT; END;
				END ELSE BEGIN success := FALSE; EXIT; END;
			END ELSE BEGIN success := FALSE; EXIT; END; 
			{sem}
				AppendToActionList(topLeft.x,topLeft.y,Move,dataList);
				AppendToActionList(bottomRight.x,topLeft.y,Draw,dataList);
				AppendToActionList(bottomRight.x,bottomRight.y,Draw,dataList);
				AppendToActionList(topLeft.x,bottomRight.y,Draw,dataList);
				AppendToActionList(topLeft.x,topLeft.y,Draw,dataList);
			{endSem}
		END ELSE BEGIN success := FALSE; EXIT; END;
	END ELSE BEGIN success := FALSE; EXIT; END;

END;

PROCEDURE POLYGON({Param}VAR dataList : ActionList{Param});
VAR
{local}
	center, startP : TPoint;
	sides,x,y,radius,i  : Integer;
	radiants : Double;
{endLocal}
BEGIN
	GetNextSymbol;
	IF CurrentSymbol = polySym THEN BEGIN
		GetNextSymbol;
		IF CurrentSymbol = atSym THEN BEGIN
			GetNextSymbol;
			IF CurrentSymbol = leftParSym THEN BEGIN
				POINT({Param}center{Param});
				GetNextSymbol;
				IF CurrentSymbol = radiusSym THEN BEGIN
					GetNextSymbol;
					IF CurrentSymbol = numberSym THEN BEGIN
						{sem}radius := CurrentNumberValue; {endSem}
						GetNextSymbol;
						IF CurrentSymbol = sideSym THEN BEGIN
							GetNextSymbol;
							IF CurrentSymbol = numberSym THEN BEGIN
								{sem}sides := CurrentNumberValue; {endSem}
									IF CurrentNumberValue < 2 THEN 
									BEGIN success := FALSE; EXIT; END;
								{sem}
									radiants := 360 / sides / (180 / system.pi);
									startP.x := center.x + radius;
									startP.y := center.y;
									AppendToActionList(startP.x,startP.y,Move,dataList);
									FOR i := 1 TO sides DO BEGIN
										x := Round(center.x + radius * cos(radiants*i));
										y := Round(center.y + radius * sin(radiants*i));
										
										AppendToActionList(x,y,Draw,dataList);
									END;
								{endSem}
								GetNextSymbol;
							END ELSE BEGIN success := FALSE; EXIT; END;
						END ELSE BEGIN success := FALSE; EXIT; END;	 
					END ELSE BEGIN success := FALSE; EXIT; END;				
				END ELSE BEGIN success := FALSE; EXIT; END;
			END ELSE BEGIN success := FALSE; EXIT; END;
		END ELSE BEGIN success := FALSE; EXIT; END;
	END ELSE BEGIN success := FALSE; EXIT; END;
END;

PROCEDURE LINE({Param}VAR dataList : ActionList{EndParam});
Var
{local}
	startP, endP : TPoint;
{endLocal}
BEGIN
	GetNextSymbol;
	IF CurrentSymbol = atSym THEN BEGIN
		GetNextSymbol;
		IF CurrentSymbol = leftParSym THEN BEGIN
			POINT({Param}startP{endParam});
			GetNextSymbol;
			IF CurrentSymbol = toSym THEN BEGIN
				GetNextSymbol;
				POINT({Param}endP{endParam});
				{sem}
					AppendToActionList(startP.x,startP.y,Move,dataList);
					AppendToActionList(endP.x, endP.y, Draw,dataList);
				{endSem}
				GetNextSymbol;
			END ELSE BEGIN success := FALSE; EXIT; END;
		END ELSE BEGIN success := FALSE; EXIT; END;
	END ELSE BEGIN success := FALSE; EXIT; END;
END;

PROCEDURE SHAPE({Param}VAR dataList : ActionList{EndParam});
BEGIN
	WHILE (CurrentSymbol <> endSym) and (CurrentSymbol <> noSym) DO BEGIN
		case CurrentSymbol of
			lineSym : BEGIN
				LINE({Param}dataList{EndParam});
			END;
			rectSym : BEGIN
				RECTANGLE({Param}dataList{EndParam});
			END;
			regularSym : BEGIN
				POLYGON({Param}dataList{EndParam});
			END;
			ELSE BEGIN success := FALSE; EXIT; END;
		end;
	END;
END;

PROCEDURE POINT({Param}VAR coords : TPoint{EndParam});
BEGIN
	IF CurrentSymbol = leftParSym THEN BEGIN
		GetNextSymbol;
		IF CurrentSymbol = numberSym THEN BEGIN
			{sem}coords.x := CurrentNumberValue;{endSem}
			GetNextSymbol;
			IF CurrentSymbol = commaSym THEN BEGIN
				GetNextSymbol;
				IF CurrentSymbol = numberSym THEN BEGIN
					{sem} coords.y := CurrentNumberValue; {endSem}
					GetNextSymbol;
					IF CurrentSymbol <> rightParSym THEN
					BEGIN success := FALSE; EXIT; END;
				END ELSE BEGIN success := FALSE; EXIT; END;
			END ELSE BEGIN success := FALSE; EXIT; END;
		END ELSE BEGIN success := FALSE; EXIT; END;
	END ELSE BEGIN success := FALSE; EXIT; END;
END;

PROCEDURE Parse(Var inputFile : Text; Var ok : Boolean; {Param}VAR dataList : ActionList{EndParam});
begin
	success := TRUE;
	InitScanner(inputFile);
	SHAPE({Param}dataList{EndParam});
	ok := success and (CurrentSymbol = endSym);
end;
end.

