PROGRAM plotter;

USES Parser, Windows, WinApp,PlotDat;
VAR
	ok : Boolean;
	paintDataList : ActionList;
	inputFile: TEXT;

PROCEDURE Paint(wmd: HWnd; dc: HDC);
VAR
	p: ActionList;	
begin
	p:= paintDataList;
	WHILE p<> NIL DO begin
		DeleteObject(SelectObject(dc, CreatePen(PS_SOLID,1,RGB(0,0,0))));
		IF(p^.kind = Draw) THEN BEGIN
			LineTo(dc,p^.x,p^.y);
		END ELSE BEGIN
			MoveToEx(dc,p^.x,p^.y,NIL);
		END;
		p:= p^.next;
	END;
END;

BEGIN
	IF ParamCount = 1 THEN BEGIN	
		Assign(inputFile,ParamStr(1));
		(*$I-*)
		Reset(inputFile);
		(*$I+*)
		IF IOResult <> 0 THEN BEGIN
		WriteLn('Error, opening source file.');
		Halt(1);
		END;

		paintDataList := NIL;
		Parse(inputFile,ok,paintDataList);
		IF not ok THEN BEGIN
			WriteLn('Syntax Error');
		END;
		OnPaint := Paint;
		Run;
	END;

END. (* plotter *)