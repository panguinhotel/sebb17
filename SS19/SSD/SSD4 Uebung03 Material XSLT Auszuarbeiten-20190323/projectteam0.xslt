<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="html" version="5.0" encoding="ISO-8859-1" indent="yes"/>
	<xsl:preserve-space elements="*"></xsl:preserve-space>
	
	<xsl:template match="/">
		<xsl:text>+++++++++++++++++++</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>+++++++++++++++++++</xsl:text>
	</xsl:template>
	
	<xsl:template match="comment()">
		<xsl:value-of select="upper-case(.)"></xsl:value-of>
	</xsl:template>
</xsl:stylesheet>
