PROGRAM Hashing;

TYPE
	PNode = ^TNode;
	TNode = Record
		data: String;
	End;

	THashTableIndex = 0..9;
	THashTable = Array[THashTableIndex] OF PNode;
(*Describe me*)
FUNCTION CreateNode(data: String) : PNode;
Var
	node: PNode;
BEGIN
	New(node);
	node^.data :=data;
	CreateNode := node;
END; (* CreateNode *)

(*Describe me*)
FUNCTION ComputeHash(data: String) : THashTableIndex;
Var
	hash: LongInt;
	i: Byte;
BEGIN
	hash:=0;
	FOR i:=1 TO Length(data) DO BEGIN
		hash:= (hash+ ORD(data[i])) MOD
			(High(THashTableIndex) - Low(THashTableIndex)+1);
	END;
	ComputeHash := hash + Low(THashTableIndex);
END; (* ComputeHash *)
(*Describe me*)
PROCEDURE InitHashTable(VAR ht: THashTable);
VAR
	i: THashTableIndex;
BEGIN
	FOR i:= Low(THashTableIndex) TO High(THashTableIndex) DO BEGIN
		ht[i] := NIL;	
	END;

END; (* InitHashTable *)

(*Describe me*)
PROCEDURE DisplayHashTable(ht: THashTable);
Var
	i: THashTableIndex;
BEGIN
	FOR i:=Low(THashTableIndex) TO High(THashTableIndex) DO BEGIN
		Write(i, ': ');
		IF ht[i] = NIL  THEN Write('---') ELSE Write(ht[i]^.data);
		WriteLn;
	END;
END; (* DisplayHashTable *)

(*Describe me*)
PROCEDURE DisposeHashTable(Var ht: THashTable);
Var
	i: THashTableIndex;
BEGIN
	FOR i:= Low(THashTableIndex) TO High(THashTableIndex) DO BEGIN
		IF ht[i] <> NIL THEN BEGIN
			Dispose(ht[i]);
			ht[i] := NIL;
		END;
	END;
END; (* DisposeHashTable *)

(*Describe me*)
PROCEDURE AddNodeToHashTable(VAR ht: THashTable; node: PNode);
BEGIN
	IF node <> NIL  THEN BEGIN
		ht[ComputeHash(node^.data)] := node;
	END;
END; (* AddNodeToHashTable *)

(*Describe me*)
FUNCTION ContainsValue(ht: THashTable; data:String) : Boolean;
VAR
	node: PNode;
BEGIN
	node := ht[ComputeHash(data)];
	ContainsValue := (node <> NIL) and (data = node^.data);
END; (* ContainsValue *)

Var
	hashTable: THashTable;
	data: String;
	hash : THashTableIndex;
	node: PNode;
(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	InitHashTable(hashTable);
	Write('Add: ');
	ReadLn(data);
	WHILE data <> ''  DO BEGIN
		node := CreateNode(data);
		hash := ComputeHash(node^.data);
		WriteLn('Hash: ', hash);
		AddNodeToHashTable(hashTable,node);
		WriteLn('Add: ');
		ReadLn(data);
	END;

	WriteLn;
	DisplayHashTable(hashTable);
	WRite('Search for: ');
	ReadLn(data);
	WHILE data <> ''  DO BEGIN
		WriteLn(ContainsValue(hashTable,data));
		Write('Search for: ');
		ReadLn(data);
	END;

	DisposeHashTable(hashTable);
END. (* Hashing *)