unit KompositionUnit;
	
interface
uses
	VectorUnit;
	
TYPE
	EvenQueue = Object
		PUBLIC
			PROCEDURE Enqueue(value: INTEGER;Var ok : Boolean);
			PROCEDURE Dequeue(position: INTEGER; VAR value: INTEGER; VAR ok: BOOLEAN);
			FUNCTION IsEmpty: BOOLEAN;
		PRIVATE
			data : Vector;
	END;
	LimitedStack = Object
		PUBLIC
			PROCEDURE Init(min, max : Integer);
			PROCEDURE Push(value: INTEGER;Var ok : Boolean);
			PROCEDURE Pop(VAR value: INTEGER; VAR ok: BOOLEAN);
			FUNCTION IsEmpty: BOOLEAN;
		PRIVATE
			data : Vector;
			min,max : Integer;
	END;
	RandomPickBox = Object
		PUBLIC
			FUNCTION RandomPick : Integer;
			PROCEDURE Add(value : Integer; VAR ok : Boolean);
		PRIVATE
			isRandomized : Boolean;
			data : Vector;
	END;
implementation
	
PROCEDURE EvenQueue.Enqueue(value: INTEGER; VAR ok : Boolean);
begin
	ok :=  value MOD 2 = 0;
	IF ok THEN
		data.Add(value);
end;

PROCEDURE EvenQueue.Dequeue(position: INTEGER; VAR value: INTEGER; VAR ok: BOOLEAN);
begin
	data.RemoveElementAt(position,value,ok);
end;

FUNCTION EvenQueue.IsEmpty: BOOLEAN;
begin
	IsEmpty := data.Size = 0;
end;

PROCEDURE LimitedStack.Init(min, max : Integer);
begin
	self.min := min;
	self.max := max;
end;

PROCEDURE LimitedStack.Push(value: INTEGER;Var ok : Boolean);
begin
	ok :=  (value >= min) and (value <= max);
	IF ok THEN
		data.Add(value);
end;

PROCEDURE LimitedStack.Pop(VAR value: INTEGER; VAR ok: BOOLEAN);
begin
	value := 0;
	data.GetElementAt(data.Size-1,value,ok);
	if ok THEN
		data.RemoveElementAt(data.Size-1,value,ok);
end;

FUNCTION LimitedStack.IsEmpty: BOOLEAN;
begin
	IsEmpty := data.Size = 0;
end;

FUNCTION RandomPickBox.RandomPick : Integer;
VAR
	value : Integer;
	ok : Boolean;
begin
	if not isRandomized THEN BEGIN
		Randomize;
		isRandomized:= true;
	END;

	data.GetElementAt(Random(data.Size-1),value,ok);
	RandomPick := value;
end;

PROCEDURE RandomPickBox.Add(value : Integer; Var ok : Boolean);
begin
	ok := data.HasElement(value);
	if not ok THEN BEGIN
		data.Add(value);
	END;
	
	ok := not ok;
end;

end.