package swe4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SearchNode implements Comparable<SearchNode> {
	
	Board m_board;
	SearchNode m_predecessor = null;
	int m_costsFromStart = 0;
	int m_costsToTarget = 0;
	int m_totalCosts = 0;
	
	// Suchknoten mit Board-Konfiguration initialisieren.
	public SearchNode(Board board)
	{
		m_board = board.copy();
	}
	// Gibt Board-Konfiguration dieses Knotens zur�ck.
	public Board getBoard()
	{
		return m_board;
	}
	// Gibt Referenz auf Vorg�ngerknoten zur�ck.
	public SearchNode getPredecessor()
	{
		return m_predecessor;
	}
	// Setzt den Verweis auf den Vorg�ngerknoten.
	public void setPredecessor(SearchNode predecessor)
	{
		m_predecessor = predecessor;
	}
	// Gibt Kosten (= Anzahl der Z�ge) vom Startknoten bis zu diesem Knoten zur�ck.
	public int costsFromStart()
	{
		return m_costsFromStart;
	}
	// Gibt gesch�tzte Kosten bis zum Zielknoten zur�ck. Die Absch�tzung
	// kann mit der Summe der Manhatten-Distanzen aller Kacheln erfolgen.
	public int estimatedCostsToTarget()
	{
		int costs = 0;
		for(int i = 0; i < Math.pow(m_board.size(), 2); ++i)
		{
			int row = i / m_board.size();
			int column = i % m_board.size();
			
			int tile;
			try {
				tile = m_board.getTile(row+1, column+1);
			} catch (InvalidBoardIndexException e) {
				tile = 0;
			}
			if(tile != 0)
			{
				costs += Math.abs(row -(tile-1)/m_board.size())+ 
						Math.abs(column - (tile-1)%m_board.size());
			}
		}
		
		return costs;
	}
	// Setzt die Kosten vom Startknoten bis zu diesem Knoten.
	public void setCostsFromStart(int costsFromStart)
	{
		m_costsFromStart = costsFromStart;
		m_costsToTarget = estimatedCostsToTarget();
		m_totalCosts = estimatedTotalCosts();
	}
	// Gibt Sch�tzung der Wegkosten vom Startknoten �ber diesen Knoten bis zum
	// Zielknoten zu-r�ck.
	public int estimatedTotalCosts()
	{
		return estimatedCostsToTarget() + m_costsFromStart;
	}
	// Gibt zur�ck, ob dieser Knoten und der Knoten other dieselbe
	// Board-Konfiguration darstellen.
	// Vorsicht: Knotenkonfiguration vergleichen, nicht die Referenzen.
	public boolean equals(Object other)
	{		
		return m_board.equals(((SearchNode)other).getBoard());
	}
	// Vergleicht zwei Knoten auf Basis der gesch�tzten Gesamtkosten.
	// <1: Kosten dieses Knotens sind geringer als Kosten von other.
	// 0: Kosten dieses Knotens und other sind gleich.
	// >1: Kosten dieses Knotens sind h�her als Kosten von other.
	public int compareTo(SearchNode other)	
	{
//		return estimatedCostsToTarget() == other.estimatedCostsToTarget() ? 0
//				: (estimatedCostsToTarget() < other.estimatedCostsToTarget() ? -1 : 2);
		
		return estimatedTotalCosts() == other.estimatedTotalCosts() ? 0
				: (estimatedTotalCosts() < other.estimatedTotalCosts() ? -1 : 2);
	}
	// Konvertiert die Knotenliste, die bei diesem Knoten ihren Ausgang hat,
	// in eine Liste von Z�gen. Da der Weg in umgekehrter Reihenfolge gespeichert
	// ist, muss die Zugliste invertiert werden.
	public List<Move> toMoves()
	{
		List<Move> resList = new ArrayList<Move>();
		SearchNode node = this;
		while(node.getPredecessor() != null)
		{
			Move m = new Move();
			m.column = node.getBoard().getEmptyTileColumn();
			m.row = node.getBoard().getEmptyTileRow();
			resList.add(m);
			
			node = node.getPredecessor();
		}
		
		
		Collections.reverse(resList);						
		return resList;
	}
	
}