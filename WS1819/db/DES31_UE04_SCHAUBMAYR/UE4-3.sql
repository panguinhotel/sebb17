CREATE OR REPLACE PACKAGE top_salary_pkg AS
  PROCEDURE GetTopSalaries(pLowSalary IN NUMBER, pHighSalary IN NUMBER);
  PROCEDURE DoubleTopSalaries(pLowSalary IN NUMBER, pHighSalary IN NUMBER);
  PROCEDURE FillTopSalaries(num IN NUMBER);
END;
/
SHOW ERR

CREATE OR REPLACE PACKAGE BODY top_salary_pkg AS
  PROCEDURE GetTopSalaries(pLowSalary IN NUMBER, pHighSalary IN NUMBER)
  IS
  CURSOR cTopSalaries(pLowSalary IN NUMBER, pHighSalary IN NUMBER) IS
    SELECT 'Salary:'||to_char(SALARY)||'. EmpCnt:'||to_char(EMP_CNT)||
      '. Erstellt:'||to_char(CREATEDBY)||'/'||to_char(DATECREATED)||
      '. Ge�ndert:'||to_char(MODIFIEDBY)||'/'||to_char(DATEMODIFIED)||'.' text 
    FROM top_salaries
    where salary between pLowSalary and pHighSalary
    ORDER BY SALARY DESC;
  BEGIN
    FOR vTopSalaries IN cTopSalaries (pLowSalary ,pHighSalary) LOOP
      DBMS_OUTPUT.PUT_LINE(vTopSalaries.text);
    END LOOP;
  END;
  PROCEDURE DoubleTopSalaries(pLowSalary IN NUMBER, pHighSalary IN NUMBER)
  IS
  CURSOR cDoubleSal(pLowSalary IN NUMBER, pHighSalary IN NUMBER) IS
    SELECT salary 
    from top_salaries
    where salary between pLowSalary and pHighSalary
    FOR UPDATE NOWAIT;
  BEGIN
    FOR vDoubleSal in cDoubleSal (pLowSalary,pHighSalary) LOOP
      UPDATE top_salaries 
      SET salary=salary*2,modifiedBy=USER,datemodified=SYSDATE 
      where salary = vDoubleSal.salary;
    END LOOP;
  END;
  PROCEDURE FillTopSalaries(num IN NUMBER)
  IS
    sal employees.salary%TYPE;
    vEmp_cnt top_salaries.emp_cnt%TYPE;
    CURSOR emp_cursor IS
      SELECT salary, COUNT (*)
        FROM employees
      group by salary	
      ORDER BY salary DESC;
  BEGIN
   OPEN emp_cursor;
    DELETE FROM top_salaries;
    FETCH emp_cursor INTO  sal, vEmp_cnt;
    WHILE emp_cursor%ROWCOUNT <= num AND emp_cursor%FOUND LOOP
      INSERT INTO top_salaries (salary,emp_cnt, createdBy, dateCreated, modifiedBy, dateModified)
      VALUES (sal,vEmp_cnt, USER, SYSDATE, USER, SYSDATE);
      FETCH emp_cursor INTO sal, vEmp_cnt;
    END LOOP;
    CLOSE emp_cursor;  
  END;
END;
/
SHOW ERR

SET SERVEROUTPUT ON
ALTER SESSION SET nls_date_format = 'dd.mm.yyyy hh24:mi:ss';
BEGIN
  top_salary_pkg.FillTopSalaries(3);
  dbms_output.put_line('....................');
  top_salary_pkg.GetTopSalaries(0,100000);
  dbms_output.put_line('....................');
  top_salary_pkg.DoubleTopSalaries(11000,13500);
  dbms_output.put_line('....................');
  top_salary_pkg.GetTopSalaries(0,100000);
  dbms_output.put_line('....................');
END;
/

Select * from top_salaries;

select
sys_context('USERENV','SID')
from dual;
BEGIN
  top_salary_pkg.DoubleTopSalaries(16000,18000);
END;
/
Commit;