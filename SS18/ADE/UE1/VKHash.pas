unit VKHash;
	
interface
uses
	MyExtensions;

TYPE
	PNode = ^TNode;
	TNode = Record
		data : AddressBookEntry;
		next : PNode;
	End;

	THashTableIndex = 0..37;
	THashTable = Array[THashTableIndex] OF PNode;

(*Initialize HashTable*)
PROCEDURE InitHashTable(Var ht: THashTable);
(*Add Entry to HashTable*)
FUNCTION AddNodeToHashTable(Var ht: THashTable; book:AddressBookEntry) : Boolean;
(*Dispose HashTable*)
PROCEDURE DisposeHashTable(Var ht: THashTable);
(*DisposeEntry*)
FUNCTION DisposeEntry(Var ht: THashTable; vorname,nachname : string) : Boolean;
(*getNumberForName*)
FUNCTION getNumberForName(Var ht: THashTable; book:AddressBookEntry) : string;
(*Display HashTable*)
PROCEDURE DisplayHashTable(Var ht: THashTable);
implementation

(*Initialize HashTable*)
PROCEDURE InitHashTable(Var ht: THashTable);
Var
	i : THashTableIndex;
BEGIN
	FOR i:=Low(THashTableIndex) TO High(THashTableIndex) DO BEGIN
		ht[i] := NIL;
	END;
END; (* InitHashTable *)
	
(*ComputeHash*)
FUNCTION ComputeHash(data: String) : THashTableIndex;
Var
	hash : LongInt;
	i:Integer;
BEGIN
	hash:=0;
	FOR i:=1 TO Length(data) DO BEGIN
		hash:=(hash + ORD(data[i])) MOD (High(THashTableIndex) - Low(THashTableIndex));
	END;
	ComputeHash := hash + Low(THashTableIndex);
END; (* ComputeHash *)
(*ContainsValue*)
FUNCTION ContainsValue(Var ht: THashTable; vorname,nachname : string) : Boolean;
Var
	node: PNode;
	found: Boolean;
BEGIN
	node := ht[ComputeHash(vorname+nachname)];
	found := false;
	WHILE (not found) and (node <> NIL)  DO BEGIN
		found:= (node^.data.vorname = vorname) and (node^.data.nachname = nachname);
		node := node^.next;
	END;
	ContainsValue := found;
END; (* ContainsValue *)
(*Add Entry to HashTable*)
FUNCTION AddNodeToHashTable(Var ht: THashTable; book:AddressBookEntry) : Boolean;
Var
	hash: THashTableIndex;
	node: PNode;
BEGIN
	IF not ContainsValue(ht,book.vorname,book.nachname)  THEN BEGIN
		New(node);
		node^.data := book;
		node^.next := NIL;
		hash:= ComputeHash(node^.data.vorname+node^.data.nachname);
		IF ht[hash] = NIL  THEN BEGIN
			ht[hash] := node;
		END
		ELSE BEGIN
			node^.next := ht[hash];
			ht[hash] := node;
		END;
		AddNodeToHashTable:= true;
	END
	ELSE AddNodeToHashTable:= false;
	
END; (* AddNodeToHashTable *)
(*Describe me*)
FUNCTION getNumberForName(Var ht: THashTable; book:AddressBookEntry) : string;
Var
	node: PNode;
BEGIN
	node:= ht[ComputeHash(book.vorname+book.nachname)];
	WHILE (node <> NIL)  DO BEGIN
		IF (node^.data.vorname = book.vorname) and (node^.data.nachname = book.nachname)  THEN BEGIN
			getNumberForName := node^.data.nummer;
			EXIT;
		END;
		node := node^.next;
	END;
END; (* getNumberForName *)
(*Display HashTable*)
PROCEDURE DisplayHashTable(Var ht: THashTable);
VAR
    i: INTEGER;
	node : PNode;
BEGIN
    FOR i := LOW(ht) TO High(ht) DO BEGIN
		node := ht[i];
		WHILE(node <> NIL) DO BEGIN
			WriteLn(node^.data.vorname,^i,node^.data.nachname,^i,node^.data.nummer);
			node := node^.next;
		END;
    END; (* FOR *)
END; (* DisplayHashTable *)
(*Dispose HashTable*)
PROCEDURE DisposeHashTable(Var ht: THashTable);
Var
	i: Integer;
	node,n : PNode;
BEGIN
	FOR i := LOW(ht) TO High(ht) DO BEGIN
		node := ht[i];
				WHILE(node <> NIL) DO BEGIN
				n := node;
				node := node^.next;
				Dispose(n);
				END;
			Dispose(ht[i]);
			ht[i] := NIL;	
    END; (* FOR *)
END; (* DisposeHashTable *)
(*DisposeEntry*)
FUNCTION DisposeEntry(Var ht: THashTable; vorname,nachname : string) : Boolean;
Var
	node,n: PNode;
	hash: THashTableIndex;
BEGIN
	hash := ComputeHash(vorname+nachname);
	node:= ht[hash];
	n:= node;
	WHILE (node <> NIL)  DO BEGIN
		IF (node^.data.vorname = vorname) and (node^.data.nachname = nachname)  THEN BEGIN
			IF node = ht[hash]  THEN BEGIN
				ht[hash] := node^.next;
			END
			ELSE BEGIN
				n^.next := node^.next;
			END;
			Dispose(node);
			DisposeEntry:=true;
			EXIT;
		END;
		n := node;
		node := node^.next;
	END;
	DisposeEntry:=false;
END; (* DisposeEntry *)
end.