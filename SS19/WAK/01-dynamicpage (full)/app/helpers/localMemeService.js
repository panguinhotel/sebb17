const fs = require('fs');
const uuid = require('uuid/v1');
const async = require('async');

const memeDirectory = './memes'
const storeDirectory = './store'

class MemeService {

    loadMemes(callback) {
        fs.readdir(storeDirectory, function (err, files) {
            if (err) {
                callback(err, null);
            }

            let result = [];
            async.map(files.map(f => storeDirectory + '/' + f), fs.readFile, function (err, data) {
                callback(err, data.map(d => JSON.parse(d)).sort((o1, o2) => o2.createdAt > o1.createdAt));
            });
        });
    }

    storeMeme(meme, callback) {
        const id = uuid();

        fs.writeFile(`${memeDirectory}/${id}.png`, meme.data, {
            encoding: 'base64'
        });
        fs.writeFile(`${storeDirectory}/${id}.data`, JSON.stringify({
            id: id,
            imageUrl: `./memes/${id}.png`,
            createdAt: new Date().getTime(),
            creator: meme.creator
        }), () => callback());
    }
}

module.exports = new MemeService();