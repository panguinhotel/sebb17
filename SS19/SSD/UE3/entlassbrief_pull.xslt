<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.fh-ooe.at/xml4/entlassungsbrief" xpath-default-namespace="http://www.fh-ooe.at/xml4/entlassungsbrief">
	<xsl:output method="html" encoding="ISO8859-1" indent="yes"/>
	<xsl:include href="ArzneimittelForDiagnose.xslt"/>
	
	<xsl:template name="Formatierung" match="/">
		<html>
			<head>
				<title>Entlassungsbrief</title>
				<style type="text/css">
					table.t2 {
						border-collapse: collapse;
						font-family: Georgia;
					  }
					  .t2 caption {
						padding-bottom: 0.5em;
						font-weight: bold;
						font-size: 16px;
					  }
					  .t2 th, .t2 td {
						padding: 4px 8px;
						border: 2px solid #fff;
						background: #dbe5f0;
					  }
					  .t2 thead th {
						padding: 2px 8px;
						background: #4f81bd;
						text-align: left;
						font-weight: normal;
						font-size: 13px;
						color: #fff;
					  }
					  .t2 tbody tr:nth-child(odd) *:nth-child(even), .t2 tbody tr:nth-child(even) *:nth-child(odd) {
						background: #4f81bd;
					  }
					  .t2 tr *:nth-child(3), .t2 tr *:nth-child(4) {
						text-align: right;
					  }
				</style>
			</head>
			<body>
				<xsl:call-template name="Entlassungsbrief"/>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template name="Entlassungsbrief">
		<h1>Entlassungsbrief</h1>
		Erzeugt am 
		<xsl:value-of select='format-dateTime(/Entlassungsbrief/@erzeugt, "[D]. [MNn] [Y] um [H01]:[m01] Uhr", "de", (), ())'/>
		| Version: 
		<xsl:value-of select="/Entlassungsbrief/@version"/>
		<xsl:for-each select="/Entlassungsbrief/Ersteller">
			<xsl:call-template name="Ersteller">
				<xsl:with-param name="value" select="."/>
			</xsl:call-template>
		</xsl:for-each>
		
		<xsl:for-each select="/Entlassungsbrief/Empfänger">
			<xsl:call-template name="Empfänger">
				<xsl:with-param name="value" select="."/>
			</xsl:call-template>
		</xsl:for-each>
		
		<xsl:for-each select="/Entlassungsbrief/Patient">
			<xsl:call-template name="Patient">
				<xsl:with-param name="value" select="."/>
			</xsl:call-template>
		</xsl:for-each>
		
		<xsl:for-each select="/Entlassungsbrief/Aufenthalt">
			<xsl:call-template name="Aufenthalt">
				<xsl:with-param name="value" select="."/>
			</xsl:call-template>
		</xsl:for-each>
		
		<xsl:for-each select="/Entlassungsbrief/Befundtext">
			<xsl:call-template name="Befundtext">
				<xsl:with-param name="value" select="."/>
			</xsl:call-template>
		</xsl:for-each>
		
		<xsl:for-each select="/Entlassungsbrief/Diagnosen">
			<xsl:call-template name="Diagnosen">
				<xsl:with-param name="value" select="."/>
			</xsl:call-template>
		</xsl:for-each>
		
		<xsl:for-each select="/Entlassungsbrief/Medikation">
			<xsl:call-template name="Medikation">
				<xsl:with-param name="value" select="."/>
			</xsl:call-template>
		</xsl:for-each>		
	</xsl:template>
	
	
	<xsl:template name="Ersteller" match="Ersteller">
		<xsl:param name="value" select="true"/>
		<b>Erstellt von</b>: <xsl:value-of select="$value/text()"/><br/>
	</xsl:template>
	<xsl:template name="Empfänger" match="Empfänger">
		<xsl:param name="value" select="true"/>	
		<b>An</b>: <xsl:value-of select="$value/text()"/><br/><br/>
	</xsl:template>
	
	<xsl:template name="Patient" match="Patient">
		<xsl:param name="value" select="true"/>
		<h2>Patient</h2>
			<xsl:for-each select="$value/Titel[@position='vor']">
				<xsl:value-of select="text()"/>&#160;
			</xsl:for-each>
			<xsl:for-each select="$value/Vorname">
				<xsl:value-of select="text()"/>&#160;
			</xsl:for-each>
			<xsl:for-each select="$value/Nachname">
				<xsl:value-of select="text()"/>,
			</xsl:for-each>
			<xsl:for-each select="$value/Titel[@position='nach']">
				<xsl:value-of select="text()"/>&#160;
			</xsl:for-each>
			<br/>
			<xsl:for-each select="$value/Geschlecht">
				<b>Geschlecht</b>: <xsl:value-of select="text()"/> | 
			</xsl:for-each>
			<xsl:for-each select="$value/Geburtsdatum">
				<b>geboren am</b>: <xsl:value-of select='format-date(text(), "[D]. [MNn] [Y]", "de", (), ())'/> | 
			</xsl:for-each>
			<xsl:for-each select="$value/SVN">
				<b>SVN</b>: <xsl:value-of select="text()"/>
			</xsl:for-each>
			<br/><br/>
		<xsl:value-of select="$value/text()"/><br/>
	</xsl:template>
			
	<xsl:template name="Aufenthalt" match="Aufenthalt">
		<xsl:param name="value" select="true"/>
	<h2>Aufenthalt</h2>
		<xsl:value-of select="$value/text()"/><br/>
		<a><b><xsl:value-of select="$value/@art"/> von</b>:
		<xsl:value-of select='format-date($value/@von, "[D]. [MNn] [Y]", "de", (), ())'/> <b> bis</b>: 
		<xsl:value-of select='format-date($value/@bis, "[D]. [MNn] [Y]", "de", (), ())'/></a>
	</xsl:template>
	
	<xsl:template name="Befundtext" match="Befundtext">
		<xsl:param name="value" select="true"/>
	<hr/>
		<xsl:for-each select="Anrede">
			<xsl:value-of select="$value/Anrede/text()"/><br/><br/>
		</xsl:for-each>
		<xsl:for-each select="Text">
			<i><xsl:value-of select="$value/Text/text()"/></i>
		</xsl:for-each>
		<hr/>
	</xsl:template>
	<xsl:template name="Diagnosen" match="Diagnosen">
		<xsl:param name="value" select="true"/>
		<h2>Diagnosen bei Entlassung</h2>
		<table class="t2">
			<thead>
				<tr>
					<th>Diagnose</th>
					<th>Datum von</th>
					<th>Datum bis</th>
					<th>Status</th>
					<th>Arzneimittel</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="Diagnose">
					<tr>
						<td><b><xsl:value-of select="@code"/></b>&#160;<xsl:value-of select="text()"/></td>
						<td><xsl:value-of select="@von"/></td>
						<td><xsl:value-of select="@bis"/></td>
						<td><xsl:value-of select="@status"/></td>
						<td>
							<xsl:call-template name="selectArzneimittelForDiagnose">
								<xsl:with-param name="diagnose" select="@code"/>
							</xsl:call-template>
						</td>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
	<xsl:template name="Medikation" match="Medikation">
		<xsl:param name="value" select="true"/>		
		<h2>Diagnosen bei Entlassung</h2>
		<table class="t2">
			<thead>
				<tr>
					<th>Arzneimittel</th>
					<th>Einnahme</th>
					<th>Dosierung bis</th>
					<th>Hinweis</th>
					<th>Diagnose</th>
					<th>Zusatzinformation</th>
				</tr>
			</thead>
			<tbody>
					<xsl:for-each select="$value/Arzneimittel[not(@diagnose)]">
					<tr>
						<td><b><xsl:value-of select="text()"/></b></td>
						<td><xsl:value-of select="@einnahme"/></td>
						<td><xsl:value-of select="@dosierung"/></td>
						<td><xsl:value-of select="@hinweis"/></td>
						<td><xsl:value-of select="@diagnose"/></td>
						<td>
							<xsl:choose>
							<xsl:when test="@von">
								<b>Start</b>: <xsl:value-of select="@von"/><br/>
							</xsl:when>
							</xsl:choose>
							
							<xsl:choose>
							<xsl:when test="@bis">
								<b>Ende</b>: <xsl:value-of select="@bis"/><br/>
							</xsl:when>
							</xsl:choose>
							
							<xsl:choose>
							<xsl:when test="@anwendung">
								<b>Anwendung</b>: <xsl:value-of select="@anwendung"/>
							</xsl:when>
							</xsl:choose>
							
						</td>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
	
</xsl:stylesheet>
