PROGRAM DictionaryADSTest;

USES
	DictUnit;
VAR
	i : Integer;
	value :String;
	found :Boolean;
	instance1: Dictionary;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	WriteLn('TEST Dictionary');
	WriteLn('Test : Size : Expected = 0 -> Result = ',instance1.Size());
	WriteLn('Test : PutElement  : key: Test , value : 0');instance1.PutElement('Test','0');	
	WriteLn('Test : PutElement  : key: Hallo , value : 1');instance1.PutElement('Hallo','1');
	WriteLn('Test : PutElement  : key: Fred , value : 2');instance1.PutElement('Fred','2');
	WriteLn('Test : PutElement  : key: Affe , value : 3');instance1.PutElement('Affe','3');
	WriteLn('Test : PutElement  : key: Banane , value : 4');instance1.PutElement('Banane','4');
	WriteLn('Test : PutElement  : key: Haus , value : 5');instance1.PutElement('Haus','5');
	WriteLn('Test : PutElement  : key: Tor , value : 6');instance1.PutElement('Tor','6');
	WriteLn('Test : PutElement  : key: Papier , value : 7');instance1.PutElement('Papier','7');
	WriteLn('Test : PutElement  : key: Stein , value : 8');instance1.PutElement('Stein','8');
	WriteLn('Test : PutElement  : key: Schere , value : 9');instance1.PutElement('Schere','9');
	WriteLn('Test : Size : Expected = 10 -> Result = ',instance1.Size());
	WriteLn('Test : PutElement  : key: Fred , value : 11');instance1.PutElement('Fred','11');
	WriteLn('Test : Size : Expected = 10 -> Result = ',instance1.Size());
	WriteLn('Test : PutElement  : key: Erdbeere , value : 11');instance1.PutElement('Erdbeere','11');
	WriteLn('Test : Size : Expected = 11 -> Result = ',instance1.Size());
	instance1.GetElement('Banane',value,found);
	WriteLn('Test : GetElement  : key: Banane  : Expected : value = 4 , found = TRUE -> Result : value = ',value,' found = ',found);
	instance1.GetElement('Erdbeere',value,found);
	WriteLn('Test : GetElement  : key: Erdbeere  : Expected : value = 11 , found = TRUE -> Result : value = ',value,' found = ',found);
	WriteLn('Test : PutElement  : key: Erdbeere , value : -5');instance1.PutElement('Erdbeere','-5');
	instance1.GetElement('Erdbeere',value,found);
	WriteLn('Test : GetElement  : key: Erdbeere  : Expected : value = -5 , found = TRUE -> Result : value = ',value,' found = ',found);
	instance1.GetElement('Schwein',value,found);
	WriteLn('Test : GetElement  : key: Schwein  : Expected : value = 0 , found = FALSE -> Result : value = ',value,' found = ',found);
	WriteLn('Test : Size : Expected = 11 -> Result = ',instance1.Size());
	WriteLn('Test : RemoveElement : key : Stein ');
	instance1.RemoveElement('Stein',value,found);
	WriteLn('Test : Size : Expected = 10 -> Result = ',instance1.Size());
	instance1.Delete(found);
	WriteLn('Test : Delete : Expected = TRUE -> Result = ',found);
END. (* DictTest *)