unit Parser;
	
interface
	PROCEDURE InitParser(inp : String);
	FUNCTION Parse : Boolean;
	FUNCTION ErrorPos : Integer;
	FUNCTION GetResult : INTEGER;
	
implementation
	USES
		Scanner;
	
	VAR
		success : Boolean;

	PROCEDURE Expr; FORWARD;
	PROCEDURE Term; FORWARD;
	PROCEDURE Fact; FORWARD;

	PROCEDURE Expr;
	BEGIN
		Term; IF NOT success THEN EXIT;
		WHILE (CurrentSymbol = plusSym)
			OR (CurrentSymbol = minusSym)  DO BEGIN
			CASE CurrentSymbol OF 
				plusSym : BEGIN
					GetNextSymbol;
					Term; IF NOT success THEN EXIT;
					Write('+');
				END;
				minusSym : BEGIN
					GetNextSymbol;
					Term; IF NOT success THEN EXIT;
					Write('-');
				END;
			END;
		END;
	END;

	PROCEDURE Term;
	BEGIN
      Fact; IF NOT success THEN exit;
      WHILE (CurrentSymbol = multSym) OR (CurrentSymbol = divSym) DO BEGIN
        CASE CurrentSymbol OF
          multSym : BEGIN
            GetNextSymbol;
            Fact; IF NOT success THEN exit;
			Write('*');
          END;
          divSym : BEGIN
            GetNextSymbol;
            Fact; IF NOT success THEN exit;
			Write('/');
          END;
        END;
      END;    
    END;

	PROCEDURE Fact;
	BEGIN
		CASE CurrentSymbol OF
			numberSym: begin
				Write(CurrentNumberValue, ' ');
				GetNextSymbol;
			end;
			leftParSym: begin
				GetNextSymbol;
				Expr; IF NOT success Then EXIT;
				IF CurrentSymbol <> rightParSym Then begin
					success := FALSE; EXIT;
				END;
			END;
			ELSE BEGIN
				success := FALSE; EXIT;
			END;
		END;
	END;

	PROCEDURE InitParser(inp : String);
	BEGIN
		InitScanner(inp);
		success := false;
	END;

	FUNCTION Parse : Boolean;
	BEGIN
		success := TRUE;
		Expr;
		success := success and (CurrentSymbol = endSym);
		Parse :=success;
	END;

	FUNCTION ErrorPos : Integer;
	BEGIN
		ErrorPos := CurrentSymbolPos;
	END;

	FUNCTION GetResult : INTEGER;
	BEGIN
		GetResult := 0;
	END;
end.