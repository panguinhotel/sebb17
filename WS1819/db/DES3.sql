--1
Select f.title from film f
where f.film_id not in (Select i.film_id from inventory i);

--2
select Count(*) as Summe,c.first_name as Vorname, c.last_name as Nachname 
from rental r
join customer c on c.customer_id = r.customer_id
where r.return_date is null
group by r.customer_id, c.first_name, c.last_name
order by Count(*) desc;

--3
SELECT title
FROM film
WHERE original_language_id IS NOT NULL;	

--4
Select Count(*)
from rental r
where r.RENTAL_DATE between to_date('01.01.2014','dd.mm.yyyy') and  to_date('31.12.2014','dd.mm.yyyy');

--5
Select i.inventory_id from inventory i
where i.inventory_id not in 
(Select Distinct t.inventory_id from rental t);

--6
SELECT first_name, last_name, COUNT(film_id)
FROM actor
   INNER JOIN film_actor USING (actor_id)
GROUP BY first_name, last_name
ORDER BY COUNT(film_id) desc;

--7

--8
