PROGRAM ScannerTest;

USES
	Scanner;

VAR
	input : String;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
		input := 'true+(not false => t+ false * - true <=>t eq(t+-f)    ) or t and f imp t';
		InitScanner(input);
		WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN GetNextSymbol;END;
		Write('TEST - EXPRECTED : Success -> RESULT : ');
		IF CurrentSymbol = noSym THEN begin WriteLn('ERROR at position :  ', CurrentSymbolPos);
		end ELSE BEGIN WriteLn('Success');END;

		input := 'true+(not false =b> t+ false * - true <=>t eq(t+-f)    ) or t and f imp t';
		InitScanner(input);
		WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN GetNextSymbol;END;
		Write('TEST - EXPRECTED : ERROR -> RESULT : ');
		IF CurrentSymbol = noSym THEN begin WriteLn('ERROR at position :  ', CurrentSymbolPos);
		end ELSE BEGIN WriteLn('Success');END;

		input := 'true+(not false => t+ false * dasf- true <=>t eq(t+-f)    ) or t and f imp t';
		InitScanner(input);
		WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN GetNextSymbol;END;
		Write('TEST - EXPRECTED : ERROR -> RESULT : ');
		IF CurrentSymbol = noSym THEN begin WriteLn('ERROR at position :  ', CurrentSymbolPos);
		end ELSE BEGIN WriteLn('Success');END;

		input := 'true+(not false => t+ false * - true <=>t eq(t+-f)))))    ) or t and f imp t';
		InitScanner(input);
		WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN GetNextSymbol;END;
		Write('TEST - EXPRECTED : Success -> RESULT : ');
		IF CurrentSymbol = noSym THEN begin WriteLn('ERROR at position :  ', CurrentSymbolPos);
		end ELSE BEGIN WriteLn('Success');END;

		input := 'true+(not false => t+ false * - true <=>t eq(t+-f)))))    ) or t and f imp t';
		InitScanner(input);
		WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN GetNextSymbol;END;
		Write('TEST - EXPRECTED : Success -> RESULT : ');
		IF CurrentSymbol = noSym THEN begin WriteLn('ERROR at position :  ', CurrentSymbolPos);
		end ELSE BEGIN WriteLn('Success');END;

		input := 'true+(not false => t+ false * - true <=>t eq(t+-f)))))    ) or t and f imp t';
		InitScanner(input);
		WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN GetNextSymbol;END;
		Write('TEST - EXPRECTED : Success -> RESULT : ');
		IF CurrentSymbol = noSym THEN begin WriteLn('ERROR at position :  ', CurrentSymbolPos);
		end ELSE BEGIN WriteLn('Success');END;

		input := 'true+not im abcdFFF';
		InitScanner(input);
		WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN GetNextSymbol;END;
		Write('TEST - EXPRECTED : ERROR -> RESULT : ');
		IF CurrentSymbol = noSym THEN begin WriteLn('ERROR at position :  ', CurrentSymbolPos);
		end ELSE BEGIN WriteLn('Success');END;

		input := 'true+true t true t t t t t t tttttttt    ) or t and f imp t';
		InitScanner(input);
		WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN GetNextSymbol;END;
		Write('TEST - EXPRECTED : Success -> RESULT : ');
		IF CurrentSymbol = noSym THEN begin WriteLn('ERROR at position :  ', CurrentSymbolPos);
		end ELSE BEGIN WriteLn('Success');END;

END. (* ScannerTest *)