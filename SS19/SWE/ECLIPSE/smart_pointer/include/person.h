#pragma once

#include <iostream>
#include <istream>
#include <string>

class person {
public:
  person();
  person(std::string first_name, std::string last_name);
  person(const person &p);
  person(person &&);
  person& operator = (person v);
  virtual ~person();

  std::string get_first_name() const;
  std::string get_last_name() const;

  bool operator < (const person &p) const;

protected:
  virtual void print(std::ostream &os) const;
  virtual void read(std::istream &os);

  friend std::ostream& operator << (std::ostream &os, const person &p);
  friend std::istream& operator >> (std::istream &is, person &p);

  friend void swap(person &a, person &b);

  std::string first_name;
  std::string last_name;

};
