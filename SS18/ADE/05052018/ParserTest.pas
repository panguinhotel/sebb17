PROGRAM ParserTest;

USES
	ParserCalc;
VAR
	input: String;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	Write('-->');
	ReadLn(input);
	WHILE input <> ''  DO BEGIN
		InitParser(input);
		IF NOT Parse THEN BEGIN
			WriteLn('ERROR at position :  ', ErrorPos);
		END ELSE BEGIN
			WriteLn('Success. The result is: ', GetResult);
		END;
		Write('-->');
		ReadLn(input);
	END;

END. (* ParserTest *)