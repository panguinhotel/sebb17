unit UnitListDL;
	
interface
Uses UnitDate, UnitList;

TYPE    Testrun = RECORD    
		CreationDate: Date;
		Testresults: TestResultList;
		End;

		Node = ^TestrunRec;
		TestrunRec = RECORD
		CreationDate: Date;
		Testresults: TestResultList;
		prev, next: Node;
		END;
		List = Node;
    
(*Insert new Node after given*)
PROCEDURE InsertAfter(VAR l: List; value: Date);
(*Prints List*)
PROCEDURE DisplayList(VAR l: List);
(*Clears List*)
PROCEDURE ClearList(VAR l: List);
(*Initialize List*)
PROCEDURE InitList(VAR l: List);
implementation
(*Initialize List*)
PROCEDURE InitList(VAR l: List);
VAR 
  n: Node;
BEGIN
  (*Create Anchor*)
  New(n);
  n^.prev := n; (*!!!*)
  n^.next := n;
  l := n;
END;(*Initialize List*)

(*Disposes List*)
PROCEDURE DisposeList(l: List);
BEGIN
  ClearList(l);
  (*Dispose Anchor*)
  Dispose(l);
END;
(*ClearList*)
PROCEDURE ClearList(VAR l: List);
VAR  
  n, next: NODE;
BEGIN
  n := l^.next; 
  WHILE  n <> l DO BEGIN
    next := n^.next;
    Dispose(n);
    n := next;
  END;
  l^.next := l; 

END;(*ClearList*)

(*Display List*)
PROCEDURE DisplayList(VAR l: List);
VAR 
  n: Node;
BEGIN
  n := l^.next; 
  WHILE n <> l DO BEGIN
  	WriteLn('Testrun ',DateToString(n^.CreationDate));
    UnitList.DisplayList(n^.Testresults);
	WriteLn('=========================='); 
    n := n^.next
  END;
END;(*Display List*)

(*Creates Note*)
FUNCTION CreateNode(VAR n: Node; value: Date):BOOLEAN;
BEGIN
  New(n); (*TODO Check if this worked*)
  IF n = NIL  THEN BEGIN
		CreateNode := false;
	END
  ELSE BEGIN
	n^.CreationDate := value;
	UnitList.InitList(n^.Testresults);
	n^.prev := n; (*!!!*)
	n^.next := n; 

	CreateNode := true;
	End;
END;(*Creates Note*)
(*Inserts new Node after given*)
PROCEDURE InsertAfter(VAR l: List; value: Date);
VAR
	newNode: Node;
BEGIN
	CreateNode(newNode,value);
	l^.next^.prev := newNode;
	newNode^.next := l^.next;
	l^.next := newNode;
	newNode^.prev := l;
END; (* InsertAfter *)
end.