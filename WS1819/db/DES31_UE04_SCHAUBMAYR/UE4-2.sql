--OLD
DROP TABLE top_salaries;
CREATE TABLE top_salaries 	(salary	NUMBER(8,2));



ALTER TABLE top_salaries add emp_cnt NUMBER(3) DEFAULT NULL; -- od. and. numerischer Datentyp, ohne Dezimalstellen
ALTER TABLE top_salaries ADD CONSTRAINT top_salaries_pk PRIMARY KEY (salary);
ALTER TABLE top_salaries ADD CONSTRAINT top_salaries_emp_cnt_gt0 CHECK (emp_cnt > 0);

DELETE FROM top_salaries;

DECLARE
  num         NUMBER(3) := &p_num;
  sal         employees.salary%TYPE;
  vEmp_cnt    top_salaries.emp_cnt%TYPE;
  CURSOR        emp_cursor IS
SELECT   salary, COUNT (*)
    FROM employees
group by salary	
ORDER BY salary DESC;
BEGIN
  OPEN emp_cursor;
  FETCH emp_cursor INTO  sal, vEmp_cnt;
  WHILE emp_cursor%ROWCOUNT <= num AND emp_cursor%FOUND LOOP
    INSERT INTO top_salaries (salary, emp_cnt)
    VALUES (sal, vEmp_cnt);	
    FETCH emp_cursor INTO sal, vEmp_cnt;
  END LOOP;
  CLOSE emp_cursor;  
END;
/

SELECT * FROM top_salaries;

--2.1
ALTER TABLE top_salaries add(
createdBy VARCHAR2(256) DEFAULT NULL,
dateCreated Date Default Null,
modifiedBy VARCHAR2(256) DEFAULT NULL,
dateModified Date DEFAULT NULL);

--2.2
/
CREATE OR REPLACE PROCEDURE InsertTopSalaries(pSalary IN NUMBER, 
pEmp_cnt in NUMBER)
IS
BEGIN
    INSERT INTO top_salaries (salary,emp_cnt, createdBy, dateCreated, modifiedBy, dateModified)
    VALUES (pSalary,pEmp_cnt, USER, SYSDATE, USER, SYSDATE);	
END;
/
BEGIN
  InsertTopSalaries(1000,4);
END;


--2.3
DECLARE
  num         NUMBER(3) := &p_num;
  sal         employees.salary%TYPE;
  vEmp_cnt    top_salaries.emp_cnt%TYPE;
  CURSOR        emp_cursor IS
SELECT   salary, COUNT (*)
    FROM employees
group by salary	
ORDER BY salary DESC;
BEGIN
  OPEN emp_cursor;
  FETCH emp_cursor INTO  sal, vEmp_cnt;
  WHILE emp_cursor%ROWCOUNT <= num AND emp_cursor%FOUND LOOP
    InsertTopSalaries(sal,vEmp_cnt);
    FETCH emp_cursor INTO sal, vEmp_cnt;
  END LOOP;
  CLOSE emp_cursor;  
END;

Select * from top_salaries;