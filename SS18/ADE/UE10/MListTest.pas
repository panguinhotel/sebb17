PROGRAM MListTest;

USES
   MetaInfo, MLStr, MLint, MLColl, MLVect, MLObj,MLListe;
VAR
	v: MLList;
	i: INTEGER;
	revIterator: MLIterator;
	w: MLVector;
   	vectorIterator: MLIterator;
	o: MLObject;
	iStr : String;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	v := NewMLList;
   	FOR i := 1 TO 10 DO BEGIN
	   	Str(i,iStr);
    	WriteLn('Test Add '+iStr);v^.Add(NewMLInteger(i));
   	END;
   	WriteLn('Test Add '+'Test');v^.Add(NewMLString('Test'));
   	v^.WriteAsString;

	WriteLn('Test ReverseIterator ');
	revIterator := v^.NewReverseIterator;
	o := revIterator^.Next;
	while o <> NIL DO BEGIN
       o^.WriteAsString;
       o := revIterator^.Next;
    END;
	Dispose(revIterator,Done);
	WriteLn();

	WriteLn('Test Iterator ');
    revIterator := v^.NewIterator;
	o := revIterator^.Next;
	while o <> NIL DO BEGIN
       o^.WriteAsString;
       o := revIterator^.Next;
   	END;

	WriteLn('Test Prepend "Value" ');
	v^.Prepend(NewMLString('Value'));
	v^.WriteAsString;

	WriteMetaInfo;   
	WriteLn('Test Dispose Iterator ');Dispose(revIterator,Done);

	WriteMetaInfo;
	WriteLn('Test Dispose Elements ');v^.DisposeElements;
	WriteMetaInfo;
	WriteLn('Test Dispose List ');Dispose(v,Done);
	WriteMetaInfo;

END. (* MListTest *)