PROGRAM jmin;

USES 
  Parser;
  
VAR
  inputFile: TEXT;
  outputFile: TEXT;
  ok,useFile: BOOLEAN;
  errLine, errColumn: INTEGER;
  
BEGIN
  IF ParamCount < 1 THEN BEGIN
    WriteLn('Error: No sourcefile');
    Halt(1);
  END
  ELSE IF ParamCount > 2 THEN BEGIN
    WriteLn('Error: Too many Parameters');
    Halt(2);
  END
  ELSE BEGIN
    Assign(inputFile, ParamStr(1));
    (*$I-*)
    Reset(inputFile);
    (*$I+*)
    IF IOResult <> 0 THEN BEGIN
      WriteLn('Error, opening source file.');
      Halt(3);
    END;
    useFile := ParamCount = 2;
    IF useFile THEN BEGIN
      Assign(outputFile, ParamStr(2));
      (*$I-*)
      Reset(outputFile);
      Rewrite(outputFile);
      //Flush(Output);
      (*$I+*)
      IF IOResult <> 0 THEN BEGIN
        WriteLn('Error, opening destination file.');
        Halt(4);
      END;
    END;
  END;

  InitParser(inputFile,outputFile,useFile);
  ok := Parse;
  Close(inputFile);
  if(useFile) THEN
    Close(outputFile);
    
 
END.