/*
 * skiplist.h
 *
 *  Created on: 15.03.2019
 *      Author: schau
 */

#ifndef SKIPLIST_H_
#define SKIPLIST_H_

#include <vector>
#include <limits>
#include <cstdlib>
#include <iostream>

template<class T,int MAXSIZE>
class skip_list {

private:
	template<typename U>
	struct skip_node{
		U key;
		std::vector<skip_node*> forward;
		skip_node<U> (U k,  int level): key(k),forward(level,nullptr){}
	};

public:
	skip_list(){

		T headKey = std::numeric_limits<T>::min();
		m_head = new skip_node<T>(headKey, MAXSIZE);

		T tailKey = std::numeric_limits<T>::max();
		m_tail = new skip_node<T>(tailKey, MAXSIZE);

		std::fill(begin(m_head->forward), end(m_head->forward), m_tail);

	}

	virtual ~skip_list() {
		auto* node = m_head;
		while (node->forward.at(0))
		{
			auto* tmp = node;
			node = node->forward.at(0);
			delete tmp;
		}
		delete node;
	}
	void init(int probability = 0.5){
		m_probability = probability;
	}

	skip_node<T>* find(T key){
		skip_node<T>* result = nullptr;
		if (auto x = lower_bound(key))
		{
			if(x->key == key && x != m_tail)
				result = x;
		}
		return result;
	}



	void insert(T key){

		if(!find(key))
		{
			std::vector<skip_node<T>*> pred = predecessors(key);
			const int nextLevel = random_level();
			auto * node = make_node(key,nextLevel);

			for(int i = 0; i < nextLevel; ++i)
			{
				node->forward[i] = pred[i]->forward[i];
				pred[i]->forward[i] = node;
			}
		}

	}

	void erase(T key){
		auto pred = predecessors(key);
		auto *node = pred.at(0)->forward.at(0);
		if(node->key != key || node == m_tail)
			return;

		for(auto i = 0; i < node_level(node); ++i)
		{
			pred[i].forward[i] = node->forward.at(i);
		}
		delete node;
	}

	typedef skip_set_iterator iterator;
	iterator find(T value);
	iterator begin();
	iterator end();
private:

	skip_node<T>* m_head;
	skip_node<T>* m_tail;
	float m_probability = 0.5;
	int m_max_level = MAXSIZE;


	skip_node<T>* lower_bound(T key) const{
		auto *node = m_head;
		for(auto i = m_head->forward.size(); i-- > 0;)
		{
			while(node->forward.at(i)->key < key)
				node = node->forward.at(i);
		}
		return node->forward.at(0);
	}
	int random_level() const{
		int tmp = 1;
		while ((static_cast<double>(std::rand()) /RAND_MAX) < m_probability && tmp < m_max_level)
		{
			tmp++;
		}
		return tmp;
	}
	skip_node<T>* make_node(T key, int level){
		return new skip_node<T>(key,level);
	}

	std::vector<skip_node<T>*> predecessors(T key) const
							{
		std::vector<skip_node<T>*> returnVal(m_head->forward.size(),nullptr);
		auto *node = m_head;

		for(int i = m_head->forward.size(); i-- > 0;)
		{
			while(node->forward.at(i)->key < key)
				node = node->forward.at(i);

			returnVal[i] = node;
		}

		return returnVal;
							}

};



#endif /* SKIPLIST_H_ */
