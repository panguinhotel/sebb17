<?php
   include('session.php');
?>
<html>  
   <head>
    <title>Welcome</title>
          <link rel="stylesheet" type="text/css" href="style.css">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
   </head>
   
   <body>
   		<h1 class="headLine">Bücher</h2>
   		<div class="mainList">
		   	<button class="collapsible">Buch</button>
				<div class="collapsDiv">
					<button class="mainButton lowerButton" onclick="window.location.href='./books/showa.php'">Alle Bücher</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./books/show.php'">Meine Bücher</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./books/new.php'">Einfügen</button><br>
				</div>
			<button class="collapsible">Autor</button>
				<div class="collapsDiv">
					<button class="mainButton lowerButton" onclick="window.location.href='./autor/show.php'">Anzeige</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./autor/new.php'">Einfügen</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./autor/edit.php'">Ändern</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./autor/delete.php'">Löschen</button><br>
				</div>
			<button class="collapsible">Kategorie</button>
				<div class="collapsDiv">
					<button class="mainButton lowerButton" onclick="window.location.href='./kategorie/show.php'">Anzeige</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./kategorie/new.php'">Einfügen</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./kategorie/edit.php'">Ändern</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./kategorie/delete.php'">Löschen</button><br>
				</div>
			<button class="collapsible">Buchtyp</button>
				<div class="collapsDiv">
					<button class="mainButton lowerButton" onclick="window.location.href='./typ/show.php'">Anzeige</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./typ/new.php'">Einfügen</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./typ/edit.php'">Ändern</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./typ/delete.php'">Löschen</button><br>
				</div>	
			<button class="collapsible">Verlag</button>
				<div class="collapsDiv">
					<button class="mainButton lowerButton" onclick="window.location.href='./verlag/show.php'">Anzeige</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./verlag/new.php'">Einfügen</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./verlag/edit.php'">Ändern</button><br>
					<button class="mainButton lowerButton" onclick="window.location.href='./verlag/delete.php'">Löschen</button><br>
				</div>
			<button class="backbutton" onclick="window.location.href='welcome.php'">Zurück</button>
		</div>
		
	<script>
		var coll = document.getElementsByClassName("collapsible");
		var i;

		for (i = 0; i < coll.length; i++) {
		coll[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var content = this.nextElementSibling;
			if (content.style.display === "block") {
			content.style.display = "none";
			} else {
			content.style.display = "block";
			}
		});
		}
	</script>
   </body>
   
</html>