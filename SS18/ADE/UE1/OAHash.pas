unit OAHash;
	
interface
uses
	MyExtensions;

TYPE
	PNode = ^TNode;
	TNode = Record
		data : AddressBookEntry;
	End;

	THashTableIndex = 0..37;
	OTHashTable = Array[THashTableIndex] OF PNode;

(*Initialize HashTable*)
PROCEDURE InitHashTable(Var ht: OTHashTable);
(*Add Entry to HashTable*)
FUNCTION AddNodeToHashTable(Var ht: OTHashTable; book:AddressBookEntry) : Boolean;
(*Dispose HashTable*)
PROCEDURE DisposeHashTable(Var ht: OTHashTable);
(*DisposeEntry*)
FUNCTION DisposeEntry(Var ht: OTHashTable; vorname,nachname : string) : Boolean;
(*getNumberForName*)
FUNCTION getNumberForName(Var ht: OTHashTable; book:AddressBookEntry) : string;
(*Display HashTable*)
PROCEDURE DisplayHashTable(Var ht: OTHashTable);
implementation

(*Initialize HashTable*)
PROCEDURE InitHashTable(Var ht: OTHashTable);
Var
	i : THashTableIndex;
BEGIN
	FOR i:=Low(THashTableIndex) TO High(THashTableIndex) DO BEGIN
		ht[i] := NIL;
	END;
END; (* InitHashTable *)
	
(*ComputeHash*)
FUNCTION ComputeHash(data: String) : THashTableIndex;
Var
	hash : LongInt;
	i:Integer;
BEGIN
	hash:=0;
	FOR i:=1 TO Length(data) DO BEGIN
		hash:=(hash + ORD(data[i])) MOD (High(THashTableIndex) - Low(THashTableIndex));
	END;
	ComputeHash := hash + Low(THashTableIndex);
END; (* ComputeHash *)
(*ContainsValue*)
FUNCTION ContainsValue(Var ht: OTHashTable; vorname,nachname : string) : Boolean;
VAR
	hash: THashTableIndex;
	found : Boolean;
	tryCnt:Integer;
	hashTableSize : Integer;
BEGIN
		hash := ComputeHash(vorname + nachname);
		tryCnt := 0;
		found := FALSE;

		hashTableSize := High(THashTableIndex) - Low(THashTableIndex) +1;
		WHILE (NOT found) AND (tryCnt < hashTableSize)  DO BEGIN
			IF ht[hash] <> NIL  THEN BEGIN
				found := (ht[hash]^.data.vorname = vorname) and (ht[hash]^.data.nachname = nachname);
			END;			
			hash := (hash +1) MOD hashTableSize;
			Inc(tryCnt);
		END;
	ContainsValue := found;
END; (* ContainsValue *)
(*Add Entry to HashTable*)
FUNCTION AddNodeToHashTable(Var ht: OTHashTable; book:AddressBookEntry) : Boolean;
Var
	hash: THashTableIndex;
	node: PNode;
	tryCnt:Integer;
	hashTableSize : Integer;
BEGIN
	IF not ContainsValue(ht,book.vorname,book.nachname)  THEN BEGIN
		New(node);
		node^.data := book;
		hash := ComputeHash(book.vorname+book.nachname);
		tryCnt := 0;
		hashTableSize := High(THashTableIndex) - Low(THashTableIndex) +1;
		WHILE (tryCnt < hashTableSize) DO BEGIN
			hash := (hash+1) MOD hashTableSize;
			Inc(tryCnt);
		END;

		IF ht[hash] <> NIL  THEN BEGIN
			AddNodeToHashTable:= false;
			EXIT;
		END;
		AddNodeToHashTable:= true;
		ht[hash] := node;
		
	END
	ELSE AddNodeToHashTable:= false;
	
END; (* AddNodeToHashTable *)
(*Describe me*)
FUNCTION getNumberForName(Var ht: OTHashTable; book:AddressBookEntry) : string;
Var
	hash: THashTableIndex;
	hashTableSize : Integer;
	tryCnt:Integer;
BEGIN
	tryCnt:= 0;
	hash := ComputeHash(book.vorname+book.nachname);
	WHILE tryCnt <> High(THashTableIndex)  DO BEGIN
		IF ht[hash] <> NIL  THEN BEGIN
			IF (ht[hash]^.data.vorname = book.vorname) and (ht[hash]^.data.nachname = book.nachname)  THEN BEGIN
			getNumberForName := ht[hash]^.data.nummer;
			EXIT;
			END;
		END;
		
		hashTableSize := High(THashTableIndex) - Low(THashTableIndex) +1;
		hash := (hash+1) MOD hashTableSize;
		Inc(tryCnt);
	END;
END; (* getNumberForName *)
(*Display HashTable*)
PROCEDURE DisplayHashTable(Var ht: OTHashTable);
VAR
    i: INTEGER;
	node : PNode;
BEGIN
    FOR i := LOW(ht) TO High(ht) DO BEGIN
		node := ht[i];

		IF node <> NIL  THEN BEGIN
			WriteLn(node^.data.vorname,^i,node^.data.nachname,^i,node^.data.nummer);
		END;
    END; (* FOR *)
END; (* DisplayHashTable *)
(*Dispose HashTable*)
PROCEDURE DisposeHashTable(Var ht: OTHashTable);
Var
	i: Integer;
BEGIN
	FOR i := LOW(ht) TO High(ht) DO BEGIN
			Dispose(ht[i]);
			ht[i] := NIL;
    END; (* FOR *)
END; (* DisposeHashTable *)
(*DisposeEntry*)
FUNCTION DisposeEntry(Var ht: OTHashTable; vorname,nachname : string) : Boolean;
Var
	hash: THashTableIndex;
	hashTableSize : Integer;
	tryCnt:Integer;
BEGIN
	tryCnt := 0;
	hash := ComputeHash(vorname+nachname);
	WHILE tryCnt <> High(THashTableIndex) DO BEGIN
		IF (ht[hash] <> NIL) and  (ht[hash]^.data.vorname = vorname) and (ht[hash]^.data.nachname = nachname)  THEN BEGIN
			Dispose(ht[hash]);
			ht[hash] := NIL;
			DisposeEntry:=true;
			EXIT;
		END;
		hashTableSize := High(THashTableIndex) - Low(THashTableIndex) +1;
		hash := (hash+1) MOD hashTableSize;
		Inc(tryCnt);
	END;
	DisposeEntry:=false;
END; (* DisposeEntry *)
end.