#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
    int level = 0;
    char name[11];
    char password[11];

    if (argc != 2) {
        printf("usage: buffer-overflow <username>\n");
        printf("e.g. \"buffer-overflow admin\"\n");
        printf("then enter your password\n");
        printf("NOTE: max 10 chars are allowed\n");
        return -1;
    }

    strcpy(name, argv[1]);
    printf("password: ");
    scanf("%s", password);

    if (strcmp(name, "admin") == 0 &&
        strcmp(password, "pass") == 0) {
            level = 97;
    }

    printf("username = \"%s\", password = \"%s\", access level = %d (0x%x, '%c')\n", name, password, level, level, level);
    if (level == 97) {
      printf("FULL ACCESS GRANTED\nSource Code Password is \"bufovfl\"");
    } else {
      printf("You need access level = %d (0x%x, '%c') to access the ZIP-File password", 97, 97, 97);
    }

    return 0;
}