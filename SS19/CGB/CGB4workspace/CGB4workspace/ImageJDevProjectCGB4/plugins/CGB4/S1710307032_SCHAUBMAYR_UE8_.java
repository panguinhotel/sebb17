import java.awt.Rectangle;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.gui.Roi;
import ij.plugin.filter.PlugInFilter;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;



public class S1710307032_SCHAUBMAYR_UE8_ implements PlugInFilter {

	public static int BG_VAL = 255;
	public static int FG_VAL = 0;
	public static boolean isBilinear = true;
	public static int regRounds = 5;
	public static int m_stepsPerSide = 10;
	public static double m_stepSize = 2;

	public int setup(String arg, ImagePlus imp) {
		if (arg.equals("about"))
		{showAbout(); return DONE;}
		return DOES_8G+DOES_STACKS+SUPPORTS_MASKING;
	} //setup



	/**
	 * Method to Transform Img
	 * 
	 * @param inImg
	 * @param width
	 * @param height
	 * @param transX
	 * @param transY
	 * @param rotAngle
	 * @param destWidth
	 * @param destHeight
	 * @param useBilinear
	 * @return transformed image
	 */
	public int[][] transformImg(int[][] inImg, int width, int height, 
			double transX, double transY, double rotAngle, int destWidth, int destHeight, boolean useBilinear) 
	{
		return transformImg(inImg,width,height,transX,transY,rotAngle,destWidth,destHeight,new Roi(new Rectangle(0,0,destWidth,destHeight)),useBilinear);
	}
	
	/**
	 * Method to Transform Img
	 * 
	 * @param inImg
	 * @param width
	 * @param height
	 * @param transX
	 * @param transY
	 * @param rotAngle
	 * @param useBilinear
	 * @return transformed image
	 */
	public int[][] transformImg(int[][] inImg, int width, int height, 
			double transX, double transY, double rotAngle,boolean useBilinear) 
	{
		return transformImg(inImg,width,height,transX,transY,rotAngle,width,height,useBilinear);
	}

	/**
	 * Method to Transform Img
	 * 
	 * @param inImg
	 * @param width
	 * @param height
	 * @param transX
	 * @param transY
	 * @param rotAngle
	 * @param destWidth
	 * @param destHeight
	 * @param region
	 * @param useBilinear
	 * @return transformed image
	 */
	public int[][] transformImg(int[][] inImg, int width, int height, 
			double transX, double transY, double rotAngle, 
			int destWidth, int destHeight, Roi region, boolean useBilinear) {

		int[][] tmpImg = new int[destWidth][destHeight];
		for(int i = 0; i < destWidth; ++i)
		{
			for(int k = 0; k < destHeight ; ++k)
			{
				if(i < width && k < height)
					tmpImg[i][k] = inImg[i][k];
				else
					tmpImg[i][k] = FG_VAL;
			}
		}

		//apply backward mapping
		int[][] resultImg = new int[region.getBounds().width][region.getBounds().height];

		//convert from degree to rad
		double rotAngleRad = -rotAngle * Math.PI / 180.0;
		double cosTheta = Math.cos(rotAngleRad);
		double sinTheta = Math.sin(rotAngleRad);

		double halfWidth = destWidth / 2.0;
		double halfHeight = destHeight / 2.0;


		//apply backward mapping from result imgae coordinates in input image are calculated
		for(int x = 0; x < region.getBounds().width; x++) {
			for(int y = 0; y < region.getBounds().height; y++) {
				double posX = region.getBounds().x+x;
				double posY = region.getBounds().y+y;

				//step1 move to center
				posX -= halfWidth;
				posY -= halfHeight;


				//step2 rotate
				double rotatedX = posX * cosTheta + posY * sinTheta;
				double rotatedY = -posX * sinTheta + posY * cosTheta;
				posX = rotatedX;
				posY = rotatedY;

				//step3 move back from center
				posX += halfWidth;
				posY += halfHeight;


				//step 4 translate
				posX -= transX;
				posY -= transY;

				//step5 apply value using NN interpoaltion
				int finalVal;
				
				if(useBilinear)
					finalVal = getBilinearInterpolatedValue(posX,posY,destWidth,destHeight, tmpImg);
				else 
					finalVal = getNNinterpolatedValue(posX, posY, destWidth, destHeight, tmpImg);

				resultImg[x][y] = finalVal;
			}
		}


		return resultImg;
	}

	/**
	 * Method to calculate Error Metric
	 * 
	 * @param refImg - static reference image
	 * @param testImg - moving image getting transformed
	 * @param width
	 * @param height
	 * @return error metric
	 */
	public double getImgDiffSSE(int[][] refImg, int[][] testImg, int width, int height) {
		return getImgDiffSSE(refImg,testImg,new Roi(0,0,width,height));
	}
	
	/**
	 * Method to calculate Error Metric
	 * 
	 * @param refImg
	 * @param testImg
	 * @param region
	 * @return error metric
	 */
	public double getImgDiffSSE(int[][] refImg, int[][] testImg, Roi region) {
		double totalError = 0.0;

		for(int x = 0; x < region.getBounds().width; x++) {
			for(int y = 0; y < region.getBounds().height; y++) {
				int diff = refImg[x+region.getBounds().x][y+region.getBounds().y] - testImg[x][y];
				totalError += diff * diff;
			} 
		}

		return totalError;
	}


	/**
	 * Method to get Nearest Neighbour Interpolated Value
	 * 
	 * @param xIdx - double x-image coordinate
	 * @param yIdx - double y-image coordinate
	 * @param width
	 * @param height
	 * @param img
	 * @return interpolated value
	 */
	public static int getNNinterpolatedValue(double xIdx, double yIdx, int width, int height, int[][] img) {
		//just round the coordinates
		int xCoord = (int) (xIdx + 0.5);
		int yCoord = (int) (yIdx + 0.5);

		if(xCoord >= 0 && xCoord < width && yCoord >= 0 && yCoord < height) {
			return img[xCoord][yCoord];
		}
		return FG_VAL; 
	}

	/**
	 * Method to get Bilinear Interpolated Value
	 * 
	 * @param xIdx
	 * @param yIdx
	 * @param width
	 * @param height
	 * @param img
	 * @return interpolated value
	 */
	public static int getBilinearInterpolatedValue(double xIdx, double yIdx, int width, int height, int[][] img)
	{
		int lowX = (int) Math.floor(xIdx);
		int highX = (int) Math.ceil(xIdx);
		int lowY = (int) Math.floor(yIdx);
		int highY = (int) Math.ceil(yIdx);

		if(lowX < 0 && highX >=0)
			lowX = 0;
		
		if(highX >= width && lowX < width)
			highX = width-1;
		
		if(lowY < 0 && highY >=0)
			lowY = 0;
		
		if(highY >= height && lowY < height)
			highY = height-1;
		
		
		
		double leftValue = 0;
		double rightValue = 0;
		
		
		if(lowX >= 0 && lowX < width && highX < width && highX >= 0 && lowY >= 0 && lowY < height && highY < height && highY >= 0 )
		{		
			if(lowY == highY)
			{
				leftValue = img[lowX][lowY];
				rightValue = img[highX][lowY];
			}
			else
			{
				leftValue = (highY-yIdx)/(highY-lowY)*img[lowX][lowY] + (yIdx-lowY)/(highY-lowY)*img[lowX][highY];
				rightValue = (highY-yIdx)/(highY-lowY)*img[highX][lowY] + (yIdx-lowY)/(highY-lowY)*img[highX][highY];
			}
					
			if(lowX == highX)
				return (int) leftValue;
			else
			{
				return (int) ((highX-xIdx)/(highX-lowX)*leftValue + (xIdx-lowX)/(highX-lowX)*rightValue);
			}
			
		}
		return FG_VAL;

	}

	/**
	 * method for automated registration utilizing 21x21x21 permutations for the transformation parameters
	 * 
	 * @param refImg
	 * @param testImg
	 * @param width
	 * @param height
	 * @return registered Image
	 */
	public int[][] getRegisteredImage(int[][] refImg, int[][] testImg, int width,int height) {
		return getRegisteredImage(refImg, testImg, width, height, new Roi(0,0,width,height));
	}
	
	/**
	 * method for automated registration utilizing 21x21x21 permutations for the transformation parameters
	 * 
	 * @param refImg
	 * @param testImg
	 * @param width
	 * @param height
	 * @param region
	 * @return registered Image
	 */
	public int[][] getRegisteredImage(int[][] refImg, int[][] testImg, int width, int height,Roi region) {
		//global best solution
		double bestTx = 0.0;
		double bestTy = 0.0;
		double bestRot = 0.0;
		double bestError = getImgDiffSSE(refImg, testImg, region);

		double stepSize = m_stepSize;
		int stepsPerSide= m_stepsPerSide; //i.e. radius ==> 21 iterations per dimension, 21^3 == 9261 in total 

		//6.2. c) use one for loop with 5 iterations there reducing the stepSize incrementally
		for(int i = 0; i < regRounds; ++i)
		{
			double LocalbestTx = 0.0;
			double LocalbestTy = 0.0;
			double LocalbestRot = 0.0;
			double LocalbestError = getImgDiffSSE(refImg, testImg,region);
			
			//first check all 1331 permutations
			for(int x = -stepsPerSide; x <= stepsPerSide; x++) { //21 steps, r=10
				for(int y = -stepsPerSide; y <= stepsPerSide; y++) { //21 steps, r=10
					for(int r = -stepsPerSide; r <= stepsPerSide; r++) { //21 steps, r=10 for ROT
						double tXtoUse = x * stepSize; 
						double tYtoUse = y * stepSize; 
						double rotToUse = r * stepSize; //better search at lower granularity for rotation

						int[][] transformedImg = transformImg(testImg, region.getBounds().width, region.getBounds().height, tXtoUse, tYtoUse,rotToUse,isBilinear);
						double currError = getImgDiffSSE(refImg, transformedImg,region);
						//better solution found??? if yes - then adjust the global best

						if(currError < LocalbestError) {
							LocalbestError = currError;
							LocalbestTx = tXtoUse;
							LocalbestTy = tYtoUse;
							LocalbestRot = rotToUse;
							IJ.log("Local better solution at Ty=" + LocalbestTx + " Ty=" + LocalbestTy + " rot= " + LocalbestRot + ", ERR= " + LocalbestError);
						}
					}
				} 
			}
			if(LocalbestError < bestError) {
				bestError = LocalbestError;
				bestTx = LocalbestTx;
				bestTy = LocalbestTy;
				bestRot = LocalbestRot;
				
				IJ.log("Global better solution at Ty=" + bestTx + " Ty=" + bestTy + " rot= " + bestRot + ", ERR= " + bestError);
			}
			
			stepSize = stepSize*0.9;
		}
	
		//finally calc the best solution
		int[][] resultTransImg = transformImg(testImg, region.getBounds().width,  region.getBounds().height,  bestTx, bestTy, bestRot,true);
		return resultTransImg;
	}

	/**
	 *  static member function run of ImageJ Plugin
	 */
	public void run(ImageProcessor ip) {
		byte[] pixels = (byte[])ip.getPixels();
		int width = ip.getWidth();
		int height = ip.getHeight();
		int[][] inDataArrInt = ImageJUtility.convertFrom1DByteArr(pixels, width, height);

		String rightPath;
		String leftPath;
		
		 GenericDialog gd = new GenericDialog("New Image");
	      gd.addStringField("Left Image Absolute Path: ", "C:\\Users\\schau\\Documents\\FH-GIT\\SS19\\CGB\\CGB4workspace\\CGB4workspace\\ImageJDevProjectCGB4\\right_part.tif",100);
	      gd.addStringField("Right Image Absolute Path: ", "C:\\Users\\schau\\Documents\\FH-GIT\\SS19\\CGB\\CGB4workspace\\CGB4workspace\\ImageJDevProjectCGB4\\left_part_inverted.tif",100);
	      gd.addChoice("Interpolation method", new String[]{"Bilinear(slow)", "NN(faster)"},"Bilinear(slow)");
	      gd.addNumericField("Registration Rounds:", regRounds, 1);
	      gd.addNumericField("Steps per Side:", m_stepsPerSide, 1);
	      gd.addNumericField("Step Size:", m_stepSize,3);
	      gd.showDialog();
	     
	      if (gd.wasCanceled()) return;
	      rightPath = gd.getNextString();
	      leftPath = gd.getNextString();
	      isBilinear = gd.getNextChoice() == "Bilinear(slow)";
	      regRounds = (int) gd.getNextNumber();
	      m_stepsPerSide= (int) gd.getNextNumber();
	      m_stepSize = gd.getNextNumber();
	      
					
		
		Rectangle roiRectR = new Rectangle(1710,270,100,400);
		int[][] transformedImageR;
		int[][] transformedImageR_ROI;
		int[][] autoRegResultImgR;
		ImagePlus ipRight = new ImagePlus(rightPath);
		{

			ByteProcessor bpR = new ByteProcessor(ipRight.getBufferedImage());
			int widthB = bpR.getWidth(); int heightB = bpR.getHeight();
			byte[] pixelsB = (byte[])bpR.getPixels();
			int[][] in_B = ImageJUtility.convertFrom1DByteArr(pixelsB, widthB, heightB);
			ImageJUtility.showNewImage(in_B, widthB, heightB, "right image B");

			//initially transform input image to get a registration task
			double transX = 1700;
			double transY = 160;
			double rot = 0.0;


			transformedImageR = transformImg(in_B, widthB, heightB, transX, transY, rot,width,height,isBilinear);
			ImageJUtility.showNewImage(transformedImageR, width, height, "1_3_right");
			
			//ROI
			transformedImageR_ROI = transformImg(in_B, widthB, heightB, transX, transY, rot,width,height, new Roi(roiRectR),isBilinear);
			double initError = getImgDiffSSE(inDataArrInt, transformedImageR_ROI, new Roi(roiRectR));
			IJ.log("R_ROI init error = " + initError);
			
			ImageJUtility.showNewImage(transformedImageR_ROI,roiRectR.width, roiRectR.height, "R_ROI init moving image");
			
			autoRegResultImgR = getRegisteredImage(inDataArrInt, transformedImageR_ROI, width, height,new Roi(roiRectR));
			ImageJUtility.showNewImage(autoRegResultImgR, roiRectR.width, roiRectR.height, "R_ROI final reg result");
		}

		Rectangle roiRectL = new Rectangle(780,404,180,400);
		int[][] transformedImageL;
		int[][] transformedImageL_ROI;
		int[][] autoRegResultImgL;
		ImagePlus ipLeft = new ImagePlus(leftPath);
		{

			ByteProcessor bpR = new ByteProcessor(ipLeft.getBufferedImage());
			int widthB = bpR.getWidth(); int heightB = bpR.getHeight();
			byte[] pixelsB = (byte[])bpR.getPixels();
			int[][] in_B = ImageJUtility.convertFrom1DByteArr(pixelsB, widthB, heightB);
			ImageJUtility.showNewImage(in_B, widthB, heightB, "left image B");

			//initially transform input image to get a registration task
			double transX = 110;
			double transY = 350;
			double rot = 0.0;

			transformedImageL = transformImg(in_B, widthB, heightB, transX, transY, rot,width,height,isBilinear);	
			ImageJUtility.showNewImage(transformedImageL, width, height, "1_3_left");
			
			//ROI
			transformedImageL_ROI = transformImg(in_B, widthB, heightB, transX, transY, rot,width,height, new Roi(roiRectL),isBilinear);
			double initError = getImgDiffSSE(inDataArrInt, transformedImageL_ROI, new Roi(roiRectL));
			
			IJ.log("");
			IJ.log("");
			IJ.log("L_ROI init error = " + initError);
			
			ImageJUtility.showNewImage(transformedImageL_ROI,roiRectL.width, roiRectL.height, "L_ROI init moving image");
			
			autoRegResultImgL = getRegisteredImage(inDataArrInt, transformedImageL_ROI, width, height,new Roi(roiRectL));
			ImageJUtility.showNewImage(autoRegResultImgL, roiRectL.width, roiRectL.height, "L_ROI final reg result");
		}

		int[][] Arr1_4a = new int[width][height];
		int[][] Arr1_4b = new int[width][height];
		
		for(int i = 0; i < width; ++i)
		{
			for(int k = 0; k < height; ++k)
			{
				int sum = 0;
				int valueA = 0;
				int valueB = 0;
				if(transformedImageL[i][k] != BG_VAL && transformedImageL[i][k] != FG_VAL)
				{
					valueA += transformedImageL[i][k];
					valueB = transformedImageL[i][k];
					sum++;
				}
				if(transformedImageR[i][k] != BG_VAL && transformedImageR[i][k] != FG_VAL)
				{
					valueA += transformedImageR[i][k];
					valueB = transformedImageR[i][k];
					sum++;
				}
				if(inDataArrInt[i][k] != BG_VAL && inDataArrInt[i][k] != FG_VAL)
				{
					valueA += inDataArrInt[i][k];
					sum++;
				}



				if(sum > 0)
					Arr1_4a[i][k] = valueA / sum;
				else
					Arr1_4a[i][k] = FG_VAL;

				if(sum == 2)
					Arr1_4b[i][k] = valueB;
				else
					Arr1_4b[i][k] = Arr1_4a[i][k];
			}
		}


		ImageJUtility.showNewImage(Arr1_4a, width, height, "1_4a");
		ImageJUtility.showNewImage(Arr1_4b, width, height, "1_4b");

		
		//Put final image together
		int[][] finalData_A = new int[width][height];
		int[][] finalData_B = new int[width][height];
		for(int i = 0; i < width; ++i)
		{
			for(int k = 0; k < height; ++k)
			{
				int sum = 0;
				int valueA = 0;
				int valueB = 0;
				
				if(i >= roiRectL.x && i < roiRectL.x+roiRectL.width && k >= roiRectL.y && k < roiRectL.y+roiRectL.height
						&& autoRegResultImgL[i-roiRectL.x][k-roiRectL.y] != BG_VAL && autoRegResultImgL[i-roiRectL.x][k-roiRectL.y] != FG_VAL)
				{
					valueA += autoRegResultImgL[i-roiRectL.x][k-roiRectL.y];
					valueB = autoRegResultImgL[i-roiRectL.x][k-roiRectL.y];
					sum++;
					
				}
				else if(transformedImageL[i][k] != BG_VAL && transformedImageL[i][k] != FG_VAL)
				{
					valueA += transformedImageL[i][k];
					valueB = transformedImageL[i][k];
					sum++;
				}
				
				
				if(i >= roiRectR.x && i < roiRectR.x+roiRectR.width && k >= roiRectR.y && k < roiRectR.y+roiRectR.height
						&& autoRegResultImgR[i-roiRectR.x][k-roiRectR.y] != BG_VAL && autoRegResultImgR[i-roiRectR.x][k-roiRectR.y] != FG_VAL)
				{
					
					valueA += autoRegResultImgR[i-roiRectR.x][k-roiRectR.y];
					valueB = autoRegResultImgR[i-roiRectR.x][k-roiRectR.y];
					sum++;
				}
				else if(transformedImageR[i][k] != BG_VAL && transformedImageR[i][k] != FG_VAL)
				{
					valueA += transformedImageR[i][k];
					valueB = transformedImageR[i][k];
					sum++;
				}
				
				if(inDataArrInt[i][k] != BG_VAL && inDataArrInt[i][k] != FG_VAL)
				{
					valueA += inDataArrInt[i][k];
					sum++;
				}



				if(sum > 0)
					finalData_A[i][k] = valueA / sum;
				else
					finalData_A[i][k] = FG_VAL;

				if(sum == 2)
					finalData_B[i][k] = valueB;
				else
					finalData_B[i][k] = finalData_A[i][k];
			}
		}
		
		ImageJUtility.showNewImage(finalData_A, width, height, "3_a");
		ImageJUtility.showNewImage(finalData_B, width, height, "3_b");




	} //run

	/**
	 * ImageJ Plugin Show About Method
	 */
	void showAbout() {
		IJ.showMessage("About Template_...",
				"this is a PluginFilter template\n");
	} //showAbout

}