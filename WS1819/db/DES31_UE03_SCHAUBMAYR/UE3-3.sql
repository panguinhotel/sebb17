--UE3-3.1a
Select employee_id, last_name,hire_date,salary
from employees
where manager_id = 102;

--UE3-3.1b
COLUMN org_chart FORMAT A25
SELECT LPAD(last_name, LENGTH(last_name)+(LEVEL*2)-2,'_') AS org_chart, employee_id, hire_date, salary
  FROM employees
START WITH last_name='De Haan'
CONNECT BY PRIOR employee_id=manager_id;

--UE3-3.2
COLUMN org_chart FORMAT A25
SELECT LPAD(last_name, LENGTH(last_name)+(LEVEL*2)-2,'_') AS org_chart, employee_id, manager_id, LEVEL
  FROM employees
START WITH manager_id is null
CONNECT BY PRIOR employee_id = manager_id;