<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.fh-ooe.at/xml4/entlassungsbrief" xpath-default-namespace="http://www.fh-ooe.at/xml4/entlassungsbrief">
	<xsl:output method="html" encoding="ISO8859-1" indent="yes"/>
	<xsl:template match="/">
		<html>
			<head>
				<title>Entlassungsbrief</title>
				<style type="text/css">
					table.t2 {
						border-collapse: collapse;
						font-family: Georgia;
					  }
					  .t2 caption {
						padding-bottom: 0.5em;
						font-weight: bold;
						font-size: 16px;
					  }
					  .t2 th, .t2 td {
						padding: 4px 8px;
						border: 2px solid #fff;
						background: #dbe5f0;
					  }
					  .t2 thead th {
						padding: 2px 8px;
						background: #4f81bd;
						text-align: left;
						font-weight: normal;
						font-size: 13px;
						color: #fff;
					  }
					  .t2 tbody tr:nth-child(odd) *:nth-child(even), .t2 tbody tr:nth-child(even) *:nth-child(odd) {
						background: #4f81bd;
					  }
					  .t2 tr *:nth-child(3), .t2 tr *:nth-child(4) {
						text-align: right;
					  }
				</style>
			</head>
			<body>
				<xsl:apply-templates/>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="Entlassungsbrief">
		<h1>Entlassungsbrief</h1>
		Erzeugt am 
		<xsl:value-of select='format-dateTime(@erzeugt, "[D]. [MNn] [Y] um [H01]:[m01] Uhr", "de", (), ())'/>
		| Version: 
		<xsl:value-of select="@version"/>
		<hr/>
		<xsl:apply-templates/>
	</xsl:template>
		
	<xsl:template match="Ersteller">
		<b>Erstellt von</b>: <xsl:value-of select="text()"/><br/>
	</xsl:template>
	<xsl:template match="Empfänger">	
		<b>An</b>: <xsl:value-of select="text()"/><br/><br/>
	</xsl:template>
	
	<xsl:template match="Patient">
		<h2>Patient</h2>
			<xsl:for-each select="Titel[@position='vor']">
				<xsl:value-of select="text()"/>&#160;
			</xsl:for-each>
			<xsl:for-each select="Vorname">
				<xsl:value-of select="text()"/>&#160;
			</xsl:for-each>
			<xsl:for-each select="Nachname">
				<xsl:value-of select="text()"/>,
			</xsl:for-each>
			<xsl:for-each select="Titel[@position='nach']">
				<xsl:value-of select="text()"/>&#160;
			</xsl:for-each>
			<br/>
			<xsl:for-each select="Geschlecht">
				<b>Geschlecht</b>: <xsl:value-of select="text()"/> | 
			</xsl:for-each>
			<xsl:for-each select="Geburtsdatum">
				<b>geboren am</b>: <xsl:value-of select='format-date(text(), "[D]. [MNn] [Y]", "de", (), ())'/> | 
			</xsl:for-each>
			<xsl:for-each select="SVN">
				<b>SVN</b>: <xsl:value-of select="text()"/>
			</xsl:for-each>
			<br/><br/>
		<xsl:value-of select="text()"/><br/>
	</xsl:template>
			
	<xsl:template match="Aufenthalt">
	<h2>Aufenthalt</h2>
		<xsl:value-of select="text()"/><br/>
		<a><b><xsl:value-of select="@art"/> von</b>:
		<xsl:value-of select='format-date(@von, "[D]. [MNn] [Y]", "de", (), ())'/> <b> bis</b>: 
		<xsl:value-of select='format-date(@bis, "[D]. [MNn] [Y]", "de", (), ())'/></a>
	</xsl:template>
	
	<xsl:template match="Befundtext">
	<hr/>
		<xsl:for-each select="Anrede">
			<xsl:value-of select="text()"/><br/><br/>
		</xsl:for-each>
		<xsl:for-each select="Text">
			<i><xsl:value-of select="text()"/></i>
		</xsl:for-each>
		<hr/>
	</xsl:template>
	<xsl:template match="Diagnosen">
		<h2>Diagnosen bei Entlassung</h2>
		<table class="t2">
			<thead>
				<tr>
					<th>Diagnose</th>
					<th>Datum von</th>
					<th>Datum bis</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="Diagnose">
					<tr>
						<td><b><xsl:value-of select="@code"/></b>&#160;<xsl:value-of select="text()"/></td>
						<td><xsl:value-of select="@von"/></td>
						<td><xsl:value-of select="@bis"/></td>
						<td><xsl:value-of select="@status"/></td>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
	<xsl:template match="Medikation">
		<table class="t2">
			<thead>
				<tr>
					<th>Arzneimittel</th>
					<th>Einnahme</th>
					<th>Dosierung</th>
					<th>Hinweis</th>
					<th>Diagnose</th>
					<th>Zusatzzinformation</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="Arzneimittel">
					<tr>
						<td><xsl:value-of select="."></xsl:value-of></td>
						<td><xsl:value-of select="@einnahme"></xsl:value-of></td>
						<td><xsl:value-of select="@dosierung"></xsl:value-of></td>
						<td><xsl:value-of select="@hinweis"></xsl:value-of></td>
						<td><xsl:value-of select="@diagnose"></xsl:value-of></td>
						<td>
							<xsl:choose>
								<xsl:when test="@von">
								Start:<xsl:value-of select="@von"></xsl:value-of><br/>
								</xsl:when>
							</xsl:choose>
							<xsl:choose>
							<xsl:when test="@bis">
								Ende:<xsl:value-of select="@bis"></xsl:value-of><br/>
							</xsl:when>
							</xsl:choose>
							<xsl:choose>
							<xsl:when test="@anwendung">
								Anwendung:<xsl:value-of select="@anwendung"></xsl:value-of>
							</xsl:when>
							</xsl:choose>
						</td>
					</tr>
				</xsl:for-each>
				
			</tbody>
		</table>
	</xsl:template>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<xsl:template match="Medikatio1n">
		<h2>Diagnosen bei Entlassung</h2>
		<table class="t2">
			<thead>
				<tr>
					<th>Arzneimittel</th>
					<th>Einnahme</th>
					<th>Dosierung bis</th>
					<th>Hinweis</th>
					<th>Diagnose</th>
					<th>Zusatzinformation</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="Arzneimittel">
					<tr>
						<td><b><xsl:value-of select="text()"/></b></td>
						<td><xsl:value-of select="@einnahme"/></td>
						<td><xsl:value-of select="@dosierung"/></td>
						<td><xsl:value-of select="@hinweis"/></td>
						<td><xsl:value-of select="@diagnose"/></td>
						<td>
							<xsl:choose>
							<xsl:when test="@von">
								<b>Start</b>: <xsl:value-of select="@von"/><br/>
							</xsl:when>
							</xsl:choose>
							
							<xsl:choose>
							<xsl:when test="@bis">
								<b>Ende</b>: <xsl:value-of select="@bis"/><br/>
							</xsl:when>
							</xsl:choose>
							
							<xsl:choose>
							<xsl:when test="@anwendung">
								<b>Anwendung</b>: <xsl:value-of select="@anwendung"/>
							</xsl:when>
							</xsl:choose>
							
						</td>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
	
</xsl:stylesheet>
