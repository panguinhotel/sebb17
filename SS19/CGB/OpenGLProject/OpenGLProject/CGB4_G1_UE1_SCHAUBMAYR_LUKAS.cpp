//#include "GL\glew.h"
//#include "GL\freeglut.h"
//#include "iostream"
//#include <stdlib.h>
//#include <vector>
//#include <time.h>
//#include <thread>
//
//using namespace std;
//int windowid;
//std::vector<int> m_turnList; // Liste wo die aktuelle Farbsequenz gespeichert wird
//int m_score = 0; // Score - pro richtiges 1 Punkt
//int m_turns = 0; // Aktueller Zug -> Zug x : x Felder
//bool m_stopped = false;
//int m_random = 0; // Nummer welches Feld (1|2|3|4) heller gezeichnet werden soll: Variable == 0 ? Alle Felder Dunkel
//int m_timeLastSignal; //Fortlaufende Nummer wann ein Userinput erfolgt
//bool m_blocked = true; //User Input blockieren wenn Sequenz gezeigt wird
//
//bool m_useStaticDefinedTurns = false; //true => Vordefinierte Farbsequenz verwenden | false => dynamische Sequez
//std::vector<int> m_staticTurnList = std::vector<int>{ 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4 }; //Vordefinierte Farbsequenz
//void nextframeFunction(int tmp);
//
//void scissors(int highlighted = 0)
//{
//	int window_width = glutGet(GLUT_WINDOW_WIDTH);
//	int window_height = glutGet(GLUT_WINDOW_HEIGHT);
//
//	m_blocked = true;
//	switch (highlighted)
//	{
//	case 1: //Zeichne Rotes Feld hell - Rest Dunkel
//		glScissor(0, window_height / 2, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(1.0, 0.0, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(window_width / 2, window_height / 2, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.5, 0.5, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(0, 0, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.0, 0.5, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(window_width / 2, 0, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.0, 0.0, 0.5, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		break;
//	case 2: //Zeichne Gelbes Feld hell - Rest Dunkel
//		glScissor(0, window_height / 2, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.5, 0.0, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(window_width / 2, window_height / 2, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(1.0, 1.0, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(0, 0, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.0, 0.5, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(window_width / 2, 0, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.0, 0.0, 0.5, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//		break;
//	case 3: //Zeichne Gr�nes Feld hell - Rest Dunkel
//		glScissor(0, window_height / 2, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.5, 0.0, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(window_width / 2, window_height / 2, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.5, 0.5, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(0, 0, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.0, 1.0, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(window_width / 2, 0, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.0, 0.0, 0.5, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//		break;
//	case 4: //Zeichne Blaues Feld hell - Rest Dunkel
//		glScissor(0, window_height / 2, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.5, 0.0, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(window_width / 2, window_height / 2, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.5, 0.5, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(0, 0, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.0, 0.5, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(window_width / 2, 0, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.0, 0.0, 1.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		break;
//	default:
//		m_blocked = false;
//		glScissor(0, window_height / 2, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.5, 0.0, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(window_width / 2, window_height / 2, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.5, 0.5, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(window_width / 2, 0, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.0, 0.0, 0.5, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//
//		glScissor(0, 0, window_width / 2, window_height / 2);
//		glEnable(GL_SCISSOR_TEST);
//		glClearColor(0.0, 0.5, 0.0, 1.0);
//		glClear(GL_COLOR_BUFFER_BIT);
//		glDisable(GL_SCISSOR_TEST);
//		break;
//	}
//	glClearColor(1.0, 1.0, 1.0, 1.0);
//}
//
//void show_no_highlight(int) //Scene wird mit random = 0 neu gezeichnet (alle dunkel)
//{
//	m_random = 0;
//	glutPostRedisplay();
//}
//
//void stopEvent(int tmp)
//{
//	if (m_timeLastSignal == tmp)
//	{
//		m_stopped = true;
//		cout << "Timeout!\tScore:" << m_score << "\tRound:" << m_turns << endl;
//		m_turns = 0;
//		m_score = 0;
//		m_turnList.clear();
//		
//		glutTimerFunc(1000, nextframeFunction, 0);
//		m_stopped = false;
//	}
//
//}
//
//void nextframeFunction(int tmp) //Neues Feld wird zuf�llig ausgew�hlt und Scene wird mit random=Zufallszahl neu gezeichnet
//{
//	if (!m_stopped)
//	{
//		if (m_useStaticDefinedTurns) //Vordefinierte Farbenfolge wird verwendet
//		{
//			m_random = m_staticTurnList.at(tmp);
//		}
//		else
//		{
//			int lastNumber = m_random;
//			while (lastNumber == m_random)
//			{
//				m_random = rand() % 4 + 1;
//			}
//		}
//		m_turnList.push_back(m_random);
//
//		//Zeile kann ausgegeben werden zum Testen (gew�hltes Feld + aktueller Zug wird ausgegeben)
//		//cout << m_random << "\t" << m_turns << endl;
//
//		glutPostRedisplay();
//		if (m_turns > tmp)
//		{
//			glutTimerFunc(500, nextframeFunction, tmp + 1);
//		}
//		else
//		{
//			m_turns++;
//			glutTimerFunc(500, show_no_highlight, 0);		
//		}
//
//		
//	}	
//}
//
//
//void renderScene(void)
//{
//	glClear(GL_COLOR_BUFFER_BIT);
//	glClearColor(0.0, 0.0, 0.0, 1.0); //clear color is black
//	scissors(m_random);
//	glutSwapBuffers();
//}
//
//void signal_recieved(int sector)
//{
//	if (!m_blocked)
//	{
//		if (sector != 0)
//		{
//			if (m_turnList.size() > 0 && m_turnList.front() == sector)
//			{
//				cout << "Correct" << endl;
//				m_turnList.erase(m_turnList.begin());
//				m_score++;
//			}
//			else
//			{
//				m_blocked = true;
//				cout << "Game over!\tScore:" << m_score << "\tRound:" << m_turns << endl;
//				m_turns = 0;
//				m_score = 0;
//				m_turnList.clear();
//				glutTimerFunc(1000, nextframeFunction, 0);
//			}
//
//			if (m_turnList.size() == 0)
//			{
//				m_blocked = true;
//				cout << "Round finished!\tScore:" << m_score << "\tRound:" << m_turns << endl;
//				glutTimerFunc(1000, nextframeFunction, 0);
//			}
//		}
//		m_timeLastSignal++;
//		glutTimerFunc(15000, stopEvent, m_timeLastSignal);
//	}
//}
//
//void keyboard(unsigned char key, int x, int y)
//{
//	int sector = 0;
//	switch (key)
//	{
//		 //Selected Color
//	case 'r': //Red selected
//		sector = 1;
//		break;
//	case 'y': //Yellow selected
//		sector = 2;
//		break;
//	case 'g': //Green selected
//		sector = 3;
//		break;
//	case 'b': //Blue selected
//		sector = 4;
//		break;
//	default:
//		break;	
//	}
//	signal_recieved(sector);
//}
//
//void onMouseClick(int button, int state, int x, int y)
//{
//	int window_width = glutGet(GLUT_WINDOW_WIDTH);
//	int window_height = glutGet(GLUT_WINDOW_HEIGHT);
//
//	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
//	{
//		int sector = 0; //Selected Sector
//		if (x < window_width / 2 && y < window_height / 2) //Top Left Clicked
//			sector = 1;
//		else if (x > window_width / 2 && y < window_height / 2) //Top Right Clicked
//			sector = 2;
//		else if (x < window_width / 2 && y > window_height / 2) //Bottom Left Clicked
//			sector = 3;
//		else if (x > window_width / 2 && y > window_height / 2) //Bottom Right Clicked
//			sector = 4;
//
//		signal_recieved(sector);	
//	}
//
//}
//
//
//
//int main(int argc, char **argv)
//{
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
//	glutInitWindowPosition(500, 500); //determines the initial position of the window
//	glutInitWindowSize(800, 600); // determines the size of the window
//	windowid = glutCreateWindow("OpenGL First Window");// create and name window
//	//register callbacks
//	glutDisplayFunc(renderScene);
//	glutTimerFunc(500, nextframeFunction, 0);
//	glutKeyboardFunc(keyboard);
//	glutMouseFunc(onMouseClick);
//	glutMainLoop();// start the main loop of GLUT
//	return 0;
//}
//
//
