CREATE TABLE MY_EMPLOYEE AS (SELECT * FROM employees);

Select * from my_employee;

Update my_employee set salary = salary*1.05 
Where (job_id like '%_MAN' or job_id like '%_MGR');
Commit;

Update my_employee set salary = 6100
Where salary < 5000;

SAVEPOINT Zwischepunkt;

Select * from my_employee;
Delete from my_employee;
Select * from my_employee;

rollback to Zwischepunkt;
commit;