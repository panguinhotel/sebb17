<?php
   include('session.php');
?>
<html>
   
   <head>
    <title>Welcome</title>
          <link rel="stylesheet" type="text/css" href="style.css">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
   </head>
   
   <body>
     <h1 class="headLine">Welcome <?php echo $login_session; ?></h1> 
        <div class="mainList">
            <button class="mainButton"  onclick="window.location.href='fragespiel.php'">Fragespiel</button><br> 
            <button class="mainButton" onclick="window.location.href='books.php'">Bücher</button><br>   
            <button  <?php if($login_eier > 0) {?> onclick="window.location.href='eier.php'" <?php } ?> class="mainButton">Eier</button><br>
            <button class="mainButton" onclick="window.location.href='logout.php'">Logout</button><br>          
        </div>
   </body>
   
</html>