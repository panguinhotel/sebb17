UNIT LogExpPars;

INTERFACE
    TYPE
        Node = ^NodeRec;
        NodeRec = RECORD
            value: STRING; (* operator or operand in textual representation *)
            left, right: Node;
        END;
    (* ---------------------------------------------------------------------- *)
    (* Description: Parses and validates a logical expression.                *)
    (* Input:                                                                 *)
    (*          inputText: Text containing the logical expression to parse.   *)
    (* Input/Output:                                                          *)
    (*          ok: True if the given expression is valid, otherwise false.   *)
    (*          errorColumn: If "ok" is false, indicates the position of      *)
    (*              the incorrect syntax validation.                          *)
    (*          errorText: If "ok" is false, indicates a reason for the       *)
    (*              syntax validation.                                        *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Parse(inputText: STRING; VAR ok: BOOLEAN; VAR errorColumn: INTEGER; VAR errorText: STRING; VAR tree: Node);
IMPLEMENTATION
    USES LogExpScan;

    VAR 
        success: BOOLEAN;
        errText: STRING;
        level: INTEGER;

    PROCEDURE LogExp(VAR tree: Node); FORWARD;
    PROCEDURE Kondition(VAR tree: Node); FORWARD;
    PROCEDURE Disjunction(VAR tree: Node); FORWARD;
    PROCEDURE Konjunction(VAR tree: Node); FORWARD;
    PROCEDURE Negation(VAR tree: Node); FORWARD;    

    (* ---------------------------------------------------------------------- *)
    (* ---------- Sets the exception text for a semantic error. ------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE SemErr(msg: STRING);
    BEGIN
        success := FALSE;
        errText := msg;
    END;

    (* ---------------------------------------------------------------------- *)
    (* --------.------ Validate the logical expression. --------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE LogExp(VAR tree: Node);
    VAR exprLeft, exprRight : Node;
    BEGIN
        Kondition(tree); IF NOT success THEN Exit;

        WHILE (CurrentSymbol = biCondSym) DO BEGIN
            GetNextSymbol;
            exprLeft := tree;
            
            Kondition(tree); IF NOT success THEN Exit;
            exprRight := tree;
            
            New(tree);
            tree^.value := 'eq';
            tree^.left := exprLeft;
            tree^.right := exprRight;
        END;
    END;

    (* ---------------------------------------------------------------------- *)
    (* ---------------------- Validate the Kondition. ----------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Kondition(VAR tree: Node); 
    VAR exprLeft, exprRight : Node;
    BEGIN
        Disjunction(tree); IF NOT success THEN Exit;

        WHILE (CurrentSymbol = condSym) DO BEGIN
            GetNextSymbol;
            exprLeft := tree;
            
            Disjunction(tree); IF NOT success THEN Exit;
            exprRight := tree;
        
            New(tree);
            tree^.value := 'imp';
            tree^.left := exprLeft;
            tree^.right := exprRight;
        END;
    END;

    (* ---------------------------------------------------------------------- *)
    (* ---------------------- Validate the Disjunction. --------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Disjunction(VAR tree: Node); 
    VAR exprLeft, exprRight : Node;
    BEGIN
        Konjunction(tree); IF NOT success THEN Exit;

        WHILE (CurrentSymbol = disSym) DO BEGIN
            GetNextSymbol;
            exprLeft := tree;
            
            Konjunction(tree); IF NOT success THEN Exit;
            exprRight := tree;
            
            New(tree);
            tree^.value := 'or';
            tree^.left := exprLeft;
            tree^.right := exprRight;
        END;
    END;

    (* ---------------------------------------------------------------------- *)
    (* ---------------------- Validate the Konjunction. --------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Konjunction(VAR tree: Node); 
    VAR exprLeft, exprRight : Node;
    BEGIN
        Negation(tree); IF NOT success THEN Exit;

        WHILE (CurrentSymbol = konjSym) DO BEGIN
            GetNextSymbol;
            exprLeft := tree;
            
            Negation(tree); IF NOT success THEN Exit;
            exprRight := tree;
        
            New(tree);
            tree^.value := 'and';
            tree^.left := exprLeft;
            tree^.right := exprRight;
        END;
    END;

    (* ---------------------------------------------------------------------- *)
    (* ----------------------- Validate the negation. ----------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Negation(VAR tree: Node); 
    VAR additionalNode : Node;
    BEGIN
        additionalNode := NIL;

        (* Negation Symbol is optional *)
        IF CurrentSymbol = negSym THEN BEGIN
            New(tree);
            tree^.left := NIL;
            tree^.right := NIL;
            tree^.value := 'not';

            additionalNode := tree;

            GetNextSymbol;
        END;

        IF (CurrentSymbol = boolSym) THEN BEGIN
            New(tree);
            tree^.left := NIL;
            tree^.right := NIL;
            tree^.value := CurrentBooleanValue; (* Get boolean Symbol value *)
            
            IF(additionalNode <> NIL) THEN BEGIN
                additionalNode^.left := tree;
                tree := additionalNode;
            END;

            GetNextSymbol;
        END
        ELSE BEGIN
            IF CurrentSymbol <> leftParSym THEN BEGIN SemErr('Expected "(" or boolean!'); Exit; END;
            GetNextSymbol;

            LogExp(tree); IF NOT success THEN Exit;

            IF CurrentSymbol <> rightParSym THEN BEGIN SemErr('Expected ")"!'); Exit; END;
            GetNextSymbol;
        END;
    END;

    (* ---------------------------------------------------------------------- *)
    (* ------------- Parses and validates a logical expression. ------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Parse(inputText: STRING; VAR ok: BOOLEAN; VAR errorColumn: INTEGER; VAR errorText: STRING; VAR tree: Node);
    BEGIN
        success := TRUE;
        errText := '';
        level := 0;

        InitScanner(inputText);

        LogExp(tree);

        ok := success;
        errorText := errText;
        errorColumn := GetCurrentSymbolPosition;
    END; (* Parse *)
END. (* JMinPars *)