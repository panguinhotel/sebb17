unit StringListUnit;
	
interface
TYPE	
	Data = ^DataRec;
	DataRec = Record
	Value : String;
	isAnchor : Boolean;
	prev,next : Data; End;
	StringList = Data;

(*Init List*)
PROCEDURE Init(Var l: StringList);
(*Disposes List*)
PROCEDURE Dispose(l: StringList);
(*Clears List*)
PROCEDURE Clear(Var l: StringList);
(*Appens New Value To List*)
PROCEDURE Append(VAR l : StringList; value: String);
(*Checks if List contains Value*)
FUNCTION Contains(l: StringList; value:String) : Boolean;
implementation
(*Init List*)
PROCEDURE Init(Var l: StringList);
VAR
	n:Data;
BEGIN
	New(n);
	n^.isAnchor := true;
	n^.prev := n;
	n^.next := n;
	l := n;
END; (* Init *)
(*Disposes List*)
PROCEDURE Dispose(l: StringList);
BEGIN
	Clear(l);
	Dispose(l);
END; (* Dispose *)
(*Clears List*)
PROCEDURE Clear(Var l: StringList);
VAR
	n, next : Data;
BEGIN
	n := l^.next; 
  WHILE  n <> l DO BEGIN
    next := n^.next;
    Dispose(n);
    n := next;
  END;
  l^.next := l; 
END; (* Clear *)
(*Appens New Value To List*)
PROCEDURE Append(VAR l : StringList; value: String);
VAR
	n : Data;
BEGIN
	New(n);
	n^.value := value;
	n^.isAnchor := false;
	n^.prev := l^.prev;
	l^.prev^.next := n;
	l^.prev := n;
	n^.next := l;
END; (* Append *)	
(*Checks if List contains Value*)
FUNCTION Contains(l: StringList; value:String) : Boolean;
VAR
	n: Data;
	found : Boolean;
BEGIN
	found := false;
	n := l^.next;
	WHILE (n <> l) and not found DO BEGIN
		found := (n<>NIL) and  (n^.value = value);
		n := n^.next;
	END;
	Contains := found;
END; (* Contains *)
end.