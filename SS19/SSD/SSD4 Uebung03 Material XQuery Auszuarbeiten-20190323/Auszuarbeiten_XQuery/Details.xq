xquery version "1.0";
declare namespace po="http://www.marchal.com/2006/po";
declare namespace dt="http://www.mybooks.com/details";
<result>
{
let $po:=doc("PurchaseOrder.xml")//po:Line
let $det:=doc("BookDetails.xml")//dt:BookDetails

for $code in $po/po:Code, $bk in $det/dt:Book
(: einschränken auf alle, deren Code eine ISBN ist und 63 enthält:)
return 
<el>
{($code/text(),"    ", $code/../po:Description/text(), "     ", $bk/dt:Author/text(),"     ", $bk/dt:Editor/text()), "     ", $code/../po:Price/text()}
</el>

}
</result>
