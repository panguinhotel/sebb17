/*
 * Base_Queue.h
 *
 *  Created on: 06.03.2019
 *      Author: Lukas Schaubmayr
 */

#ifndef BASE_SIMULATOR_H_
#define BASE_SIMULATOR_H_

#include <queue>
#include <stdlib.h>
#include "Base_Event.h"

class Base_Simulator {
public:
	enum class Status{Running,Stopped};

	inline Base_Simulator(){}
	virtual ~Base_Simulator();

	virtual void addEvent(Base_Event *event);

	inline void set_max_size(int size){m_max_size = size;}
	inline int get_max_size() const {return m_max_size;}

	inline void set_allowed_ticks(int ticks){m_allowed_ticks = ticks;}
	inline void set_allowed_items(int items){m_allowed_items = items;}

	virtual void step();
	virtual void run();

protected:
	int m_max_size = 0;
	std::priority_queue<Base_Event*,std::vector<Base_Event*>,Event_compare> m_events;

	long long unsigned int m_break_queued_items = 0;
	int m_current_ticks = 0;

	Status m_status = Status::Running;

	int m_allowed_items = -1;
	int m_allowed_ticks = -1;
};

#endif /* BASE_SIMULATOR_H_ */
