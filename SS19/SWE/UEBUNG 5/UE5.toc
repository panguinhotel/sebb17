\contentsline {chapter}{\numberline {1}L\IeC {\"o}sungsidee}{2}
\contentsline {section}{\numberline {1.1}Board}{2}
\contentsline {section}{\numberline {1.2}Move}{2}
\contentsline {section}{\numberline {1.3}SearchNode}{2}
\contentsline {subsubsection}{estimatedCostsToTarget}{2}
\contentsline {section}{\numberline {1.4}SlidingPuzzle}{3}
\contentsline {subsubsection}{search(Board board)}{3}
\contentsline {chapter}{\numberline {2}Implemetierung}{4}
\contentsline {section}{\numberline {2.1}Board}{5}
\contentsline {section}{\numberline {2.2}Move}{9}
\contentsline {section}{\numberline {2.3}SearchNode}{10}
\contentsline {section}{\numberline {2.4}SlidingPuzzle}{12}
\contentsline {section}{\numberline {2.5}BoardException}{14}
\contentsline {section}{\numberline {2.6}InvalidBoardException}{15}
\contentsline {section}{\numberline {2.7}IllegalMoveException}{16}
\contentsline {section}{\numberline {2.8}InvalidTileNumberException}{17}
\contentsline {section}{\numberline {2.9}NoSolutionException}{18}
\contentsline {chapter}{\numberline {3}Tests}{19}
\contentsline {section}{\numberline {3.1}BoardTest}{20}
\contentsline {section}{\numberline {3.2}SearchNodeTest}{25}
\contentsline {section}{\numberline {3.3}SlidingPuzzleSolverTest}{28}
\contentsline {chapter}{\numberline {4}Testresults}{31}
