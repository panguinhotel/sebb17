package swe4.collections;

public interface Hasher<K> {
	int hash(K key);
}
