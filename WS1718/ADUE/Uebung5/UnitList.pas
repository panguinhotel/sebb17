unit UnitList;
	
interface
Uses UnitDate;

TYPE   TestType= RECORD    
		Name : string;
		DurationTime : Int64;
		Result: (passed,failed); 
		End;
		
 		TestResult = ^TestResultRec;   
		TestResultRec = RECORD    
		Test: TestType;
		next: TestResult;   END;
		TestResultList = TestResult;	
PROCEDURE AddValueToList(Var l :TestResultList; value: TestType);
procedure InitList(VAR l: TestResultList);
PROCEDURE DisplayList(l: TestResultList);
(*Describe me*)
PROCEDURE DisposeList(Var l : TestResultList);
(*Describe me*)
FUNCTION CountList(param: TestResultList) : INTEGER;
implementation
procedure InitList(VAR l: TestResultList);
begin
	l:=NIL;
end;
(*Describe me*)
PROCEDURE DisplayList(l: TestResultList);
Var	
	n: TestResult;
BEGIN
	n:= l;
	WHILE n <> NIL  DO BEGIN
		WriteLn(n^.Test.Name,^i,n^.Test.DurationTime,^i,n^.Test.Result);
		n:=n^.next;
	END;
END; (* DisplayList *)
(*Describe me*)
PROCEDURE AddValueToList(Var l :TestResultList; value: TestType);
VAR
	n,temp:TestResult;
BEGIN
	New(n);
	IF n = NIL  THEN BEGIN
		WriteLn('NO more space on the heap');
		Halt;
	END;
	n^.Test := value;
	n^.next := nil;
	IF l = Nil  THEN BEGIN
		l:= n
	End
	ELSE BEGIN
		(*frind last node *);
		temp := l;
		WHILE temp^.next <> NIL  DO BEGIN
			temp := temp^.next;
		END;
		(*set next of last node to n *);
		temp^.next := n;
		
	END;
END; (* AddValueToList *)	
(*Describe me*)
PROCEDURE DisposeList(Var l : TestResultList);
BEGIN
	l:=nil;
END; (* DisposeList *)
(*Describe me*)
FUNCTION CountList(param: TestResultList) : INTEGER;
Var
	n:TestResult;
	i:Integer;
BEGIN
	n:=param;
	i:= 0;
	WHILE n <> NIL  DO BEGIN
		i:= i+1;
		n:= n^.next;
	END;
	CountList := i;
END; (* MyFunction *)
end.