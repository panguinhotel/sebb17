/*
 * Base_Queue.h
 *
 *  Created on: 06.03.2019
 *      Author: Lukas Schaubmayr
 */

#ifndef BASE_QUEUE_H_
#define BASE_QUEUE_H_

#include <queue>
#include <stdlib.h>

#include "Base_Event.h"


class Event_compare
{
public:
	bool operator() (Base_Event *a, Base_Event *b) const
	{
		return a->get_start_delay() < b->get_start_delay();
	}
};

class Base_Queue {
public:
	enum class Status{Running,Stopped};
	Base_Queue();
	virtual ~Base_Queue();

	void addEvent(Base_Event *event);

	inline void set_max_size(int size){m_max_size = size;}
	inline int get_max_size() const {return m_max_size;}

	inline void set_break_after_steps(int steps = -1){m_break_steps = steps;}
	inline void set_break_queued_items(long long unsigned int items = -1){m_break_queued_items = items;}

	void step();
	void run();

private:
	int m_max_size = 0;
	std::priority_queue<Base_Event*,std::vector<Base_Event*>,Event_compare> m_events;

	int m_break_steps = -1;
	long long unsigned int m_break_queued_items = 0;
	int m_current_steps = 0;

	Status check_status();
};

#endif /* BASE_QUEUE_H_ */
