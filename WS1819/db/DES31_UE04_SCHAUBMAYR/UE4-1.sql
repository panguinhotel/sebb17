CREATE or replace PACKAGE top_customer_pkg AS
  FUNCTION GetFilmcount(cust_id IN NUMBER) RETURN NUMBER;
  PROCEDURE GetTop10Customers;
  PROCEDURE GetTopNCustomers(n_count IN NUMBER, begin_date IN DATE,end_date IN DATE);
END;
/
CREATE or replace PACKAGE BODY top_customer_pkg AS
  FUNCTION GetFilmcount(cust_id IN NUMBER)
    RETURN NUMBER
    IS
      n_film NUMBER;    
    BEGIN
      SELECT COUNT(*) INTO n_film
      FROM rental r
      INNER JOIN inventory USING (inventory_id)
      INNER JOIN film USING (film_id)
      WHERE r.customer_id = cust_id
      AND length >= 60;
      RETURN n_film;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
      RETURN 0;
  END;
  PROCEDURE GetTop10Customers
  IS
    CURSOR tmp_cur IS
      Select customer_id as Id FROM(
        Select r.customer_id, Count(*) as tmp
        from rental r
        INNER JOIN inventory USING (inventory_id)
        INNER JOIN film USING (film_id)
        group by r.customer_id
        ORDER BY tmp DESC)
      WHERE ROWNUM <= 10;
      Nm  VARCHAR2(50);
  BEGIN
    dbms_output.put_line('The top 10 customers are:');
    FOR REC IN tmp_cur
    LOOP
      Select c.first_name||' '||c.last_name into Nm
      from customer c
      where c.customer_id = REC.Id;
      
      dbms_output.put_line(Nm||' '||top_customer_pkg.GetFilmcount(REC.Id)||' Films');
    END LOOP;
  END;
  PROCEDURE GetTopNCustomers(n_count IN NUMBER, begin_date IN DATE,end_date IN DATE)
  IS
    CURSOR tmp_cur IS
      Select customer_id as Id FROM(
        Select r.customer_id, Count(*) as tmp
        from rental r
        INNER JOIN inventory USING (inventory_id)
        INNER JOIN film USING (film_id)
        where r.rental_date Between begin_date and end_date
        group by r.customer_id
        ORDER BY tmp DESC)
      WHERE ROWNUM <= n_count;
      Nm  VARCHAR2(50);
  BEGIN
    dbms_output.put_line('The top '||n_count||' customers from '
    ||begin_date||' and '||end_date||' are:');
    FOR REC IN tmp_cur
    LOOP
      Select c.first_name||' '||c.last_name into Nm
      from customer c
      where c.customer_id = REC.Id;
      
      dbms_output.put_line(Nm||' '||top_customer_pkg.GetFilmcount(REC.Id)||' Films');
    END LOOP;
  END;
END;

BEGIN
dbms_output.put_line('TEST 1');
dbms_output.put_line('');
dbms_output.put_line('Customer with id 100 : '||top_customer_pkg.GetFilmcount(100)||' Films');
dbms_output.put_line('');
dbms_output.put_line('');
dbms_output.put_line('TEST 2');
dbms_output.put_line('');
top_customer_pkg.GetTop10Customers;
dbms_output.put_line('');
dbms_output.put_line('');
dbms_output.put_line('TEST 3');
dbms_output.put_line('');
top_customer_pkg.GetTopNCustomers(5,'01.01.2007','31.12.2016');
END;

drop package top_customer_pkg;