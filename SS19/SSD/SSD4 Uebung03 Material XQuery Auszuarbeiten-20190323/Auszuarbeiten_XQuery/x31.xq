xquery version "1.0";
(:Comment:)
declare default element namespace "http://www.fh-hagenberg.at/projectteam";
<result>
{
(:let $doc := doc("person_dtd.xml"):)
let $doc := doc("person.xml")

for $pers in $doc//Person
let $persId := $pers/@id
let $persName := $pers/LastName

(:where boolean($persId = "ssn1000" or $persId = "ssn1001"):)
where exists($pers/MiddleName)

order by $pers/LastName ascending (:descending:)
return 
  if ($pers/FirstName='Markus')
  then element {node-name($pers)} {attribute ide  {$pers/@id}, element lastname {data($pers/LastName)}}
  else element {node-name($pers)} {attribute ide  {$pers/@id}, attribute ggg {"sss"}, element lastname {data($pers/LastName)}}
}
</result>
