unit Parser;
	
interface
	PROCEDURE InitParser(inp : String);
	FUNCTION Parse : Boolean;
	FUNCTION ErrorPos : Integer;
	
implementation
	USES
		Scanner;
	
	VAR
		success : Boolean;
	
	PROCEDURE Expr; FORWARD;
	PROCEDURE KLExpr; FORWARD;

	PROCEDURE KLExpr;
	BEGIN
		WHILE not (CurrentSymbol <> rightParSym) and success DO BEGIN
			GetNextSymbol;
			if CurrentSymbol = endSym THEN BEGIN success := false; EXIT; END;
			Expr;
		END;
		GetNextSymbol;
	END;

	PROCEDURE Expr;
	BEGIN
		CASE CurrentSymbol OF
			negSym : BEGIN
				GetNextSymbol;
				Expr;
			END;
			leftParSym : BEGIN
				GetNextSymbol;
				KLExpr;
			END;
			exprSym : BEGIN
				GetNextSymbol;
				IF CurrentSymbol in [konjSym,disSym,kondSym,bikoSym] THEN BEGIN
					GetNextSymbol;
					Expr;
				END ELSE BEGIN success := FALSE; EXIT; END;
			END
			ELSE BEGIN success := FALSE; EXIT; END;	
		END;		 
	END;

	PROCEDURE InitParser(inp : String);
	BEGIN
		InitScanner(inp);
		success := false;
	END;

	FUNCTION Parse : Boolean;
	BEGIN
		success := TRUE;
		Expr;
		success := success and (CurrentSymbol = endSym);
		Parse :=success;
	END;

	FUNCTION ErrorPos : Integer;
	BEGIN
		ErrorPos := CurrentSymbolPos;
	END;


END.