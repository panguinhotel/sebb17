(*MLListe:                                              
  ------
  Classes MLList and MLListIterator.
========================================================================*)
unit MLListe;
	
interface
	
	uses  MLObj, MLColl;

	TYPE
	MLListNodePtr = ^MLListNode;
	MLListNode = Record
		data : MLObject;
		prev,next : MLListNodePtr;
	End;

	MLListIterator = ^MLListIteratorObj; (*full declaration below MLList*)
	MLListReverseIterator = ^MLListIteratorObj; (*full declaration below MLList*)

(*=== class MLList ===*)
	MLList = ^MLListObj;
	MLListObj = Object(MLCollectionObj)
		public
			CONSTRUCTOR Init;
			DESTRUCTOR Done; VIRTUAL;
(*--- overridden "abstract" methods ---*)
			FUNCTION Size: INTEGER; VIRTUAL;
			(*returns number of elements in collection*)

			PROCEDURE Add(o: MLObject); VIRTUAL;
			(*adds element at right end*)

			FUNCTION Remove(o: MLObject): MLObject; VIRTUAL;			
			 (*removes first element = o, shifts rest to the left
        	and returns removed element*)

			FUNCTION Contains(o: MLObject): BOOLEAN; VIRTUAL;
			(*returns whether collection contains an element = o*)

			PROCEDURE Clear; VIRTUAL;
			(*removes all elements WITHOUT disposing them*)

			FUNCTION NewIterator: MLIterator; VIRTUAL;
			(*returns a list iterator which has to be disposed after usage*)

			FUNCTION NewReverseIterator: MLIterator; VIRTUAL;
			(*returns a reversedList iterator which has to be disposed after usage*)
(*--- new methods ---*)
			FUNCTION GetElements: MLListNodePtr; VIRTUAL;
			(*returns elements*)

			PROCEDURE Prepend(o: MLObject);
			(*adds element at beginning*)
		private
			m_count : Integer; (*curr. number of elements*)
			elements: MLListNodePtr; (*dyn. alloc. array of MLObject pointers*)
	End;(*MLListObj*)

(*=== class MLListIterator && MLListReverseIterator ===*)
	(* Pointer Declaration above*)
	MLListIteratorObj = OBJECT(MLIteratorObj)
		public
			CONSTRUCTOR Init(list: MLList);
    		DESTRUCTOR Done; VIRTUAL;
	(*--- implementation of "abstract" method ---*)
			FUNCTION Next: MLObject; VIRTUAL;
			(*returns next element or NIL if "end of" collection reached*)
		private
			l:MLList; (*list to iterate over*)
			currNode:MLListNodePtr; (*current node in l*)
			isReverse:Boolean; (*iterate in reverse order?*)
	END;(*MLListIteratorObj*)
   
	
	FUNCTION NewMLList: MLList;
(*======================================================================*)
implementation
(*=== class NewMLList ===*)
FUNCTION NewMLList: MLList;
VAR
    v: MLList;
BEGIN
    New(v, Init);
    NewMLList := v;
END; 

CONSTRUCTOR MLListObj.Init;
VAR
	n:MLListNodePtr;
begin
	INHERITED Init;
    Register('MLList', 'MLCollection');
	New(n);
	n^.next := n;
	n^.prev := n;
	n^.data := nil;
	elements := n;
end;

DESTRUCTOR MLListObj.Done;
VAR
	n,m: MLListNodePtr;
begin
	n := elements^.next;
	while(n <> elements) do
	begin
		m:=n^.next;
		Dispose(n^.data,Done);
		Dispose(n);
		n:=m;
	end;
	Dispose(elements);
	elements := NIL;
	m_count := 0;
	INHERITED Done;
end;

FUNCTION MLListObj.Size: INTEGER;
begin
	Size := m_count;
end;

PROCEDURE MLListObj.Add(o: MLObject);
VAR
	n:MLListNodePtr;
begin
	New(n);
	if(n <> NIL) then
	begin
		n^.data := o;
		n^.next := elements;
		n^.prev := elements^.prev;
		elements^.prev^.next := n;
		elements^.prev := n;
		Inc(m_count);		
	end;
end;

PROCEDURE MLListObj.Prepend(o: MLObject);
VAR
	n:MLListNodePtr;
begin
	New(n);
	if(n <> NIL) then
	begin
		n^.data := o;
		n^.prev := elements;
		n^.next := elements^.next;
		elements^.next^.prev := n;
		elements^.next := n;
		Inc(m_count);		
	end;
end;

FUNCTION MLListObj.Remove(o: MLObject): MLObject;
VAR
	n:MLListNodePtr;
begin
	Remove := nil;
	n:=elements^.next;
	while(n <> elements) do
	begin
		if(n^.data = o) then
		begin
			n^.prev^.next := n^.next;
			n^.next^.prev := n^.prev;
			Dec(m_count);
			Remove:=n^.data;
			n:=elements;
		end
		ELSE BEGIN
			n:=n^.next;
		END;
	end;
end;

FUNCTION MLListObj.Contains(o: MLObject): BOOLEAN;
VAR
	n:MLListNodePtr;
	cont : Boolean;
begin
	cont := false;
	n:=elements^.next;
	while((n <> elements) and (not cont)) do
	begin
		cont := n^.data = o;
		n := n^.next;
	end;
	Contains := cont;
end;

PROCEDURE MLListObj.Clear;
begin
	elements^.next := elements;
	elements^.prev := elements;
	m_count := 0;
end;

FUNCTION MLListObj.NewIterator: MLIterator;
VAR
    it: MLListIterator;
BEGIN
    New(it, Init(@SELF));
	it^.isReverse := false;
    NewIterator := it;
END;
FUNCTION MLListObj.NewReverseIterator: MLIterator;
VAR
    it: MLListReverseIterator;
BEGIN
    New(it, Init(@SELF));
	it^.isReverse := true;
    NewReverseIterator := it;
END;

FUNCTION MLListObj.GetElements : MLListNodePtr;
BEGIN
	GetElements := elements;
END;

(*=== MLListIterator ===*)
CONSTRUCTOR MLListIteratorObj.Init(list: MLList);
BEGIN
	INHERITED Init;
    Register('MLListIterator', 'MLIterator');
	l := list;
	currNode := list^.GetElements;
END;

DESTRUCTOR MLListIteratorObj.Done;
BEGIN
	INHERITED Done;
END;

FUNCTION MLListIteratorObj.Next: MLObject;
VAR
	o:MLObject;
BEGIN
	o:=NIL;
	if(isReverse) then
		currNode := currNode^.prev
	else
		currNode := currNode^.next;

	while (o = NIL) and (currNode <> NIL) and (currNode <> l^.GetElements) do
	begin
		o := currNode^.data;
		if(o = NIL) then begin
			if(isReverse) then
				currNode := currNode^.prev
			else
				currNode := currNode^.next;
		end;
	end;
	Next := o;
END;

end.