PROGRAM StackADTTest;

USES
	StackADT2a;
VAR
	i: Int64;
	ok, ok1:Boolean;
	val : Integer;
	s, s1: Stack;


(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	i:=1;
	Init(s);
	Init(s1);
	repeat
		Push(s, i*i,ok);
		Push(s1, i*i,ok);
		i := i+1;
	until (i> 10 ) or Not ok;

	IF Not ok Then begin
		WriteLn('Fehler!');
	end;

	repeat
		Pop(s, val, ok);
		WriteLn('popped #', val);
	until (Empty(s) or Not ok);

	IF Not ok Then begin
		WriteLn('Fehler!');
	end;

	repeat
		Pop(s1, val, ok1);
		WriteLn('popped #', val);
	until (Empty(s1) or Not ok1);

	IF Not ok1 Then begin
		WriteLn('Fehler!');
	end;

	// Pop(val,ok);
	// WriteLn(ok);

	// Push(99,ok);
	// WriteLn(ok);

	// Pop(val,ok);
	// WriteLn(ok);
	// WriteLn(val);

	Done(s);
	Done(s1);

END. (* StackADSTest *)