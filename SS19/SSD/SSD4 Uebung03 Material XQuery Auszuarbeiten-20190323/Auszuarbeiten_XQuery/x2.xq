xquery version "1.0";
(: Sequences und Generelle Vergleichsop :)
<r>
{
let $a:=(1,2)
let $b:=(3,4)
let $c:=(2,3)
let $d:=(1,2)
return 
	(
	<x>a=b: {$a=$b}</x>,
	<x>a=c: {$a=$c}</x>,
	<x>a=d: {$a=$d}</x>,
	<x>a!=b: {$a!=$b}</x>,
	<x>a!=c: {$a!=$c}</x>,
	<x>a!=d: {$a!=$d}</x>,
	
	<z>{reverse($c)}</z>
	)
}
</r>

(:

The problem is when you think that you reverse the logic by changing != into =. But they are not the opposite of each other.

= is true when any item in the left-hand side sequence is equal to any item in the right-hand side sequence. != is true when any item in the left-hand side sequence is different from any item in the right-hand side sequence.

( 1, 2, 3 ) = ( 1, 5 )  (: true :)
( 1, 2, 3 ) = ( )       (: false :)
( 1, 2, 3 ) != ( 1, 5 ) (: true :)

The opposite of x=y, as you found out, is not(x=y).

When the left-hand side sequence or the right-hand side sequence is a singleton, then by definition not(x=y) equals x!=y.

:)