PROGRAM PointEd;


USES
	Windows,
	WinApp;

TYPE
	Point = ^PointRec;
	PointRec = RECORD
		x,y : INTEGER;
		next : Point;
	END;
	PointList = Point;
VAR 
	points: PointList;

PROCEDURE Paint(wmd: HWnd; dc: HDC);
VAR
	p: Point;
begin
	p:= points;
	WHILE p<> NIL DO begin
		DeleteObject(SelectObject(dc, CreatePen(PS_DASHDOT,3,RGB(155,233,80))));
		DeleteObject(SelectObject(dc, CreateSolidBrush(RGB(70,93,228))));
		//Ellipse(dc, p^.x-100, p^.y-100, p^.x+100,p^.y+100);
		//Rectangle(dc, p^.x-100, p^.y-100, p^.x+100,p^.y+100);
		Rectanlge(dc, p^.x-100, p^.y-100, p^.x+100,p^.y+100);
		p:= p^.next;
	END;
END;

PROCEDURE LeftMousePressed(wnd: HWnd; x,y: INTEGER);
VAR 
	p: Point;
begin
	New(p);
	p^.x := x;
	p^.y := y;
	{prepend}
	p^.next := points;
	points := p;

	// redraw?
	InvalidateRect(wnd,NIL,TRUE);
end;
BEGIN
	OnPaint := Paint;
	OnMouseDown := LeftMousePressed;
	Run;
END. (* PointEd *)