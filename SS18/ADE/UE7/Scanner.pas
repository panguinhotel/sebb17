unit Scanner;


interface
	TYPE Symbol = (noSym,numberSym,leftparSym,rightparSym,
	commaSym,endSym,lineSym,atSym,
	toSym,rectSym, widthSym, heightSym, 
	regularSym,polySym, radiusSym, sideSym);
CONST
    TAB = Chr(9);
    LF = Chr(10);
    CR = Chr(13);
    BLANK = ' ';
	ENDOFINPUT = CHR(0);
PROCEDURE InitScanner(VAR inputFile:TEXT);
PROCEDURE GetNextSymbol;
FUNCTION CurrentSymbol:Symbol;
PROCEDURE GetCurrentSymbolPosition(Var line,column: INTEGER);
FUNCTION CurrentText : STRING;
FUNCTION CurrentNumberValue : INTEGER;
implementation

VAR
    inpFile: Text;
    curChar: CHAR;
    charLine, charColumn: INTEGER;
    curSymbol: Symbol;
    symbolLine, symbolColumn: INTEGER;
	currTxt : String;
	curNumberValue : LONGINT;

PROCEDURE GetNextChar;
 BEGIN
	IF not EOF(inpFile) THEN BEGIN
		Read(inpFile, curChar);
			IF (curChar = CR) OR (curChar = LF) THEN BEGIN
			IF (charColumn > 0) THEN
				Inc(charLine);
			curChar := BLANK;
			charColumn := 0;
			END
		ELSE
		Inc(charColumn);
	END ELSE 
	BEGIN
		curChar := ENDOFINPUT;
	END;
END;
    
PROCEDURE InitScanner(VAR inputFile : TEXT);
BEGIN
    inpFile := inputFile;
    charLine := 1;
    charColumn := 0;
	curNumberValue := 0;
    GetNextChar;
    GetNextSymbol;
END;

PROCEDURE GetNextSymbol;
VAR
	numberStr : String;
	code : LongInt;
BEGIN
	WHILE ((curChar = BLANK) OR (curChar = TAB)) and (curChar <> Chr(0)) DO
    	GetNextChar;
    
    symbolLine := charLine;
    symbolColumn := charColumn;
	currTxt := curChar;
    
    CASE curChar OF
		ENDOFINPUT : BEGIN curSymbol := endSym; END;
		',' : BEGIN curSymbol := commaSym; GetNextChar; END;
		'(' : BEGIN curSymbol := leftparSym; GetNextChar; END;
		')' : BEGIN curSymbol := rightparSym; GetNextChar; END;
		'0'..'9' : BEGIN
				numberStr := '';
				WHILE (curChar >= '0') AND (curChar <= '9') DO begin
					numberStr := numberStr + curChar;
					GetNextChar;
				end;
				curSymbol := numberSym;
				Val(numberStr, curNumberValue,code);
		END;
		'A' : BEGIN
			GetNextChar;
			IF curChar = 'T' THEN BEGIN 
			curSymbol := atSym; GetNextChar; END
			ELSE BEGIN curSymbol := noSym; END;
		END;
		'T' : BEGIN
			GetNextChar;
			IF curChar = 'O' THEN BEGIN 
			curSymbol := toSym; GetNextChar; END
			ELSE BEGIN curSymbol := noSym; END;
		END;
		'L' : BEGIN
			GetNextChar;
			IF curChar = 'I' THEN BEGIN GetNextChar;
				IF curChar = 'N' THEN BEGIN GetNextChar;
					IF curChar = 'E' THEN BEGIN 
					curSymbol := lineSym; 
					GetNextChar;
					END ELSE BEGIN curSymbol := noSym; END
				END ELSE BEGIN curSymbol := noSym; END
			END ELSE BEGIN curSymbol := noSym; END;
		END;
		'W' : BEGIN
			GetNextChar;
			IF curChar = 'I' THEN BEGIN GetNextChar;
				IF curChar = 'D' THEN BEGIN GetNextChar;
					IF curChar = 'T' THEN BEGIN GetNextChar;
						IF curChar = 'H' THEN BEGIN 
						curSymbol := widthSym; 
						GetNextChar;
						END ELSE BEGIN curSymbol := noSym; END
					END ELSE BEGIN curSymbol := noSym; END
				END ELSE BEGIN curSymbol := noSym; END
			END ELSE BEGIN curSymbol := noSym; END;
		END;
		'S' : BEGIN
			GetNextChar;
			IF curChar = 'I' THEN BEGIN GetNextChar;
				IF curChar = 'D' THEN BEGIN GetNextChar;
					IF curChar = 'E' THEN BEGIN GetNextChar;
						IF curChar = 'S' THEN BEGIN 
						curSymbol := sideSym; 
						GetNextChar;
						END ELSE BEGIN curSymbol := noSym; END
					END ELSE BEGIN curSymbol := noSym; END
				END ELSE BEGIN curSymbol := noSym; END
			END ELSE BEGIN curSymbol := noSym; END;
		END;
		'H' : BEGIN
			GetNextChar;
			IF curChar = 'E' THEN BEGIN GetNextChar;
				IF curChar = 'I' THEN BEGIN GetNextChar;
					IF curChar = 'G' THEN BEGIN GetNextChar;
						IF curChar = 'H' THEN BEGIN GetNextChar;
							IF curChar = 'T' THEN BEGIN 
							curSymbol := heightSym; 
							GetNextChar;
							END ELSE BEGIN curSymbol := noSym; END
						END ELSE BEGIN curSymbol := noSym; END
					END ELSE BEGIN curSymbol := noSym; END
				END ELSE BEGIN curSymbol := noSym; END
			END ELSE BEGIN curSymbol := noSym; END;
		END;
		'P' : BEGIN
			GetNextChar;
			IF curChar = 'O' THEN BEGIN GetNextChar;
				IF curChar = 'L' THEN BEGIN GetNextChar;
					IF curChar = 'Y' THEN BEGIN GetNextChar;
						IF curChar = 'G' THEN BEGIN GetNextChar;
							IF curChar = 'O' THEN BEGIN GetNextChar;
								IF curChar = 'N' THEN BEGIN 
								curSymbol := polySym; 
								GetNextChar;
								END ELSE BEGIN curSymbol := noSym; END
							END ELSE BEGIN curSymbol := noSym; END
						END ELSE BEGIN curSymbol := noSym; END
					END ELSE BEGIN curSymbol := noSym; END
				END ELSE BEGIN curSymbol := noSym; END
			END ELSE BEGIN curSymbol := noSym; END;
		END;
		'R' : BEGIN
			GetNextChar;
			IF curChar = 'E' THEN BEGIN GetNextChar;
				IF curChar = 'C' THEN BEGIN GetNextChar;
					IF curChar = 'T' THEN BEGIN GetNextChar;
						IF curChar = 'A' THEN BEGIN GetNextChar;
							IF curChar = 'N' THEN BEGIN GetNextChar;
								IF curChar = 'G' THEN BEGIN GetNextChar;
									IF curChar = 'L' THEN BEGIN GetNextChar;
										IF curChar = 'E' THEN BEGIN 
										curSymbol := rectSym; 
										GetNextChar;
										END ELSE BEGIN curSymbol := noSym; END
									END ELSE BEGIN curSymbol := noSym; END
								END ELSE BEGIN curSymbol := noSym; END
							END ELSE BEGIN curSymbol := noSym; END
						END ELSE BEGIN curSymbol := noSym; END
					END ELSE BEGIN curSymbol := noSym; END
				END ELSE IF curChar = 'G' THEN BEGIN
					GetNextChar;
					IF curChar = 'U' THEN BEGIN GetNextChar;
						IF curChar = 'L' THEN BEGIN GetNextChar;
							IF curChar = 'A' THEN BEGIN GetNextChar;
								IF curChar = 'R' THEN BEGIN 
								curSymbol := regularSym; 
								GetNextChar;
								END ELSE BEGIN curSymbol := noSym; END
							END ELSE BEGIN curSymbol := noSym; END
						END ELSE BEGIN curSymbol := noSym; END
					END ELSE BEGIN curSymbol := noSym; END
				END ELSE BEGIN curSymbol := noSym; END
			END ELSE IF curChar = 'A' THEN BEGIN
				GetNextChar;
				IF curChar = 'D' THEN BEGIN GetNextChar;
						IF curChar = 'I' THEN BEGIN GetNextChar;
							IF curChar = 'U' THEN BEGIN GetNextChar;
								IF curChar = 'S' THEN BEGIN 
								curSymbol := radiusSym; 
								GetNextChar;
								END ELSE BEGIN curSymbol := noSym; END
							END ELSE BEGIN curSymbol := noSym; END
						END ELSE BEGIN curSymbol := noSym; END
					END ELSE BEGIN curSymbol := noSym; END
			END ELSE BEGIN curSymbol := noSym; END;
		END
		ELSE curSymbol := noSym;
		END;
END;

FUNCTION CurrentSymbol: Symbol;
BEGIN
	CurrentSymbol := curSymbol;
END;
PROCEDURE GetCurrentSymbolPosition(VAR line, column: INTEGER);
BEGIN
	line := symbolLine;
	column := symbolColumn;
END;

FUNCTION CurrentText : STRING;
BEGIN
    CurrentText := currTxt;
END;
FUNCTION CurrentNumberValue : Integer;
	begin
		CurrentNumberValue := curNumberValue
	end;
end.