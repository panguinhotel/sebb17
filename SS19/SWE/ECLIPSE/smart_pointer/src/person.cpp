#include <iostream>
#include <string>

#include "person.h"

using namespace std;

person::person() {
  cout << "INFO: person()" << endl;
}

person::person(string first_name, string last_name)
  : first_name(first_name), last_name(last_name) {
  cout << "INFO: person(" << first_name
       << ", " << last_name << ")" << endl;
}

person::person(const person &p)
  : first_name(p.first_name),
    last_name(p.last_name) {
  cout << "INFO: copy person(" << first_name
       << ", " << last_name << ")" << endl;
}

person::person(person &&p) {
  swap(*this, p);
  cout << "INFO: move person(" << first_name
       << ", " << last_name << ")" << endl;
}

person& person::operator = (person p) {
  swap(*this, p);
  cout << "INFO: assign person(" << first_name
       << ", " << last_name << ")" << endl;
  return *this;
}

person::~person() {
  cout << "INFO: destruct person(" << first_name
       << ", " << last_name << ")" << endl;
}

string person::get_first_name() const { return first_name; }

string person::get_last_name() const { return last_name; }

bool person::operator < (const person &p) const {
  return last_name < p.last_name ||
         (last_name == p.last_name &&
          first_name < p.first_name);
}

void person::print(ostream &os) const {
  os << first_name << ' ' << last_name;
}

void person::read(istream &is) {
  is >> first_name >> last_name;
}

ostream& operator << (ostream &os, const person &p) {
  p.print(os);
  return os;
}

istream& operator >> (istream &is, person &p) {
  p.read(is);
  return is;
}

void swap(person &a, person &b) {
  using std::swap;
  swap(a.first_name, b.first_name);
  swap(a.last_name, b.last_name);
}
