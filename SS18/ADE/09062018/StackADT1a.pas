UNIT StackADT1a;

INTERFACE

TYPE Stack = POINTER;

PROCEDURE Init(VAR s: Stack);
// Pushes a value onto a stack
// INOUT of the stack to use
// IN e: value will be pushed on the stack
// OUT ok: FALSE in case of any erros
PROCEDURE Push(VAR s: Stack; e :INTEGER; VAR ok: BOOLEAN);

// Pops the most recently pushed  value from the stack
// INOUT of the stack to use
// OUT e: value popped from the stack
// Out ok: FALSE in case of any errors
PROCEDURE Pop(VAR s: Stack; VAR e: INTEGER; VAR ok: BOOLEAN);

// returns true is stack is empty
FUNCTION Empty(s: Stack): BOOLEAN;

PROCEDURE Done(s: Stack);

IMPLEMENTATION

CONST
	max = 100;

TYPE
	State = ^StateRec;
	StateRec = RECORD
		top: INTEGER;
		data: ARRAY[1..max] OF INTEGER;
	END;


PROCEDURE Init(VAR s: Stack);
VAR
	st: State;
BEGIN
	New(st);
	st^.top := 0;
	s := st;
END;


PROCEDURE Push(VAR s: Stack; e :INTEGER; VAR ok: BOOLEAN);
BEGIN
	IF State(s)^.top = max THEN BEGIN
		ok := FALSE;
	END ELSE BEGIN
		State(s)^.top += 1;
		State(s)^.data[State(s)^.top] := e;
		ok := TRUE;
	END;
END;


PROCEDURE Pop(VAR s: Stack; VAR e: INTEGER; VAR ok: BOOLEAN);
BEGIN
	IF empty(s) THEN BEGIN
		ok := FALSE;
	END ELSE BEGIN
		e := State(s)^.data[State(s)^.top];
		State(s)^.top -= 1;
		ok := TRUE;
	END;
END;


FUNCTION Empty(s: Stack): BOOLEAN;
BEGIN
	Empty := (State(s)^.top = 0);
END;

PROCEDURE Done(s: Stack);
BEGIN
	Dispose(State(s));
END;

BEGIN
	// Init;
END.