unit UnitList;
	
interface
Uses UnitDate;

TYPE   TestType= RECORD    
		Name : string;
		DurationTime : Int64;
		Result: (passed,failed); 
		End;
		
 		TestResult = ^TestResultRec;   
		TestResultRec = RECORD    
		Test: TestType;
		next: TestResult;   END;
		TestResultList = TestResult;	
(*Add a new Test to List*)
FUNCTION AddValueToList(Var l :TestResultList; value: TestType) : Boolean;
(*Initialize List*)
procedure InitList(VAR l: TestResultList);
(*Prints list on Console*)
PROCEDURE DisplayList(l: TestResultList);
(*Disposes List*)
PROCEDURE DisposeList(Var l : TestResultList);
(*Return amout of Testresults in List*)
FUNCTION CountList(param: TestResultList) : INTEGER;
implementation
(*Initialize List*)
procedure InitList(VAR l: TestResultList);
begin
	l:=NIL;
end;
(*Prints list on Console*)
PROCEDURE DisplayList(l: TestResultList);
Var	
	n: TestResult;
BEGIN
	n:= l;
	WHILE n <> NIL  DO BEGIN
		WriteLn(n^.Test.Name,^i,n^.Test.DurationTime,^i,n^.Test.Result);
		n:=n^.next;
	END;
END; (* DisplayList *)
(*Add a new Test to List*)
FUNCTION AddValueToList(Var l :TestResultList; value: TestType) : Boolean;
VAR
	n,temp:TestResult;
BEGIN
	New(n);
	IF n = NIL  THEN BEGIN
		AddValueToList := false;
	END
	ELSE BEGIN
		n^.Test := value;
		n^.next := nil;
		IF l = Nil  THEN BEGIN
			l:= n
		End
		ELSE BEGIN
			(*frind last node *);
			temp := l;
			WHILE temp^.next <> NIL  DO BEGIN
				temp := temp^.next;
			END;
			(*set next of last node to n *);
			temp^.next := n;
			
		END;
		AddValueToList := true;
	End;
END; (* AddValueToList *)	
(*Disposes List*)
PROCEDURE DisposeList(Var l : TestResultList);
VAR  
  n, next: TestResult;
BEGIN
  n := l;
  WHILE  n <> nil DO BEGIN
    next := n^.next;
    Dispose(n);
    n := next;
  END;
  l:=nil;
END; (* DisposeList *)
(*Return amout of Testresults in List*)
FUNCTION CountList(param: TestResultList) : INTEGER;
Var
	n:TestResult;
	i:Integer;
BEGIN
	n:=param;
	i:= 0;
	WHILE n <> NIL  DO BEGIN
		i:= i+1;
		n:= n^.next;
	END;
	CountList := i;
END; (* CountList *)
end.