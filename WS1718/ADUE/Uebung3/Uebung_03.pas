Program Uebung_03;
uses
regexpr;
type
timespan = Record
			hours: Int64;
			minutes: 0..59;
			seconds: 0..59
			End;
sums = Record
			sumA: Int64;
			sumB: Int64
			End;

(*Function converts Timespan to amout of seconds*)
Function TimeSpanToSeconds(time:timespan) : Int64;
Begin
	if(time.hours < 0) Then TimeSpanToSeconds :=-1
	else
	TimeSpanToSeconds := time.hours*3600 + time.minutes*60 +time.seconds;
End;
(*Function converts Amount of seconds to Timespan*)
Function SecondsToTimeSpan(s:Int64) : string;
Var	span : timespan;
	x:string;
Begin
	if s < 0 Then SecondsToTimeSpan:= 'ungueltig'
	else
	begin
		span.hours:=s DIV 3600;
		span.minutes:= (s-span.hours*3600) DIV 60;
		span.seconds:= s-span.hours*3600-span.minutes*60;
		str (span.hours,x);
		SecondsToTimeSpan := x;
		str (span.minutes,x);
		if(span.minutes < 10) Then SecondsToTimeSpan:= SecondsToTimeSpan+':0' +x
		else SecondsToTimeSpan:= SecondsToTimeSpan+':'+x;
		str (span.seconds,x);
		if(span.seconds < 10) Then SecondsToTimeSpan:= SecondsToTimeSpan+':0'+x
		else SecondsToTimeSpan:= SecondsToTimeSpan+':'+x;
		
	end;
End;
(*Function calculates amout of seconds between 2 Timespans*)
Function TimeDifference(time1,time2:timespan) : Int64;
Begin
	TimeDifference := time1.hours*3600-time2.hours*3600 + 
					time1.minutes*60-time2.minutes*60 +
					time1.seconds-time2.seconds;
	if TimeDifference < 0 Then
		TimeDifference := TimeDifference *-1;
End;
(*Function calculates sumA and sumB*)
Function ComputeSums(arr: array of Integer): sums;
var
firstNumber,item:Integer;
Begin
	firstNumber:= 0;
	ComputeSums.sumA := 0;
	ComputeSums.sumB := 0;

    FOR item IN arr DO BEGIN
        IF item = 0 THEN BREAK
        ELSE 
		BEGIN
			if firstNumber = 0 Then firstNumber:= item;
			if item >= firstNumber THEN ComputeSums.sumA := ComputeSums.sumA + item
			Else ComputeSums.sumB := ComputeSums.sumB + item;
		End;
    END;    
End;
(*Procedure reverts each word of a string*)
Procedure Revert(Str:string; var result : string);
var
  i,k,startIDX,tmp: Integer;
  Len: Integer;
  nextWord: boolean;
  RegexObj: TRegExpr;
Begin
	Len := Length(Str);
	nextWord := true;
	RegexObj := TRegExpr.Create;
	RegexObj.Expression := '[a-zA-Z]';
	result := Str;
	i:= 1;
	startIDX := 1;
	tmp:= 0;
	nextWord := true;
	
	while i <= Len Do
	Begin
		if RegexObj.Exec(Str[i]) Then
		Begin
			if nextWord = true Then
			Begin
				startIDX:= i;
				nextWord:= false;
			End;
		End
		else
		Begin
			if tmp <> startIDX Then
			Begin
				k:=startIDX;
				tmp:=startIDX;
				nextWord:=true;
				while k < i Do
				Begin
					if upCase(result[k]) = result[k] Then
					Begin
						result[k] := upCase(Str[i-k+startIDX-1]);
					End
					else result[k] := lowerCase(Str[i-k+startIDX-1]);
					
					k:=k+1;
				End;
			End;
		End;
		i:=i+1;
		
	End;
End;
Var
	Str,result,result1: string;
	span,span1 : timespan;
	arr : array[0..100] of Integer;
Begin
	WriteLn('1. Timespan reloaded');
	WriteLn('a) Zeitspanne in Sekunden');
	Write('Test TimeSpanToSeconds: 4:13:23  => 15203 :',^i' Expected: TRUE' ,^i); 
	span.hours := 4; span.minutes := 13; span.seconds := 23; if TimeSpanToSeconds(span) = 15203 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 125:06:59  => 450419 :',^i' Expected: FALSE' ,^i); 
	span.hours := 125; span.minutes := 5; span.seconds := 59; if TimeSpanToSeconds(span) = 450419 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 0:00:00  => 0 :',^i' Expected: TRUE' ,^i); 
	span.hours := 0; span.minutes := 0; span.seconds := 0; if TimeSpanToSeconds(span) = 0 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 1:11:61  => -1 :',^i' Expected: FALSE' ,^i); 
	span.hours := 1; span.minutes := 11; span.seconds := 61; if TimeSpanToSeconds(span) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 1:62:17  => -1 :',^i' Expected: FALSE' ,^i); 
	span.hours := 1; span.minutes := 62; span.seconds := 17; if TimeSpanToSeconds(span) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: -4:13:23  => -1 :',^i' Expected: FALSE' ,^i); 
	span.hours := -4; span.minutes := 13; span.seconds := 23; if TimeSpanToSeconds(span) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 4:-13:23  => -1 :',^i' Expected: FALSE' ,^i); 
	span.hours := 4; span.minutes := -13; span.seconds := 23; if TimeSpanToSeconds(span) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 4:13:-23  => -1 :',^i' Expected: FALSE' ,^i); 
	span.hours := 4; span.minutes := 13; span.seconds := -23; if TimeSpanToSeconds(span) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test SecondsToTimeSpan: 15203  => 4:13:23 :',^i' Expected: TRUE' ,^i); if SecondsToTimeSpan(15203) = '4:13:23' Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test SecondsToTimeSpan: 450419  => 125:06:59 :',^i' Expected: TRUE' ,^i); if SecondsToTimeSpan(450419) = '125:06:59' Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test SecondsToTimeSpan: 0  => 0:00:00 :',^i' Expected: TRUE' ,^i); if SecondsToTimeSpan(0) = '0:00:00' Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test SecondsToTimeSpan: -15203  => ungueltig:',^i' Expected: TRUE' ,^i); if SecondsToTimeSpan(-15203) = 'ungueltig' Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test SecondsToTimeSpan: 15203  => 4:13:23 :',^i' Expected: TRUE' ,^i); if SecondsToTimeSpan(15203) = '4:13:23' Then WriteLn('TRUE') Else WriteLn('FALSE');

	WriteLn(#13#10,'b) Time Difference');
	Write('Test TimeDifference: 4:13:23 & 6:13:23 => 7200 :',^i' Expected: TRUE' ,^i); span.hours := 4; span.minutes := 13; span.seconds := 23;
	span1.hours := 6; span1.minutes := 13; span1.seconds := 23; if TimeDifference(span,span1) = 7200 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeDifference: 4:13:23 & 4:15:23 => 120 :',^i' Expected: TRUE' ,^i); span.hours := 4; span.minutes := 13; span.seconds := 23;
	span1.hours := 4; span1.minutes := 15; span1.seconds := 23; if TimeDifference(span,span1) = 120 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeDifference: 4:13:23 & 4:13:28 => 5 :',^i' Expected: TRUE' ,^i); span.hours := 4; span.minutes := 13; span.seconds := 23;
	span1.hours := 4; span1.minutes := 13; span1.seconds := 28; if TimeDifference(span,span1) = 5 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeDifference: -4:-13:-23 & 0:00:00 => 0 :',^i' Expected: FALSE' ,^i); span.hours := -4; span.minutes := -13; span.seconds := -23;
	span1.hours := 0; span1.minutes := 0; span1.seconds := 0; if TimeDifference(span,span1) = 0 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeDifference: 0:00:00 & 1:30:30 => 3810 :',^i' Expected: FALSE' ,^i); span.hours := 0; span.minutes := 0; span.seconds := 0;
	span1.hours := 1; span1.minutes := 30; span1.seconds := 30; if TimeDifference(span,span1) = 3810 Then WriteLn('TRUE') Else WriteLn('FALSE');

	WriteLn(#13#10,'2. Add up numbers');
	arr[0] := 4; arr[1] := 8; arr[2] := -3; arr[3] := 1; arr[4] := -8;
	arr[5] := 13; arr[6] := 3;	arr[7] := 2; arr[8] := -10; arr[9] := 0;
	Write('Test ComputeSums Expected: sumA:25 - sumB:-15 : ' ); if (ComputeSums(arr).sumA = 25) and (ComputeSums(arr).sumB = -15) Then WriteLn('TRUE') Else WriteLn('FALSE');	
	arr[0] := 10; arr[1] := 9; arr[2] := 11; arr[3] := 14; arr[4] := 6;
	arr[5] := 2; arr[6] := -10;	arr[7] := 0; arr[8] := 8; arr[9] := 11;
	Write('Test ComputeSums Expected: sumA:46 - sumB:15 : ' ); if (ComputeSums(arr).sumA = 35) and (ComputeSums(arr).sumB = 7) Then WriteLn('TRUE') Else WriteLn('FALSE');	
	arr[0] := -10; arr[1] := 0; arr[2] := 10; arr[3] := 5; arr[4] := 1;
	arr[5] := -1; arr[6] := -15;arr[7] := -11; arr[8] := 0; arr[9] := 10;
	Write('Test ComputeSums Expected: sumA:-10 - sumB:0 : ' ); if (ComputeSums(arr).sumA = -10) and (ComputeSums(arr).sumB = 0) Then WriteLn('TRUE') Else WriteLn('FALSE');	
	arr[0] := 5; arr[1] := 10; arr[2] := -20; arr[3] := 0; arr[4] := 10;
	arr[5] := 10; arr[6] := 10;	arr[7] := 10; arr[8] := 10; arr[9] := 10;
	Write('Test ComputeSums Expected: sumA:25 - sumB:-15 : ' ); if (ComputeSums(arr).sumA = 15) and (ComputeSums(arr).sumB = -20) Then WriteLn('TRUE') Else WriteLn('FALSE');	
	arr[0] := 0; arr[1] := 10; arr[2] := -20; arr[3] := 0; arr[4] := 10;
	arr[5] := 10; arr[6] := 10;	arr[7] := 10; arr[8] := 10; arr[9] := 10;
	Write('Test ComputeSums Expected: sumA:0 - sumB:0 : ' ); if (ComputeSums(arr).sumA = 0) and (ComputeSums(arr).sumB = 0) Then WriteLn('TRUE') Else WriteLn('FALSE');	
	
	WriteLn(#13#10,'3. Revert String ');
	Str :='Jemand musste Josef K. verleumdet haben, denn ohne dass er etwas Boeses getan haette, wurde er eines Morgens verhaftet. "Wie ein Hund!" sagte er, es war, als sollte die Scham ihn ueberleben, als Gregor Samsa eines Morgens aus unruhigen Traeumen erwachte.';
	Revert(Str,result);
	Revert(result,result1);
	if result1 = Str Then WriteLn('TEST SUCCESS') ELSE WriteLn('TEST FAILURE');
	
End.