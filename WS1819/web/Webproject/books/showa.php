<?php
   include('../session.php');
   include('../config.php');
?>
<html>  
   <head>
    <title>Bücher anzeigen</title>
        <link rel="stylesheet" type="text/css" href="../style.css">
        <meta http-equiv="content-type" content="text/html; charset=utf-8"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
   </head>
   
   <body>
   <h1 class="headLine">Bücher</h1>
   <div class="widemainList">
            <button class="backbutton" onclick="window.location.href='../books.php'">Zurück</button>
			<input type="text" id="myInput" class="searchInput" onkeyup="myFunction()" placeholder="Search..." title="Type in a name">
			<table id="myTable">
                <thead>
                    <tr>
                        <th>Autor</th>
                        <th>Kategorie</th>
                        <th>Titel</th>
                        <th>Status</th>
                        <th>Typ</th>
                        <th>Info</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $sql = "SELECT a.name as 'Autor',k.name as 'Kategorie',b.titel as 'Titel', 
                        IF(b.kauf = 'JA', IF(b.`kauf`='JA','Gekauft Gelesen','Gekauft') ,IF(b.`read`='JA','Gelesen','-')) as Status,
                        t.name as 'Typ',IF(b.beschreibung != '',b.beschreibung,'-') as 'Info' 
                        from buch b
                        left join autor a on a.id_autor = b.fk_id_autor
                        left join kategorie k on k.id_kategorie = b.fk_id_kategorie
                        left join buch_type t on t.id_buchtype = b.fk_id_buchtype";
                        $result = mysqli_query($db,$sql);
                        while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                            echo "<tr>";
                            echo "<td data-column=\"Titel\">" . $row['Titel'] . "</td>";
                            echo "<td data-column=\"Kategorie\">" . $row['Kategorie'] . "</td>";
                            echo "<td data-column=\"Autor\">" . $row['Autor'] . "</td>";
                            echo "<td data-column=\"Status\">" . $row['Status'] . "</td>";
                            echo "<td data-column=\"Typ\">" . $row['Typ'] . "</td>";
                            echo "<td data-column=\"Info\">" . $row['Info'] . "</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody> 
			</table>
            <button class="backbutton" onclick="window.location.href='../books.php'">Zurück</button>
            </div>
    <script>
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue, tds,k, dontHide;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 1; i < tr.length; i++) {
            tds = tr[i].getElementsByTagName("td");
            dontHide = false;
            for(k = 0; k < tds.length; k++){
            td = tds[k];     
            if (td) {
                txtValue = td.textContent || td.innerText;
                dontHide = dontHide || (txtValue.toUpperCase().indexOf(filter) > -1);
            } 
            }
            if (dontHide) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
    </script>
   </body> 
</html>