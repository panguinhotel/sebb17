unit UnitList;
	
interface
Uses MyExtensions;

TYPE  	TestResult = ^TestResultRec;   
		TestResultRec = RECORD    
		Book: AddressBookEntry;
		next: TestResult;   END;
		TestResultList = TestResult;	
(*Add a new Test to List*)
FUNCTION AddValueToList(Var l :TestResultList; value: AddressBookEntry) : Boolean;
(*Initialize List*)
procedure InitList(VAR l: TestResultList);
(*Prints list on Console*)
PROCEDURE DisplayList(l: TestResultList);
(*Disposes List*)
PROCEDURE DisposeList(Var l : TestResultList);
(*Return amout of Testresults in List*)
FUNCTION CountList(param: TestResultList) : INTEGER;
(* Delete Value From List *)
FUNCTION DeleteValueFromList(Var l : TestResultList; vorname,nachname : string)	:Boolean;
(*Find Before Node for Node*)
FUNCTION FindBefore(list: TestResultList; toFind : TestResult) : TestResultList;
(*Find Entry*)
FUNCTION FindEntry(list: TestResultList; entry : AddressBookEntry) : TestResultList;
implementation
(*Initialize List*)
procedure InitList(VAR l: TestResultList);
begin
	l:=NIL;
end;
(*Prints list on Console*)
PROCEDURE DisplayList(l: TestResultList);
Var	
	n: TestResult;
BEGIN
	n:= l;
	WHILE n <> NIL  DO BEGIN
		WriteLn(n^.Book.vorname,^i,n^.Book.nachname,^i,n^.Book.nummer);
		n:=n^.next;
	END;
END; (* DisplayList *)
(*Add a new Value to List*)
FUNCTION AddValueToList(Var l :TestResultList; value: AddressBookEntry) : Boolean;
VAR
	n,temp,tmp2:TestResult;
	cont : boolean;
BEGIN
	New(n);
	IF n = NIL  THEN BEGIN
		AddValueToList := false;
	END
	ELSE BEGIN	
		tmp2 := l;
		cont := false;
		WHILE (tmp2 <> NIL) and (not cont)  DO BEGIN
			cont :=  (tmp2^.Book.vorname = value.vorname) and (tmp2^.Book.nachname = value.nachname);
			tmp2 := tmp2^.next;
		END;

		IF cont  THEN BEGIN
			AddValueToList := false;
		END
		ELSE BEGIN
			n^.Book := value;
			n^.next := nil;
			IF l = Nil  THEN BEGIN
				l:= n
			End
			ELSE BEGIN
				(*frind last node *);
				temp := l;
				WHILE temp^.next <> NIL  DO BEGIN
					temp := temp^.next;
				END;
				(*set next of last node to n *);
				temp^.next := n;
				
			END;
			AddValueToList := true;
		END;
	End;
END; (* AddValueToList *)
(* Delete Value From List *)
FUNCTION DeleteValueFromList(Var l : TestResultList; vorname,nachname : string)	:Boolean;
VAR  
  	n, tmp: TestResult;
begin
	n:=l;
	tmp:=l;
	WHILE n <> NIL  DO BEGIN
		IF (n^.Book.vorname = vorname) and (n^.Book.nachname = nachname)  THEN BEGIN
			IF n = l THEN BEGIN
				l := l^.next;
				Dispose(n);
				DeleteValueFromList:=true;		
			END
			ELSE if(FindBefore(l,n) <> NIL) THEN
				FindBefore(l,n)^.next := n^.next
			ELSE
				n:=nil;
				Dispose(n);
				DeleteValueFromList:=true;			
			EXIT;
		END;
		n := n^.next;
		tmp := n;
	END;
	DeleteValueFromList:=false;
end;(* Delete Value From List *)
(*Disposes List*)
PROCEDURE DisposeList(Var l : TestResultList);
VAR  
  n, next: TestResult;
BEGIN
  n := l;
  WHILE  n <> nil DO BEGIN
    next := n^.next;
    Dispose(n);
    n := next;
  END;
  l:=nil;
END; (* DisposeList *)
(*Return amout of Testresults in List*)
FUNCTION CountList(param: TestResultList) : INTEGER;
Var
	n:TestResult;
	i:Integer;
BEGIN
	n:=param;
	i:= 0;
	WHILE n <> NIL  DO BEGIN
		i:= i+1;
		n:= n^.next;
	END;
	CountList := i;
END; (* CountList *)
(*Find Before Node for Node*)
FUNCTION FindBefore(list: TestResultList; toFind : TestResult) : TestResultList;
Var
	n:TestResult;
BEGIN
	IF list = toFind  THEN BEGIN
		FindBefore := nil;
	END
	ELSE BEGIN
		n:=list;
		WHILE (n^.next <> toFind) and (n^.next <> NIL)  DO BEGIN
			n := n^.next;
		END;
		if(n^.next = toFind) THEN
			FindBefore := n
		ELSE FindBefore := NIL;	
	END;
END; (* FindBefore *)
(*FindEntry*)
FUNCTION FindEntry(list: TestResultList; entry : AddressBookEntry) : TestResultList;
VAR
	tmp : TestResult;
BEGIN
	tmp := list;
	WHILE tmp <> NIL  DO BEGIN
		IF (tmp^.Book.vorname = entry.vorname) and (tmp^.Book.nachname = entry.nachname)  THEN BEGIN
			FindEntry := tmp;
			EXIT;
		END;
		tmp := tmp^.next;
	END;
	FindEntry := NIL;
END; (* FindEntry *)
end.