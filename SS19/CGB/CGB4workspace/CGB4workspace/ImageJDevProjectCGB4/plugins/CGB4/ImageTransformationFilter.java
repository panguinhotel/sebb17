

public class ImageTransformationFilter {

	/**
	 * apply scalar transformation
	 * 
	 * @param inImg
	 * @param width
	 * @param height
	 * @param transferFunction
	 * @return
	 */
	public static int[][] getTransformedImage(int[][] inImg, int width, int height, int[] transferFunction) {
		int[][] returnImg = new int[width][height];
		
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				returnImg[x][y] = transferFunction[inImg[x][y]];
			}
		}
		
		return returnImg;
	}
	
	/**
	 * get transfer function for contrast inversion 
	 * 
	 * @param maxVal
	 * @return
	 */
	public static int[] getInversionTF(int maxVal) {
		int[] transferFunction = new int[maxVal + 1];
		for(int i = 0; i <= maxVal; i++) {
			transferFunction[i] = maxVal - i;
		}
		
		return transferFunction;
	}

	
}
