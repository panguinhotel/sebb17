/*
 * Extended_Simulator.cpp
 *
 *  Created on: 07.03.2019
 *      Author: Lukas Schaubmayr
 */

#include "Extended_Simulator.h"
#include "Producer_Event.h"
#include "Consumer_Event.h"
#include "Supervisor_Event.h"
#include <iostream>

void Extended_Simulator::step()
{
	if(!m_events.empty())
	{
		m_current_ticks++;
		Base_Event *customEvent = m_events.top();
		customEvent->register_child_events(&m_events);
		m_events.pop();

		if(dynamic_cast<Producer_Event*>(customEvent))
		{
			Producer_Event* p_event = dynamic_cast<Producer_Event*>(customEvent);
			if(!p_event->produce(&m_data,m_max_size))
			{
				p_event->postpone(1);
				addEvent(p_event);
			}
			else
				delete p_event;

		}
		else if(dynamic_cast<Consumer_Event*>(customEvent))
		{
			Consumer_Event* c_event = dynamic_cast<Consumer_Event*>(customEvent);
			if(!c_event->consume(&m_data))
			{
				c_event->postpone(1);
				addEvent(c_event);
			}
			else
				delete c_event;
		}
		else if(dynamic_cast<Supervisor_Event*>(customEvent))
		{
			Supervisor_Event* s_event = dynamic_cast<Supervisor_Event*>(customEvent);
			if(s_event->continue_operation(m_current_ticks, m_allowed_ticks, m_data.size(), m_allowed_items))
				m_status = Status::Running;
			else
			{
				m_status = Status::Stopped;
				delete s_event;
			}
		}
	}
	else
		std::cout <<"Nothing to do";
}

