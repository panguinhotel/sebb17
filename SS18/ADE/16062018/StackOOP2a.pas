unit StackOOP2a;

interface

    Type
        Node = ^NodeRec;
        NodeRec = Record
            value : Integer;
            next : Node;
        END;
      
        Stack = OBJECT

		(*Initializes the stackn *)
		Procedure Init;

		(* resets the stack. Stack can be reused afterwards *)
		Procedure Done;

		(*Pushes a value onto the stack*)
		(*In value : value that will be pushed on the stack*)
		(* Out ok : False in case of any errors; TRUE otherwise
			IF false, the value will has not been pushed onto the stack *)
		PROCEDURE Push(value:Integer; Var ok : Boolean);

		(* Pops the most recently pushed value from the stack *)
		(* Out value : value popped from the stack. *)
		(* Out ok : False in case of any errors (e.g. stack is empty)
			In this case, the returned value mustn't be used *)
		PROCEDURE Pop(Var value:Integer; Var ok : Boolean);

		(*Checks if stack is currently empty*)
		(*Returns : True if stack is empty; False otherwise*)
		Function Empty: Boolean;

		private
			top : Node;
	END;

implementation
 
    Procedure Stack.Init;
    BEGIN
     top := NIL;
    END; (* Init *)

    Procedure Stack.Done;
    Var
        next : Node;
    BEGIN
    	WHILE top <> NIL DO Begin
			next := top^.next;
			Dispose(top);
			top := next;
		END;
    END; (* Done *)

    PROCEDURE Stack.Push(value:Integer; Var ok : Boolean);
    Var
        n : Node;
    BEGIN
        New(n);
		IF  n <> NIL THEN BEGIN
			n^.value := value;
			n^.next := top;
			top := n;
			ok := TRUE;
		END ELSE BEGIN
			ok := FALSE;
		END;
    END; (* Push *)

    PROCEDURE Stack.Pop(Var value:Integer; Var ok : Boolean);
    Var
        n: Node;
    BEGIN
        IF not Empty THEN BEGIN
			value := top^.value;
			n := top^.next;
			Dispose(top);
			top := n;
			ok := TRUE;
		END ELSE BEGIN
			ok := FALSE;
	    END;
    END; (* Pop *)

    Function Stack.Empty: Boolean;
    Begin
        Empty := top = NIL;
    END;
end.