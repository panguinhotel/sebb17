PROGRAM Sierpinski;
(*Berechnet die Fläche des Dreiecks für x Interationen  (Rekursiv)*)
function CalculateArea(x:Integer) : Real;
begin
	IF (x = 0) Then
		CalculateArea := 1
	ELSE
		CalculateArea := CalculateArea(x-1)*0.75
end;(* CalculateArea *)
(*Berechnet die Länge aller Kanten von x Interationen (Rekursiv)*)
FUNCTION CalculateLine(x: Integer) : Real;
BEGIN
	IF(x = 0) Then
		CalculateLine:= 3 (*3= Umfang Startdreieck mit Seitenlänge 1*)
	ELSE
		CalculateLine := CalculateLine(x-1)*1.5;
END; (* CalculateLine *)
(*Berechnet die Fläche des Dreiecks für x Interationen (Iterativ)*)
function CalculateAreaIT(x:Integer) : Real;
VAR
	i: Integer;
	tmp : Real;
BEGIN
	tmp:= 1;
	FOR i:= 1 TO x DO BEGIN
		tmp:= tmp * 0.75
	END;
	CalculateAreaIT := tmp;
end;(* CalculateAreaIT *)
(*Berechnet die Länge aller Kanten von x Interationen (Iterativ)*)
FUNCTION CalculateLineIT(x: Integer) : Real;
VAR
	i: Integer;
	tmp : Real;
BEGIN
	tmp:= 3; (*3= Umfang Startdreieck mit Seitenlänge 1*)
	FOR i:= 1 TO x DO BEGIN
		tmp:= tmp * 1.5
	END;
	CalculateLineIT := tmp;
END; (* CalculateLineIT *)
(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	WriteLn('Test - Area after n Iterations - Rekursiv');
	WriteLn('Area after 0 Iterations : ',CalculateArea(0):6:10);
	WriteLn('Area after 1 Iterations : ',CalculateArea(1):6:10);
	WriteLn('Area after 2 Iterations : ',CalculateArea(2):6:10);
	WriteLn('Area after 3 Iterations : ',CalculateArea(3):6:10);
	WriteLn('Area after 4 Iterations : ',CalculateArea(4):6:10);

	WriteLn('Test - Area after n Iterations - Iterativ');
	WriteLn('Area after 0 Iterations : ',CalculateAreaIT(0):6:10);
	WriteLn('Area after 1 Iterations : ',CalculateAreaIT(1):6:10);
	WriteLn('Area after 2 Iterations : ',CalculateAreaIT(2):6:10);
	WriteLn('Area after 3 Iterations : ',CalculateAreaIT(3):6:10);
	WriteLn('Area after 4 Iterations : ',CalculateAreaIT(4):6:10);

	WriteLn('Test - SumLine after n Iterations - Rekursiv');
	WriteLn('SumLine after 0 Iterations : ',CalculateLine(0):6:10);
	WriteLn('SumLine after 1 Iterations : ',CalculateLine(1):6:10);
	WriteLn('SumLine after 2 Iterations : ',CalculateLine(2):6:10);
	WriteLn('SumLine after 3 Iterations : ',CalculateLine(3):6:10);
	WriteLn('SumLine after 4 Iterations : ',CalculateLine(4):6:10);

	WriteLn('Test - SumLine after n Iterations - Iterativ');
	WriteLn('SumLine after 0 Iterations : ',CalculateLineIT(0):6:10);
	WriteLn('SumLine after 1 Iterations : ',CalculateLineIT(1):6:10);
	WriteLn('SumLine after 2 Iterations : ',CalculateLineIT(2):6:10);
	WriteLn('SumLine after 3 Iterations : ',CalculateLineIT(3):6:10);
	WriteLn('SumLine after 4 Iterations : ',CalculateLineIT(4):6:10);
END. (* Sierpinski *)