#include <iostream>
#include <set>
#include <iterator>  // ostream_iterator
#include <algorithm> // for_each
#include <memory>    // unique_ptr, shared_ptr

#include "scoped_ptr.h"
#include "person.h"
#include "student.h"
#include "utils.h"

using namespace std;

static void static_collection_test() {

}

static void simple_pointer_test() {

}

static void scoped_pointer_test() {

}

static void unique_pointer_test() {

}

static void counted_pointer_test() {

}

static void shared_pointer_test() {

}

static void cyclic_references_test() {

}

static void weak_pointer_test() {

}


int main() {
  cout << std::boolalpha;
  // RUN(static_collection_test());
  // RUN(simple_pointer_test());
  // RUN(scoped_pointer_test());
  // RUN(unique_pointer_test());
  // RUN(counted_pointer_test());
  // RUN(shared_pointer_test());
  // RUN(cyclic_references_test());
  // RUN(weak_pointer_test());
}
