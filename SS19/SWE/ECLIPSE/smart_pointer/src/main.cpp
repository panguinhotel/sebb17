#include <iostream>
#include <set>
#include <iterator>  // ostream_iterator
#include <algorithm> // for_each
#include <memory>    // unique_ptr, shared_ptr

#include "scoped_ptr.h"
#include "person.h"
#include "student.h"
#include "utils.h"

using namespace std;

static void static_collection_test() {
	set<person> s;
	s.insert(person("Bruno","Buchberger"));
	s.emplace("Armin","Assinger");

	//NOTE: object slicing, do not do this
	s.insert(student("Christian","Chemniz","S1710123"));

	copy(begin(s), end(s),ostream_iterator<person>(cout,"\n"));
}

static void simple_pointer_test() {
	set<person*> s;
	s.insert(new person("Bruno","Buchberger"));
	s.insert(new person("Armin","Assinger"));
	s.insert(new student("Christian","Chemniz","S1710123"));

	for(auto p : s)
	{
		DEBUG(*p);
	}

	auto s2 = s;

	for_each(begin(s),end(s),[](person * p){delete p;});

	s.clear();
}

constexpr auto less_ptr = [](const auto &a, const auto &b)
{
	return *a < *b;
};

static void scoped_pointer_test() {

	scoped_ptr<person> sp1(new person("Bruno","Buchberger"));
	DEBUG(*sp1);

	//scoped_ptr<person> sp2(sp1);
	scoped_ptr<person> sp2(std::move(sp1));
	DEBUG(sp1);
	DEBUG(*sp2);


	scoped_ptr<person> sp3;
	sp3 = std::move(sp2);
	DEBUG(sp2);
	DEBUG(*sp3);

	set<scoped_ptr<person>, decltype(less_ptr)> s(less_ptr);
	s.insert(make_scoped<person>("Bruno","Buchberger"));
	s.insert(make_scoped<person>("Armin","Assinger"));
	s.insert(scoped_ptr<person>(new student("Christian","Chemniz","S1710123")));

	for(const scoped_ptr<person> &p : s)
	{
		cout << *p << endl;
	}

	auto s2 = std::move(s);
	DEBUG(s.size());
	DEBUG(s2.size());

}

//auto close_file = [](FILE*f) {fclose(f); };
//std::unique_ptr<FILE,decltype(close_file)> file(fopen("file.txt","r"),close_file);

static void unique_pointer_test() {

		unique_ptr<person> sp1(new person("Bruno","Buchberger"));
		DEBUG(*sp1);

		//scoped_ptr<person> sp2(sp1);
		unique_ptr<person> sp2(std::move(sp1));
		DEBUG(sp1.get());
		DEBUG(*sp2);


		unique_ptr<person> sp3;
		sp3 = std::move(sp2);
		DEBUG(sp2.get());
		DEBUG(*sp3);

		set<unique_ptr<person>, decltype(less_ptr)> s(less_ptr);
		s.insert(make_unique<person>("Bruno","Buchberger"));
		s.insert(make_unique<person>("Armin","Assinger"));
		s.insert(unique_ptr<person>(new student("Christian","Chemniz","S1710123")));

		for(const unique_ptr<person> &p : s)
		{
			cout << *p << endl;
		}

		auto s2 = std::move(s);
		DEBUG(s.size());
		DEBUG(s2.size());
}

static void counted_pointer_test() {

}

static void shared_pointer_test() {

}

static void cyclic_references_test() {

}

static void weak_pointer_test() {

}


int main() {
	cout << std::boolalpha;
	RUN(static_collection_test());
	RUN(simple_pointer_test());
	RUN(scoped_pointer_test());
	// RUN(unique_pointer_test());
	// RUN(counted_pointer_test());
	// RUN(shared_pointer_test());
	// RUN(cyclic_references_test());
	// RUN(weak_pointer_test());
}
