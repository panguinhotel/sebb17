#include <stdio.h>
int main(int argc, char const *argv[])
{
	//a
	int a_i = 25;
	float a_f = 12.25;
	double a_d = a_i+a_f;
	printf("a) Ergebnis : %f\n",a_d);
	a_d = (double)((float)a_i+a_f);
	printf("a1) Ergebnis : %f\n",a_d);

	//b
	int b_i = 25;
	char b_c = 'a';
	int b_j = b_i+b_c;
	printf("b) Ergebnis : %f\n",b_j);
	b_j = b_i+(int)b_c;
	printf("b1) Ergebnis : %f\n",b_j);


	//c
	int c_i = 10;
	int c_j = 3;
	float c_f = 10.25;
	double c_d = c_f +c_i / c_j;
	printf("c) Ergebnis : %f\n",c_d);
	c_d = (double)(c_f +(float)(1.0*c_i / c_j));
	printf("c1) Ergebnis : %f\n",c_d);

	//d
	char d_c = 'a';
	int d_i = 1;
	float d_f = 1.11;
	double d_d = 2.22;
	printf("d) Ergebnis : %f\n",(d_c+d_i)*(d_f/d_d));
	printf("d1) Ergebnis : %f\n",((double)((int)d_c+d_i)*((double)d_f/d_d)));

	//e
	printf("e) Ergebnis : ");
	int e_x= -25;
	unsigned int e_y = 10;
	if((e_x+e_y) <0)
		printf("A\n");
	else printf("B\n");

	printf("e1) Ergebnis : ");
	if(((unsigned int)e_x+e_y) <0)
		printf("A\n");
	else printf("B\n");

	return 0;
}
