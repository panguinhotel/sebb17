#include <stdlib.h>
#include <time.h>

#include "TheSimulator.h"
#include "Warehouse.h"

#define RAND_MAX 200

static bool gotStopEvent = false;
static int hRandom = 1;

using namespace std;

EventQueue* theSimulator::createEvents(EventQueue& eq, int n) {
	/* initialize random seed: */
	srand (time(nullptr));

	/* generate n random events */
	for (int c = 0; c < (n -1) ; c++) {
		string makeEvent = "n";
		int randomDate  = (rand() % 100 + 1);
		hRandom++;
		int randomEvent = (rand() % 100 + 1);
		hRandom++;
		if (randomEvent <= 40) {	/* means 60-40 producer-consumer relationship */
			makeEvent = "C";
		} else {
			makeEvent = "P";
		}

		Event* newEvent = new Event(randomDate, makeEvent, false);
		eq.add(newEvent);
	}

	/* random stop event */
	int randomStopDate  = (rand() % 100 + 1);
	hRandom++;
	Event* newEvent = new Event(randomStopDate, "X", true);
	eq.add(newEvent);

	return &eq;
}

EventQueue* theSimulator::step(EventQueue& eq, int steps) {
	Warehouse wh = Warehouse();
	step(eq, wh, steps);
	return &eq;
}

EventQueue* theSimulator::step(EventQueue& eq, Warehouse& wh, int steps) {
	gotStopEvent = false;
	int i = 0;

	while((i < steps) && (!eq.isEmpty()) && (!gotStopEvent)) {
		/* initialize random seed: */
		srand (time(NULL));

		i++;

		Event* gotEvent = eq.getNextEvent();
		cout << "step: " << gotEvent->getEventDateTime() << "-" << gotEvent->getEventType() << "-" << gotEvent->isStopEvent() << endl;

		if (gotEvent->isStopEvent()) {
			cout << "stopEvent detected !! " << endl;
			gotStopEvent = true;
			break;
		}

		if ((gotEvent->getEventType() == "P") && (!wh.isFull())) {
			cout << "  +P - add a product " << endl;
			wh.addProduct();

		} else if ((gotEvent->getEventType() == "C") && (wh.anythingToBuy())) {
			cout << "  -C - buy a product " << endl;
			wh.buyProduct();
		} else {
			/* create newEvent and reschedule it for later (here random) */
			srand (time(NULL));
			int randomReDate  = ((rand() * hRandom) + 1) % 100 +1;
			hRandom++;
			cout << "  >> warehouse closed - come back later (reschedule event (" << randomReDate << ") << " << endl;
			Event* newEvent = new Event(randomReDate, gotEvent->getEventType(), gotEvent->isStopEvent());
			eq.add(newEvent);
		}

		cout << "   *warehouse  size is now ("  << wh.countProducts() << ")" << endl;
		cout << "   *eventqueue size is now ("  << eq.getSize()       << ")" << endl;
	}

	return &eq;
}

void theSimulator::run(EventQueue& eq) {
	cout << "+++ Simulation start for run +++" << endl;

	Warehouse wh = Warehouse();

	while((!eq.isEmpty()) && (!gotStopEvent)) {

		cout << "next step" << endl;
		step(eq, wh, 1);

		if (gotStopEvent) {
			break;
		} else if (eq.isEmpty()) {
			cout << "EventQueue has no more events" << endl;
		}
	}
	cout << "+++ Simulation end +++" << endl;
}


