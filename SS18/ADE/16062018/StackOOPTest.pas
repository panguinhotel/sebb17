PROGRAM StackADTTest;

USES
	StackOOP1a;
VAR
	i: Int64;
	ok, ok1:Boolean;
	val : Integer;
	s, s1: Stack;

	es : ExtendedStack;


(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	i:=1;
	s.Init;
	s1.Init;
	WriteLn(es.CurrentSize);
	repeat
		s.Push(i*i,ok);
		s1.Push(i*i,ok);
		i := i+1;
	until (i> 10 ) or Not ok;

	IF Not ok Then begin
		WriteLn('Fehler!');
	end;

	repeat
		s.Pop(val, ok);
		WriteLn('popped #', val);
	until (s.Empty or Not ok);

	IF Not ok Then begin
		WriteLn('Fehler!');
	end;

	repeat
		s1.Pop(val, ok1);
		WriteLn('popped #', val);
	until (s1.Empty or Not ok1);

	IF Not ok1 Then begin
		WriteLn('Fehler!');
	end;

	// Pop(val,ok);
	// WriteLn(ok);

	// Push(99,ok);
	// WriteLn(ok);

	// Pop(val,ok);
	// WriteLn(ok);
	// WriteLn(val);

	s.Done;
	s1.Done;

END. (* StackADSTest *)