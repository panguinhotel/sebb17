<?php
   include('../session.php');
   include('../config.php');

   if($_SERVER["REQUEST_METHOD"] == "POST") {
	$name = mysqli_real_escape_string($db,$_POST['username']);
	$oldname = mysqli_real_escape_string($db,$_POST['b_autor']);
	$date = date('Y-m-d');

	$sql = "Update buch_type set name ='$name' where name = '$oldname' and fk_id_user = $login_userID";
	$result = mysqli_query($db,$sql);

	if($result) {
		echo '<script language="javascript">';
		echo 'alert("Update erfolgreich")';
		echo '</script>';
	}else {
	  	echo '<script language="javascript">';
	  	echo 'alert("Update fehlgeschlagen")';
	  	echo '</script>';
	}
 }
?>
<html>  
   <head>
    <title>Buchtyp bearbeiten</title>
          <link rel="stylesheet" type="text/css" href="../style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
   </head>
   
   <body>
   		<h1 class="headLine">Buchtyp bearbeiten</h1>
   		<div class="mainList">
			<button id="autorNameBtn"  onclick="myFunction()" class="mainButton">< Select Buchtyp ></button>
			<div class="dropdown mainList">
			<div id="myDropdown" class="dropdown-content">
				<input type="text" class="searchInput" placeholder="Search.." id="myInput" onkeyup="filterFunction()">
				<?php
					$sql = "SELECT id_buchtype, name FROM buch_type where fk_id_user = $login_userID;";
					$result = mysqli_query($db,$sql);
					while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
						echo "<a class=\"ddItem\" href=\"#\" data-id=\"".$row['id_buchtype']."\">".$row['name']."</a>";
					}
				?> 
			</div>
			</div>
			<form action = "" method = "post" >
				<div id ="updateDiv" class="updateDivClass">
					<input type="hidden" id="b_autor" name="b_autor" value="0"/><br>
					<input id="inp_authname" class="loginInput" type="text" placeholder="" name="username" required><br>
					<input class="mainButton" type="submit" value="Ändern"><br>
				</div>
			</form>		
			<button class="backbutton" onclick="window.location.href='../books.php'">Zurück</button>
		</div>
		
		
	<script>
	function myFunction() {
		document.getElementById("myDropdown").classList.toggle("show");
		var x = document.getElementsByClassName("ddItem");

		for(var i = 0; i < x.length; i++)
		{
			x[i].addEventListener("click", setName);
		}
	}

	function filterFunction() {
		var input, filter, ul, li, a, i;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		div = document.getElementById("myDropdown");
		a = div.getElementsByTagName("a");
		for (i = 0; i < a.length; i++) {
			txtValue = a[i].textContent || a[i].innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
			a[i].style.display = "";
			} else {
			a[i].style.display = "none";
			}
		}
	}

	function setName(event){
		var clickedElement = event.target;
		document.getElementById("inp_authname").value = clickedElement.innerHTML;
		document.getElementById("myDropdown").classList.toggle("show");
		document.getElementById("b_autor").value = clickedElement.innerHTML;
		document.getElementById("updateDiv").style.display = "block";

	}
	</script>
   </body> 
</html>