<?php
   include('../session.php');
   include('../config.php');
?>
<html>  
   <head>
    <title>Kategorie anzeigen</title>
          <link rel="stylesheet" type="text/css" href="../style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
   </head>
   
   <body>
   <h1 class="headLine">Kategorien</h1>
   		<div class="mainList">
		   <button class="backbutton" onclick="window.location.href='../books.php'">Zurück</button>
			<input type="text" class="searchInput" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
			<ul id="myUL" class="myUL">
				<?php
					$sql = "SELECT id_kategorie, name FROM kategorie where fk_id_user = $login_userID;";
					$result = mysqli_query($db,$sql);
					while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
						echo "<li><a href=\"#\">".$row['name']."</a></li>";
					}
				?> 
			</ul>
			<button class="backbutton" onclick="window.location.href='../books.php'">Zurück</button>
		</div>
	<script>
	function myFunction() {
		var input, filter, ul, li, a, i, txtValue;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		ul = document.getElementById("myUL");
		li = ul.getElementsByTagName("li");
		for (i = 0; i < li.length; i++) {
			a = li[i].getElementsByTagName("a")[0];
			txtValue = a.textContent || a.innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				li[i].style.display = "";
			} else {
				li[i].style.display = "none";
			}
		}
	}
	</script>
   </body> 
</html>