unit Scanner;


interface
	TYPE Symbol = (noSym, trueSym,falseSym,
	nullSym,numberSym,stringSym,
	gkleftSym, gkrightSym, ekleftSym, 
	ekrightSym, dpSym, commaSym,endSym);
CONST
    TAB = Chr(9);
    LF = Chr(10);
    CR = Chr(13);
    BLANK = ' ';
PROCEDURE InitScanner(VAR inputFile,outputFile : TEXT; useFile : Boolean);
PROCEDURE GetNextSymbol;
FUNCTION CurrentSymbol:Symbol;
PROCEDURE GetCurrentSymbolPosition(Var line,column: INTEGER);
implementation

VAR
    inpFile,outFile: Text;
    curChar: CHAR;
    charLine, charColumn: INTEGER;
    curSymbol: Symbol;
    symbolLine, symbolColumn: INTEGER;
	usFile : Boolean;

PROCEDURE GetNextChar;
 BEGIN
	IF not EOF(inpFile) THEN BEGIN
		Read(inpFile, curChar);
			IF (curChar = CR) OR (curChar = LF) THEN BEGIN
			IF (charColumn > 0) THEN
				Inc(charLine);
			curChar := BLANK;
			charColumn := 0;
			END
		ELSE
		Inc(charColumn);
		
		{sem}IF(curChar <> BLANK) AND (curChar <> TAB) THEN BEGIN
				IF usFile THEN BEGIN
					WRITE(outFile,curChar); END
				ELSE BEGIN Write(curChar); END;
		END;{endsem}
	END ELSE 
	BEGIN
		curChar := Chr(0);
	END;
   
END;
    
PROCEDURE InitScanner(VAR inputFile,outputFile : TEXT; useFile : Boolean);
BEGIN
    inpFile := inputFile;
	outFile := outputFile;
    charLine := 1;
    charColumn := 0;
	usFile := useFile;
    GetNextChar;
    GetNextSymbol;
END;

PROCEDURE GetNextSymbol;
VAR
	isEscapeBefore : Boolean;
BEGIN
	WHILE ((curChar = BLANK) OR (curChar = TAB)) and (curChar <> Chr(0)) DO
    GetNextChar;
    
    symbolLine := charLine;
    symbolColumn := charColumn;
    
    CASE curChar OF
		Chr(0) : BEGIN curSymbol := endSym; END;
		':' : BEGIN curSymbol := dpSym; GetNextChar; END;
		',' : BEGIN curSymbol := commaSym; GetNextChar; END;
		'[' : BEGIN curSymbol := ekleftSym; GetNextChar; END;
		']' : BEGIN curSymbol := ekrightSym; GetNextChar; END;
		'{' : BEGIN curSymbol := gkleftSym; GetNextChar; END;
		'}' : BEGIN curSymbol := gkrightSym; GetNextChar; END;
		'n' : BEGIN
				GetNextChar;
				IF curChar = 'u'  THEN BEGIN
					GetNextChar;
					IF curChar = 'l' THEN BEGIN
						GetNextChar;
						IF curChar = 'l' THEN BEGIN
							curSymbol := nullSym;
							GetNextChar;
						END ELSE curSymbol := noSym; 
					END ELSE curSymbol := noSym; 
				END ELSE curSymbol := noSym; 
			END;
		't' : BEGIN
				GetNextChar;
				IF curChar = 'r'  THEN BEGIN
					GetNextChar;
					IF curChar = 'u' THEN BEGIN
						GetNextChar;
						IF curChar = 'e' THEN BEGIN
							curSymbol := trueSym;
							GetNextChar;
						END ELSE curSymbol := noSym; 
					END ELSE curSymbol := noSym; 
				END ELSE curSymbol := noSym; 
			END;
		'f' : BEGIN
				GetNextChar;
				IF curChar = 'a'  THEN BEGIN
					GetNextChar;
					IF curChar = 'l' THEN BEGIN
						GetNextChar;
						IF curChar = 's' THEN BEGIN
							GetNextChar;
							IF curChar = 'e' THEN BEGIN
								curSymbol := falseSym;
								GetNextChar;
							END ELSE curSymbol := noSym;
						END ELSE curSymbol := noSym; 
					END ELSE curSymbol := noSym; 
				END ELSE curSymbol := noSym; 
			END;		
		'"' : BEGIN 
				isEscapeBefore := curChar = '\';
				GetNextChar;
				WHILE (curChar <> '"') or ((curChar = '"') and isEscapeBefore) DO BEGIN //TODO
					isEscapeBefore := curChar = '\';
					GetNextChar;
				END;
				curSymbol := stringSym;
				GetNextChar;
			END;
		'-','0'..'9' : BEGIN
				GetNextChar;
				WHILE curChar in ['0'..'9'] DO BEGIN
					GetNextChar;
				END;
				IF curChar = '.' THEN BEGIN
					GetNextChar;
					IF curChar in ['0'..'9'] THEN BEGIN
						WHILE curChar in ['0'..'9']  DO BEGIN
							GetNextChar;
						END;
						IF curChar = 'E'  THEN BEGIN
							GetNextChar;
							IF curChar in ['-','+']  THEN BEGIN
								GetNextChar;
								IF curChar in ['0'..'9'] THEN BEGIN
									 WHILE curChar in ['0'..'9'] DO BEGIN
									 	GetNextChar;
									 END;
									 curSymbol := numberSym;
								END ELSE curSymbol := noSym;
							END ELSE curSymbol := noSym;
						END ELSE curSymbol := numberSym;
					END ELSE curSymbol := noSym;
				END ELSE curSymbol := numberSym;		
			END
			ELSE curSymbol := noSym;
			END;
END;

FUNCTION CurrentSymbol: Symbol;
BEGIN
	CurrentSymbol := curSymbol;
END;
PROCEDURE GetCurrentSymbolPosition(VAR line, column: INTEGER);
BEGIN
	line := symbolLine;
	column := symbolColumn;
END;
end.