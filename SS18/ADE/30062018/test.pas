(*Author: Simon Schroffner*)
(*Date: 30-06-2018*)
(*Version: 0.1*)
(*Desc: TODO:*)
   
PROGRAM BasicOperations;
   
USES
   MetaInfo, MLStr, MLint, MLColl, MLVect, MLObj;
   
VAR
   s: MLString;
   c: MLCollection;
   i: INTEGER;
   v: MLVector;
   vectorIterator: MLIterator;
   o: MLObject;
BEGIN
   c := NewMLVector;
   s := NewMLString('Hagenberg');
   WriteLn(s^.Class);
   //Dispose(s, Done);
   c^.Add(s);
   c^.Add(NewMLString('FH'));
   
   v := NewMLVector;
   FOR i := 1 TO 10 DO BEGIN
       v^.Add(NewMLInteger(i));
   END;
   v^.Add(NewMLString('Test'));
   v^.WriteAsString;
   
   vectorIterator := v^.NewIterator;
   o := vectorIterator^.Next;

   while o <> NIL DO BEGIN
       IF (o^.Class = 'MLString') THEN BEGIN
           o^.WriteAsString;
           WriteLn(MLString(o)^.Length);
       END;
       o^.WriteAsString;
       o := vectorIterator^.Next;
   END;

   v^.DisposeElements;
   c^.DisposeElements;
   Dispose(v, Done);
   WriteMetaInfo;
END.