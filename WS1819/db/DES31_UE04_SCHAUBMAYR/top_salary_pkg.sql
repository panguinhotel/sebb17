CREATE OR REPLACE PACKAGE top_salary_pkg AS
  PROCEDURE GetTopSalaries;
END;
/
SHOW ERR

CREATE OR REPLACE PACKAGE BODY top_salary_pkg AS
  PROCEDURE GetTopSalaries
  IS
  CURSOR cTopSalaries IS
    SELECT 'Salary:'||to_char(SALARY)||'. EmpCnt:'||to_char(EMP_CNT)||
      '. Erstellt:'||to_char(CREATEDBY)||'/'||to_char(DATECREATED)||
      '. Ge�ndert:'||to_char(MODIFIEDBY)||'/'||to_char(DATEMODIFIED)||'.' text 
    FROM top_salaries
    ORDER BY SALARY DESC;
  BEGIN
    FOR vTopSalaries IN cTopSalaries (pLowSalary ,pHighSalary) LOOP
      DBMS_OUTPUT.PUT_LINE(vTopSalaries.text);
    END LOOP;
  END;
END;
/
SHOW ERR 
