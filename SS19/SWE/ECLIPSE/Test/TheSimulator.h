#ifndef THESIMULATOR_H
#define THESIMULATOR_H

#include "EventQueue.h"
#include "Warehouse.h"

class theSimulator {
public:
	theSimulator() = default;
	~theSimulator() = default;
	EventQueue* createEvents(EventQueue& eq, int num);
	EventQueue* createStopEvent(EventQueue& eq);
	EventQueue* step(EventQueue& eq, int n = 1);
	EventQueue* step(EventQueue& eq, Warehouse& wh, int n = 1);
	void        run(EventQueue& eq);
};

#endif
