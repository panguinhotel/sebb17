#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include <queue>
#include <iostream>
#include <queue>
#include <vector>

#include "Event.h"

#define queue_params Event*, std::vector<Event*>, greater_ptr<Event*>
template <typename T>
struct greater_ptr {
	bool operator()(const T& lhs, const T& rhs) const {
		return *lhs > *rhs;
	}
};

using namespace std;

class EventQueue {
public:
	EventQueue();
	~EventQueue();
	int    add(Event* newEvent);
	bool   isEmpty();
	Event* getNextEvent();
	int    getSize();
protected:
	std::priority_queue <queue_params> queue;
	int    counter;
};

#endif
