unit UnitTest;

interface
Uses UnitDate,UnitList,UnitListDL;
TYPE  Response = RECORD
		status : (Failure,Success);
		message : string;
End;

TYPE   Testresult = RECORD    
		Name : string;
		DurationTime : Int64;
		Result: (passed,failed);
End;
TYPE   Testrun = RECORD    
		CreationDate: Date;
		Testresults: TestResultList;
End;

Var
	Testruns : List;
	idxLastTest : Date;
(*Adds a TestRun with CrationDate to Testhistory max 20*)
PROCEDURE AddTestRun(d: Date; VAR res : Response);
(*Adds a Testresult with Name, DurationTime and Result to most recent Testrun (max 50)*)
PROCEDURE AddTestResultForMostRecentTestRun(param: Testresult; VAR res : Response);
(*Prints whole TestHistory on Console*)
PROCEDURE PrintTestHistory();
(*Calculates the average Test ExecutionTime for a specific name*)
FUNCTION AverageTestExecutionTime(name: string) : Int64;
(*Calculates the amount of times a Testresult occurs in the TestHistory*)
FUNCTION NumberOfTestExecutions(name: string) : INTEGER;
(*Calculates the number of failed Testruns*)
FUNCTION TotalNumberOfFailedTests() : INTEGER;
(*Clears the TestHistory*)
PROCEDURE ClearTestHistory();
(*Prints Testresults for Testrun with specific creationDate*)
PROCEDURE PrintStatisticsForTest(creationDate: Date);
(*Deletes all Testresults for given Testrun *)
PROCEDURE DeleteAllTestResultsForTest(creationDate: Date;Var res:Response);
(*Deltes whole Testrun*)
PROCEDURE DeleteTestRun(creationDate: Date;Var res:Response);
(*Returns number of Testruns*)
FUNCTION NumberOfTestRuns () : INTEGER;
(*Returns percentage of Successful Testsruns*)
FUNCTION TotalSuccessRatio () : Double;
implementation

(*Converts TestResult to Test for List*)
FUNCTION TestResultToTest(value: TestResult) : TestType;
BEGIN
	TestResultToTest.Name := value.Name;
	TestResultToTest.DurationTime := value.DurationTime;
	IF value.Result = passed  THEN BEGIN
		TestResultToTest.Result := UnitList.passed;
	END
	ELSE BEGIN
		TestResultToTest.Result := UnitList.failed;
	END;
	
END; (* TestResultToTest *)

PROCEDURE AddTestRun(d: Date ;VAR res : Response);
Var
tmp: Boolean;
n,m:List;
BEGIN
	IF IsValidDate(d)  THEN BEGIN
		tmp := false;
		n:=Testruns^.next;
		m:=Testruns;

		WHILE n<>Testruns DO BEGIN
			IF LiesBefore(d,n^.CreationDate) THEN BEGIN
				m:=n^.prev;
				break;
			END;
			WriteLn(DateToString(d));
			WriteLn(DateToString(n^.CreationDate));
			IF CompareDate(d,n^.CreationDate) THEN Begin
				tmp:=true;
				break;
			End;
			n := n^.next;
		END;
		IF not tmp  THEN BEGIN
			InsertAfter(m,d);
			idxLastTest:=d;
			res.status := Success;
			res.message := 'Testrun added';		
		END
		ELSE BEGIN
			res.status := Failure;
			res.message := 'Testrun already exists';
		END;

	END
	ELSE BEGIN
			res.status := Failure;
			res.message := 'Invalid CreationDate';
	END;
	
END; (* AddTestRun *)
PROCEDURE AddTestResultForMostRecentTestRun(param: Testresult; VAR res : Response);
Var
	n:List;
	found:boolean;
BEGIN
	if Testruns <> nil Then Begin
		found:=false;
		n:=Testruns^.next;
		WHILE (n<>Testruns) and (not found) DO BEGIN
			if CompareDate(idxLastTest,n^.CreationDate) Then Begin
				IF AddValueToList(n^.Testresults, TestResultToTest(param)) THEN BEGIN				 
						res.message:='Testresult: '+param.Name+' added';
						res.status:=Success;
					END
					ELSE BEGIN
						res.message:='No more space on Heap!';
						res.status:=Failure;
				END;
				found:=true;
			End;
			n := n^.next;
		END;
		
	End;
	
	
END; (* AddTestResultForMostRecentTestRun *)
PROCEDURE PrintTestHistory();
BEGIN
	UnitListDL.DisplayList(Testruns);
END; (* PrintTestHistory *)
PROCEDURE ClearTestHistory();
BEGIN
	UnitListDL.ClearList(Testruns);
END; (* ClearTestHistory *)
FUNCTION AverageTestExecutionTime(name: string) : Int64;
Var
	i: List;
	anz,sum : Int64;
	n :UnitList.TestResult;
BEGIN
	anz := NumberOfTestExecutions(name);
	sum := 0;
	i:=Testruns^.next;
	While(i<> Testruns) DO BEGIN
		n := i^.Testresults;
		WHILE n <> NIL  DO BEGIN
			IF n^.Test.Name = name  THEN BEGIN
				sum := sum + n^.Test.DurationTime;
			END;
			n:= n^.next;
		END;
		i:=i^.next;
	END;
	IF anz = 0  THEN AverageTestExecutionTime:= 0
	ELSE AverageTestExecutionTime:= sum DIV anz;

END; (* AverageTestExecutionTime *)
FUNCTION NumberOfTestExecutions(name: string) : INTEGER;
Var
	i :List;
	n :UnitList.TestResult;
BEGIN
	NumberOfTestExecutions:= 0;
	i:= Testruns^.next;
	WHILE (i<> Testruns)  DO BEGIN
		n:=i^.Testresults;
		WHILE n <> NIL  DO BEGIN
			IF n^.Test.Name = name  THEN BEGIN
				NumberOfTestExecutions := NumberOfTestExecutions + 1;
			END;
			n:= n^.next;
		END;
		i:= i^.next;
	END;
END; (* NumberOfTestExecutions *)
FUNCTION TotalNumberOfFailedTests() : INTEGER;
Var
	i:List;
	n :UnitList.TestResult;
BEGIN
	TotalNumberOfFailedTests:= 0;
	i:= Testruns^.next;
	WHILE (i<>Testruns)  DO BEGIN
		n:=i^.Testresults;
		WHILE n <> NIL  DO BEGIN
			IF n^.Test.Result = UnitList.failed  THEN BEGIN
				TotalNumberOfFailedTests := TotalNumberOfFailedTests + 1;
				break;
			END;
			n:= n^.next;
		END;
		i:= i^.next;
	END;
END; (* TotalNumberOfFailedTests *)
(*Deletes all Testresults for given Testrun *)
PROCEDURE DeleteAllTestResultsForTest(creationDate: Date;Var res:Response);
Var
	i:List;
	found :Boolean;
BEGIN
	i:=Testruns^.next;
	found := false;
	WHILE (i<>Testruns) and (not found)  DO BEGIN
		IF CompareDate(creationDate,i^.CreationDate) Then Begin
			found:= true;
		End
		ELSE BEGIN
			i:= i^.next;
		END;
	END;
	IF not found  THEN BEGIN
		res.status := failure;
		res.message := 'Testrun not found';
	END
	ELSE BEGIN
		UnitList.DisposeList(i^.Testresults);	
		res.status := success;
		res.message := 'Testresults successfully removed';
	END;
END; (* DeleteAllTestResultsForTest  *)
(*Deltes whole Testrun*)
PROCEDURE DeleteTestRun(creationDate: Date;Var res:Response);
VAR
	i:List;
	found:boolean;
BEGIN
	i:=Testruns^.next;
	found := false;
	WHILE (i<>Testruns) and (not found)  DO BEGIN
		UnitListDL.ClearList(Testruns);		
		i:= i^.next;
	END;
END; (* DeleteTestRun *)
(*Prints Testresults for Testrun with specific creationDate*)
PROCEDURE PrintStatisticsForTest(creationDate: Date);
VAR 
  n: List;
  found: boolean;
BEGIN
  n := Testruns^.next; 
  found := false;
  WHILE (n <> Testruns) and (not found) DO BEGIN
	IF CompareDate(creationDate,n^.creationDate)  THEN BEGIN
		WriteLn('Testrun ',DateToString(n^.CreationDate));
    	UnitList.DisplayList(n^.Testresults);
		WriteLn('=========================='); 
		found:=true;
	END;
    n := n^.next
  END;
END; (* PrintStatisticsForTest *)
(*Returns number of Testruns*)
FUNCTION NumberOfTestRuns () : INTEGER;
Var
	i:List;
BEGIN
	NumberOfTestRuns:= 0;
	i := Testruns^.next;
	while(i<> Testruns) DO Begin
		NumberOfTestRuns := NumberOfTestRuns +1;
		i:=i^.next;
	End;
END; (* NumberOfTestRuns  *)
(*Calculates the amout of Testruns containing Results*)
FUNCTION AmoutOfTestsWithResults() : INTEGER;
Var
	i:List;
	count : Integer;
BEGIN
	count := 0;
	i:= Testruns^.next;
	WHILE i<>Testruns  DO BEGIN
		IF (CountList(i^.Testresults) > 0) THEN count := count +1;
		i:=i^.next;
	END;
	AmoutOfTestsWithResults := count;
END; (* AmoutOfTestsWithResults *)
(*Returns percentage of Successful Testsruns*)
FUNCTION TotalSuccessRatio () : Double;
VAR
	x:Integer;
BEGIN
	x:= AmoutOfTestsWithResults();
	if( x <> 0) Then
		TotalSuccessRatio := 1- TotalNumberOfFailedTests() / x
	ELSE TotalSuccessRatio:= 0;
END; (* TotalSuccessRatio  *)
begin
	UnitListDL.InitList(Testruns);
end.