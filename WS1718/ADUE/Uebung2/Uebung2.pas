Program Uebung_02;
(*Funktion liest 2 64-bit Integer Werte ein und gibt den größten der 2 Werte zurück*)
Function Max2(x,y:Int64):Int64;
Begin
	if x<=y Then Max2:=y else Max2:=x;
End;
(*Funktion liest 3 64-bit Integer Werte ein und gibt den größten der 3 Werte zurück
Hierbei darf die Funktion Max2 nicht verwendet werden*)
Function Max3a(x,y,z:Int64):Int64;
Begin
	if (x>=y) and (x>= z) Then Max3a:=x
	else
		if y>=z Then Max3a:=y
		else Max3a:=z;
End;
(*Funktion liest 3 64-bit Integer Werte ein und gibt den größten der 3 Werte zurück
Hierbei soll die Funktion Max2 so effizient wie möglich verwendet werden *)
Function Max3b(x,y,z:Int64):Int64;
Begin
	Max3b:= Max2(x,Max2(y,z));
End;
(*Funktion liest 2 Integer Werte ein und gibt den Wert Basis^Exponent vom Typ Int64 zurük*)
Function Power(basis,exponent:Integer):Int64;
Var
	i:Integer;
Begin
	Power:=1;
	for i:=1 to exponent do
	Begin
		Power:= Power*basis;
	end;
End;
(*Funktion liest 2  Integer Werte ein und gibt den Wert der Binomischen Formel zurük*)
Function Binomial(x,y:Integer):Int64;
Begin
	Binomial:= Power(x,2) + 2*x*y + Power(y,2);
End;
(*Procedure liest 2 Integer Werte ein (Unter. und Obergrenze) und gibt eine Tabelle mit den berechneten Werten aus*)
Procedure DisplayBFTable(minA,maxA:Integer);
Var i,k:Integer;
Begin

	for k:= 1 TO 10 DO begin
		if k=1 Then Write('a\b',^i,'|');
		Write(^i,k);
	end;
	WriteLn();
	for k:= 1 TO 10 DO begin
		if k=1 Then Write('-----------+--');
		Write('------------');
	end;
	WriteLn();
	for i:= minA TO maxA DO begin
		for k:= 1 TO 10 DO begin
			if k=1 Then Write(i,^i,'|');
			Write(^i,Binomial(k,i));
			end;
		WriteLn();
	end;
	
End;
(*Funktion berechnet aus h(Int64) und m,s(Int8) die Anzahl der Sekunden
Ausgabe von 0 bei ungueltiger Eingabe*)
Function TimeSpanToSeconds(h:Int64; m,s:Int8) : Int64;
Begin
	if(m > 59) or (s > 59) or (h < 0) or (m < 0) or (s < 0) Then TimeSpanToSeconds:= -1
	else TimeSpanToSeconds := h*3600 + m*60 +s;
End;
(*Funktion berechnet aus einer Zahl von Sekunden die h,m,s und gibt einen String Wert zurück
Ausgabe von ungueltig bei ungueltiger eingabe*)
Function SecondsToTimeSpan(s:Int64) : string;
Var	newm,news : Int8;
	newh: Int64;
	x:string;
Begin
	if s < 0 Then SecondsToTimeSpan:= 'ungueltig'
	else
	begin
		newh:=s DIV 3600;
		newm:= (s-newh*3600) DIV 60;
		news:= s-newh*3600-newm*60;
		str (newh,x);
		SecondsToTimeSpan := x;
		str (newm,x);
		if(newm < 10) Then SecondsToTimeSpan:= SecondsToTimeSpan+':0' +x
		else SecondsToTimeSpan:= SecondsToTimeSpan+':'+x;
		str (news,x);
		if(news < 10) Then SecondsToTimeSpan:= SecondsToTimeSpan+':0'+x
		else SecondsToTimeSpan:= SecondsToTimeSpan+':'+x;
		
	end;
End;
Begin
	(*Test des Beispiels 1*)
	WriteLn('1. Maximum von zwei oder drei Werten');
	WriteLn('a) 2 Werte');
	Write('Test Max2: x=0,y=0 -> 0 : ',^i); if Max2(0,0) = 0 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max2: x=5,y=3 -> 5 : ',^i); if Max2(5,3) = 5 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max2: x=-4,y=4 -> 4 : ',^i); if Max2(-4,4) = 4 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max2: x=-4,y=-4 -> -4 : ',^i); if Max2(-4,-4) = -4 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max2: x=4,y=-4 -> 4 : ',^i); if Max2(4,-4) = 4 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max2: x=60000,y=4 -> 60000 : ',^i); if Max2(60000,4) = 60000 Then WriteLn('TRUE') Else WriteLn('FALSE');
	WriteLn(#13#10,'b) 3 Werte');
	Write('Test Max3a: x=0,y=0,z=0 -> 0 : ',^i); if Max3a(0,0,0) = 0 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max3a: x=1,y=2,z=3 -> 3 : ',^i); if Max3a(1,2,3) = 3 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max3a: x=-1,y=4,z=2 -> 4 : ',^i); if Max3a(-1,4,2) = 4 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max3a: x=-1,y=-2,z=-3 -> -1 : ',^i); if Max3a(-1,-2,-3) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max3a: x=-55,y=60000,z=19 -> 60000 : ',^i); if Max3a(-55,60000,19) = 60000 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max3a: x=1,y=5,z=77 -> 77 : ',^i); if Max3a(1,5,77) = 77 Then WriteLn('TRUE') Else WriteLn('FALSE');
	WriteLn(#13#10,'c) 3 Werte - Verwendung von Funktion Max2');
	Write('Test Max3a: x=0,y=0,z=0 -> 0 : ',^i); if Max3b(0,0,0) = 0 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max3a: x=1,y=2,z=3 -> 3 : ',^i); if Max3b(1,2,3) = 3 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max3a: x=-1,y=4,z=2 -> 4 : ',^i); if Max3b(-1,4,2) = 4 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max3a: x=-1,y=-2,z=-3 -> -1 : ',^i); if Max3b(-1,-2,-3) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max3a: x=-55,y=60000,z=19 -> 60000 : ',^i); if Max3b(-55,60000,19) = 60000 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test Max3a: x=1,y=5,z=77 -> 77 : ',^i); if Max3b(1,5,77) = 77 Then WriteLn('TRUE') Else WriteLn('FALSE');
	
	
	(*Test des Beispiels 2*)
	WriteLn(#13#10,'2. Eine Tabelle fuer die binomische Formel');
	DisplayBFTable(8,13);
	
	(*Test des Beispiels 3*)
	WriteLn(#13#10,'3. Uhrzeitkonvertierung');
	WriteLn('a) Zeitspanne in Sekunden');
	Write('Test TimeSpanToSeconds: 4:13:23  => 15203 :' ,^i); if TimeSpanToSeconds(4,13,23) = 15203 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 125:06:59  => 450419 :' ,^i); if TimeSpanToSeconds(125,6,59) = 450419 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 0:00:00  => 0 :' ,^i); if TimeSpanToSeconds(0,0,0) = 0 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 1:11:61  => -1 :' ,^i); if TimeSpanToSeconds(1,11,61) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 1:62:17  => -1 :' ,^i); if TimeSpanToSeconds(1,62,17) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: -4:13:23  => -1 :' ,^i); if TimeSpanToSeconds(-4,13,23) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 4:-13:23  => -1 :' ,^i); if TimeSpanToSeconds(4,-13,23) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test TimeSpanToSeconds: 4:13:-23  => -1 :' ,^i); if TimeSpanToSeconds(4,13,-23) = -1 Then WriteLn('TRUE') Else WriteLn('FALSE');
	
	WriteLn(#13#10,'b) Sekunden in Zeitspanne');
	Write('Test SecondsToTimeSpan: 15203  => 4:13:23 :' ,^i); if SecondsToTimeSpan(15203) = '4:13:23' Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test SecondsToTimeSpan: 450419  => 125:06:59 :' ,^i); if SecondsToTimeSpan(450419) = '125:06:59' Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test SecondsToTimeSpan: 0  => 0:00:00 :' ,^i); if SecondsToTimeSpan(0) = '0:00:00' Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test SecondsToTimeSpan: -15203  => ungueltig:' ,^i); if SecondsToTimeSpan(-15203) = 'ungueltig' Then WriteLn('TRUE') Else WriteLn('FALSE');
	Write('Test SecondsToTimeSpan: 15203  => 4:13:23 :' ,^i); if SecondsToTimeSpan(15203) = '4:13:23' Then WriteLn('TRUE') Else WriteLn('FALSE');
End.