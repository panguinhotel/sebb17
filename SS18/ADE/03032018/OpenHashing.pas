PROGRAM OpenHashing;

TYPE
	PNode = ^TNode;
	TNode = Record
		data: String;
		next : PNode;
	End;

	THashTableIndex = 0..9;
	THashTable = Array[THashTableIndex] OF PNode;
(*CreateNode*)
FUNCTION CreateNode(data: String) : PNode;
Var
	node: PNode;
BEGIN
	New(node);
	node^.data :=data;
	node^.next := NIL;
	CreateNode := node;
END; (* CreateNode *)

(*ComputeHash*)
FUNCTION ComputeHash(data: String) : THashTableIndex;
Var
	hash: LongInt;
	i: Byte;
BEGIN
	hash:=0;
	FOR i:=1 TO Length(data) DO BEGIN
		hash:= (hash+ ORD(data[i])) MOD
			(High(THashTableIndex) - Low(THashTableIndex)+1);
	END;
	ComputeHash := hash + Low(THashTableIndex);
END; (* ComputeHash *)
(*InitHashTable*)
PROCEDURE InitHashTable(VAR ht: THashTable);
VAR
	i: THashTableIndex;
BEGIN
	FOR i:= Low(THashTableIndex) TO High(THashTableIndex) DO BEGIN
		ht[i] := NIL;	
	END;

END; (* InitHashTable *)

(*DisplayHashTable*)
PROCEDURE DisplayHashTable(ht: THashTable);
Var
	i: THashTableIndex;
BEGIN
	FOR i:=Low(THashTableIndex) TO High(THashTableIndex) DO BEGIN
		Write(i, ': ');
		WHILE (ht[i] <> NIL)  DO BEGIN
			Write('-->', ht[i]^.data);
			ht[i] := ht[i]^.next;
		END;
		Write('--|');
		WriteLn;
	END;
END; (* DisplayHashTable *)

(*DisposeHashTable*)
PROCEDURE DisposeHashTable(Var ht: THashTable);
Var
	i: THashTableIndex;
	node : PNode;
BEGIN
	FOR i:= Low(THashTableIndex) TO High(THashTableIndex) DO BEGIN
		WHILE ht[i] <> NIL  DO BEGIN
			node := ht[i]^.next;
			Dispose(ht[i]);
			ht[i] := node;
		END;
	END;
END; (* DisposeHashTable *)

(*AddNodeToHashTable*)
PROCEDURE AddNodeToHashTable(VAR ht: THashTable; node: PNode);
Var
	hash : THashTableIndex;
BEGIN
	IF node <> NIL  THEN BEGIN
		hash := ComputeHash(node^.data);
		IF ht[hash] = NIL  THEN BEGIN
			ht[hash] := node
		END
		ELSE BEGIN
			node^.next := ht[hash];
			ht[hash] := node;
		END;
	END;
END; (* AddNodeToHashTable *)

(*ContainsValue*)
FUNCTION ContainsValue(ht: THashTable; data:String) : Boolean;
VAR
	node: PNode;
	found: BOOLEAN;
BEGIN
	node := ht[ComputeHash(data)];
	found := false;
	WHILE (NOT found) and (node <> NIL)  DO BEGIN
		found := node^.data = data;
		node := node^.next;	
	END;
	ContainsValue := found;
END; (* ContainsValue *)

Var
	hashTable: THashTable;
	data: String;
	hash : THashTableIndex;
	node: PNode;
(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	InitHashTable(hashTable);
	Write('Add: ');
	ReadLn(data);
	WHILE data <> ''  DO BEGIN
		node := CreateNode(data);
		hash := ComputeHash(node^.data);
		WriteLn('Hash: ', hash);
		AddNodeToHashTable(hashTable,node);
		WriteLn('Add: ');
		ReadLn(data);
	END;

	WriteLn;
	DisplayHashTable(hashTable);
	WRite('Search for: ');
	ReadLn(data);
	WHILE data <> ''  DO BEGIN
		WriteLn(ContainsValue(hashTable,data));
		Write('Search for: ');
		ReadLn(data);
	END;

	DisposeHashTable(hashTable);
END. (* Hashing *)