UNIT PMBib44;

INTERFACE 

TYPE 
	TPmStatistics = RECORD
		comps: LONGINT;
	END;

PROCEDURE BruteSearchLR(s, p: STRING; VAR pos: INTEGER; VAR ismatch : Boolean);
PROCEDURE RabinKarp(s,p : String; VAR pos : Integer; VAR ismatch : Boolean);
PROCEDURE GetPMStatistics(VAR stat: TPmStatistics);
PROCEDURE InitPMStatistics;

IMPLEMENTATION
CONST 
	MAX_STR_LENGTH = 255;
	ASCII_TABLE_SIZE = 128;
VAR 
	ps: TPmStatistics;

PROCEDURE InitPMStatistics;
BEGIN
	WITH ps DO BEGIN
		comps := 0;
	END;
END;

PROCEDURE GetPMStatistics(VAR stat: TPmStatistics);
BEGIN
	stat := ps;
END;

FUNCTION EQ(ch1, ch2: CHAR): BOOLEAN;
BEGIN
	EQ := (ch1 = ch2);
	Inc(ps.comps);
END;

FUNCTION EQArr(ch1: CHAR;Var valArr : Array of Integer): BOOLEAN;
BEGIN
	IF valArr[Ord(ch1)] > 0  THEN BEGIN
		Dec(valArr[Ord(ch1)]);
		EQArr := true;
	END
	ELSE
		EQArr :=false;
	//EQ := (ch1 = ch2);
	Inc(ps.comps);
END;
PROCEDURE BruteSearchLR(s, p: STRING; VAR pos: INTEGER; VAR ismatch : Boolean);
VAR 
	sLen, pLen, i, j,l: INTEGER;
	valArr : Array[ASCII_TABLE_SIZE] of Integer;
	match : boolean;
BEGIN
	sLen := Length(s);
	pLen := Length(p);
	
	ismatch:=false;
	pos := 0;
	i := 1;
	
	WHILE (pos = 0) AND (i + pLen-1 <= sLen) DO BEGIN
		j := 1;		

		(*Init PAttern Array*)
		FOR l:=Low(valArr) TO High(valArr) DO BEGIN
			valArr[l] := 0;
		END;
		(*Pattern to Array*)
		FOR l:=0 TO pLen DO BEGIN
			valArr[Ord(p[l+1])] += 1;
		END;

		WHILE (j <= pLen) AND (EQArr(s[i+j-1], valArr)) DO BEGIN
			Inc(j);
		END;

		IF j > pLen THEN BEGIN
			pos := i;
		END
		ELSE BEGIN
			Inc(i);
		END;
	END;
END;
(*Describe me*)
PROCEDURE RabinKarp(s,p : String; VAR pos : Integer; VAR ismatch : Boolean);
CONST
	q = 8355967; (*Long Random prime*)
	r = 256;
VAR
	hp : LongInt;
	hs : LongInt;
	rm : LongInt;

	sLen,pLen, i,j : Integer;
	iMax : Integer;
	sPos : Integer;
BEGIN
	sLen := Length(s);
	pLen := Length(p);
	rm := 1;
	pos := 0;

	FOR i := 1 TO pLen -1 DO BEGIN
		rm:=(r*rm) MOD q;
	END;
	 hp := 0;
	 hs := 0;

	 FOR i := 1 TO pLen DO BEGIN
	 	hp := (r*hp + ORD(p[i])) MOD q;
		hs := (r*hs + ORD(s[i])) MOD q;
	 END;
	
	 i := 1;
	 j := 1;
	 iMax := sLen - pLen + 1;

	 
	 WHILE (i <= iMax) AND (j <= pLen)  DO BEGIN
	 	IF hp = hs  THEN BEGIN
			j := 1;
			sPos := i;
			WHILE (j <= pLen) and EQ(p[j], s[sPos])  DO BEGIN
				Inc(j);
				Inc(sPos);
			END;
		END;
		 hs := (hs +q -rm * ORD(s[i]) MOD q) MOD q;
	 	 hs := (hs * r + Ord(s[i+pLen])) MOD q;
		Inc(i);
	 END;
	 IF (i > iMax) AND (j <= pLen)  THEN BEGIN
	 	pos := 0;
	 END
	 ELSE BEGIN
	 	pos := i-1;
	 END;
END; (* RabinKarp *)

BEGIN
END.