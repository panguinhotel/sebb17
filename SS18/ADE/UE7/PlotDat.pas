(* PlotDat:                              			  	  GHO, 28.05.2010
   --------
   Module with data structures for plotter application.
==========================================================================*)
UNIT PlotDat;

INTERFACE

TYPE
	ActionKind = (Move, Draw);

    ActionNode = ^ActionNodeRec;
    ActionNodeRec = RECORD
        x, y: INTEGER;
        kind: ActionKind;
    	next: ActionNode;
    END;
    ActionList = ActionNode;

FUNCTION NewActionNode(x, y: INTEGER; kind: ActionKind): ActionNode;
PROCEDURE AppendToActionList(x,y : INTEGER; kind: ActionKind; VAR list : ActionList);

IMPLEMENTATION

FUNCTION NewActionNode(x, y: INTEGER; kind: ActionKind): ActionNode;
VAR
	n: ActionNode;
BEGIN
	New(n);
    n^.x := x;
    n^.y := y;
    n^.kind := kind;
    n^.next := NIL;
    NewActionNode := n;
END;

PROCEDURE AppendToActionList(x,y : INTEGER; kind: ActionKind; VAR list : ActionList);
VAR
    n: ActionList;
BEGIN
    
    IF list = NIL THEN BEGIN
        list := NewActionNode(x,y,kind);
    END ELSE BEGIN      
        n:= list;
        WHILE n^.next <> NIL  DO BEGIN
            n:= n^.next;
        END;
        n^.next := NewActionNode(x,y,kind);
    END;
END;

END.