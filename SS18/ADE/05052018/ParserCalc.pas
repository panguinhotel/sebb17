unit ParserCalc;
	
interface
	PROCEDURE InitParser(inp : String);
	FUNCTION Parse : Boolean;
	FUNCTION ErrorPos : Integer;
	FUNCTION GetResult : Double;
	
implementation
	USES
		Scanner;
	
	VAR
		success : Boolean;
		result : Double;

	PROCEDURE Expr(Var e:Double); FORWARD;
	PROCEDURE Term(Var t:Double); FORWARD;
	PROCEDURE Fact(Var f:Double); FORWARD;

	PROCEDURE Expr(Var e:Double);
	{local} VAR temp: Double; {endlocal}
	BEGIN
		Term(e); IF NOT success THEN EXIT;
		WHILE (CurrentSymbol = plusSym)
			OR (CurrentSymbol = minusSym)  DO BEGIN
			CASE CurrentSymbol OF 
				plusSym : BEGIN
					Write('+');
					GetNextSymbol;
					Term(temp); IF NOT success THEN EXIT;
					{sem} e := e+temp; {endsem}
				END;
				minusSym : BEGIN
					Write('-');
					GetNextSymbol;
					Term(temp); IF NOT success THEN EXIT;
					{sem} e := e-temp; {endsem}
				END;
			END;
		END;
	END;

	PROCEDURE Term(Var t:Double);
	{local} VAR temp: Double; {endlocal}
	BEGIN
      Fact(t); IF NOT success THEN exit;
      WHILE (CurrentSymbol = multSym) OR (CurrentSymbol = divSym) DO BEGIN
        CASE CurrentSymbol OF
          multSym : BEGIN
		  	Write('*');
            GetNextSymbol;
            Fact(temp); IF NOT success THEN exit;
			{sem} t := t*temp; {endsem}
          END;
          divSym : BEGIN
		  	Write('/');
            GetNextSymbol;
            Fact(temp); IF NOT success THEN exit;
			{sem}
			IF temp = 0 THEN BEGIN
				WriteLn('Division by zero!');
				success := false;
				EXIT;
			END;
			t := t / temp; {endsem}
          END;
        END;
      END;    
    END;

	PROCEDURE Fact(Var f:Double);
	BEGIN
		CASE CurrentSymbol OF
			numberSym: begin
				{sem} f:= CurrentNumberValue; {endsem}
				GetNextSymbol;
			end;
			leftParSym: begin
				GetNextSymbol;
				Expr(f); IF NOT success Then EXIT;
				IF CurrentSymbol <> rightParSym Then begin
					success := FALSE; EXIT;
				END;
			END;
			ELSE BEGIN
				success := FALSE; EXIT;
			END;
		END;
	END;

	PROCEDURE InitParser(inp : String);
	BEGIN
		InitScanner(inp);
		success := false;
		result:= 0;
	END;

	FUNCTION Parse : Boolean;
	BEGIN
		success := TRUE;
		Expr(result);
		success := success and (CurrentSymbol = endSym);
		Parse :=success;
	END;

	FUNCTION ErrorPos : Integer;
	BEGIN
		ErrorPos := CurrentSymbolPos;
	END;

	FUNCTION GetResult : Double;
	BEGIN
		GetResult := result;
	END;
end.