--UE3-2.1
SELECT * FROM
(Select f.Title,r.rental_date as Verleihdatum,
  DENSE_RANK() OVER (order by r.rental_date) as Datumsrang
from film f
join inventory i on i.film_id = f.film_id
join rental r on r.inventory_id = i.inventory_id
order by Datumsrang)
WHERE ROWNUM <= 10 OR Verleihdatum = 
(SELECT tmp1 FROM(
Select ROWNUM as "NUM",rental_date as tmp1 FROM
(SELECT t.rental_date from rental t order by t.rental_date))
WHERE NUM = 10);

--UE3-2.2
Select Kategorie,Filmtitel,Erscheinungsdatum
FROM(
Select c.name as Kategorie,f.Title as Filmtitel,f.release_year as Erscheinungsdatum,
  ROW_NUMBER() OVER (PARTITION BY c.name order by f.release_year desc) as KategorieRang
from category c
join film_category fc on c.category_id = fc.category_id
join film f on f.film_id = fc.film_id)
WHERE KategorieRang <= 3;

--UE3-2.3

Select Name, Round(AVG(newRD-oldRD),0) as AvgLeihdauer
from(
Select c.first_name||' '||c.last_name as Name,r.rental_date as newRD,
  LAG(r.rental_date) OVER (Partition by c.customer_id order by r.rental_date) as oldRD
FROM customer c
join rental r on (r.customer_id = c.customer_id))
where oldRD is not null
group by Name
order by Name;
