///*
// * skiplist.cpp
// *
// *  Created on: 15.03.2019
// *      Author: schau
// */
//
//#include "skiplist.h"
//
//
////template<class T,int MAXSIZE>
////skip_list<T,MAXSIZE>::skip_list() {
////
////	T headKey = std::numeric_limits<T>::min();
////	m_head = new skip_node<T>(headKey, {}, MAXSIZE);
////
////	T tailKey = std::numeric_limits<T>::max();
////	m_tail = new skip_node<T>(tailKey, {}, MAXSIZE);
////
////	std::fill(begin(m_head->forward), end(m_head->forward), m_tail);
////
////}
////
////template<class T,int MAXSIZE>
////skip_list<T,MAXSIZE>::~skip_list() {
////	auto* node = m_head;
////	while (node->forward.at(0))
////	{
////		auto* tmp = node;
////		node = node->forward.at(0);
////		delete tmp;
////	}
////	delete node;
////}
//
//template<class T, int MAXSIZE>
//void skip_list<T,MAXSIZE>::init(int probability)
//{
//	m_probability = probability;
//}
//
//template<class T, int MAXSIZE>
//skip_list<T,MAXSIZE>::skip_node<T>* skip_list<T,MAXSIZE>::find(T key)
//{
//	skip_node<T>* result = nullptr;
//	if (auto x = lower_bound(key))
//	{
//		if(x->key == key && x != m_tail)
//			result = x;
//	}
//	return result;
//}
//
//template<class T, int MAXSIZE>
//void skip_list<T,MAXSIZE>::insert(T key, T value)
//{
//	auto pred = predecessors(key);
//	const int nextLevel = random_level();
//	auto * node = make_node(key,value,nextLevel);
//
//	for(int i = 0; i < nextLevel; ++i)
//	{
//		node->forward[i] = pred.at(i).forward.at(i);
//		pred[i]->forward[i] = node;
//	}
//}
//
//template<class T, int MAXSIZE>
//void skip_list<T,MAXSIZE>::erase(T key)
//{
//	auto pred = predecessors(key);
//	auto *node = pred.at(0)->forward.at(0);
//	if(node->key != key || node == m_tail)
//		return;
//
//	for(auto i = 0; i < node_level(node); ++i)
//	{
//		pred[i].forward[i] = node->forward.at(i);
//	}
//	delete node;
//}
//
//template<class T, int MAXSIZE>
//int skip_list<T,MAXSIZE>::random_level() const
//{
//	int tmp = 1;
//	while ((static_cast<double>(std::rand()) /RAND_MAX) < m_probability && tmp < m_max_level)
//	{
//		tmp++;
//	}
//	return tmp;
//}
//
//template<class T, int MAXSIZE>
//int skip_list<T,MAXSIZE>::node_level(const skip_node<T> * value)
//{
//	return value->forward.size();
//}
//
//template<class T, int MAXSIZE>
//skip_list<T,MAXSIZE>::skip_node<T>* skip_list<T,MAXSIZE>::lower_bound(T key) const
//{
//	auto *node = m_head;
//	for(auto i = node_level(m_head); i > 0 ; i--)
//	{
//		while(node->forward.at(i)->key < key)
//			node = node->forward.at(i);
//	}
//	return node->forward.at(0);
//}
//
//template<class T, int MAXSIZE>
//skip_list<T,MAXSIZE>::skip_node<T>* skip_list<T,MAXSIZE>::make_node(T key, T value,int level)
//{
//	return new skip_node<T>(key,value,level);
//}
//
