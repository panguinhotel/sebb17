#ifndef WAREHOUSE_H
#define WAREHOUSE_H

#define MAXSIZE 10

class Warehouse {
private:
	int  maxSize = MAXSIZE;
	int  products{0};
public:
	Warehouse();
	~Warehouse() = default;
	void addProduct();
	void buyProduct();
	bool anythingToBuy();
	bool isFull();
	int  countProducts();
};

#endif
