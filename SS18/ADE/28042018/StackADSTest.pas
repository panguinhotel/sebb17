PROGRAM StackADSTest;

USES
	StackADS2;
VAR
	i: Int64;
	ok:Boolean;
	val : Integer;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	i:=1;
	Init;
	repeat
		Push(i*i,ok);
		i := i+1;
	until (i> 100000000 ) or Not ok;

	IF Not ok Then begin
		WriteLn('Fehler!');
	end;
	
	repeat
		Pop(val, ok);
		WriteLn('popped #', val);
	until (IsEmpty or Not ok);

	IF Not ok Then begin
		WriteLn('Fehler!');
	end;

	Pop(val,ok);
	WriteLn(ok);

	Push(99,ok);
	WriteLn(ok);

	Pop(val,ok);
	WriteLn(ok);
	WriteLn(val);

	Done;
END. (* StackADSTest *)