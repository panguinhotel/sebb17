<?php
   include('../session.php');
   include('../config.php');

   if($_SERVER["REQUEST_METHOD"] == "POST") {
	$katname = mysqli_real_escape_string($db,$_POST['katname']);

	$sql = "Delete from buch_type where fk_id_user = $login_userID and name = '$katname'";
	$result = mysqli_query($db,$sql);

	if($result) {
		echo '<script language="javascript">';
		echo 'alert("Löschen erfolgreich")';
		echo '</script>';
	}else {
	  	echo '<script language="javascript">';
	  	echo 'alert("Löschen fehlgeschlagen")';
	  	echo '</script>';
	}
}
?>
<html>  
   <head>
    <title>Buchtyp löschen</title>
          <link rel="stylesheet" type="text/css" href="../style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
   </head>
   
   <body>
   		<h1 class="headLine">Buchtyp löschen</h1>
   		<div class="mainList">
			<button id="autorNameBtn"  onclick="myFunction()" class="mainButton">< Select Buchtyp ></button>
			<div class="dropdown mainList">
			<div id="myDropdown" class="dropdown-content">
				<input type="text" class="searchInput" placeholder="Search.." id="myInput" onkeyup="filterFunction()">
				<?php
					$sql = "SELECT id_buchtype, name FROM buch_type where fk_id_user = $login_userID;";
					$result = mysqli_query($db,$sql);
					while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
						echo "<a class=\"ddItem\" href=\"#\" >".$row['name']."</a>";
					}
				?> 
			</div>
			</div>
			<div id ="updateDiv" class="updateDivClass">
				<form action = "" method = "post" >
				<input id="inp_authname" class="loginInput" type="text" placeholder="" name="katname" required><br>
				<input class="deleteButton" type="submit" value="Löschen"><br>
				</form>
			</div>
			<button class="backbutton" onclick="window.location.href='../books.php'">Zurück</button>
		</div>
		
		
	<script>
	function myFunction() {
		document.getElementById("myDropdown").classList.toggle("show");
		var x = document.getElementsByClassName("ddItem");

		for(var i = 0; i < x.length; i++)
		{
			x[i].addEventListener("click", setName);
		}
	}

	function filterFunction() {
		var input, filter, ul, li, a, i;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		div = document.getElementById("myDropdown");
		a = div.getElementsByTagName("a");
		for (i = 0; i < a.length; i++) {
			txtValue = a[i].textContent || a[i].innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
			a[i].style.display = "";
			} else {
			a[i].style.display = "none";
			}
		}
	}

	function setName(event){
		var clickedElement = event.target;
		document.getElementById("inp_authname").value = clickedElement.innerHTML;
		document.getElementById("myDropdown").classList.toggle("show");
		document.getElementById("updateDiv").style.display = "block"; 

	}
	</script>
   </body> 
</html>