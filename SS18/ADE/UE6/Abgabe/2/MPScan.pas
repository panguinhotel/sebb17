UNIT MPScan;
INTERFACE
	TYPE 
		Symbol = (noSym,beginSym, endSym, integerSym, programSym, readSym, 
			varSym, writeSym,ifSym, thenSym, elseSym, whileSym, doSym,
			plusSym, minusSym, multSym, divSym, leftParSym, rightParSym,
            commaSym, colonSym, assignSym, semicolonSym, periodSym,identSym, numberSym);

PROCEDURE InitScanner(VAR inputFile: TEXT);
PROCEDURE GetNextSymbol;
FUNCTION CurrentSymbol : Symbol;
FUNCTION CurrentIdentName : STRING;
FUNCTION CurrentNumberValue : INTEGER;
PROCEDURE GetCurrentSymbolPosition(VAR line, column: INTEGER);

Implementation
	Const
		TAB = Chr(9);
		LF = Chr(10);
		CR = Chr(13);
		BLANK = ' ';
	VAR
		inpFile : TEXT;
		curChar : Char;
		charLine, charColumn : Integer;
		curSymbol : Symbol;
		symbolLine, symbolColumn : Integer;
		curIdentName : String;
		curNumberValue: Integer;
PROCEDURE GetNextChar;
begin
	Read(inpFile,curChar);

	IF (curChar = CR) OR (curChar = LF) THEN BEGIN
        IF (curChar = LF) THEN
            charLine := charLine + 1;

        charColumn := 0;
        curChar := BLANK;
    END ELSE
        charColumn := charColumn + 1;
END;

PROCEDURE InitScanner(VAR inputFile: TEXT);
begin
	inpFile := inputFile;
	charLine := 1;
	charColumn := 0;
	GetNextChar;
	GetNextSymbol;
end;

PROCEDURE GetNextSymbol;
begin
	While (curChar = BLANK) OR (curChar = TAB) DO 
		GetNextChar;

	symbolLine := charLine;
	symbolColumn := charColumn;

	CASE curChar OF
	  	'+': BEGIN curSymbol := plusSym; GetNextChar; END;
        '-': BEGIN curSymbol := minusSym; GetNextChar; END;
        '*': BEGIN curSymbol := multSym; GetNextChar; END;
        '/': BEGIN curSymbol := divSym; GetNextChar; END;
        '(': BEGIN curSymbol := leftParSym; GetNextChar; END;
        ')': BEGIN curSymbol := rightParSym; GetNextChar; END;
        '.': BEGIN curSymbol := periodSym; GetNextChar; END;
        ',': BEGIN curSymbol := commaSym; GetNextChar; END;
        ';': BEGIN curSymbol := semicolonSym; GetNextChar; END;
        ':': BEGIN
                GetNextChar;

                IF curChar = '=' THEN BEGIN
                    curSymbol := assignSym;
                    GetNextChar;
                END ELSE BEGIN
                    curSymbol := colonSym;
                END;
             END;
		'a'..'z','A'..'Z','_' : BEGIN
			curIdentName := '';
			WHILE curChar in ['a'..'z','A'..'Z','_'] DO BEGIN
				curIdentName := curIdentName + UpCase(curChar);
				GetNextChar;
			END;
			IF(curIdentName = 'BEGIN') THEN
                    curSymbol := beginSym
                ELSE IF(curIdentName = 'END') THEN
                    curSymbol := endSym
                ELSE IF(curIdentName = 'INTEGER') THEN
                    curSymbol := integerSym
                ELSE IF(curIdentName = 'PROGRAM') THEN
                    curSymbol := programSym
                ELSE IF(curIdentName = 'READ') THEN
                    curSymbol := readSym
                ELSE IF(curIdentName = 'VAR') THEN
                    curSymbol := varSym
                ELSE IF(curIdentName = 'WRITE') THEN
                    curSymbol := writeSym
                ELSE IF(curIdentName = 'IF') THEN
                    curSymbol := ifSym
                ELSE IF(curIdentName = 'THEN') THEN
                    curSymbol := thenSym
                ELSE IF(curIdentName = 'ELSE') THEN
                    curSymbol := elseSym
                ELSE IF(curIdentName = 'WHILE') THEN
                    curSymbol := whileSym
                ELSE IF(curIdentName = 'DO') THEN
                    curSymbol := doSym
                ELSE
                    curSymbol := identSym;
		END;
		'0'..'9': BEGIN
            curSymbol := numberSym;
            curNumberValue := 0;
                
            WHILE curChar IN ['0'..'9'] DO BEGIN
                curNumberValue := curNumberValue * 10 + Ord(curChar) - Ord('0');
                GetNextChar;
            END;
        END;
        ELSE BEGIN curSymbol := noSym; GetNextChar; END;
	END;

end;

FUNCTION CurrentSymbol : Symbol;
begin
	CurrentSymbol := curSymbol;
end;

FUNCTION CurrentIdentName : STRING;
begin
	 CurrentIdentName := curIdentName;
end;

FUNCTION CurrentNumberValue : INTEGER;
begin
	CurrentNumberValue := curNumberValue;
end;

PROCEDURE GetCurrentSymbolPosition(VAR line, column: INTEGER);
begin
	line := symbolLine;
    column := symbolColumn;
end;
end.