class MemeEditor {

    constructor() {
    }

    init() {
        $.getJSON("/api/templates", function (templates) {
            this.templates = templates;
            this.template = templates[0];
            this.viewer = null;
            this.renderUi();
        }.bind(this));
    }

    renderUi() {
        
        $('#templates').empty();
        for (let idx = 0; idx < this.templates.length; idx++) {
            $('#templates').append(`
            <button type="button" id="template-${idx}" data-idx="${idx}" class="template-img">
                <img src="${this.templates[idx].image.path}" height="50">
            </button>`);
        }

        $('#textParts').empty();
        for (let idx = 0; idx < this.template.textParts.length; idx++) {
            $('#templates').append(`
            <div class="form-group">
                <span id="deleteBtn${idx}" data-idx="${idx}" class="deleteBtn" aria-hidden="true"></span>
                <label for="exampleInputEmail1">Text ${idx + 1}:</label>
                <textarea id="input${idx}" data-idx="${idx}" class="textInput form-control">${this.template.textParts[idx].text}</textarea>
            </div>`);
        }

        this.viewer = new MemeViewer(document.getElementById("meme-view"));
        this.viewer.loadTemplate(this.template);

        this.bindListner();
    }

    bindListner() {

        $(".textInput").unbind();
        $(".deleteBtn").unbind();
        $("#addText").unbind();
        $('#fontSize').unbind();
        $('#fontSize').unbind();
        $('#downloadMeme').unbind();
        $('#saveMeme').unbind();
        $(".template-img").unbind();

        $(".textInput").on("input", this.onInputText.bind(this));
        $(".deleteBtn").click(this.clickRemoveText.bind(this));
        $("#addText").click(this.clickAddText.bind(this));
        $('#fontSize').val(this.template.fontSize);
        $('#fontSize').on('change', this.changeFontSize.bind(this));
        $('#downloadMeme').click(this.clickDownloadMeme.bind(this));
        $('#saveMeme').click(this.clickSaveMeme.bind(this));
        $(".template-img").click(this.clickChangeTemplate.bind(this));
    }

    clickAddText(e) {
        e.preventDefault();

        this.template.textParts.push({
            x: 20,
            y: 20,
            text: "New Text"
        });
        this.renderUi();
    }

    clickDownloadMeme() {
        var dt = this.viewer.generateDataUrl();
        document.getElementById("downloadMeme").href = dt.replace(/^data:image\/[^;]*/, 'data:application/octet-stream');
    }

    clickSaveMeme() {
        var dt = this.viewer.generateDataUrl();
        $.post("/api/memes", {
            data: dt.split(",")[1],
            creator: $('#author').val()
        }, function (data) {
            console.log(data);
            window.location.href = "/";
        });
    }

    clickChangeTemplate(e) {
        this.template = this.templates[e.currentTarget.dataset.idx];
        this.renderUi();
    }

    changeFontSize(e) {
        this.template.fontSize = e.currentTarget.value;
        this.viewer.render();
    }

    onInputText(e) {
        if (e.currentTarget.dataset.idx != undefined) {
            this.template.textParts[e.currentTarget.dataset.idx].text = e.currentTarget.value;
            this.viewer.render();
        }
    }

    clickRemoveText(e) {
        let idx = e.currentTarget.dataset.idx;
        this.template.textParts.splice(idx, idx + 1);
        this.updateUi();
    }
}