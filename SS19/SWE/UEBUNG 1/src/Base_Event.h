/*
 * Base_Event.h
 *
 *  Created on: 06.03.2019
 *      Author: Lukas Schaubmayr
 */

#ifndef BASE_Base_Event_H_
#define BASE_Base_Event_H_


#include <stdlib.h>
#include <vector>
#include <queue>

class Event_compare;
class Base_Event {
public:
	Base_Event();
	virtual ~Base_Event();

	inline void set_start_delay(int delay){m_start_delay = delay;}
	inline int get_start_delay() const {return m_start_delay;}
	inline void postpone(int ticks){m_start_delay+=ticks;}

	virtual void add_child_event(Base_Event* event);

	virtual void register_child_events(std::priority_queue<Base_Event*,std::vector<Base_Event*>,Event_compare>  *qu);
private:
	int m_start_delay = -1;
	std::priority_queue<Base_Event*> m_child_events;

};

class Event_compare
{
public:
	bool operator() (Base_Event *a, Base_Event *b) const
	{
		return a->get_start_delay() > b->get_start_delay();
	}
};


#endif /* BASE_Base_Event_H_ */
