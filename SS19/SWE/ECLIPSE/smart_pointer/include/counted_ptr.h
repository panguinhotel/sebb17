// file: include/counted_ptr.h
#pragma once

#include <cassert>
#include <utility>

template <typename T>
class counted_ptr {
public:
	using value_type = T;

	explicit counted_ptr(T *ptr = nullptr) {
		if (ptr) {
			ref_cnt = new counter(ptr);
		}
	}

	counted_ptr(const counted_ptr &p) {
		acquire(p.ref_cnt);
	}

	counted_ptr(counted_ptr &&p) {
		using std::swap;
		swap(ref_cnt, p.ref_cnt);
	}

	~counted_ptr() { release(); }

	counted_ptr& operator = (const counted_ptr &p) {
		if (this != &p) {
			release();
			acquire(p.ref_cnt);
		}
		return *this;
	}
	counted_ptr& operator = (counted_ptr &&p) {
		release();
		using std::swap;
		swap(ref_cnt, p.ref_cnt);
		return *this;
	}

	T& operator * () {
		assert(ref_cnt);
		return *ref_cnt->ptr;
	}

	const T& operator * () const {
		assert(ref_cnt);
		return *ref_cnt->ptr;
	}

	T* operator -> () {
		assert(ref_cnt);
		return ref_cnt->ptr;
	}

private:
	struct counter {
		T *ptr { nullptr };
		unsigned int count { 1 };
		counter(T *ptr) : ptr(ptr) {}
		friend void swap(counter &a, counter &b) {
			using std::swap;
			swap(a.ptr, b.ptr);
			swap(a.count, b.count);
		}
	};

	counter *ref_cnt { nullptr };

	void acquire(counter *c) {
		assert(ref_cnt == nullptr);
		ref_cnt = c;
		if (c) {
			c->count++;
		}
	}

	void release() {
		if (ref_cnt == nullptr) return;
		ref_cnt->count--;
		if (ref_cnt->count == 0) {
			delete ref_cnt->ptr;
			delete ref_cnt;
		}
		ref_cnt = nullptr;
	}

	template <typename U>
	friend void swap(counted_ptr<U> &a, counted_ptr<U> &b) {
		using std::swap;
		swap(a.ref_cnt, b.ref_cnt);
	}
};

template <typename T, typename... Args>
counted_ptr<T> make_counted(Args&&... args) {
	return counted_ptr<T>(new T(std::forward<Args>(args)...));
}
