#include <stdio.h>

int main() {
	const double G = 6.67384e-11;

	printf("G = %g\n",G);
	printf("address of G = %p\n",(void*)&G);
	double *p = &G;
	*p = 6.0;
	printf("G=%g\n",G);
	return 0;
}