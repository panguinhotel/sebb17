unit StackADT2a;
	
interface

	Type 
		Stack = Pointer;
	
	(*Initializes the stack *)
	(*In/Out s: the Stack that should be initialized*)
	Procedure Init(VAR s: Stack);

	(* resets the stack. Stack can be reused afterwards *)
	Procedure Done(VAR s: Stack);

	(* Pushes a value onto the stack*)
	(* In value : value that will be pushed on the stack*)
	(* In/Out s: the Stack that should be used*)
	(* Out ok : False in case of any errors; TRUE otherwise
		IF false, the value will has not been pushed onto the stack *)
	PROCEDURE Push(value:Integer; Var ok : Boolean;VAR s: Stack);

	(* Pops the most recently pushed value from the stack *)
	(* Out value : value popped from the stack. *)
	(* In/Out s: the Stack that should be used*)
	(* Out ok : False in case of any errors (e.g. stack is empty)
		In this case, the returned value mustn't be used *)
	PROCEDURE Pop(Var value:Integer; Var ok : Boolean;VAR s: Stack);

	(* Checks if stack is currently empty*)
	(* In/Out s: the Stack that should be used*)
	(* Returns : True if stack is empty; False otherwise*)
	Function IsEmpty(Var s: Stack): Boolean;

implementation

	TYPE
		Node = ^NodeRec;
		NodeRec = Record
			value : Integer;
			next : Node;
		END;
		State  = ^StateRec;
		StateRec = Record
			top:Node;
		END;


	Procedure Init(VAR s: Stack);
	VAR
		st : State;
	BEGIN
		New(st);
		st^.top:= NIL;
		s:= st;
	END; (* Init *)

	Procedure Done(VAR s: Stack);
	Var
		next : Node;
	BEGIN
		While State(s)^.top <> NIL Do Begin
			next := State(s)^.top^.next;
			Dispose(State(s)^.top);
			State(s)^.top := next;
		End;
		Dispose(State(s));
		State(s)^.top := NIL;
	END; (* Done *)

	PROCEDURE Push(value:Integer; Var ok : Boolean;VAR s: Stack);
	Var
		n : Node;
	BEGIN
		New(n);
		ok := n <> NIL;
		IF ok Then Begin
			n^.value := value;
			n^.next := State(s)^.top;
			State(s)^.top := n;
		END;
	END; (* Push *)

	PROCEDURE Pop(Var value:Integer; Var ok : Boolean;VAR s: Stack);
	Var
		n: Node;
	BEGIN
		IF not IsEmpty(s) Then Begin
			value := State(s)^.top^.value;
			n := State(s)^.top^.next;
			Dispose(State(s)^.top);
			State(s)^.top := n;
			ok := true;
		End ELSE BEGIN
			ok := False;
		END;
	END; (* Pop *)

	Function IsEmpty(Var s: Stack): Boolean;
	Begin
		IsEmpty := State(s)^.top = NIL;
	END;
end.