UNIT PMBib2;

INTERFACE 

TYPE 
	TPmStatistics = RECORD
		comps: LONGINT;
	END;

PROCEDURE BruteSearchLR(s,p : String; VAR pos : Integer; VAR ismatch : Boolean);
PROCEDURE GetPMStatistics(VAR stat: TPmStatistics);
PROCEDURE InitPMStatistics;

IMPLEMENTATION
CONST 
	MAX_STR_LENGTH = 255;
VAR 
	ps: TPmStatistics;

PROCEDURE InitPMStatistics;
BEGIN
	WITH ps DO BEGIN
		comps := 0;
	END;
END;

PROCEDURE GetPMStatistics(VAR stat: TPmStatistics);
BEGIN
	stat := ps;
END;

FUNCTION EQ(ch1, ch2: CHAR): BOOLEAN;
BEGIN
	IF ch2 = '?'  THEN BEGIN
		EQ := true;				
	END
	ELSE
		EQ := (ch1 = ch2);

	Inc(ps.comps);
END;


FUNCTION IsMatching(p,s: string) : BOOLEAN;
VAR
	firstCharMatch : BOOLEAN;
	pLen,sLen : Integer;
	nextS, nextP : String;
	i : Integer;
	nextMatchIDX,nextCharIdxonP : Integer;
BEGIN
	pLen := Length(p);
	sLen := Length(s);

	nextS := '';
	nextP := '';

	IF p[1] = '$'  THEN BEGIN
		IsMatching := s[1] = '$';
	END
	ELSE IF p[1] = '*'  THEN BEGIN
		IF p[2] = '$'  THEN BEGIN
			IsMatching:=true;
		END
		ELSE BEGIN
			nextMatchIDX := -1;
			nextCharIdxonP:= -1;
			i:= 1;
			WHILE (i<pLen) and (nextCharIdxonP = -1)  DO BEGIN
				IF p[i] <> '*'  THEN BEGIN
					nextCharIdxonP := i;
				END;
				Inc(i);
			END;
			i:= 1;

			WHILE (i < sLen) and (nextMatchIDX = -1)  DO BEGIN
				if(p[nextCharIdxonP] = s[i]) Then BEGIN
					nextMatchIDX := i;
				END;
				Inc(i);
			END;
			IF nextMatchIDX <> -1  THEN BEGIN
				Setlength(nextS,sLen- nextMatchIDX+1);
				FOR i:=nextMatchIDX TO sLen DO BEGIN
					nextS[i-nextMatchIDX+1] := s[i];
				END;
				Setlength(nextP,pLen- nextCharIdxonP+1);
				FOR i:=nextCharIdxonP TO pLen DO BEGIN
					nextP[i-nextCharIdxonP+1] := p[i];
				END;
				IsMatching := true and IsMatching(nextP,nextS);
			END
			ELSE IsMatching := false;
		END;
	END
	ELSE IF (s[1] = '$') Then BEGIN
		IsMatching := false;
	END
	ELSE IF (p[1] = s[1]) or (p[1] = '?')  THEN BEGIN
		Setlength(nextS,sLen- 1);
		FOR i:=2 TO sLen DO BEGIN
			nextS[i-2+1] := s[i];
		END;
		Setlength(nextP,pLen- 1);
		FOR i:=2 TO pLen DO BEGIN
			nextP[i-2+1] := p[i];
		END;

		IsMatching := true and IsMatching(nextP,nextS);
	END
	ELSE IsMatching := false;

END; (* IsMatching *)

PROCEDURE BruteSearchLR(s, p: STRING; VAR pos: INTEGER; VAR ismatch: Boolean);
VAR 
	sLen, i,l: INTEGER;
	nextS, nextP : String;
BEGIN
	sLen := Length(s);
	ismatch:= false;

	pos := 0;
	i := 1;

	nextS := s;

	IF (sLen = 1) and (s[1] = '$')  THEN BEGIN
		ismatch := (p = '*$') or (p = '$');
	END
	ELSE BEGIN
		WHILE (not ismatch) and (Length(nextS) > 0)  DO BEGIN
			sLen := Length(nextS);
			IF (EQ(nextS[i],p[1]) or (p[1] = '*') or (p[1] = '?'))  THEN BEGIN
				ismatch := IsMatching(p,nextS);
			END;

			IF(not ismatch) THEN BEGIN
				Setlength(nextS,sLen - i+1);
				FOR l:=i TO sLen DO BEGIN
					nextS[l-i+1] := s[l];
				END;
				Inc(i);
			END
		END;
	END;

	
END;
BEGIN
END.