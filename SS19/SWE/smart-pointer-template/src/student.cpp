#include <string>
#include <iostream>

#include "student.h"

using namespace std;

student::student() {
  cout << "INFO: student()" << endl;
}

student::student(string first_name,
                 string last_name,
                 string registration_nr)
  : person(first_name, last_name),
    registration_nr(registration_nr) {
  cout << "INFO: student(" << first_name
       << ", " << last_name
       << ", " << registration_nr << ")" << endl;
}

student::student(const student &s)
  : person(s), registration_nr(s.registration_nr) {
  cout << "INFO: copy student(" << first_name
       << ", " << last_name
       << ", " << registration_nr << "))" << endl;
}

student::student(student &&s) {
  swap(*this, s);
  cout << "INFO: move student(" << first_name
       << ", " << last_name
       << ", " << registration_nr << "))" << endl;
}

student& student::operator = (student s) {
  swap(*this, s);
  cout << "INFO: assign student(" << first_name
       << ", " << last_name
       << ", " << registration_nr << "))" << endl;
  return *this;
}

student::~student() {
  cout << "INFO: destruct student(" << first_name
       << ", " << last_name
       << ", " << registration_nr << ")" << endl;
}

void student::print(ostream &os) const {
  person::print(os);
  os << ' ' << registration_nr;
}

void student::read(istream &is) {
  person::read(is);
  is >> registration_nr;
}

void swap(student &a, student &b) {
  using std::swap;
  swap(static_cast<person&>(a), static_cast<person&>(b));
  swap(a.registration_nr, b.registration_nr);
}
