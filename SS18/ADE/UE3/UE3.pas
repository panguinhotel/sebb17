PROGRAM UE3;
USES Crt,RegExpr,StringListUnit;

VAR TextFile : Text;
	StringRead, Path : String;
	RegexObj: TRegExpr;
	i: Integer;
	isBlackListFile,stringIsFiltered : boolean;
	blackList,n : StringList;

	

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
StringListUnit.Init(blackList);

	IF(ParamCount >= 3) THEN BEGIN
	isBlackListFile := true;
	i:=1;
	WHILE (i <= ParamCount) and (ParamCount >= 2) and(ParamStr(1) = 'filter')  DO BEGIN
		Path := ParamStr(2);
		IF(i >= 3) Then Begin
	  	isBlackListFile := ParamStr(3) = '-f';
			if((isBlackListFile) and (i > 3)) Then begin
				Assign(TextFile,ParamStr(i));
				{$I-}
				Reset(TextFile);
				IF(IOResult = 0) THEN BEGIN
				repeat
					ReadLn(TextFile,StringRead);
					IF( not StringListUnit.Contains(blackList,StringRead)) Then
						StringListUnit.Append(blackList,StringRead);
				until (EOF(TextFile));
				Close(TextFile);
				END
				ELSE BEGIN
					WriteLn('File not found');
				END;
				{$I+}
				i := ParamCount +1;
			End
			ELSE begin
				StringListUnit.Append(blackList,ParamStr(i));
			END;
		END;
		Inc(i);
	END;
	
		Assign(TextFile,Path);
		{$I-}
		Reset (TextFile);
		IF(IOResult = 0) THEN BEGIN
		RegexObj := TRegExpr.Create;
		n := blackList^.next;
		stringIsFiltered:=false;
		repeat
			ReadLn(TextFile,StringRead);
			WHILE (not stringIsFiltered) and (n <> blackList)  DO BEGIN		
				RegexObj.Expression := n^.value;
				stringIsFiltered := RegexObj.Exec(StringRead);		
				n := n^.next;
			END;
			
			IF(not stringIsFiltered) Then
				WriteLn(StringRead);

			n:=blackList^.next;
			stringIsFiltered := false;
		until (EOF(TextFile));

		Close (TextFile);
		END
		ELSE BEGIN
			WriteLn('File not found');
		END;
		{$I+}
		ReadKey;
	END
	ELSE WriteLn('Missing Parameters! Expected Parameters: filter inputFile ( -f blackListFile | -l blackListEntry1 ... blackListEntryN ) ');
END. (* UE3 *)