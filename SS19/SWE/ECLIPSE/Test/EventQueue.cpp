#include <iostream>
#include <queue>

#include "EventQueue.h"
#include "Event.h"

using namespace std;

EventQueue::EventQueue() {
}

EventQueue::~EventQueue() {
	while(!queue.empty()){
		delete queue.top();
		queue.pop();
	}
}

int EventQueue::add(Event* newEvent) {
	counter++;
	queue.push(newEvent);
	return 0;
}

bool EventQueue::isEmpty() {
	if ((queue.empty() == true) && (counter == 0)) {
		return true;
	} else {
		return false;
	}
}

Event* EventQueue::getNextEvent() {
	if (!this->isEmpty()) {
		counter--;
		Event* e = queue.top();
		queue.pop();
		return e;
	} else {
		cout << "EventQueue has no elements left" << endl;
	}
}

int EventQueue::getSize() {
	return queue.size();
}
