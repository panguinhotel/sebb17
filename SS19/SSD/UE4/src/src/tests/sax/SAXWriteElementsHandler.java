package tests.sax;

import java.io.PrintWriter;
import java.util.Date;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXWriteElementsHandler extends DefaultHandler {

  PrintWriter writer = null;
  //a
  boolean elememt_ISBN_found = false;
  
  //b
  boolean element_autor_found = false;
  int autorCount = 0;
  int buecherVonMehrAlsEinenAutorVerkauft = 0;
  
  //c
  boolean element_preis_foundANDIsDollar = false;
  
  

  public void setWriter(PrintWriter writer) {
    this.writer = writer;
  }

  @Override
  public void startDocument() throws SAXException {
    writer.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>"); // additional checking has to be done if version
    // is correct!
    writer.println("<!DOCTYPE Rechnungen SYSTEM \"books.dtd\">");
    writer.println("<!-- Date and Time: " + (new Date()).toString() + " -->");
  }

  @Override
  public void endDocument() throws SAXException {
    writer.flush();
    writer.close();
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
	elememt_ISBN_found = qName.equals("ISBN");
	element_autor_found = qName.equals("Autor");
    writer.print("<" + qName);
    for (int i = 0; i < attributes.getLength(); i++) {   	
    	String attVal = attributes.getValue(i);
    	element_preis_foundANDIsDollar = (qName.equals("Preis")) && (attributes.getQName(i).equals("waehrung"))
    			&& (attributes.getValue(i).equals("Dollar"));
    	
	    if(element_preis_foundANDIsDollar)
	    	attVal = "Euro";
    	
      writer.print(" " + attributes.getQName(i) + "=\"" + attVal + "\"");
    }
    writer.print(">");
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    String s = new String(ch, start, length);
    if(elememt_ISBN_found)
    {
    	s = s.replace("ISBN:", "");
    }
    
    if(element_autor_found)
    {
    	autorCount += s.split(",").length;
    }
    
    if(element_preis_foundANDIsDollar)
    {
    	s =  String.format("%.2f", Double.parseDouble(s)/1.12);   	
    	element_preis_foundANDIsDollar = false;
    }
    	
    	
    writer.print(s);
  }

  @Override
  public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
    // do nothing
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
	 
	if(qName.equals("Buch"))
	{
		if(autorCount > 1)
			buecherVonMehrAlsEinenAutorVerkauft++;
		
		autorCount = 0;
	}
	if(qName.equals("Rechnungen"))
	{
		writer.println("<Statistik>");
		writer.println("<Autoren mehrAls=\"1\">"+buecherVonMehrAlsEinenAutorVerkauft+"</Autoren>");
		writer.println("</Statistik>");
	}
		
    writer.println("</" + qName + ">");
  }

} // class Writer
