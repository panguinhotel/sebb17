<?php
   include('../session.php');
   include('../config.php');
   $game_id = $_GET['id'];

   if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['bt_set'])) {
    $sql = "Delete from game_input where fk_id_game = $game_id";
	$result = mysqli_query($db,$sql);
	if(!$result) {
	  	echo '<script language="javascript">';
        echo 'alert("Fehler")';
	  	echo '</script>';
    }
}
?>
<html>
<head>
    <title>Fragespiel</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="refresh" content="2">
    </head>
    <body>
        <h1 class="headLine">Game ID = <?php echo($game_id) ?></h1>
        <div class="mainList">
        <table id="myTable">          
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Zeit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						$sql = "Select u.name as Name, g.tickstamp - (Select MIN(e.tickstamp) from game_input e where e.fk_id_game = $game_id)  as zeit from game_input g
                                join `user` u on u.id_user = g.fk_id_user
                                where fk_id_game = $game_id order by zeit;";
                        $result = mysqli_query($db,$sql);
                        while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                            echo "<tr name=\"namerow\">";
                            echo "<td>" . $row['Name'] . "</td>";
                            echo "<td>" . $row['zeit'] . "</td>";
                            echo "</tr>";
						}
                    ?>
                </tbody> 
            </table>
            <br>
            <form class="container" action = "" method = "post" >
                <input class="deleteButton" type="submit" name="bt_set" value="Nächste Runde" placeholder="">
            </form>
			<button class="backbutton" onclick="window.location.href='../fragespiel.php'">Zurück</button>
		</div>	
    </body>
</html>

