PROGRAM ScannerTest;

USES
	Scanner;

VAR
	input : String;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	Write('-->');
	ReadLn(input);
	WHILE input <> ''  DO BEGIN
		InitScanner(input);
		WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN
			GetNextSymbol;
		END;

		IF CurrentSymbol = noSym THEN begin
			WriteLn('ERROR at position :  ', CurrentSymbolPos);
		end ELSE BEGIN
			WriteLn('Success');
		END;
		Write('-->');
		ReadLn(input);
	END;

END. (* ScannerTest *)