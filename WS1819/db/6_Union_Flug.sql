SET AUTOTRACE ON;

SELECT Abflug, Ziel -- Direktfl�ge
  FROM Flug
 WHERE Abflug = 'Z�rich'
UNION               -- 2-Stopp-Fl�ge
SELECT F1.Abflug, F2.Ziel
  FROM Flug F1, Flug F2
 WHERE F1.Abflug = 'Z�rich'
   AND F1.Ziel = F2.Abflug
UNION             -- 3-Stopp-Fl�ge
SELECT F1.Abflug, F3.Ziel
  FROM Flug F1, Flug F2, Flug F3
 WHERE F1.Abflug = 'Z�rich'
   AND F1.Ziel = F2.Abflug
   AND F2.Ziel = F3.Abflug
UNION             -- 4-Stopp-Fl�ge
SELECT F1.Abflug, F4.Ziel
  FROM Flug F1, Flug F2, Flug F3, Flug F4
 WHERE F1.Abflug = 'Z�rich'
   AND F1.Ziel = F2.Abflug
   AND F2.Ziel = F3.Abflug
   AND F3.Ziel = F4.Abflug;
   
   
-- Etwas schneller durch Indizes, siehe Explain Plan
CREATE INDEX Flug_Idx ON Flug (Abflug, Ziel);
DROP INDEX Flug_Idx;
 