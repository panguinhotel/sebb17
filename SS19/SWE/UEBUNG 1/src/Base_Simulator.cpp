/*
 * Base_Queue.cpp
 *
 *  Created on: 06.03.2019
 *      Author: Lukas Schaubmayr
 */

#include "Base_Simulator.h"

#include <iostream>

 Base_Simulator::~Base_Simulator()
{
	 while(m_events.size() > 0)
	 {
		 auto* event = m_events.top();
		 delete event;
		 m_events.pop();
	 }
}
void Base_Simulator::addEvent(Base_Event *event)
{
	m_events.push(event);
}

void Base_Simulator::step()
{
	if(!m_events.empty())
	{
		m_current_ticks++;
		Base_Event *customEvent = m_events.top();
		customEvent->register_child_events(&m_events);
		delete customEvent;
		m_events.pop();
	}
	else
		std::cout <<"Nothing to do";
}
void Base_Simulator::run()
{
	while(m_events.size() > 0 && m_status == Status::Running && m_allowed_ticks > m_current_ticks)
	{
		step();
	}
	std::cout<<"FINISHED"<<std::endl;
}
