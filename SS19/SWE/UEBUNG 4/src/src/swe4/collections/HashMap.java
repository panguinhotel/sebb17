/**
 * 
 */
package swe4.collections;
import java.util.Iterator;
/**
 * @author schau
 *
 */
public interface HashMap<K,V> extends Iterable<KeyValuePair<K,V>> {

	V get(K key);
	boolean containsKey(K key);
	boolean containsValue(V value);
	V put(K key, V value);
	boolean isEmpty();
	V remove(K key);
	void clear();
	int size();
	Iterable<K> keys();
	Iterable<V> values();
	Iterator<KeyValuePair<K,V>> iterator();
	double getMaxLoadFactor();
	double getLoadFactor();
	void rehash();
}
