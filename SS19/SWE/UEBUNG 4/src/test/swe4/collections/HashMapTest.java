package swe4.collections;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HashMapTest {

	private HashMap<Integer,String> map;
	
	@BeforeEach
	void setUp() throws Exception 
	{
		map = new LinkedListChainingHashMap<Integer,String>(4,1.1);
	}

	@AfterEach
	void tearDown() throws Exception 
	{
		map = null;
	}
	
	@Test
	void testPut()
	{
		assertEquals(map.put(1,"Test"), null);
		assertEquals(map.put(1,"Test"), "Test");
		
		assertEquals(map.put(11,"Haus"), null);
		assertEquals(map.put(3,"Baum"), null);
		assertEquals(map.put(3,"Fred"), "Baum");
		
		assertEquals(map.put(4,"Katze"), null);
		assertEquals(map.put(4,"Maus"), "Katze");
		
		assertEquals(map.put(2,"Stein"), null);
		assertEquals(map.put(2,"See"), "Stein");
	}
	
	@Test
	void testGet() 
	{
		testPut();
		assertEquals("Test", map.get(1));
	}
	
	@Test
	void testContainsKey() 
	{
		testPut();
		assertEquals(true,map.containsKey(1));
		assertEquals(false,map.containsKey(7));
	}
	
	@Test
	void testContainsValue() 
	{
		testPut();
		assertEquals(true,map.containsValue("Test"));
		assertEquals(true,map.containsValue("Haus"));
		assertEquals(false,map.containsValue("Baum"));
		assertEquals(true,map.containsValue("Fred"));
	}
	
	@Test
	void testIsEmpty() 
	{
		assertEquals(true,map.isEmpty());
		testPut();		
		assertEquals(false,map.isEmpty());
	}
	
	@Test
	void testClear() 
	{
		testIsEmpty();
		assertEquals(false,map.isEmpty());
		map.clear();
		assertEquals(true,map.isEmpty());
	}
	
	@Test
	void testRemove() 
	{
		testPut();
		assertEquals(true,map.containsKey(1));
		map.remove(1);
		assertEquals(false,map.containsKey(1));
	}
	
	@Test
	void testSize() 
	{
		assertEquals(0,map.size());
		testPut();
		assertEquals(5,map.size());
	}
	
	@Test
	void testRehash()
	{
		testPut();
		assertEquals(5,map.size());
		assertEquals(true,map.containsValue("Test"));
		assertEquals(true,map.containsValue("Haus"));
		assertEquals(false,map.containsValue("Baum"));
		assertEquals(true,map.containsValue("Fred"));
		assertEquals(true,map.containsValue("Maus"));
		
		map.rehash();
		
		assertEquals(5,map.size());
		assertEquals(true,map.containsValue("Test"));
		assertEquals(true,map.containsValue("Haus"));
		assertEquals(false,map.containsValue("Baum"));
		assertEquals(true,map.containsValue("Fred"));
		assertEquals(true,map.containsValue("Maus"));
	}
	
	@Test
	void testMaxLoadFactor()
	{
		assertEquals(1.1,map.getMaxLoadFactor());
		testPut();
		assertEquals(2.2,map.getMaxLoadFactor());
	}
	
	@Test
	void testLoadFactor()
	{
		assertEquals(0,map.getLoadFactor());
		testPut();
		assertEquals(1.25,map.getLoadFactor());
	}
	
	@Test
	void testIterator()
	{
		Iterator<KeyValuePair<Integer,String>> it = map.iterator();
		assertEquals(false,it.hasNext());
		testPut();
		it = map.iterator();
		assertEquals(true,it.hasNext());
		for(int i = 0; i < map.size(); ++i)
		{
			assertEquals(false,it.next() == null);
		}
		assertEquals(false,it.hasNext());
	}
	
	@Test
	void testKeys()
	{
		Iterable<Integer> tmp = map.keys();
		Iterator<Integer> it = tmp.iterator();
		for(int i = 0; i < map.size(); ++i)
		{
			assertEquals(false,it.next() == null);
		}
		assertEquals(false,it.hasNext());
	}

	@Test
	void testValues()
	{
		Iterable<String> tmp = map.values();
		Iterator<String> it = tmp.iterator();
		for(int i = 0; i < map.size(); ++i)
		{
			assertEquals(false,it.next() == null);
		}
		assertEquals(false,it.hasNext());
	}
}
