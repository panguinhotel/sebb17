\contentsline {chapter}{\numberline {1}NODEJS}{3}
\contentsline {section}{\numberline {1.1}Basics}{3}
\contentsline {paragraph}{Motivation}{3}
\contentsline {paragraph}{Recap}{3}
\contentsline {section}{\numberline {1.2}Module}{4}
\contentsline {subsection}{\numberline {1.2.1}Modultypen}{4}
\contentsline {paragraph}{Native/Build-in module}{4}
\contentsline {paragraph}{Lokale Module}{5}
\contentsline {paragraph}{Installierte Module}{5}
\contentsline {section}{\numberline {1.3}WEB FRAMEWORK}{7}
\contentsline {subsection}{\numberline {1.3.1}Features}{7}
\contentsline {paragraph}{MVC Architecture}{7}
\contentsline {paragraph}{Express}{7}
\contentsline {paragraph}{Express-Routing}{8}
\contentsline {paragraph}{Express-Middleware}{8}
\contentsline {subparagraph}{Use Cases}{9}
\contentsline {paragraph}{Error Handling}{10}
\contentsline {paragraph}{Template Engine}{10}
\contentsline {subsection}{\numberline {1.3.2}Templates Engines}{12}
\contentsline {subsubsection}{Handlebar JS}{12}
\contentsline {subsubsection}{Razor (ASP.net)}{14}
\contentsline {subsubsection}{JSP (Java)}{14}
\contentsline {subsubsection}{Velocity (Java)}{14}
\contentsline {chapter}{\numberline {2}ES6 \& More}{15}
\contentsline {section}{\numberline {2.1}Template Literals}{15}
\contentsline {section}{\numberline {2.2}Constants}{15}
\contentsline {section}{\numberline {2.3}Default Parameters}{15}
\contentsline {section}{\numberline {2.4}Classes}{16}
\contentsline {section}{\numberline {2.5}Global Symbol}{17}
\contentsline {section}{\numberline {2.6}Set}{18}
\contentsline {section}{\numberline {2.7}Map}{18}
\contentsline {chapter}{\numberline {3}MongoDB}{19}
\contentsline {section}{\numberline {3.1}No SQL = Not Only SQL}{19}
\contentsline {subsection}{\numberline {3.1.1}Types}{19}
\contentsline {section}{\numberline {3.2}CAP Theorem}{20}
\contentsline {section}{\numberline {3.3}MongoDb}{20}
\contentsline {section}{\numberline {3.4}Datastructure}{21}
\contentsline {subsection}{\numberline {3.4.1}Datatypes (BSON)}{21}
\contentsline {section}{\numberline {3.5}CRUD}{22}
\contentsline {subsection}{\numberline {3.5.1}Create}{22}
\contentsline {subsection}{\numberline {3.5.2}Read/Query}{22}
\contentsline {subsection}{\numberline {3.5.3}Update}{23}
\contentsline {subsection}{\numberline {3.5.4}Delete}{23}
\contentsline {section}{\numberline {3.6}MongoDB-Docker}{24}
\contentsline {chapter}{\numberline {4}TypeScript}{25}
\contentsline {section}{\numberline {4.1}Basics}{25}
\contentsline {section}{\numberline {4.2}Setup}{26}
\contentsline {section}{\numberline {4.3}Language}{27}
\contentsline {subsection}{\numberline {4.3.1}Basic Types}{27}
\contentsline {subsection}{\numberline {4.3.2}Interface}{27}
\contentsline {subsection}{\numberline {4.3.3}Class}{28}
\contentsline {subsection}{\numberline {4.3.4}Enums}{29}
\contentsline {subsection}{\numberline {4.3.5}ForEach}{29}
\contentsline {section}{\numberline {4.4}Definition File}{29}
\contentsline {chapter}{\numberline {5}SPA}{30}
\contentsline {section}{\numberline {5.1}Idea}{30}
\contentsline {section}{\numberline {5.2}Basic}{31}
\contentsline {section}{\numberline {5.3}Technologies}{31}
\contentsline {section}{\numberline {5.4}Frameworks}{31}
\contentsline {chapter}{\numberline {6}PWA}{33}
\contentsline {section}{\numberline {6.1}History}{33}
\contentsline {section}{\numberline {6.2}Features}{36}
\contentsline {section}{\numberline {6.3}FIRE Principle}{37}
\contentsline {subsection}{\numberline {6.3.1}Fast}{37}
\contentsline {subsection}{\numberline {6.3.2}Integrated}{37}
\contentsline {subsection}{\numberline {6.3.3}Reliable}{37}
\contentsline {subsection}{\numberline {6.3.4}Engaging}{37}
\contentsline {section}{\numberline {6.4}Technologien}{38}
\contentsline {subsection}{\numberline {6.4.1}Web Manifest}{38}
\contentsline {paragraph}{Display Mode}{38}
\contentsline {paragraph}{Orientation}{39}
\contentsline {paragraph}{Related}{39}
\contentsline {subsection}{\numberline {6.4.2}Service Worker}{40}
\contentsline {paragraph}{Use Cases}{40}
\contentsline {subsection}{\numberline {6.4.3}Cache API}{41}
\contentsline {subsection}{\numberline {6.4.4}WEB Worker API}{43}
\contentsline {subsection}{\numberline {6.4.5}IndexedDb}{43}
\contentsline {subsection}{\numberline {6.4.6}Web Storage}{46}
\contentsline {subsection}{\numberline {6.4.7}A2H (Add 2 Homescreen)}{46}
\contentsline {paragraph}{Requirements}{46}
\contentsline {paragraph}{Web Manifest}{46}
\contentsline {paragraph}{Limitierungen}{46}
\contentsline {subsection}{\numberline {6.4.8}Web Push API}{46}
\contentsline {subsection}{\numberline {6.4.9}Notifications API}{46}
\contentsline {section}{\numberline {6.5}How to Deploy}{47}
\contentsline {subsection}{\numberline {6.5.1}NODEJS Server only}{47}
\contentsline {subsection}{\numberline {6.5.2}NODEJS API Server + CDN}{48}
\contentsline {subsection}{\numberline {6.5.3}SERVERLESS + CDN}{48}
