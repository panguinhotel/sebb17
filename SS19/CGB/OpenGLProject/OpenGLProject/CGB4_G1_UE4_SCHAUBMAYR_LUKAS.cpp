#include "GL/glew.h"
#include "GL/freeglut.h"
#include <iostream>
#include <stdio.h>
#include <map>
#include <string>
#include <cstdlib>
#include <vector>

using namespace std;

#define checkImageWidth 64
#define checkImageHeight 64
static GLubyte checkImage[checkImageHeight][checkImageWidth][4];
static GLuint texName;
const int m_initWidth = 800;
const int m_initHeight = 600;
const int m_rows = 3;
const int m_columns = 4;
const float m_cardWidth = 0.5;
const float m_cardHeight = 0.75;
const float m_cardMargin = 0.25;
const float m_bitMapPartSize = 0.1;
const float m_bitMapSkip = 1;
const float m_pushBackStep = 0.1;
int m_turns = 0;

//Forward Declaration
int revealedCards();
void concealCards();
void checkPair();
void pushBackEvent(int value);

// Data read from the header of the BMP file
unsigned char header[54]; // Each BMP file begins by a 54-bytes header
unsigned int dataPos;     // Position in the file where the actual data begins
unsigned int imageWidth, imageHeight;
unsigned int imageSize;   // = width*height*3
						  // Actual RGB data
unsigned char * imageData;

std::string str = "textures.bmp";

char *filenameandpath = &str[0];

struct Player
{
	std::string name;
	int successCount = 0;
	int errorCount = 0;

	Player(std::string Name) { name = Name; }
};

struct Card
{
	float x;
	float y;
	int styleNumber;
	bool isVisible = true;
	bool showFront = false;
	float z = 0.0;
};

Player m_player1("Spieler1");
Player m_player2("Spieler2");
Card m_cardArray[m_rows][m_columns];

// GLUT Window ID
int windowid;

int loadBMP_custom(char *imagepath) {
	FILE * file;
	fopen_s(&file, imagepath, "rb"); // Open the file
	if (!file) {
		cout << "Image could not be opened" << endl;
		return 0;
	}

	if (fread(header, 1, 54, file) != 54) { // If not 54 bytes read : problem
		cout << "Not a correct BMP file" << endl;
		return 0;
	}

	if (header[0] != 'B' || header[1] != 'M') {
		cout << "Not a correct BMP file" << endl;
		return 0;
	}

	// Read ints from the byte array
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	imageWidth = *(int*)&(header[0x12]);
	imageHeight = *(int*)&(header[0x16]);
	// Some BMP files are misformatted, guess missing information
	// 3 : one byte for each Red, Green and Blue component
	if (imageSize == 0)    imageSize = imageWidth * imageHeight * 3;
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way
	imageData = new unsigned char[imageSize]; // Create a buffer
	fread(imageData, 1, imageSize, file); // Read the actual data from the file into the buffer

	fclose(file); //Everything is in memory now, the file can be closed
}


void makeCheckImage(void) {
	int i, j, c;

	for (i = 0; i < checkImageHeight; i++) {
		for (j = 0; j < checkImageWidth; j++) {
			c = ((((i & 0x8) == 0) ^ ((j & 0x8)) == 0)) * 255;
			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
			checkImage[i][j][3] = (GLubyte)255;
		}
	}
}

void initTextures(void) {
	loadBMP_custom(filenameandpath);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &texName);
	glBindTexture(GL_TEXTURE_2D, texName);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, checkImageWidth, checkImageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkImage);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, imageData);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	//Set Background Color
	glColor3f(50, 130, 0);

	glBindTexture(GL_TEXTURE_2D, texName);
	glBegin(GL_QUADS);;
	//Karten Zeichnen
	for (int i = 0; i < m_rows; ++i)
	{
		for (int k = 0; k < m_columns; ++k)
		{
			Card c = m_cardArray[i][k];
			if (c.isVisible)
			{
				//Muster aus Bitmap laden
				float bitMapStart = c.showFront ? m_bitMapSkip*m_bitMapPartSize + m_bitMapPartSize*c.styleNumber : 0.0;

				glTexCoord2f(bitMapStart, 0.0); glVertex3f(c.x, c.y, c.z);
				glTexCoord2f(bitMapStart, 1.0); glVertex3f(c.x, c.y + m_cardHeight, c.z);
				glTexCoord2f(bitMapStart + m_bitMapPartSize, 1.0); glVertex3f(c.x + m_cardWidth, c.y + m_cardHeight, c.z);
				glTexCoord2f(bitMapStart + m_bitMapPartSize, 0.0); glVertex3f(c.x + m_cardWidth, c.y, c.z);
			}
		}
	}
	glEnd();
	glFlush();
	glDisable(GL_TEXTURE_2D);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -4.5);
	glutSwapBuffers();
}

/*-[Keyboard Callback]-------------------------------------------------------*/
void keyboard(unsigned char key, int x, int y) {

	int idxRow = -1;
	int idxColumn = -1;
	switch (key) {
	case 'r': // lowercase character 'r'
		idxRow = 2;
		idxColumn = 0;
		break;
	case 't': // lowercase character 't'
		idxRow = 2;
		idxColumn = 1;
		break;
	case 'z': // lowercase character 'z'
		idxRow = 2;
		idxColumn = 2;
		break;
	case 'u': // lowercase character 'u'
		idxRow = 2;
		idxColumn = 3;
		break;
	case 'f': // lowercase character 'f'
		idxRow = 1;
		idxColumn = 0;
		break;
	case 'g': // lowercase character 'g'
		idxRow = 1;
		idxColumn = 1;
		break;
	case 'h': // lowercase character 'h'
		idxRow = 1;
		idxColumn = 2;
		break;
	case 'j': // lowercase character 'j'
		idxRow = 1;
		idxColumn = 3;
		break;
	case 'c': // lowercase character 'c'
		idxRow = 0;
		idxColumn = 0;
		break;
	case 'v': // lowercase character 'v'
		idxRow = 0;
		idxColumn = 1;
		break;
	case 'b': // lowercase character 'b'
		idxRow = 0;
		idxColumn = 2;
		break;
	case 'n': // lowercase character 'n'
		idxRow = 0;
		idxColumn = 3;
		break;
	case 27: // Escape key
		glutDestroyWindow(windowid);
		exit(0);
		break;
	}

	//If card cann be revealed -> showFront = true;
	if (idxRow != -1 && idxColumn != -1)
	{
		if (revealedCards() == 2)
		{
			concealCards();
		}

		if (revealedCards() != 2)
		{
			m_cardArray[idxRow][idxColumn].showFront = true;
		
		}

		if (revealedCards() == 2)
		{
			checkPair();
		}
		glutPostRedisplay();
	}
}

/*-[MouseClick Callback]-----------------------------------------------------*/
void onMouseClick(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {

		//Translate X and Y from pixel to Coordinates
		double translatedX = 1.0*(x - glutGet(GLUT_WINDOW_WIDTH) / 2)*(2.17*glutGet(GLUT_WINDOW_WIDTH) / 2 / 400) 
			/ (glutGet(GLUT_WINDOW_WIDTH) / 2) / glutGet(GLUT_WINDOW_HEIGHT) * m_initHeight;
		double translatedY = 1.0*(glutGet(GLUT_WINDOW_HEIGHT) / 2 - y) / (glutGet(GLUT_WINDOW_HEIGHT) / 3.5);

		//Conceal cards if revealed cards count 2 already
		if (revealedCards() == 2)
		{
			concealCards();
		}


		for (int i = 0; i < m_rows; ++i)
		{
			for (int k = 0; k < m_columns; ++k)
			{
				if (m_cardArray[i][k].isVisible && !m_cardArray[i][k].showFront)
				{
					if (translatedX >= m_cardArray[i][k].x && translatedX <= (m_cardArray[i][k].x + m_cardWidth))
					{
						if (translatedY >= m_cardArray[i][k].y && translatedY <= (m_cardArray[i][k].y + m_cardHeight))
						{
								//If card cann be revealed -> showFront = true;
								if (revealedCards() != 2)
								{
									m_cardArray[i][k].showFront = true;

									//Check cards if revealed cards count 2
									if (revealedCards() == 2)
									{
										checkPair();
									}
								}
								glutPostRedisplay();
						}
					}						
				}
			}
		}
	}
}

/*-[Reshape Callback]--------------------------------------------------------*/
void reshapeFunc(int x, int y) {
	if (y == 0 || x == 0) return;  //Nothing is visible then, so return

	glMatrixMode(GL_PROJECTION); //Set a new projection matrix
	glLoadIdentity();
	//Angle of view: 40 degrees
	//Near clipping plane distance: 0.5
	//Far clipping plane distance: 20.0

	gluPerspective(40.0, (GLdouble)x / (GLdouble)y, 0.5, 40.0);
	glViewport(0, 0, x, y);  //Use the whole window for rendering
}


//Populate Card Array width:
//	Random card values
//	Position to draw card
void populateCardArray()
{
	//Calculate Start Row and Start Column
	float startRow = -0.5* (m_columns*m_cardWidth + m_cardMargin*m_columns - m_cardMargin);
	float startColumn = -0.5* (m_rows*m_cardHeight + m_cardMargin*m_rows - m_cardMargin);

	//Fill Map to check that a random number does not appear more than 2 times
	map<int, int> randomNumbers;
	for (int i = 0; i < m_columns*m_rows / 2; ++i)
	{
		randomNumbers.insert({ i ,2 });
	}

	for (int i = 0; i < m_rows; ++i)
	{
		for (int k = 0; k < m_columns; ++k)
		{
			Card c;
			c.isVisible = true;
			c.showFront = false;

			//Generate Random Number
			int value = rand() % (m_columns*m_rows / 2);
			while (randomNumbers.at(value) == 0) //When current value is already used 2 times
			{
				value = rand() % (m_columns*m_rows / 2);
			}
			randomNumbers[value]--;
			c.styleNumber = value;
			//Set Card Bottom Left Position
			c.y = startRow + i*(m_cardHeight + m_cardMargin);
			c.x = startColumn + k*(m_cardWidth + m_cardMargin);
			//Add Card to Array
			m_cardArray[i][k] = c;
		}
	}
}

//Returns number of revealedCards
int revealedCards()
{
	int showFrontCards = 0;
	for (int i = 0; i < m_rows; ++i)
	{
		for (int k = 0; k < m_columns; ++k)
		{
			//When card-front is shown and card has not moved away (z==0)
			if (m_cardArray[i][k].showFront && m_cardArray[i][k].z == 0)
				showFrontCards++;
		}
	}
	return showFrontCards;
}

//Conceals all cards that have not moved away (z==0)
void concealCards()
{
	for (int i = 0; i < m_rows; ++i)
	{
		for (int k = 0; k < m_columns; ++k)
		{
			//Check if card moved away
			if(m_cardArray[i][k].z == 0)
				m_cardArray[i][k].showFront = false;
		}
	}

}

//Check if current Visible cards match
void checkPair()
{
	map<int,vector<std::pair<int,int>>> resultMap;
	bool found = false;
	for (int i = 0; i < m_rows; ++i)
	{
		for (int k = 0; k < m_columns; ++k)
		{
			//Card front has to be shown + card is visible and card has not moved away
			if (m_cardArray[i][k].showFront && m_cardArray[i][k].isVisible && m_cardArray[i][k].z == 0)
			{
				int styleNumber = m_cardArray[i][k].styleNumber; //style number = randomly generated pattern number
				resultMap[m_cardArray[i][k].styleNumber].push_back({ i,k });

				//Check if 2 same cards are shown
				if (resultMap[m_cardArray[i][k].styleNumber].size() == 2)
				{
					//Move cards to background
					for (auto pair : resultMap[m_cardArray[i][k].styleNumber])
					{						
						m_cardArray[pair.first][pair.second].z--;						
						glutTimerFunc(100, pushBackEvent, 0);
					}
				
					//Set Player Success Count
					if (m_turns % 2 == 0)
						m_player1.successCount++;
					else
						m_player2.successCount++;

					//Decrement turns 
					m_turns--;
					found = true;
				}
			}
		}
	}

	//If 2 different cards are visible - Increment player errorCount
	if (!found)
	{
		if (m_turns % 2 == 0)
			m_player1.errorCount++;
		else
			m_player2.errorCount++;
	}

	//Increase turns
	m_turns++;

	//Status anzeigen
	cout << "Aktueller Spieler : " + ((m_turns % 2 == 0) ? m_player1.name : m_player2.name) << endl;
	cout << m_player1.name << "\tFehler : " << m_player1.errorCount << "\tErfolge : " << m_player1.successCount << endl;
	cout << m_player2.name << "\tFehler : " << m_player2.errorCount << "\tErfolge : " << m_player2.successCount << endl;
	cout << "========================"<< endl<<endl;

}

//Decrements z value of already revealed pairs
void pushBackEvent(int value)
{
	int continuePushBack = false;
	for (int i = 0; i < m_rows; ++i)
	{
		for (int k = 0; k < m_columns; ++k)
		{
			if (m_cardArray[i][k].z < 0)
			{
				continuePushBack = true;
				m_cardArray[i][k].z -= m_pushBackStep;
				//Move card backwards till z reaches -1
				if (m_cardArray[i][k].z > -1)
				{
					m_cardArray[i][k].isVisible = false;
				}
			}
			
		}
	}

	if (continuePushBack)
	{
		glutTimerFunc(100, pushBackEvent, 0);
		glutPostRedisplay();
	}
}

//Returns Current Player
Player getCurrentPlayer()
{
	if (m_turns % 2 == 0)
		return m_player1;
	else
		return m_player2;
}


int main(int argc, char **argv) {

	populateCardArray();

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE | GLUT_DEPTH);
	glutInitWindowPosition(500, 500); //determines the initial position of the window
	glutInitWindowSize(m_initWidth, m_initHeight);	  //determines the size of the window
	windowid = glutCreateWindow("Our Fourth OpenGL Window"); // create and name window

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_DEPTH_TEST);

	// register callbacks
	initTextures();

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(onMouseClick);
	glutReshapeFunc(reshapeFunc);
	cout << "AUFDECKEN DER KARTEN VIA TASTENDRUCK " << endl << "R-T-Z-U\nF-G-H-J\nC-V-B-N" << endl << "Roation der Karten ist nicht implementiert" << endl;
	glutMainLoop(); // start the main loop of GLUT


	
	return 0;
}

