/*
 * SupervisorEvent.h
 *
 *  Created on: 10.03.2019
 *      Author: Lukas Schaubmayr
 */

#ifndef SUPERVISOR_EVENT_H_
#define SUPERVISOR_EVENT_H_

#include "Base_Event.h"
#include <iostream>

class Supervisor_Event : public Base_Event {
public:
	inline Supervisor_Event(){}
	inline virtual ~Supervisor_Event(){std::cout <<"S_E : DESTRUCTOR" << std::endl;}
	inline bool continue_operation(int current_ticks, int max_ticks, int current_size, int allowed_size) const
	{
		bool value = current_ticks < max_ticks && current_size < allowed_size;
			std::cout <<"S_E : "<< (value?"continue": "stop") << std::endl;

		return value;
	}
};

#endif /* SUPERVISOR_EVENT_H_ */
