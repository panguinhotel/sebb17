unit MyExtensions;
	
interface
TYPE	Response = Record
		status: (Failure, Success);
		message : string;
		End;

TYPE	AddressBookEntry = Record
		vorname : string;
		nachname : string;
		nummer : string;
		End;
TYPE	Modus  = (Liste,CLHa,OHa);

FUNCTION IsSuccess(resp: Response) : boolean;
FUNCTION ResponseToString(resp: Response) : string;
FUNCTION CompareAddressBookEntry(param1,param2: AddressBookEntry) : Boolean;
implementation
	
(*IsSuccess*)
FUNCTION IsSuccess(resp: Response) : boolean;
BEGIN
	IsSuccess := resp.status = Success;
END; (* IsSuccess *)

(*ResponseToString*)
FUNCTION ResponseToString(resp: Response) : string;
VAR
	x : string;
BEGIN
	Str(resp.status,x);
	ResponseToString := 'Status: ' + x + ^i+'Message: ' + resp.message;
END; (* ResponseToString *)

(*CompareAddressBookEntry*)
FUNCTION CompareAddressBookEntry(param1: AddressBookEntry;param2: AddressBookEntry) : Boolean;
BEGIN
	CompareAddressBookEntry := (param1.vorname = param2.vorname) and 
							(param1.nachname = param2.nachname) and (param1.nummer = param2.nummer);
END; (* Compare *)
end.