PROGRAM Uebung4;
type 
memoryfield = Record
            displayVal : 0..30;
            editVal : 0..30;
            End;
tipp = Record
       row : 0..5;
       column : 0..4;
       End;
player = Record
        points: 0..15      
        End;
VAR
field : array [0..5] of array[0..4] of memoryfield;
players: array[0..1] of player;
counter: Integer;
mode : 0..2;
knownfield : array [0..5] of array[0..4] of Integer; 

(* -----------------------------------------------------------------------*)
(* -------- Writes current Memorytable in Console-------------------------*)
(* -----------------------------------------------------------------------*)
procedure ShowTable();
VAR
i,k : Integer;
begin
    FOR i:=0 TO 5 DO BEGIN
        FOR k:=0 TO 4 DO BEGIN
            Write(field[i,k].displayVal,^i);
        END;
        WriteLn('');
    END;
    WriteLn('==================')
end; (* ShowTable *)
(* -----------------------------------------------------------------------*)
(* ----- Check if tipp is a correct tipp and set display field------------*)
(* -----------------------------------------------------------------------*)
function SetTip(tipp1: tipp):boolean;
begin
    if field[tipp1.row,tipp1.column].displayVal <> field[tipp1.row,tipp1.column].editVal Then
    Begin
        SetTip:=true;
        WriteLn('TIPP SET [ ROW : ',tipp1.row,'  COLUMN : ',tipp1.column, ' ]');
        field[tipp1.row,tipp1.column].displayVal := field[tipp1.row,tipp1.column].editVal;
        knownfield[tipp1.row,tipp1.column] := field[tipp1.row,tipp1.column].editVal;   
        ShowTable();
    End
    else
    Begin
        SetTip:=false;
    End;
End;

procedure HideTips(tipp1,tipp2: tipp);
begin
    field[tipp1.row,tipp1.column].displayVal := 0;
    field[tipp2.row,tipp2.column].displayVal := 0;
    ShowTable();
end; (* SetTip *)

(* --------------------------------------------------------------------------*)
(* Check if Tipp is Valid and set User and points, check if game is finished *)
(* --------------------------------------------------------------------------*)
Function ValidateTipps(tipp1,tipp2: tipp) : boolean;
VAR i,sum :Integer;
x:boolean;
BEGIN
    x:=false;
    if field[tipp1.row,tipp1.column].editVal = field[tipp2.row,tipp2.column].editVal Then
    Begin
        players[counter Mod 2].points := players[counter Mod 2].points +1;
        x:=true
    End
    else HideTips(tipp1,tipp2);

    sum:= 0;
    FOR i:=Low(players) TO High(players) DO BEGIN
        sum:= sum + players[i].points;
    END; 

    if sum >= 15 Then
        ValidateTipps := true
    else
    Begin
        IF x=false  THEN BEGIN
            counter:= counter +1;
        END;
        Writeln();
        Writeln();
        Writeln('PlAYER ',(counter MOD 2)+1,' Turn');
        ValidateTipps:= false;
    End;      
END; (* ValidateTipps *)
(* -----------------------------------------------------------------------*)
(* ----- Endgame : Sows Results after game is finished--------------------*)
(* -----------------------------------------------------------------------*)
PROCEDURE EndGame();
Var i:Integer;
BEGIN
    FOR i:=0 TO 1 DO BEGIN
        WriteLn('PLAYER: ', i+1, '  POINTS: ', players[i].points);
    END;
    ReadLn();
END; (* EndGame *)
(* -----------------------------------------------------------------------*)
(* ----- Initialize memoryfield, players, Gamemodes   --------------------*)
(* -----------------------------------------------------------------------*)
procedure Init();
VAR
rand,i,k,j,l,tmp : Integer;
begin
    tmp := 0;
    players[0].points := 0;
    players[1].points := 0;
     FOR i:=0 TO 5 DO BEGIN
        FOR k:=0 TO 4 DO BEGIN
            field[i,k].displayVal:=0;
            field[i,k].editVal:=0;
                knownfield[i,k] := 0;
            tmp:= 2;
            while tmp >= 2 do begin
                tmp:=0;
                rand:=Random(15)+1;   
                FOR j:=0 TO 5 DO BEGIN
                    FOR l:=0 TO 4 DO BEGIN
                        if field[j,l].editVal = rand then
                        Begin
                            tmp := tmp +1;
                        End;                   
                    END;              
                END; 
                if tmp < 2 Then 
                begin          
                    field[i,k].editVal:=rand;
                End;
            End;
        END; 
    END; 

    counter := 0;   
    WriteLn('Gamemodes : ');
    WriteLn('0 = Player vs Player');
    WriteLn('1 = Player vs easy AI');
    WriteLn('2 = Player vs hard AI');
    Write('Selected Mode = ');
    ReadLn(mode);
    IF (mode = 1) or (mode =2)  THEN WriteLn('User = Player1');
end; (* Init *)
(* -----------------------------------------------------------------------*)
(* -------------------------- Set User TIPP ------------------------------*)
(* -----------------------------------------------------------------------*)
FUNCTION Tipp_UserInput(tipNr:Integer) : tipp;
BEGIN
      WriteLn('TIPP ',tipNr);
      Write('Row-Index    : '); ReadLn(Tipp_UserInput.row);
      Write('Column-Index : '); ReadLn(Tipp_UserInput.column);
END; (* UserInput *)

(* -----------------------------------------------------------------------*)
(* ----------------- Calculate Tipp for Hard AI---------------------------*)
(* -----------------------------------------------------------------------*)
FUNCTION Tipp_HardAI() : tipp;
Var
i,k,j,l:Integer;
setted: boolean;
BEGIN
    setted := false;
    FOR i:=0 TO 5 DO BEGIN
        FOR k:=0 TO 4 DO BEGIN
            FOR j:=0 TO 5 DO BEGIN
                FOR l:=0 TO 4 DO BEGIN
                    if (knownfield[i,k] = knownfield[j,l]) and 
                        (field[i,k].displayVal <> field[i,k].editVal) and
                        (knownfield[i,k] <> 0) and
                        ((i <> j) or (k<>l)) Then
                    Begin
                        Tipp_HardAI.row := i;
                        Tipp_HardAI.column := k;
                        setted := true;
                    End;
                END; 
            END;
        END; 
    END; 
    IF NOT setted  THEN BEGIN
        FOR i:=0 TO 5 DO BEGIN
            FOR k:=0 TO 4 DO BEGIN
                IF knownfield[i,k] = 0  THEN BEGIN
                    Tipp_HardAI.row := i;
                    Tipp_HardAI.column := k;
                    setted := true;
                END;
            END;    
        END;
    END;
    IF NOT setted  THEN
    Begin
            Tipp_HardAI.row := Random(6); Tipp_HardAI.column :=Random(5);
    End;                             
END; (* Tipp_HardAI *)

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
VAR
tmpTipp1,tmpTipp2: tipp;
BEGIN
    Randomize;
    Init();
    ShowTable();
    Writeln('PlAYER 1 Turn');
    repeat
        repeat
            case mode of
                0: Begin tmpTipp1 := Tipp_UserInput(1); End;
                1:
                Begin
                    case counter mod 2 of
                        0: Begin tmpTipp1 := Tipp_UserInput(1); End;
                        1: Begin tmpTipp1.row := Random(6); tmpTipp1.column :=Random(5); End;
                    End;             
                End;
                2:
                Begin
                     case counter mod 2 of
                        0: Begin tmpTipp1 := Tipp_UserInput(1); End;
                        1: Begin tmpTipp1 := Tipp_HardAI(); End;
                    End;            
                End;
            end;
        until (SetTip(tmpTipp1));
        repeat
            case mode of
                0: Begin tmpTipp2 := Tipp_UserInput(2); end;
                1:
                Begin 
                    case counter mod 2 of
                        0:Begin tmpTipp2 := Tipp_UserInput(2); End;
                        1:Begin tmpTipp2.row := Random(6); tmpTipp2.column :=Random(5); End;
                    End;             
                End;
                2:
                Begin
                   case counter mod 2 of
                        0: Begin tmpTipp2 := Tipp_UserInput(2); End;
                        1: Begin tmpTipp2 := Tipp_HardAI(); End;
                    End;          
                End;
            end;
        until (SetTip(tmpTipp2));       
    until (ValidateTipps(tmpTipp1,tmpTipp2));
    EndGame();
END. (* Uebung4 *)
