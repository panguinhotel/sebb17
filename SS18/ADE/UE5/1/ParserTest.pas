PROGRAM ParserTest;

USES 
  Parser;
  
VAR
  inputFile: TEXT;
  ok: BOOLEAN;
  errLine, errColumn: INTEGER;
  
BEGIN
  IF ParamCount <> 1 THEN BEGIN 
    WriteLn('USAGE: mpi<srcfile>');
    Halt(1);
  END;
  
  Assign(inputFile, ParamStr(1));
  (*$I-*)
    Reset(inputFile);
  (*$I+*)
  IF IOResult <> 0 THEN BEGIN
    WriteLn('Error, opening source file.');
    Halt(2);
    END;
 
  InitParser(inputFile);
  ok := Parse;
  Close(inputFile);
  IF NOT ok THEN BEGIN
    WriteLn('Error at line ', errLine, ', column', errColumn);
    Halt(3);
  END ELSE Write('OK');
END.