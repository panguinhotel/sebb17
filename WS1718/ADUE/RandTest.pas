Program RandTest;
CONST
	M = 32768;
Var 
	x:LongInt;

(*Describe me*)
FUNCTION IntRand: Integer;
CONST
	A  = 3421;
	K = 1;
BEGIN
	x:= (A*x +K) MOD M;
	IntRand := x;
END; (* IntRand *)

(*Describe me*)
FUNCTION RealRand : Real;
BEGIN
	RealRand:= IntRand /M;
END; (* RealRand *)

(*Describe me*)
FUNCTION RAngeRAnd(n: INTEGER) : INTEGER;
BEGIN
	RAngeRAnd:= IntRAnd MOD n;
END; (* RAngeRAnd *)

(*Describe me*)
FUNCTION RangeRand2(n: INTEGER) : INTEGER;
Var
	k, ir :LongInt;
BEGIN
	k:= (M DIV n) * n;
	repeat
		ir:= IntRand;
	until (ir < k);
	RangeRand2:= ir MOD n;
END; (* RangeRand2 *)
(*Describe me*)
FUNCTION ApproxPi(n: LongInt) : Real;
Var	
	i,k: LongInt;
	x,y:Real;
BEGIN
	k:= 0;
	FOR i:=1 TO n DO BEGIN
		x:= RealRand;
		y:= RealRand;
		IF x*x+y*y <= 1  THEN BEGIN
			k:=k+1;
		END;
	END;
	ApproxPi:= 4*k/n;
END; (* ApproxPi *)

Var
	i: Integer;
begin
	x:=12345; //Random Seed;
	WriteLn(IntRand);
	WriteLn(IntRand);
	WriteLn(IntRand);

	x:= 12345;
	//FOR i:=1 TO 100 DO BEGIN
	//	Write(RAngeRAnd(100)+1, ' ');
	//END;
	FOR i:= 1 TO 10 DO BEGIN
		x:= 12345;
		WriteLn(i*100, '--->',ApproxPi(i*100));
	END;
end.