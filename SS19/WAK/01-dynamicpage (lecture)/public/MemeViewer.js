class MemeTextDragger {

    constructor(editor) {
        this.editor = editor;
        this.canvas = editor.canvas;

        this.selectText = undefined;
        this.startClientX = -1;
        this.startClientY = -1;

        this.canvas.onmousedown = this.mouseDown.bind(this);
        this.canvas.onmouseup = this.mouseUp.bind(this);
        this.canvas.onmousemove = this.mouseMove.bind(this);
    }

    mouseDown(e) {
        this.startClientX = e.clientX;
        this.startClientY = e.clientY;

        for (let txtPart of this.editor.template.textParts) {
            if (this.hitText(txtPart, this.startClientX, this.startClientY)) {
                this.selectText = txtPart;
                break;
            }
        }
    }

    hitText(textPart, clientX, clientY) {
        let textX = this.canvas.getBoundingClientRect().x + textPart.x;
        let textY = this.canvas.getBoundingClientRect().y + textPart.y;

        let box = this.editor.measureText(textPart.text);

        return textX < clientX
            && clientX < (textX + box.width)
            && textY - 20 < clientY
            && clientY < (textY + Number(this.editor.template.fontSize))
    }

    mouseMove(e) {
        if (this.selectText) {
            let dx = e.clientX - this.startClientX;
            let dy = e.clientY - this.startClientY;

            this.startClientX = e.clientX;
            this.startClientY = e.clientY;

            this.selectText.x += dx;
            this.selectText.y += dy;

            this.editor.render();
        }
    }

    mouseUp() {
        this.selectText = undefined;
    }
}

class MemeViewer {

    constructor(canvas) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext("2d");

        this.dragger = new MemeTextDragger(this);
    }

    loadTemplate(template) {
        this.template = template;
        this.img = new Image();
        this.img.onload = this.render.bind(this);
        this.img.src = this.template.image.path;

        let ratio = this.template.image.width / this.template.image.height;
        this.canvas.height = 400 / ratio;
    }

    render() {

        let ratio = this.template.image.width / this.template.image.height;
        this.ctx.clearRect(0, 0, 400, 400);
        this.ctx.drawImage(this.img, 0, 0, 400, 400 / ratio);
        this.ctx.font = "bold " + this.template.fontSize + "px Arial";
        this.ctx.fillStyle = "white";
        this.ctx.strokeStyle = "black";
        this.ctx.shadowColor = "black";
        this.ctx.shadowOffsetX = 0;
        this.ctx.shadowOffsetY = 0
        this.ctx.shadowBlur = 4;

        for (let y = 0; y < this.template.textParts.length; y++) {
            let txtPart = this.template.textParts[y];

            var txt = txtPart.text.split("\n");
            for (let i = 0; i < txt.length; i++) {
                this.ctx.fillText(txt[i], txtPart.x, txtPart.y + (i * this.template.fontSize + 5));
                this.ctx.strokeText(txt[i], txtPart.x, txtPart.y + (i * this.template.fontSize + 5));
            }
        }
    }

    measureText(text) {
        return this.ctx.measureText(text);
    }

    generateDataUrl() {
        return this.canvas.toDataURL('image/png');
    }

}