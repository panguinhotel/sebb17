unit UnitDate;
	
interface
TYPE   Date = RECORD    
		day: 1..31;    
		month: 1..12;   
		year: INTEGER; 
End;
(* -----------------------------------------------------------------------*)
(* -----------------------Check if Date is Valid--------------------------*)
(* -----------------------------------------------------------------------*)
FUNCTION IsValidDate(d: Date) : Boolean;
(* -----------------------------------------------------------------------*)
(* ---------------------- Displaay date   --------------------------------*)
(* -----------------------------------------------------------------------*)
PROCEDURE DisplayDate(d: Date);
(* -----------------------------------------------------------------------*)
(* ------------------Check if date1 is before date 2----------------------*)
(* -----------------------------------------------------------------------*)
FUNCTION LiesBefore(date1,date2: Date) : Boolean;
(*Converts Date to String*)
FUNCTION DateToString(d: Date) : String;
(* -----------------------------------------------------------------------*)
(* ------------------Check if date1 = date 2----------------------*)
(* -----------------------------------------------------------------------*)
FUNCTION CompareDate(date1,date2: Date) : Boolean;
	
implementation
(* -----------------------------------------------------------------------*)
(* -----------------------Check if Date is Valid--------------------------*)
(* -----------------------------------------------------------------------*)
FUNCTION IsValidDate(d: Date) : Boolean;
VAR
	isSchaltjahr : Byte;
BEGIN
	isSchaltjahr := 0;
	IsValidDate:=true;
	IF ((d.year MOD 4 = 0) and (NOT(d.year MOD 100 = 0) or (d.year MOD 400 = 0))) Then
		isSchaltjahr := 1;

	IF ((d.day < 1) or (d.day > 31) or (d.month < 1) or (d.month > 12) or (d.year < 0 ))  THEN BEGIN
		IsValidDate := false;	
		Exit;
	END;

	IF (((d.month = 4) or (d.month = 6) or (d.month = 9) or (d.month = 11)) and (d.day > 30)) or
	((d.month = 2) and (d.day > 28+isSchaltjahr)) Then IsValidDate:= false;
END; (* IsValidDate *)
(*Converts Date to String*)
FUNCTION DateToString(d: Date) : String;
VAR
 x,y,z: string;
BEGIN	
	IF IsValidDate(d) THEN begin
		Str(d.day,x);
		Str(d.month,y);
		Str(d.year,z);
		DateToString :=x+'.'+y+'.'+z;
	end		
	ELSE DateToString:='Ungültiges Datum';
END; (* DateToString *)
(* -----------------------------------------------------------------------*)
(* ---------------------- Displaay date   --------------------------------*)
(* -----------------------------------------------------------------------*)
PROCEDURE DisplayDate(d: Date);
BEGIN
	WriteLn(DateToString(d));
END; (* DisplayDate *)
(* -----------------------------------------------------------------------*)
(* ------------------Check if date1 is before date 2----------------------*)
(* -----------------------------------------------------------------------*)
FUNCTION LiesBefore(date1,date2: Date) : Boolean;
BEGIN
	LiesBefore := ((IsValidDate(date1) and IsValidDate(date2)) and 
		 ((date1.year < date2.year) or ((date1.year = date2.year) and (date1.month < date2.month)) 
		or((date1.year = date2.year) and (date1.month = date2.month) and (date1.day < date2.day))));
END; (* LiesBefore *)
(* -----------------------------------------------------------------------*)
(* ------------------Check if date1 =  date 2----------------------*)
(* -----------------------------------------------------------------------*)
FUNCTION CompareDate(date1,date2: Date) : Boolean;
BEGIN
	CompareDate := ((IsValidDate(date1) and IsValidDate(date2)) and 
				(date1.day = date2.day) and (date1.month = date2.month) and (date1.year = date2.year));
END; (* CompareDate *)	
end.