/*
 * Skip_set.h
 *
 *  Created on: 14.03.2019
 *      Author: schau
 */

#ifndef SKIP_SET_H_
#define SKIP_SET_H_

#include <algorithm>
#include <limits>
#include <stdlib.h>
#include <functional>
#include <vector>
#include <iterator>

template<typename T, const int MAXLEVEL=32>
class skip_set {
private:
	template<typename U>
	struct skip_node{
		U key;
		std::vector<skip_node*> forward;
		skip_node<U> (U k,  int level): key(k),forward(level,nullptr){}
		skip_node<U>* backward = nullptr;
	};


public:
	class skip_set_iterator;

	//typedef skip_set_iterator iterator;
	skip_set(){
		m_probability = 0.5;
		T headKey = std::numeric_limits<T>::min();
		m_head = new skip_node<T>(headKey, MAXLEVEL);

		T tailKey = std::numeric_limits<T>::max();
		m_tail = new skip_node<T>(tailKey, MAXLEVEL);

		std::fill(m_head->forward.begin(),m_head->forward.end(), m_tail);
	}
	skip_set_iterator i_find(T value){
		skip_node<T>* result = nullptr;
		if (auto x = lower_bound(value))
		{
			if(x->key == value && x != m_tail)
				result = x;
		}
		return skip_set_iterator(result);
	}
	skip_set_iterator begin(){ return skip_set_iterator(m_head);}
	skip_set_iterator end(){ return skip_set_iterator(m_tail);}


	~skip_set(){
		delete m_head;
		delete m_tail;
	}
	int size() const{
		int count = 0;
		skip_node<T>* node = m_head;
		while(node->forward.at(0) != m_tail)
		{
			node = node->forward.at(0);
			count ++;
		}
		return count;
	}
	bool find(T value){
		skip_node<T>* result = nullptr;
		if (auto x = lower_bound(value))
		{
			if(x->key == value && x != m_tail)
				result = x;
		}
		return result != nullptr;
	}
	void insert(T value){
		if(!find(value))
		{
			std::vector<skip_node<T>*> pred = predecessors(value);
			const int nextLevel = random_level();
			auto * node = make_node(value,nextLevel);

			for(int i = 0; i < nextLevel; ++i)
			{
				node->forward[i] = pred[i]->forward[i];
				pred[i]->forward[i] = node;
			}
		}

	}
	bool erease(T value){
		auto pred = predecessors(value);
		auto *node = pred.at(0)->forward.at(0);
		if(node->key != value || node == m_tail)
			return false;

		for(auto i = 0; i < node_level(node); ++i)
		{
			pred[i].forward[i] = node->forward.at(i);
		}
		delete node;

		return true;
	}




	class skip_set_iterator
	{
	public:
		skip_set_iterator() noexcept : m_currNode(nullptr){}
		skip_set_iterator(const skip_node<T>* node) noexcept : m_currNode(node){}
		skip_set_iterator& operator=(skip_node<T>* node){this->m_currNode = node; return *this;}
		skip_set_iterator& operator++(){
			if(m_currNode)
				m_currNode = m_currNode->forward[0];
			return *this;
		}
		skip_set_iterator operator++(int){
			skip_set_iterator iterate = *this;
			++*this;
			return iterate;
		}
		bool operator != (const skip_set_iterator& other){
			return m_currNode != other.m_currNode;
		}
		T operator*(){
			return m_currNode->key;
		}
		skip_node<T>* operator->()
		{
			return m_currNode;
		}
	private:
		skip_node<T>* m_currNode;
	};

private:
	skip_node<T>* m_head;
	skip_node<T>* m_tail;
	float m_probability = 0.5;
	int m_max_level = MAXLEVEL;


	skip_node<T>* lower_bound(T key) const{
		auto *node = m_head;
		for(auto i = m_head->forward.size(); i-- > 0;)
		{
			while(node->forward.at(i)->key < key)
				node = node->forward.at(i);
		}
		return node->forward.at(0);
	}
	int random_level() const{
		int tmp = 1;
		while ((static_cast<double>(std::rand()) /RAND_MAX) < m_probability && tmp < m_max_level)
		{
			tmp++;
		}
		return tmp;
	}
	skip_node<T>* make_node(T key, int level){
		return new skip_node<T>(key,level);
	}

	std::vector<skip_node<T>*> predecessors(T key) const{
		std::vector<skip_node<T>*> returnVal(m_head->forward.size(),nullptr);
		auto *node = m_head;

		for(int i = m_head->forward.size(); i-- > 0;)
		{
			while(node->forward.at(i)->key < key)
				node = node->forward.at(i);

			returnVal[i] = node;
		}

		return returnVal;
	}
};

#endif /* SKIP_SET_H_ */
