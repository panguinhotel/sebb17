UNIT MPScan;

INTERFACE
    TYPE
        Symbol = (noSym,
            beginSym, endSym, integerSym, programSym, readSym, varSym, writeSym,
            ifSym, thenSym, elseSym, whileSym, doSym,
            plusSym, minusSym, multSym, divSym, leftParSym, rightParSym,
            commaSym, colonSym, assignSym, semicolonSym, periodSym,
            identSym, numberSym);

    (* ---------------------------------------------------------------------- *)
    (* Description: Initializes the scanner with a file.                      *)
    (* Input: File containing the text to scan.                               *)
    (*        Must be open und ready for read operation                       *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE InitScanner(VAR inputFile: TEXT);

    (* ---------------------------------------------------------------------- *)
    (* Description: Gets the next symbol of the text.                         *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE GetNextSymbol;

    (* ---------------------------------------------------------------------- *)
    (* Description: Gets the current symbol of the text.                      *)
    (* Output: The current symbol of the text.                                *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION CurrentSymbol : Symbol;

    (* ---------------------------------------------------------------------- *)
    (* Description: Gets the value of the current ident.                      *)
    (* Output: Value of the current entry.                                    *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION CurrentIdentName : STRING;

    (* ---------------------------------------------------------------------- *)
    (* Description: Gets the value of the current number.                     *)
    (* Output: Value of the current entry.                                    *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION CurrentNumberValue : INTEGER;

    (* ---------------------------------------------------------------------- *)
    (* Description: Gets the current postion of the symbol in the text.       *)
    (* Input/Output:                                                          *)
    (*          line: Current line-position of the symbol.                    *)
    (*          column: Current column-position of the symbol.                *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE GetCurrentSymbolPosition(VAR line, column: INTEGER);
IMPLEMENTATION
    CONST
        TAB = Chr(9);
        LF = Chr(10);
        CR = Chr(13);
        BLANK = ' ';

    VAR
        inpFile: TEXT;
        curChar: CHAR;
        charLine, charColumn: INTEGER;
        curSymbol: Symbol;
        symbolLine, symbolColumn: INTEGER;
        curIdentName: STRING;
        curNumberValue: INTEGER;

    (* ---------------------------------------------------------------------- *)
    (* -------------------- Gets the next character ------------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE GetNextChar;
    BEGIN
        Read(inpFile, curChar);

        IF (curChar = CR) OR (curChar = LF) THEN BEGIN
            IF (curChar = LF) THEN
                charLine := charLine + 1;

            charColumn := 0;
            curChar := BLANK;
        END ELSE
            charColumn := charColumn + 1;
    END; (* GetNextChar *)

    (* ---------------------------------------------------------------------- *)
    (* ---------------------- Initializes the scanner ----------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE InitScanner(VAR inputFile: TEXT);
    BEGIN
        inpFile := inputFile;
        charLine := 1;
        charColumn := 0;
        GetNextChar;
        GetNextSymbol;
    END; (* InitScanner *)

    (* ---------------------------------------------------------------------- *)
    (* ----------------- Gets the next symbol of the text ------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE GetNextSymbol;
    BEGIN
        WHILE (curChar = BLANK) OR (curChar = Tab) DO
            GetNextChar;

        symbolLine := charLine;
        symbolColumn := charColumn;

        CASE curChar OF
            '+': BEGIN curSymbol := plusSym; GetNextChar; END;
            '-': BEGIN curSymbol := minusSym; GetNextChar; END;
            '*': BEGIN curSymbol := multSym; GetNextChar; END;
            '/': BEGIN curSymbol := divSym; GetNextChar; END;
            '(': BEGIN curSymbol := leftParSym; GetNextChar; END;
            ')': BEGIN curSymbol := rightParSym; GetNextChar; END;
            '.': BEGIN curSymbol := periodSym; GetNextChar; END;
            ',': BEGIN curSymbol := commaSym; GetNextChar; END;
            ';': BEGIN curSymbol := semicolonSym; GetNextChar; END;
            ':': BEGIN
                    GetNextChar;

                    IF curChar = '=' THEN BEGIN
                        curSymbol := assignSym;
                        GetNextChar;
                    END ELSE BEGIN
                        curSymbol := colonSym;
                    END;
                END;
            'a'..'z', 'A'..'Z', '_' : BEGIN
                curIdentName := '';
                
                WHILE curChar IN ['a'..'z', 'A'..'Z', '_'] DO BEGIN
                    curIdentName := curIdentName + UpCase(curChar);
                    GetNextChar;
                END;

                IF(curIdentName = 'BEGIN') THEN
                    curSymbol := beginSym
                ELSE IF(curIdentName = 'END') THEN
                    curSymbol := endSym
                ELSE IF(curIdentName = 'INTEGER') THEN
                    curSymbol := integerSym
                ELSE IF(curIdentName = 'PROGRAM') THEN
                    curSymbol := programSym
                ELSE IF(curIdentName = 'READ') THEN
                    curSymbol := readSym
                ELSE IF(curIdentName = 'VAR') THEN
                    curSymbol := varSym
                ELSE IF(curIdentName = 'WRITE') THEN
                    curSymbol := writeSym
                ELSE IF(curIdentName = 'IF') THEN
                    curSymbol := ifSym
                ELSE IF(curIdentName = 'THEN') THEN
                    curSymbol := thenSym
                ELSE IF(curIdentName = 'ELSE') THEN
                    curSymbol := elseSym
                ELSE IF(curIdentName = 'WHILE') THEN
                    curSymbol := whileSym
                ELSE IF(curIdentName = 'DO') THEN
                    curSymbol := doSym
                ELSE
                    curSymbol := identSym;
            END;
            '0'..'9': BEGIN
                curSymbol := numberSym;
                curNumberValue := 0;
                
                WHILE curChar IN ['0'..'9'] DO BEGIN
                    curNumberValue := curNumberValue * 10 + Ord(curChar) - Ord('0');
                    GetNextChar;
                END;
            END;
            ELSE BEGIN curSymbol := noSym; GetNextChar; END;
        END;
    END; (* GetNextSymbol *)

    (* ---------------------------------------------------------------------- *)
    (* ----------------------- Gets the current symbol ---------------------- *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION CurrentSymbol : Symbol;
    BEGIN
        CurrentSymbol := curSymbol;
    END; (* CurrentSymbol *)
   
    (* ---------------------------------------------------------------------- *)
    (* ----------------- Gets the value of the current ident ---------------- *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION CurrentIdentName : STRING;
    BEGIN
        CurrentIdentName := curIdentName;
    END; (* CurrentIdentName *)

    (* ---------------------------------------------------------------------- *)
    (* --------------- Gets the value of the current number ----------------- *)
    (* ---------------------------------------------------------------------- *)
    FUNCTION CurrentNumberValue : INTEGER;
    BEGIN
        CurrentNumberValue := curNumberValue;
    END; (* CurrentNumberValue *)

    (* ---------------------------------------------------------------------- *)
    (* -------- Gets the current postion of the symbol in the text ---------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE GetCurrentSymbolPosition(VAR line, column: INTEGER);
    BEGIN
        line := symbolLine;
        column := symbolColumn;
    END; (* GetCurrentSymbolPosition *)
END. (* MPScan *)