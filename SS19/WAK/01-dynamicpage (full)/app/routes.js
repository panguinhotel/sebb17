module.exports = function (express) {
    var router = express.Router();
   router.use('/', require('./api'));
    return router;
}