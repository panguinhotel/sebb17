package swe4.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import java.util.Iterator;

public class LinkedListChainingHashMap<K,V> implements HashMap<K,V> 
{	
	private ArrayList<LinkedList<Pair<K,V>>> m_values;
	private Hasher<K> m_hasher;
	private double m_maxLoadFactor = 0;
	private int m_capacity = 0;

	public class BaseHash<K> implements Hasher<K> 
	{		
		public int hash(K key) {
			return key.hashCode();
		}
	}
	public class LinkedListChainingHashMapIterator<KeyValuePair> implements Iterator<KeyValuePair> 
	{
		Pair<K,V> next = null;
		
		public LinkedListChainingHashMapIterator()
		{
			ArrayList<LinkedList<Pair<K,V>>> tmpVal = LinkedListChainingHashMap.this.m_values;
			for(int i = 0; i < tmpVal.size(); ++i)
			{
				if(tmpVal.get(i) != null)
				{
					next = tmpVal.get(i).getFirst();
					break;
				}
			}
		} 
		
		@Override
		public boolean hasNext() {
			return next != null;
		}

		@Override
		public KeyValuePair next()
		{
			int hash = LinkedListChainingHashMap.this.getHash(next.getKey());
			ArrayList<LinkedList<Pair<K,V>>> tmpVal = LinkedListChainingHashMap.this.m_values;
			for(int i = 0 ; i < tmpVal.get(hash).size(); ++i)
			{
				if(tmpVal.get(hash).get(i) == next)
				{
					if(i+1 < tmpVal.get(hash).size())
					{
						next = tmpVal.get(hash).get(i+1);
					}
					else if(hash+1 < tmpVal.size())
					{
						for(int k = hash+1; k < tmpVal.size(); ++k)
						{
							if(tmpVal.get(k) != null)
							{
								next = tmpVal.get(k).getFirst();
								break;
							}
						}
					}
					else
						next = null;
					
					return (KeyValuePair) tmpVal.get(hash).get(i);
				}
			}
			
			return null;			
		}

	}
	public class Pair<K,V> implements KeyValuePair<K,V>
	{
		public Pair(K key, V value)
		{
			m_key = key;
			m_value = value;
		}
		@Override
		public K getKey() {
			// TODO Auto-generated method stub
			return m_key;
		}

		@Override
		public V getValue() {
			// TODO Auto-generated method stub
			return m_value;
		}
		
		public void setValue(V value) {m_value = value;}
		public void setKey(K key) {m_key = key;}
		
		private K m_key;
		private V m_value;
	}
	
	public LinkedListChainingHashMap(int capacity,double maxLoadFactor)
	{
		m_maxLoadFactor = maxLoadFactor;
		m_hasher = new BaseHash<K>();
		m_values = new ArrayList<LinkedList<Pair<K,V>>>();
		m_capacity = capacity;
		for(int i = 0; i < capacity; i++)
		{
			m_values.add(null);
		}
	}

	public LinkedListChainingHashMap(int capacity,double maxLoadFactor,Hasher<K> hasher)
	{
		m_maxLoadFactor = maxLoadFactor;
		m_values = new ArrayList<LinkedList<Pair<K,V>>>();
		m_capacity = capacity;
		for(int i = 0; i < capacity; i++)
		{
			m_values.add(null);
		}
		m_hasher = hasher;
	}

	@Override
	public Iterator<KeyValuePair<K, V>> iterator() {
		return new LinkedListChainingHashMapIterator<>();
	}

	@Override
	public V get(K key) 
	{
		int idx = getHash(key);
		
		if(m_values.get(idx) != null)
		{
			for(int i = 0; i < m_values.get(idx).size(); i++)
			{
				if(m_values.get(idx).get(i).getKey() == key)
					return m_values.get(idx).get(i).getValue();
			}
		}

		throw new NoSuchElementException();				
	}
	
	@Override
	public boolean containsKey(K key) 
	{
		int idx = getHash(key);
		if(m_values.get(idx) != null)
		{
			for(int i = 0; i < m_values.get(idx).size(); i++)
			{
				if(m_values.get(idx).get(i).getKey() == key)
					return true;
			}
		}

		return false;
	}

	@Override
	public boolean containsValue(V value) 
	{
		for(int k = 0; k < m_values.size(); ++k)
		{
			if(m_values.get(k) != null)
			{
				for(int i = 0; i < m_values.get(k).size(); i++)
				{
					if(m_values.get(k).get(i).getValue() == value)
						return true;
				}
			}
		}		
		return false;
	}

	@Override
	public V put(K key, V value) 
	{
		if(getLoadFactor() > getMaxLoadFactor())
			rehash();
		
		int hash = getHash(key);
		LinkedList<Pair<K,V>> tmpVal = m_values.get(hash);
		V retVal = null;
		if(tmpVal == null)
			tmpVal = new LinkedList<Pair<K,V>>();
		for(int i = 0; i < tmpVal.size(); ++i)
		{
			if(tmpVal.get(i).getKey() == key)
			{
				retVal = tmpVal.get(i).getValue();
				tmpVal.set(i,new Pair<K,V>(key,value));
				m_values.set(hash,tmpVal);
			}
		}

		if(retVal == null)
			tmpVal.add(new Pair<K,V>(key,value));
		
		m_values.set(hash,tmpVal);
		
		return retVal;
	}

	@Override
	public boolean isEmpty() 
	{
		for(int i = 0; i < m_values.size(); i++)
		{
			if(m_values.get(i) != null)
				return false;
		}
		
		return true;
	}

	@Override
	public V remove(K key) 
	{
		int hash = getHash(key);
		LinkedList<Pair<K,V>> tmpVal = m_values.get(hash);
		V retVal = null;
		if(tmpVal == null)
			return null;
		
		for(int i = 0; i < tmpVal.size(); ++i)
		{
			if(tmpVal.get(i).getKey() == key)
			{
				retVal = tmpVal.get(i).getValue();
				tmpVal.remove(i);
				m_values.set(hash,tmpVal);
			}
		}
		m_values.set(hash,tmpVal);	
		return retVal;
	}

	@Override
	public void clear() {
		for(int i = 0; i < m_values.size(); i++)
		{
			m_values.set(i, null);
		}
	}

	@Override
	public int size() 
	{
		int retVal = 0;
		for(int i = 0; i < m_values.size(); i++)
		{
			retVal += m_values.get(i) == null ? 0 :  m_values.get(i).size();
		}
		return retVal;
	}

	@Override
	public Iterable<K> keys() 
	{
		ArrayList<K> tmp = new ArrayList<K>();
		Iterator<KeyValuePair<K, V>>  it = iterator();
		
		while(it.hasNext())
		{
			tmp.add(it.next().getKey());
		}
		return tmp;
	}

	@Override
	public Iterable<V> values() 
	{
		ArrayList<V> tmp = new ArrayList<V>();
		Iterator<KeyValuePair<K, V>>  it = iterator();
		
		while(it.hasNext())
		{
			tmp.add(it.next().getValue());
		}
		return tmp;
	}

	@Override
	public double getMaxLoadFactor() 
	{
		return m_maxLoadFactor;
	}

	@Override
	public double getLoadFactor() 
	{
		return m_capacity == 0 ? 0 : size()*1.0/m_capacity;
	}

	@Override
	public void rehash() 
	{
		ArrayList<Pair<K,V>> tmp = new ArrayList<Pair<K,V>>();
		for(int i = 0; i < m_values.size(); ++i)
		{
			if(m_values.get(i) != null)
			{
				for(int k = 0; k < m_values.get(i).size(); ++k)
				{
					tmp.add(m_values.get(i).get(k));
				}
			}
		}
		clear();
		m_maxLoadFactor *= 2;
		for(int i = 0; i < tmp.size(); ++i)
		{
			put(tmp.get(i).getKey(), tmp.get(i).getValue());
		}

	}
	
	private int getHash(K key)
	{
		return m_hasher.hash(key)%m_capacity;
	}

}
