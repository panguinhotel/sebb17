# Exercise 1 - NodeJS

## Preparation

### 1. Install NodeJS
https://nodejs.org/en/download/

### 2. Install dependencies

```
npm install
```

## Coding

### 1. Configure HandlebarsJS for Express

See the documentation of the HandlbarsJS integration with express. https://github.com/ericf/express-handlebars 

server.js
```
var hbs = exphbs.create({
    defaultLayout: ...,
    helpers: {
        formatDate: function (datetime, format) {
            // return the datetime number as formatted string
            // use moment.js for the formating
        }
    },
    partialsDir: ...
});

app.engine(...);
app.set(...);
```

### 2. Implement first controller for meme gallery 

controller.js
```
router.get('/', function (req, res) {
    // use the local meme service to load all entries and render the gallery template with the correct menu settings
});
```

### 3. Write first handlebars template 

The snippet shows a template with pseudo code. Replace the pseduo code and the constant values with the correct template syntax. The list of memes are stored in the context object with the property name 'memes'

gallery.handlebars
```
//include navigation

<div>
    <div id="comments">
        //iterate over the memes array
        <div>
            <div>
            <div class="panel-heading">
                Thomas, May 3, 2018 11:21 PM                                     <-- use variables
            </div>
            <div>
                <img src="./memes/f7028620-4f17-11e8-bc60-572f9f5d432b.png">     <-- use variables
            </div>
        </div>
        </div>
        // iterate done
    </div>
</div>
```

### 4. Register the controllers

server.js
```
app.use('/', require('./app/controllers')(express));
```
### 5. Implement a navigation template

The result of the navigation template should look like the following html snippet. The template should use the context data 'menus'

navigation.handlebars
```
<nav>
    <a class="active" href="./">Gallery</a>
    <a href="./creator">Creator</a>
</nav>
```

### 6. Create a controller for the creator page

controller.js
```
router.get('/creator', function (req, res) {
    // render the creator template with the correct menu settings
});
```

### 7. Create a REST api to fetch all templates

templates.js
```
router.get('/', function (req, res) {
    // read all templates from the templates.json file and return the content as json
});
```

### 8: Register the REST api routes

server.js
```
app.use('/api', require('./app/routes')(express));
```

### 9: Implement a new version of the meme service, which stores the meta data of the memes into a mongo db

1. Setup monogo db locally. You can install it direclty on your os or you can use docker. If you use docker, you can run mongodb with the following command:

```
docker run -d -p 27017:27017 mongo
```
2. mongodb should run on port 27017
3. the code doesn't use any authentication
4. implement a new nodejs module 

mongoMemeService.js
```
const MongoClient = require('mongodb').MongoClient;
const memeDirectory = './memes'
const url = 'mongodb://localhost:27017';
const dbName = 'memeDB';
const collection = 'memes';

class MemeService {

    constructor() {
      // connect to mongo db
    }

    loadMemes(callback) {
        // load memes from db
    }

    storeMeme(meme, callback) {
        const id = uuid();

        // store the images on the local disc

        // store the meta data object into the db. The stored object should look like 
        <!-- {
            "id":"01d98eb0-4f16-11e8-a643-4d4dd4aba3b4",
            "imageUrl":"./memes/01d98eb0-4f16-11e8-a643-4d4dd4aba3b4.png",
            "createdAt":1525381657372,
            "creator":"Mr. X"
        } -->
    }
}
```


## Solutions

### 1. Configure HandlebarsJS for Express

server.js
```
var hbs = exphbs.create({
    defaultLayout: 'main',
    helpers: {
        formatDate: function (datetime, format) {
            if (moment) {
                return moment(datetime).format('lll');
            }
            else {
                return datetime;
            }
        }
    },
    partialsDir: [
        'views/partials/'
    ]
});

app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
```

### 2. Implement first controller for meme gallery 

controller.js
```
router.get('/', function (req, res) {
    memeService.loadMemes((err, data) => {
        res.render('gallery', {
            title: 'Meme Generator - Gallery',
            memes: data,
            menus: [
                { name: 'Gallery', url: './', active: true },
                { name: 'Creator', url: './creator', active: false }
            ]
        })
    });
});
```

### 3. Write first handlebars template 

gallery.handlebars
```
{{> navigation}}

<div>
    <div id="comments">
        {{#each memes}}
        <div>
            <div class="panel-heading">
                {{creator}}, {{formatDate createdAt}}
            </div>
            <div>
                <img src="{{imageUrl}}">
            </div>
        </div>
        {{/each}}
    </div>
</div>
```

### 5. Implement a navigation template
navigation.handlebars
```
<nav>
    {{#each menus}} {{#if active}}
    <a class="active" href="{{url}}">{{name}}</a>
    {{else}}
    <a href="{{url}}">{{name}}</a>
    {{/if}} {{/each}}
</nav>
```

### 6. Create a controller for the creator page

controller.js
```
router.get('/creator', function (req, res) {
    res.render('creator', {
        title: 'Meme Generator - Creator',
        menus: [
            { name: 'Gallery', url: './', active: false },
            { name: 'Creator', url: './creator', active: true }
        ]
    });
});
```

### 7. Create a REST api to fetch all templates

templates.js
```
router.get('/', function (req, res) {
    fs.readFile('./templates.json', 'utf8', function (err, data) {
        res.json(JSON.parse(data));
    });
});
```

### 9: Implement a new version of the meme service, which stores the meta data of the memes into a mongo db

mongoMemeService.js
```
const MongoClient = require('mongodb').MongoClient;
const memeDirectory = './memes'
const url = 'mongodb://localhost:27017';
const dbName = 'memeDB';
const collection = 'memes';

class MemeService {

    constructor() {
        MongoClient.connect(url, (err, client) => {
            this.client = client;
            this.db = client.db(dbName);
        });
    }

    loadMemes(callback) {
        const col = this.db.collection(collection);
        col.find({}).toArray(function (err, docs) {
            callback(err, docs.sort((o1, o2) => o2.createdAt > o1.createdAt));
        });
    }

    storeMeme(meme, callback) {
        const id = uuid();

        fs.writeFile(`${memeDirectory}/${id}.png`, meme.data, {
            encoding: 'base64'
        });

        const col = this.db.collection(collection);
        col.insert({
            id: id,
            imageUrl: `./memes/${id}.png`,
            createdAt: new Date().getTime(),
            creator: meme.creator
        }, (err, result) => callback());
    }
}
```