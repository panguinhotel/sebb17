var i;

var content = document.getElementById("image_modal_text");
var container = document.getElementById("modalContainer");
var image_modal = document.getElementById("modal_img");
var image = document.getElementById("mapImage");

var emperors = document.getElementsByName("imgForModal");
for (i = 0; i < emperors.length; i++) {
    emperors[i].addEventListener("click", showModal);
}

function showModal() {
    container.style.display = "block";
    image_modal.src = this.src;
    content.innerHTML = this.alt;
}

image_modal.onclick = function hideModal() {
    container.style.display = "none";
}

function onKeyEvent(event) {
    if (container.style.display == "block") {
        var prevIdx = 0;
        var nextIdx = emperors.length - 1;
        for (i = 0; i < emperors.length; i++) {
            if (image_modal.src == emperors[i].src) {
                prevIdx = Math.max(prevIdx, i - 1);
                nextIdx = Math.min(nextIdx, i + 1);
                break;
            }
        }

        switch (event.keyCode) {
            case 37: //Left Key                       
                image_modal.src = emperors[prevIdx].src;
                content.innerHTML = emperors[prevIdx].alt;
                break;
            case 39: //Right Key
                image_modal.src = emperors[nextIdx].src;
                content.innerHTML = emperors[nextIdx].alt;
                break;
            case 27: //Escape Key
                container.style.display = "none";
                break;
            default:
                break;
        }
    }
}

function PreviusPicture() {
    var prevIdx = 0;
    for (i = 0; i < emperors.length; i++) {
        if (image_modal.src == emperors[i].src) {
            prevIdx = Math.max(prevIdx, i - 1);
            break;
        }
    }

    image_modal.src = emperors[prevIdx].src;
    content.innerHTML = emperors[prevIdx].alt;
}

function NextPicture() {
    var nextIdx = emperors.length - 1;
    for (i = 0; i < emperors.length; i++) {
        if (image_modal.src == emperors[i].src) {
            nextIdx = Math.min(nextIdx, i + 1);
            break;
        }
    }
    image_modal.src = emperors[nextIdx].src;
    content.innerHTML = emperors[nextIdx].alt;
}