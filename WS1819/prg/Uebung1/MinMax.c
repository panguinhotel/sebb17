#include <stdio.h>

int main(int argc, char const *argv[])
{
	int min = 0;
	int max = 0;
	for(int i = 0; i < argc; ++i)
	{
		int num = 0;
		sscanf(argv[i], "%d",&num);
		if(num < 0 && num < min)
			min = num;
		else if(num > 0 && num > max)
			max = num;
	}
	printf("Minimum = %d\n",min);
	printf("Maximum = %d\n" ,max);
	return 0;
}
