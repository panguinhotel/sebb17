SET AUTOTRACE ON;

SELECT f.rating,k.name, Count(*) AS Anzahl , Sum(Length) AS Längensumme
FROM film f
LEFT JOIN film_category c ON f.film_id = c.film_id
LEFT JOIN category k ON k.category_id = c.category_id
WHERE k.name IN ('Comedy','Music')
GROUP BY ROLLUP ((f.rating),(f.rating,k.name))
ORDER BY f.rating,k.name;

SELECT f.rating, k.name, Count(*) AS Anzahl , Sum(Length) AS Längensumme
FROM film f
LEFT JOIN film_category c ON f.film_id = c.film_id
LEFT JOIN category k ON k.category_id = c.category_id
WHERE k.name IN ('Comedy','Music')
GROUP BY GROUPING SETS ((f.rating),(f.rating,k.name),())
ORDER BY f.rating,k.name;

SELECT rating,name, Count(*) AS Anzahl , Sum(Length) AS Längensumme
FROM film 
LEFT JOIN film_category using(film_id)
LEFT JOIN category using(category_id)
WHERE name IN ('Comedy','Music')
GROUP BY rating,name
UNION ALL
SELECT rating,null, Count(*) AS Anzahl , Sum(Length) AS Längensumme
FROM film 
LEFT JOIN film_category using(film_id)
LEFT JOIN category using(category_id)
WHERE name IN ('Comedy','Music')
GROUP BY rating
union all
SELECT null,null, Sum(Count(*)) AS Anzahl , Sum(Sum(Length)) AS Längensumme
FROM film 
LEFT JOIN film_category using(film_id)
LEFT JOIN category using(category_id)
WHERE name IN ('Comedy','Music')
GROUP BY rating
ORDER BY rating;