/*
 * skipnode.h
 *
 *  Created on: 22.03.2019
 *      Author: Lukas Schaubmayr
 */

#ifndef SKIPNODE_H_
#define SKIPNODE_H_

#include <vector>


template<typename T>
struct skip_node {
public:
	T key;
	std::vector<skip_node*> forward;
	skip_node<T> (T k,  int level): key(k),forward(level,nullptr){}
	skip_node<T>* backward = nullptr;
};

#endif /* SKIPNODE_H_ */
