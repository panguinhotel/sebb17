/*
 * skipsetiterator.h
 *
 *  Created on: 22.03.2019
 *      Author: Lukas Schaubmayr
 */

#ifndef SKIPSETITERATOR_H_
#define SKIPSETITERATOR_H_

#include "skipnode.h"

template<typename T>
class skip_set_iterator {
	friend class skip_node<T>;
public:
	skip_set_iterator() = delete;
		explicit skip_set_iterator(skip_node<T> * current)
			: m_currNode{current} {
		}

		skip_set_iterator& operator++() {
			if(m_currNode != nullptr) {
				m_currNode = m_currNode->forward[0];
			}

			return *this;
		}

		skip_set_iterator operator++(int) {
			if(m_currNode != nullptr) {
				auto * old_this = *this;
				m_currNode = m_currNode->forward[0];
				return old_this;
			}
			else
				return *this;
		}

		skip_set_iterator& operator--() {
			if(m_currNode!= nullptr) {
				m_currNode = m_currNode->backward;
			}

			return *this;
		}

		skip_set_iterator operator--(int) {
			if(m_currNode != nullptr) {
				auto * old_this = *this;
				m_currNode = m_currNode->backward;
				return old_this;
			}
			else
				return *this;
		}

		bool operator == (const skip_set_iterator &it) const {return m_currNode == it.m_currNode;};
		bool operator != (const skip_set_iterator &it) const {return !operator==(it);};

		T operator * () { return m_currNode->key; }
		T operator -> () { return operator*(); }
private:
	skip_node<T>* m_currNode;
};

#endif /* SKIPSETITERATOR_H_ */
