#include <stdio.h>
// printf("format", wert1, wert2);
// printf("i - %2d, j - %2d",i,j);
// %d Ganzzahl
// %u short int
// %o oktal formatiert
// %x hexadezimal formatiert
// %p Zeiger
// %e Fliesskomma in Exponentialdarstellung
// %f Fliesskommazahl
// %s Zeichenkette
// %c einzelnes Zeichen (char)

// %6d Interger : 6 Stellen
// %1d long int
// %*d, int , Anzahl Stellen als zusätzlicher Parameter
// %06d 6 Stellen mit führender Null
// %6.2d insg 6 Stellen davon 2 Nachkoma
// %20s Zeichenkette (20 Stellen rechtsbündig)
// %-20s Zeichenkette (20 Stellen linksbündig)
// %% -> ein Prozentzeichen

int main(int argc, char const *argv[])
{
    int i = 24;
    float x = 2.718;
    char *s = "addition";
    printf("%-12s: %03d + %f = %10.2f\n", s,i,x,i+x);
    printf("%0*.*f\n",8,3,12.34);
    return 0;
}
