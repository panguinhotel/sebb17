import java.awt.Color;
import java.awt.image.DirectColorModel;

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.Plot;
import ij.plugin.ChannelSplitter;
import ij.plugin.RGBStackMerge;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

public class UE5_ implements PlugInFilter {

	public int setup(String arg, ImagePlus imp) {
		if (arg.equals("about"))
		{showAbout(); return DONE;}
		return DOES_RGB+DOES_8G+DOES_STACKS+SUPPORTS_MASKING;
	} //setup


	public void run(ImageProcessor ip) 
	{
		//Array for images to display
		ImagePlus[] images = new ImagePlus[1];

		//1.1		
		if(ip.getColorModel() instanceof DirectColorModel) // Check if ColorModel is DirectColorModel(=RGB)
		{
			images = ChannelSplitter.split(IJ.getImage()); //Split RGB to seperate Channels
			ImagePlus[] toMergeimages = new ImagePlus[images.length]; //Create Array to put translated images in to put back together after Translation
			for(int i = 0; i < images.length; ++i) //Iterate over images
			{
				images[i].show(); //Show Original RGB - Channel Image
				//Display Translated Image + corresponding Plot
				displayTranslatesImage(
						images[i].getTitle(),
						(byte[])images[i].getProcessor().getPixels(),
						images[i].getProcessor().getWidth(),
						images[i].getProcessor().getHeight());
				//Set Tanslated Image on toMergeimages[i]
				toMergeimages[i] = WindowManager.getImage(images[i].getTitle()+"-Translated");
			}

			//Merge Images back together and show
			RGBStackMerge.mergeChannels(toMergeimages,true).show();
			
			
			//3
			int width = ip.getWidth();
			int height = ip.getHeight();
			
			//Create HSV Arrays
			float[] H = new float[width*height];
			float[] S = new float[width*height];
			byte[] V_byte = new byte[width*height];
			
			for(int i = 0; i < width; ++i)
			{
				for(int k = 0; k < height; ++k)
				{
					//Get RGB Pixel Value
					int[] rgbValue = new int[3];
					ip.getPixel(i,k,rgbValue);
					
					//Convert RGB to HSV
					float[] hsvVals = new float[3];
					Color.RGBtoHSB(rgbValue[0],rgbValue[1],rgbValue[2],hsvVals);
					
					//Fill HSV Arrays
					H[k*width+i] = hsvVals[0];
					S[k*width+i] = hsvVals[1];
					V_byte[k*width+1] = (byte)(hsvVals[2]*255);
				}
			}
			
			//Translate V value Array
			int[][] inDataArrInt = ImageJUtility.convertFrom1DByteArr(V_byte, width, height);
			int[] translData = histoFunction(V_byte,width,height,255);
			int[][] invertedImg = ImageTransformationFilter.getTransformedImage(inDataArrInt, width, height,translData);
			//Create Array for Merged RGB Image
			int[][][] rgbImage = new int[width][height][3];
			for(int i = 0; i < width; ++i)
			{
				for(int k = 0; k < height; ++k)
				{
					float v = (float) (invertedImg[i][k]/255.0);
					//Convert HSV to RGB 
					int rgbPixel = Color.HSBtoRGB(H[k*width+i], S[k*width+i], v);
					//Fill RGB Array with data
					rgbImage[i][k][0] = (rgbPixel >> 16) & 0xFF;
					rgbImage[i][k][1] = (rgbPixel >> 8) & 0xFF;
					rgbImage[i][k][2] = (rgbPixel >> 0) & 0xFF;			
				}
			}
			
			//Show Merged RGB Image 
			ImageJUtility.showNewImageRGB(rgbImage, width, height, "HSV - RGB Image");
			
		}
		else
		{
			//IF grayscale image only display transplated image
			displayTranslatesImage(IJ.getImage().getTitle(),(byte[])ip.getPixels(),ip.getWidth(),ip.getHeight());
		}

		
	}
	
	void displayTranslatesImage(String titleprefix, byte[] pixels, int width, int height)
	{
		int[][] inDataArrInt = ImageJUtility.convertFrom1DByteArr(pixels, width, height);
		int[] translData = histoFunction(pixels,width,height,255);
		int[][] invertedImg = ImageTransformationFilter.getTransformedImage(inDataArrInt, width, height,translData);
		ImageJUtility.showNewImage(invertedImg, width, height, titleprefix+"-Translated");
	
		createPlot(histoFunction(pixels,width,height,255),titleprefix+"-Translated-Chart").show();
	}
	
	Plot createPlot(int[] dataArr, String title)
	{
		float[] yArr = new float[dataArr.length];
		float[] xArr = new float[dataArr.length];
		
		for(int k = 0; k < dataArr.length; ++k)
		{
			xArr[k] = k;
			yArr[k] = dataArr[k];
		}
		
		return  new Plot(title,"X","Y",xArr,yArr);
	}

	int[] histoFunction(byte[] inImg, int width, int height, int maxVal)
	{
		int[] arrT = new int[256];
		int[] counts = new int[width*height];

		int amin = 256;
		for(int i = 0; i < width*height; i++)
		{
			if(inImg[i] >= 0)
			{
				counts[inImg[i]]++;

				if(amin > inImg[i])
					amin = inImg[i];
			}
		}

		int sum=0;
		for(int i = 0; i < 255; i++)
		{
			sum+=counts[i];			
			arrT[i] = (int) (sum*255.0/(width*height-counts[amin]));		
		}
		arrT[amin] = 0;
		return arrT;
	}

	void showAbout() {
		IJ.showMessage("About Template_...",
				"this is a PluginFilter template\n");
	} //showAbout

}
