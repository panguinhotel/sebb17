package tests.sax;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class SAXWriterTest {

  public static void main(String[] args) {
    System.out.println("SAXWriterTest BEGIN");

    SAXParserFactory factory = SAXParserFactory.newInstance();
    factory.setValidating(true);
    SAXParser saxParser;
    String xmlFilesDir = "xmlfiles/";
    String inFile = xmlFilesDir + "10Entries.xml";
    String outFile = xmlFilesDir + "10Entries_SAX_out_" + new Date().toString().replaceAll("[ :]", "_") + ".xml";
    try {
      saxParser = factory.newSAXParser();
      // parse
      File file = new File(inFile);
      SAXWriteElementsHandler writeHandler = new SAXWriteElementsHandler();

      OutputStream os = new FileOutputStream(new File(outFile));
      PrintWriter writer = new PrintWriter(os);
      writeHandler.setWriter(writer);

      //begin-d
      StartMemoryProfiling();
      StartTimeProfiling();
      //end-d
      saxParser.parse(file, writeHandler);
      //begin-d
      StopMemoryProfiling();
      StopTimeProfiling();
      System.out.println("Time : " + Difference());
      System.out.println("Memory : " + MemoryUsageInBytes());
      //end-d
      
    } catch (Throwable e) {
      System.out.println("Exception Type: " + e.getClass().toString());
      System.out.println("Exception Message: " + e.getMessage());
      e.printStackTrace();
    }
    System.out.println("A new file was created in " + outFile);

    System.out.println("SAXWriterTest END");
  }
  

  private static long startDate;
  private static long endDate;
  private static boolean timeProfilingStarted = false;

  public static void StartTimeProfiling() {
    if (!timeProfilingStarted) {
      startDate = System.currentTimeMillis();
      timeProfilingStarted = true;
    }
  }

  public static void StopTimeProfiling() {
    endDate = System.currentTimeMillis();
    timeProfilingStarted = false;
  }

  /**
   * 
   * @return the time difference in milliseconds
   */
  public static long Difference() {
    return (endDate - startDate);
  }

  /**
   * 
   * @return the time difference in seconds
   */
  public static long TimeDifferenceInSeconds() {
    return Difference() / 1000;
  }

  private static long startMemory;
  private static long endMemory;
  private static boolean memoryProfilingStarted = false;

  public static void StartMemoryProfiling() {
    if (!memoryProfilingStarted) {
      startMemory = GetSystemUsedMemory();
      memoryProfilingStarted = true;
    }
  }

  private static long GetSystemUsedMemory() {
    return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
  }

  public static void StopMemoryProfiling() {
    endMemory = GetSystemUsedMemory();
    memoryProfilingStarted = false;
  }

  private static long MemoryUsageInBytes() {
    return (endMemory - startMemory);
  }

  public static long MemoryUsageInMB() {
    return MemoryUsageInBytes() / 1024 / 1024;
  }

  public static void Reset() {
    memoryProfilingStarted = false;
    timeProfilingStarted = false;
  }
}
