package swe4.collections;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Objects;

public class SetMain {

	public static class Person implements Comparable<Person> {

		private String firstName, lastName;
		private int age;
		
		
		public Person(String firstName, String lastName, int age) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.age = age;
		}


		@Override
		public String toString() {
			//return "Person [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
			return String.format("Person [firstName=%s, lastName=%2,age=%s]", firstName,lastName,age);
		}
		
	}

	public static void main(String[] args) {
		System.out.println("---- SortedMultiSet<String> ----");

		SortedMultiSet<String> strSet = new BSTMultiSet<>();
		strSet.add("B");
		strSet.add("A");
		strSet.add("C");
		strSet.add("C");
		System.out.printf("strSet = %s%n", strSet);
		System.out.printf("strSet.get(\"A\") -> %s%n", strSet.get("A"));
		System.out.printf("strSet.contains(\"A\") -> %b%n", strSet.contains("A"));
		System.out.printf("strSet.contains(\"X\") -> %b%n", strSet.contains("X"));

		System.out.println("enumerate elements of strSet");
		for (String s : strSet)
			System.out.printf("%s, ", s);
		System.out.println();

		System.out.println("---- SortedMultiSet<Integer> ----");
		SortedMultiSet<Integer> intSet = new BSTMultiSet<>();
		intSet.add(2);
		intSet.add(1);
		intSet.add(3);
		intSet.add(10);
		intSet.add(5);
		System.out.printf("intSet.get(3) -> %d%n", intSet.get(3));
		System.out.printf("intSet.contains(5) -> %b%n", intSet.contains(5));
		System.out.printf("intSet.contains(99) -> %b%n", intSet.contains(88));
		System.out.printf("intSet  = %s%n", intSet);
		System.out.println("enumerate elements using iterator interface:");
		Iterator<Integer> intIt = intSet.iterator();
		while (intIt.hasNext())
			System.out.print(intIt.next() + " ");
		System.out.println();

		System.out.println("enumerate elements using for loop:");
		for (int i : intSet)
			System.out.print(i + " ");
		System.out.println();

		System.out.println("enumerate using forEach");
		intSet.forEach(i -> System.out.printf("%d, ", i));
		System.out.println();

		SortedMultiSet<Person> personSet = new BSTMultiSet<>();
		personSet.add(new Person("Franz", "Mayr", 30));
		personSet.add(new Person("Josef", "Huber", 20));
		personSet.add(new Person("Fritz", "Müller", 35));
		System.out.printf("personSet = %s%n", personSet);
	}
}