PROGRAM MPC;

USES MPPCOM;

VAR
	inputFile: Text;
	outputFile: FILE;
	ok : BOOLEAN;
	errLine, errColumn : Integer;
	errTxt: String;
begin
	IF ParamCount < 1 THEN BEGIN
		WriteLn('Error: No sourcefile');
		Halt(1);
    END;
		Assign(inputFile, ParamStr(1));
		(*$I-*)
		Reset(inputFile);
		(*$I+*)
		IF IOResult <> 0 THEN BEGIN
			WriteLn('Error, opening source file.');
			Halt(3);
    	END;
		Assign(outputFile,ParamStr(1)+'_c');
		(*$I-*)
		Rewrite(outputFile);
		(*$I+*)

		Parse(inputFile,outputFile,ok,errLine,errColumn,errTxt);
		IF NOT ok THEN WriteLn('Error at line ', errLine, 'column ', errColumn, ': ', errTxt);
		Close(outputFile);
		Close(inputFile);

end.