const memeService = require("./helpers/mongoMemeService")
// const memeService = require("./helpers/localMemeService")

module.exports = function (express) {
    const router = express.Router();

    router.get('/', function (req, res) {
        memeService.loadMemes((err, data) => {
            res.render('gallery', {
                title: 'Meme Generator - Gallery',
                memes: data,
                menus: [
                    { name: 'Gallery', url: './', active: true },
                    { name: 'Creator', url: './creator', active: false }
                ]
            })
        });
    });

    router.get('/creator', function (req, res) {
        res.render('creator', {
            title: 'Meme Generator - Creator',
            menus: [
                { name: 'Gallery', url: './', active: false },
                { name: 'Creator', url: './creator', active: true }
            ]
        });
    });

    return router;
}