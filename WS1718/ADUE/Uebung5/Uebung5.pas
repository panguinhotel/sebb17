PROGRAM Uebung5;

Uses UnitDate,UnitTest;
VAR
	res:Response;
	d,e,x:Date;
	tmp : Testresult;
	i,k:Integer;
(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	d.day:= 30;
	d.month := 1;
	d.year := 1;
	tmp.Name := 'TMP_TEST';
	tmp.DurationTime := 1000;
	tmp.Result := failed;
	WriteLn('TEST UNITDATE MODULE');
	x.day := 1; x.month := 1; x.year := 1; WriteLn('TEST IS VALID DATE DATE: 1.1.1  EXPECTED : TRUE   REUSLT :',IsValidDate(x));
	x.day := 1; x.month := 1; x.year := 1; Write('TEST DISPLAY DATE: 1.1.1  EXPECTED : 1.1.1   REUSLT :');DisplayDate(x);
	x.day := 1; x.month := 1; x.year := 1; WriteLn('TEST LIESBEFORE: 1.1.1 & 30.1.1  EXPECTED : TRUE   REUSLT :',LiesBefore(x,d));
	x.day := 1; x.month := 1; x.year := 1; WriteLn('TEST LIESBEFORE: 30.1.1 & 1.1.1  EXPECTED : FALSE   REUSLT :',LiesBefore(d,x));
	x.day := 1; x.month := 1; x.year := -5; WriteLn('TEST IS VALID DATE DATE: 1.1.-5  EXPECTED : FALSE   REUSLT :',IsValidDate(x));
	x.day := 1; x.month := 1; x.year := -5; Write('TEST DISPLAY DATE: 1.1.-1  EXPECTED : Ungültiges Datum   REUSLT :');DisplayDate(x);
	WriteLn();
	WriteLn('TEST UNITTEST MODULE');
	e.day:= 30;e.month:= 2; e.year := 1000;
	AddTestRun(e,res);WriteLn('TEST AddTestrun (Invalid date) : Response: Status=',res.status,' Message=',res.message);
	e.day:= 3;e.month:= 2; e.year := -5;
	AddTestRun(e,res);WriteLn('TEST AddTestrun (Invalid date) : Response: Status=',res.status,'	Message=',res.message);
	AddTestRun(d,res);WriteLn('TEST AddTestRun : Response: Status=',res.status,' Message=',res.message);
	AddTestRun(d,res);WriteLn('TEST AddDuplicateTestrun : Response: Status=',res.status,' Message=',res.message);
	AddTestResultForMostRecentTestRun(tmp,res);
	AddTestResultForMostRecentTestRun(tmp,res);
	WriteLN('TEST NUMBER OF Testruns : Expected: 1 -> Result: ',NumberOfTestRuns());
	WriteLn('TEST PRINT HISTORY');
	PrintTestHistory();
	DeleteAllTestResultsForTest(d,res);WriteLN('TEST DeleteAllTestResultsForTest ',DateToString(d),' :  Response: Status=',res.status,' Message=',res.message);
	WriteLn('TEST PRINT Statistic for Test ' , DateToString(d));
	PrintStatisticsForTest(d);
	DeleteTestRun(d,res);WriteLN('TEST DeleteTestRun ',DateToString(d),' :  Response: Status=',res.status,' Message=',res.message);
	WriteLn('TEST PRINT Statistic for Test ' , DateToString(d) ,' EXPECTED : No Results to display ');
	PrintStatisticsForTest(d);
	WriteLn('TEST Clear Testhistory');
	ClearTestHistory();
	WriteLn('TEST ADD TESTRUNS (max 20) and Testresults(max 50) (DATE descending order)');
	FOR i:= 0 TO 22 DO BEGIN
		d.day := d.day -1;		
		AddTestRun(d,res);WriteLn('TEST AddTestruns Run ',i+1,' : Response: Status=',res.status,' Message=',res.message);
			IF (i= 15)  THEN BEGIN
				tmp.Result := passed;
				FOR k:=0 TO 54 DO BEGIN
					AddTestResultForMostRecentTestRun(tmp,res);
					WriteLn('TEST ADD Testresults ',k+1,': RESULT: ',tmp.Name,' : Response: Status=',res.status,' Message=',res.message);
				END;
			End
			ELSE IF (i = 6) THEN BEGIN
				FOR k:=0 TO 54 DO BEGIN
					AddTestResultForMostRecentTestRun(tmp,res);
					WriteLn('TEST ADD Testresults ',k+1,': RESULT: ',tmp.Name,' : Response: Status=',res.status,' Message=',res.message);
				END;
			END;
		
	END;

	WriteLn('TEST PRINT HISTORY - HISTORY HAS TO BE SORTED (DATE ascending order)');
	PrintTestHistory();
	WriteLN('TEST NUMBER OF FAILED TESTS : Expected: 1 -> Result: ',TotalNumberOfFailedTests());
	WriteLN('TEST NumberOfTestExecutions Name: ',tmp.Name,' : Expected: 110 -> Result: ',NumberOfTestExecutions(tmp.Name));
	WriteLN('TEST AverageTestExecutionTime: ',tmp.Name,' : Expected: 1000 -> Result: ',AverageTestExecutionTime(tmp.Name));
	WriteLN('TEST TotalSuccessRatio : Expected: 0.50 -> Result: ',TotalSuccessRatio():6:2);

	ReadLn();
END. (* Uebung5 *)
