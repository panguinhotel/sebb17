//============================================================================
// Name        : stl_test.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <iterator>
#include <vector>
#include <set>
#include <cstdlib>
#include <map>
#include <algorithm>

using namespace std;
using namespace std::literals;

void test_vector()
{
	vector<int> v {1,2};
	v.push_back(3);

	std::cout << std::endl;

	for(auto it = begin(v); it != end(v); ++it)
	{
		std::cout << *it << ",";
	}
	std::cout << std::endl;

	for(auto e : v)
	{
		std::cout << e << ",";
	}

}

void test_set()
{
	set<int> s;
	for(int i = 0; i < 100; ++i)
	{
		s.insert(rand()%3);
	}

	for(auto it = begin(s); it != end(s); ++it)
	{
			std::cout << *it << ",";
	}

	auto p = s.insert(5);
	cout << *(p.first) << endl;
	cout << p.second << endl;

}

void test_map()
{
	map<string,int> m;
	hash<string> h;
	//m.insert({"Hello",_h("Hello")});
	//m.insert(std::make_pair<string,int>("Hello",h("Hello")));
	m.insert({"Hello",h("Hello")});
	std::cout << m["Hello"] << endl;

}

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	test_vector();
	test_set();
	test_map();
	return 0;
}
