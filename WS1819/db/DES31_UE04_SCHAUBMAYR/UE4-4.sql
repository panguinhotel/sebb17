DROP TABLE messages;
CREATE TABLE messages (results VARCHAR2(100));

CREATE OR REPLACE PROCEDURE emp_salary(emp_sal employees.salary%TYPE) IS
  lname employees.last_name%TYPE;
BEGIN
  SELECT last_name INTO lname FROM employees 
  WHERE salary = emp_sal; 
  INSERT INTO messages (results)
  VALUES (lname || '-' || emp_sal);
EXCEPTION
	WHEN TOO_MANY_ROWS THEN	
		INSERT INTO messages (results)
    VALUES ('More than one employee found with salary of '||emp_sal);
	WHEN NO_DATA_FOUND THEN	
		INSERT INTO messages (results)
    VALUES ('No employee found with salary of '||emp_sal);
  WHEN PROGRAM_ERROR THEN
    INSERT INTO messages (results)
    VALUES ('An undefined error occured');
END;
/

Begin
  dbms_output.put_line('TEST 0 line');
  emp_salary(0);
End;
/

Begin
  dbms_output.put_line('TEST 1 line');
  emp_salary(12000);
End;
/

Begin
  dbms_output.put_line('TEST multiple line');
  emp_salary(17000);
End;

Select * from messages;