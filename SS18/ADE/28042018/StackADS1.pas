(*Stack of Inteer values implemented as abstractdata structure *)
(*By default, the Stack is empty*)
unit StackADS1;
	
interface

	(*initializes the stack*)
	Procedure Init;

	(* resets the stack. Stack can be reused afterwards *)
	Procedure Done;

	(* Pushes a value onto the stack*)
	(* In value : value that will be pushed on the stack*)
	(* Out ok : False in case of any errors; TRUE otherwise
		IF false, the value will has not been pushed onto the stack *)
	PROCEDURE Push(value:Integer; Var ok : Boolean);

	(* Pops the most recently pushed value from the stack *)
	(* Out value : value popped from the stack. *)
	(* Out ok : False in case of any errors (e.g. stack is empty)
		In this case, the returned value mustn't be used *)
	PROCEDURE Pop(Var value:Integer; Var ok : Boolean);

	(* Checks if stack is currently empty*)
	(* Returns : True if stack is empty; False otherwise*)
	Function IsEmpty: Boolean;

implementation
	Const
		MaxSize = 10;

	VAR
		data : Array[1..MaxSize] Of Integer;
		top: Integer;
	
	PROCEDURE Init;
	BEGIN
		top:= 0;
	END; (* Init *)

	PROCEDURE Done;
	BEGIN
	END; (* Done *)

	PROCEDURE Push(value:Integer; Var ok : Boolean);
	BEGIN
		If top < MaxSize Then begin
			top := top +1;
			data[top] := value;
			ok := true;
		end
		ELSE Begin 
		ok := false; 
		End;

	END; (* Push *)

	PROCEDURE Pop(Var value:Integer; Var ok : Boolean);
	BEGIN
		IF Not IsEmpty Then begin
		value := data[top];
		top := top -1;
		ok:= True;
		END
		ELSE Begin
			ok := FALSE;
		END;
	END; (* Pop *)

	Function IsEmpty: Boolean;
	Begin
		IsEmpty := top = 0;
	END;
end.