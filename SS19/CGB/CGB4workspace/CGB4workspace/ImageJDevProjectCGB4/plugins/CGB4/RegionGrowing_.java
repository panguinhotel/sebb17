import ij.*;
import ij.plugin.filter.PlugInFilter;
import ij.process.*;

import java.awt.*;
import java.util.Stack;
import java.util.Vector;

import javax.naming.event.NamingExceptionEvent;

import ij.gui.GenericDialog;
import ij.gui.PointRoi;
import ij.gui.PolygonRoi;
import ij.gui.Roi;

public class RegionGrowing_ implements PlugInFilter {

	public static int BG_VAL = 0;
	public static int FG_VAL = 255;
	public static int UNPROCESSED = -1;
	public static int NB_ARR_RADIUS = 1;

	ImagePlus imagePlus = null;

	public int setup(String arg, ImagePlus imp) {
		if (arg.equals("about"))
		{showAbout(); return DONE;}

		imagePlus = imp;		
		return DOES_8G+DOES_STACKS+SUPPORTS_MASKING + ROI_REQUIRED;
	} //setup

	public static Vector<Point> GetSeedPositions(ImagePlus imagePlus) {
		PointRoi pr = (PointRoi)imagePlus.getRoi();
		int[] xCoords = pr.getXCoordinates(); 
		int[] yCoords = pr.getYCoordinates(); 
		int numOfElements = ((PolygonRoi)pr).getNCoordinates();
		Rectangle boundingRoi = pr.getBounds(); 

		Vector<Point> seedPositions = new Vector<Point>();

		for(int i = 0; i < numOfElements;i++) {
			seedPositions.add(new Point(xCoords[i] + boundingRoi.x, yCoords[i] + boundingRoi.y));
		}

		return seedPositions;
	}

	public void run(ImageProcessor ip) {
		byte[] pixels = (byte[])ip.getPixels();
		int width = ip.getWidth();
		int height = ip.getHeight();		          
		int[][] inDataArrInt = ImageJUtility.convertFrom1DByteArr(pixels, width, height);			

		//define the theshold limits
		int Tmin = 150;
		int Tmax = 240;

		//let the user input the range
		GenericDialog gd = new GenericDialog("interval border");
		gd.addSlider("Tmin", Tmin, 0,255);
		gd.addSlider("Tmax", Tmax, 0,255);
		gd.showDialog();

		if(!gd.wasCanceled())
		{
			Tmin = (int)gd.getNextNumber();
			Tmax = (int)gd.getNextNumber();
		}

		Vector<Point> seedPositions = GetSeedPositions(imagePlus);
		
		int[][] rgSegResult = PerformRegionGrowing(inDataArrInt,width,height,Tmin,Tmax, seedPositions);

		ImageJUtility.showNewImage(rgSegResult, width, height, "Segmented Image");
		
	} //run

	public static int[][] PerformRegionGrowing(int[][] inImg, int width, int height, int lowerThresh, int upperThresh,
			Vector<Point> seedPositions) {
		int[][] returnImg = new int[width][height];		

		//first assign all pixels the unprocessed value
		for(int x = 0; x < width; ++x)
		{
			for(int y = 0; y < height; ++y)
			{
				returnImg[x][y] = UNPROCESSED;
			}
		}
		
		Stack<Point> processingStack = new Stack<Point>();
		//now check all seeds
		for(Point p : seedPositions)
		{
			int actVal = inImg[p.x][p.y];
			if(returnImg[p.x][p.y] == UNPROCESSED)
			{
				//check range of actVal
				if(actVal >= lowerThresh && actVal <= upperThresh)
				{
					returnImg[p.x][p.y]= FG_VAL; 
					processingStack.push(p);
				}
				else
				{
					returnImg[p.x][p.y]= BG_VAL; 
				}
			}
		}
		
		//perform the region growing
		while(!processingStack.empty()) {
			Point actPos = processingStack.pop();
			
			//Now check all the neighbours in N8
			for(int xOffset = -1; xOffset <= 1; xOffset++)
			{
				for(int yOffset = -1; yOffset <= 1; yOffset++)
				{
					int nbX = actPos.x+xOffset;
					int nbY = actPos.y+yOffset;
					
					if(nbX >= 0 && nbX < width && nbY >= 0 && nbY < height)
					{
						int nbVal = inImg[nbX][nbY];
						if(returnImg[nbX][nbY] == UNPROCESSED)
						{
							//check Range
							if(nbVal >= lowerThresh && nbVal <= upperThresh)
							{
								returnImg[nbX][nbY] = FG_VAL;
								processingStack.push(new Point(nbX,nbY));
							}
							else
							{
								returnImg[nbX][nbY] = BG_VAL;
							}
						}
					}
				}
			}
		}
		
		//clearn-up
		for(int x = 0; x < width; ++x)
		{
			for(int y = 0; y < height; ++y)
			{
				if(returnImg[x][y] == UNPROCESSED)
				{
					returnImg[x][y] = BG_VAL;
				}
			}
		}

		return returnImg;
	}

	public static int[][] GetNeighborArrN4() {
		//TODO: implementation required
		return null;
	}

	public static int[][] GetNeighborArrN8() {
		//TODO: implementation required
		return null;
	}

	void showAbout() {
		IJ.showMessage("About Template_...",
				"this is a RegionGrowing_ template\n");
	} //showAbout

} //class RegionGrowing_

