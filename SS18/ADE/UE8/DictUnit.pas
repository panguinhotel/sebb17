unit DictUnit;
	
interface
TYPE
	
	Dictionary = OBJECT
		PUBLIC
			PROCEDURE PutElement(key, value: STRING);
			PROCEDURE GetElement(key: STRING; VAR value: STRING; VAR ok: BOOLEAN);
			PROCEDURE RemoveElement(key: STRING; VAR value: STRING; VAR ok: BOOLEAN);
			FUNCTION Size: INTEGER;
			PROCEDURE Delete(Var ok : Boolean);
		PRIVATE
			TYPE
			Node = ^NodeRec;
			NodeRec = RECORD
				key : String;
				value : String;
				prev, next : Node;
			End;
			top : Node;	
			lastFound : Node;		
END;
implementation

PROCEDURE Dictionary.PutElement(key, value: STRING);
VAR
	tmp : Node;
	ok : Boolean;
	tmpVal : String;
begin
	IF top = NIL THEN BEGIN
		New(top);
		top^.next := top;
		top^.prev := top;
	END;

	IF top <> NIL THEN BEGIN
		GetElement(key,tmpVal,ok);
		IF ok THEN BEGIN
			lastFound^.value := value;
		END ELSE BEGIN
			New(tmp);
			if tmp <> NIL THEN BEGIN
				top^.prev^.next := tmp;
				tmp^.prev := top^.prev;
				tmp^.next := top;
				top^.prev := tmp;
				tmp^.key := key;
				tmp^.value := value;
			END;
		END;
	END;
end;

PROCEDURE Dictionary.GetElement(key: STRING; VAR value: STRING; VAR ok: BOOLEAN);
VAR
	tmp : Node;
begin
	lastFound := NIL;
	ok := false;
	value := '0';
	IF top <> NIL THEN
	BEGIN
		tmp := top^.next;
		WHILE (tmp <> top) and not ok  DO BEGIN
			IF tmp^.key = key THEN BEGIN
				ok := true;
				lastFound := tmp;
				value := tmp^.value;
			END;
			tmp := tmp^.next;
		END;
	END;
end;

PROCEDURE Dictionary.RemoveElement(key: STRING; VAR value: STRING; VAR ok: BOOLEAN);
begin
	GetElement(key,value,ok);
	IF ok THEN BEGIN
		lastFound^.prev^.next := lastFound^.next;
		lastFound^.next := lastFound^.prev;
		Dispose(lastFound);
		ok := lastFound = NIL;
	END;
end;

FUNCTION Dictionary.Size: INTEGER;
VAR
	tmp : Node;
	count : Integer;
begin
	count := 0;
	IF top <> NIL THEN BEGIN 
		tmp := top^.next;
		WHILE tmp <> top  DO BEGIN
			count +=1;
			tmp := tmp^.next;
		END;
	END;
	Size := count;
end;
PROCEDURE Dictionary.Delete(Var ok : Boolean);
VAR 
	tmp,tmp1 : Node;
BEGIN
	IF top <> NIL THEN BEGIN
		tmp := top^.next;
		WHILE tmp <> top DO BEGIN
			tmp1 := tmp;
			tmp := tmp^.next;
			Dispose(tmp1);
		END;
	END;
	Dispose(top);
	top := NIL;

	ok := top = NIL;
END;
end.