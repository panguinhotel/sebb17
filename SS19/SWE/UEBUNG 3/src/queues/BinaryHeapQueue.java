package queues;

import java.util.ArrayList;

public class BinaryHeapQueue
{

	private ArrayList<Integer> m_values;
	public BinaryHeapQueue()
	{
		m_values = new ArrayList<Integer>();
	}

	public boolean isEmpty() {
		return m_values.isEmpty();
	}
	
	public void enqueue(int x) {
		assert isHeap();
		m_values.add(x); // not insert()
		upHeap();
		assert isHeap();
	}


	public boolean isHeap() {
		int i = 1;
		while (i < m_values.size() && m_values.get(parent(i)) >= m_values.get(i))
			i++;
		return i >= m_values.size();
	}


	private static int parent(int i) {
		return (i - 1) / 2;
	}

	private static int left(int i) {
		return i * 2 + 1;
	}

	private static int right(int i) {
		return i * 2 + 2;
	}

	public void upHeap() {
		int i = m_values.size() - 1;
		int x = m_values.get(i);
		while (i != 0 && m_values.get(parent(i)) < x) {
			m_values.set(i, m_values.get(parent(i)));
			i = parent(i);
		}
		m_values.set(i, x);
	}

	private void downHeap() {
		assert !m_values.isEmpty();
		int i = 0;
		int x = m_values.get(0);
		while (left(i) < m_values.size()) {
			int j = left(i); // j = larger child index
			if (right(i) < m_values.size() && m_values.get(left(i)) < m_values.get(right(i))) {
				j = right(i);
			}
			if (x >= m_values.get(j))
				break;
			m_values.set(i, m_values.get(j));
			i = j;
		}
		m_values.set(i, x);
	}

	public int dequeue() {
		if(!isHeap())
			throw new IllegalStateException("Heap Eigenschaft ist nicht erf�llt");
		else if(m_values.isEmpty())
			throw new IllegalStateException("Warteschlange ist leer");
		else
		{
			int top = m_values.get(0);
			m_values.set(0, m_values.get(m_values.size() - 1));
			m_values.remove(m_values.size() - 1);
			if (!m_values.isEmpty())
				downHeap();
			assert isHeap();
			return top;
		}
	}


	public void insertUnordered(int elem)
	{
		m_values.add(elem);
	}

	public void insertUnordered(int[] elemArray)
	{
		for(int i = 0; i < elemArray.length; i++)
		{
			insertUnordered(elemArray[i]);
		}
	}

	public void insert(int elem)
	{
		m_values.add(elem);
		upHeap();
	}

	public int max()
	{
		if(!isHeap())
			throw new IllegalStateException("Heap Eigenschaft ist nicht erf�llt");
		else if(m_values.isEmpty())
			throw new IllegalStateException("Warteschlange ist leer");
		else return m_values.get(0);

	}

	public int removeMax()
	{
		return dequeue();		
	}

	public int[] nLargest(int n)
	{
		if(!isHeap())
			throw new IllegalStateException("Heap Eigenschaft ist nicht erf�llt");
		else if(size() < n)
			throw new IllegalStateException("Warteschlange hat zu wenig elemente");
		
		int[] returnValue = new int[n];
		int count = 0;
		for(int i = 0; i < n; i++)
		{
			returnValue[count] = m_values.get(i);
			count ++;
		}
		
		return returnValue;
	}

	public int[] removeNLargest(int n)
	{
		if(!isHeap())
			throw new IllegalStateException("Heap Eigenschaft ist nicht erf�llt");

		int[] returnValue = new int[n];
		for(int i = 0; i < n && i < m_values.size();i++)
		{
			returnValue[i] = removeMax();
		}
		return returnValue;		
	}

	private int findMaxChildIdx(int idx)
	{
		int leftMax = idx;
		
		if(left(idx) < size())
		{
			int tmpIdx = findMaxChildIdx(left(idx));
			if(m_values.get(leftMax) < m_values.get(tmpIdx))
				leftMax = tmpIdx;
		}

		int rightMax = idx;		
		if(right(idx) < size())
		{
			int tmpIdx = findMaxChildIdx(right(idx));
			if(m_values.get(rightMax) < m_values.get(tmpIdx))
				rightMax = tmpIdx;
		}

		return  m_values.get(leftMax) > m_values.get(rightMax) ? leftMax : rightMax;
	}
	public void heapify()
	{
		if(!isHeap())
		{
			int start = size()/2;
			while(start >= 0)
			{
				heapifyIdx(start);	
				if(isHeap())
					break;				
				start--;
			}
		}
		
	}
	private void heapifyIdx(int idx)
	{
		int maxChildIdx = findMaxChildIdx(idx);
		if(maxChildIdx != idx)
		{
			int tmpVal = m_values.get(maxChildIdx);
			m_values.set(maxChildIdx, m_values.get(idx));
			m_values.set(idx, tmpVal);
			
		}
		
		if(left(idx) < size())
			heapifyIdx(left(idx));
		
		if(right(idx) < size())
			heapifyIdx(right(idx));
	}
	public int size()
	{
		return m_values.size();
	}

	public void merge(BinaryHeapQueue queue)
	{
		insertUnordered(queue.nLargest(queue.size()));
		heapify();
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(); // like stringstream
		sb.append("heap = [");
		for (int i = 0; i < m_values.size(); i++) {
			if (i > 0)
				sb.append(" ");
			sb.append(m_values.get(i));
		}
		sb.append("]");
		return sb.toString();
	}
}
