PROGRAM Uebung1Main;
USES Telefonbuch,MyExtensions;
(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
VAR
	resp : Response;
BEGIN

WriteLn('TEST Telefonbuch mit Liste');
Telefonbuch.Init(Modus.Liste);
resp := Telefonbuch.AddEntry('Harald','Mueller','0000000000'); WriteLn('Add Entry: ', ResponseToString(resp) );
resp := Telefonbuch.AddEntry('Harald','Mueller','1111111111'); WriteLn('Add Duplicate Entry: ', ResponseToString(resp) );
resp := Telefonbuch.AddEntry('Lisa','Huber','2222222222'); WriteLn('Add Entry: ', ResponseToString(resp) );
resp := Telefonbuch.AddEntry('Monika','Bauer','3333333333'); WriteLn('Add Entry: ', ResponseToString(resp) );
Telefonbuch.PrintOnScreen();
resp := Telefonbuch.DeleteEntry('Harald','Mueller'); WriteLn('DeleteEntry Entry: ', ResponseToString(resp) );
Telefonbuch.PrintOnScreen();
WriteLn('Get Number for Name :Monika, Bauer : ', GetNumberForName('Monika','Bauer'));
WriteLn('');
WriteLn('');
WriteLn('TEST Hashing mit Offener Adressierung');
Telefonbuch.Init(Modus.OHa);
resp := Telefonbuch.AddEntry('Hans','Hampel','4444444444'); WriteLn('Add Entry: ', ResponseToString(resp) );
resp := Telefonbuch.AddEntry('Hans','Hampel','5555555555'); WriteLn('Add Duplicate Entry: ', ResponseToString(resp) );
resp := Telefonbuch.AddEntry('Jakob','Otur','6666666666'); WriteLn('Add Entry: ', ResponseToString(resp) );
resp := Telefonbuch.AddEntry('Leopold','Ferber','7777777777'); WriteLn('Add Entry: ', ResponseToString(resp) );
Telefonbuch.PrintOnScreen();
resp := Telefonbuch.DeleteEntry('Jakob','Otur'); WriteLn('DeleteEntry Entry: ', ResponseToString(resp) );
Telefonbuch.PrintOnScreen();
WriteLn('Get Number for Name :Hans, Hampel : ', GetNumberForName('Hans','Hampel'));
WriteLn('');
WriteLn('');
WriteLn('TEST Hashing mit Verkettung');
Telefonbuch.Init(Modus.CLHa);
resp := Telefonbuch.AddEntry('Patrick','Gurt','8888888888'); WriteLn('Add Entry: ', ResponseToString(resp) );
resp := Telefonbuch.AddEntry('Patrick','Gurt','9999999999'); WriteLn('Add Duplicate Entry: ', ResponseToString(resp) );
resp := Telefonbuch.AddEntry('Max','Hut','0123456789'); WriteLn('Add Entry: ', ResponseToString(resp) );
resp := Telefonbuch.AddEntry('Franz','Wert','0011667733'); WriteLn('Add Entry: ', ResponseToString(resp) );
Telefonbuch.PrintOnScreen();
resp := Telefonbuch.DeleteEntry('Franz','Wert'); WriteLn('DeleteEntry Entry: ', ResponseToString(resp) );
Telefonbuch.PrintOnScreen();
WriteLn('Get Number for Name :Max, Hut : ', GetNumberForName('Max','Hut'));



END. (* Uebung1Main *)