UNIT MPPCom;

INTERFACE

    (* ---------------------------------------------------------------------- *)
    (* Description: Parses and validates a midi pascal program.               *)
    (* Input/Output:                                                          *)
    (*          inputFile: File containing the pascal program to parse.       *)
    (*                     file must be open for read.                        *)
    (*          outFile: File where the compiled pascal will be stored.       *)
    (*                     file must be open for read and write.              *)
    (*          ok: True if the given code is valid, otherwise false.         *)
    (*          errorColumn, errorLine: If "ok" is false, indicates the       *)
    (*                  position of the incorrect syntax validation.          *)
    (*          errorText: If "ok" is false, indicates a reason for the       *)
    (*                  syntax validation.                                    *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Parse(VAR inputFile: TEXT; VAR outFile: FILE; VAR ok: BOOLEAN; VAR errorLine, errorColumn: INTEGER; VAR errorText: STRING);
IMPLEMENTATION
    USES MPScan, SymTblC, CodeDef, CodeGen;

    VAR 
        success: BOOLEAN;
        errText: STRING;


    (* ---------------------------------------------------------------------- *)
    (* ------------- Stores an error text of a semantic error --------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE SemErr(msg: STRING);
    BEGIN
        success := FALSE;
        errText := msg;
    END;

    PROCEDURE MP(VAR outFile: FILE); FORWARD;
    PROCEDURE VarDecl; FORWARD;
    PROCEDURE StatSeq; FORWARD;
    PROCEDURE Stat; FORWARD;
    PROCEDURE Expr; FORWARD;
    PROCEDURE Term; FORWARD;
    PROCEDURE Fact; FORWARD;

    (* ---------------------------------------------------------------------- *)
    (* ------------------- Handles a MidiPascal code ------------------------ *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE MP(VAR outFile: FILE);
    BEGIN
        IF CurrentSymbol <> programSym THEN BEGIN success := FALSE; Exit; END;
        (* SEM *)
        ResetSymbolTable;
        ResetCodeGenerator;
        (* END SEM *)
        GetNextSymbol;
        IF CurrentSymbol <> identSym THEN BEGIN success := FALSE; Exit; END;
        GetNextSymbol;
        IF CurrentSymbol <> semicolonSym THEN BEGIN success := FALSE; Exit; END;
        GetNextSymbol;

        IF CurrentSymbol = varSym THEN BEGIN
            VarDecl; IF NOT success THEN Exit;
        END;

        IF CurrentSymbol <> beginSym THEN BEGIN success := FALSE; Exit; END;
        GetNextSymbol;

        StatSeq; IF NOT success THEN Exit;

        IF CurrentSymbol <> endSym THEN BEGIN success := FALSE; Exit; END;
        GetNextSymbol;
        IF CurrentSymbol <> periodSym THEN BEGIN success := FALSE; Exit; END;
        (*SEM*)
        Emit1(endOpc);
        WriteCodeToFile(outFile);
        (*ENDSEM*)
        GetNextSymbol;
    END; (* MP *)

    (* ---------------------------------------------------------------------- *)
    (* ----------- Handles a MidiPascal declaration of variables ------------ *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE VarDecl;
    VAR ok: BOOLEAN;
    BEGIN
        IF CurrentSymbol <> varSym THEN BEGIN success := FALSE; Exit; END;
        GetNextSymbol;
        IF CurrentSymbol <> identSym THEN BEGIN success := FALSE; Exit; END;
        (*SEM*)
        DeclareVar(CurrentidentName, ok);
        (*ENDSEM*)
        GetNextSymbol;

        WHILE CurrentSymbol = commaSym DO BEGIN
            GetNextSymbol;
            IF CurrentSymbol <> identSym THEN BEGIN success := FALSE; Exit; END;
            (*SEM*)
            DeclareVar(CurrentidentName, ok);
            IF NOT ok THEN BEGIN SemErr('Variable already declared ' + CurrentIdentName); Exit; END; 
            (*ENDSEM*)
            GetNextSymbol;
        END;

        IF CurrentSymbol <> colonSym THEN BEGIN success := FALSE; Exit; END;
        GetNextSymbol;

        IF CurrentSymbol <> integerSym THEN BEGIN success := FALSE; Exit; END;
        GetNextSymbol;

        IF CurrentSymbol <> semicolonSym THEN BEGIN success := FALSE; Exit; END;
        GetNextSymbol;
        
    END; (* VarDecl *)

    (* ---------------------------------------------------------------------- *)
    (* ----------------- Handles a MidiPascal stat seq ---------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE StatSeq;
    BEGIN
        Stat; IF NOT success THEN Exit;

        WHILE CurrentSymbol = semicolonSym DO BEGIN
            GetNextSymbol;
            Stat; IF NOT success THEN Exit;
        END;
    END; (* StatSeq *)
    
    (* ---------------------------------------------------------------------- *)
    (* ------------------- Handles a MidiPascal stat ------------------------ *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Stat;
    (*LOCAL*)
    VAR 
        dest: STRING;
        addr, addr2: INTEGER;
    (*ENDLOCAL*)
    BEGIN
        CASE CurrentSymbol OF
            identSym: BEGIN
                dest := CurrentIdentName;
                
                IF NOT IsDeclared(dest) THEN BEGIN
                    SemErr('Variable not declared:' + dest); Exit;
                END;
                
                GetNextSymbol;
                IF CurrentSymbol <> assignSym THEN BEGIN success := FALSE; Exit; END;
                GetNextSymbol;
                Expr; IF NOT success THEN Exit;
                Emit2(storeOpc, AddressOf(dest));
            
            END;
            readSym: BEGIN
                GetNextSymbol;
                IF CurrentSymbol <> leftParSym THEN BEGIN success := FALSE; Exit; END;
                GetNextSymbol;
                IF CurrentSymbol <> identSym THEN BEGIN success := FALSE; Exit; END;
                GetNextSymbol;

                IF NOT IsDeclared(CurrentIdentName) THEN BEGIN
                    SemErr('Variable not declared: ' + CurrentIdentName);Exit;
                END;

                Emit2(readOpc, AddressOf(CurrentIdentName));

                IF CurrentSymbol <> rightParSym THEN BEGIN success := FALSE; Exit; END;
                GetNextSymbol;
            END;
            writeSym: BEGIN
                GetNextSymbol;
                IF CurrentSymbol <> leftParSym THEN BEGIN success := FALSE; Exit; END;
                GetNextSymbol;
                Expr; IF NOT success THEN Exit;
                Emit1(writeOpc);
                IF CurrentSymbol <> rightParSym THEN BEGIN success := FALSE; Exit; END;
                GetNextSymbol;
            END;
            beginSym: BEGIN
                GetNextSymbol;
                StatSeq; IF NOT success THEN Exit;
                IF CurrentSymbol <> endSym THEN BEGIN success := FALSE; Exit; END;
                GetNextSymbol;
            END;
            ifSym: BEGIN
                GetNextSymbol;                
                Expr; IF NOT success THEN Exit;

                (* SEM *)
                Emit2(jumpZeroOpc, 0);
                addr := CurrentAddress - 2;
                (* END SEM *)

                IF CurrentSymbol <> thenSym THEN BEGIN success := FALSE; Exit; END;
                GetNextSymbol;   
                Stat; IF NOT success THEN Exit;

                IF CurrentSymbol = elseSym THEN BEGIN
                    (* SEM *)
                    Emit2(jumpOpc, 0);
                    FixUpJumpTarget(addr, CurrentAddress);
                    addr := CurrentAddress - 2;
                    (* END SEM *)

                    GetNextSymbol;
                    Stat; IF NOT success THEN Exit;

                    (* SEM *)
                    FixUpJumpTarget(addr, CurrentAddress);
                    (* END SEM *)
                END;
            END;
            whileSym: BEGIN
                (* SEM *)
                addr := CurrentAddress;
                (* END SEM *)

                GetNextSymbol;                
                Expr; IF NOT success THEN Exit;

                (* SEM *)
                Emit2(jumpZeroOpc, 0);
                addr2 := CurrentAddress - 2;
                (* END SEM *)

                IF CurrentSymbol <> doSym THEN BEGIN success := FALSE; Exit; END;
                GetNextSymbol;                
                Stat; IF NOT success THEN Exit;

                (* SEM *)
                Emit2(jumpOpc, addr);
                FixUpJumpTarget(addr2, CurrentAddress);
                (* END SEM *)
            END;
        END;
    END; (* Stat *)

    (* ---------------------------------------------------------------------- *)
    (* ------------------- Handles a MidiPascal fact ------------------------ *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Fact;
    BEGIN
        CASE CurrentSymbol OF
            identSym: BEGIN
                (* SEM *)
                IF NOT IsDeclared(CurrentIdentName) THEN BEGIN
                    SemErr('Variable not declared: ' + CurrentIdentName);Exit;
                END;

                Emit2(loadValOpc,AddressOf(CurrentIdentName));
                (* END SEM *)

                GetNextSymbol;
            END;
            numberSym: BEGIN
                Emit2(loadConstOpc, CurrentNumberValue);
                GetNextSymbol;
            END;
            leftParSym: BEGIN
                GetNextSymbol;
                Expr; IF NOT success THEN Exit;
                IF CurrentSymbol <> rightParSym THEN BEGIN success := FALSE; Exit; END;
                GetNextSymbol;
            END;
            ELSE BEGIN success := FALSE; Exit; END;
        END;
    END; (* Fact *)

    (* ---------------------------------------------------------------------- *)
    (* ------------------- Handles a MidiPascal term ------------------------ *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Term;
    BEGIN
        Fact; IF NOT success THEN EXIT;
        WHILE(CurrentSymbol = multSym) OR (CurrentSymbol = divSym) DO BEGIN
            CASE CurrentSymbol OF 
                multSym: BEGIN
                    GetNextSymbol;
                    Fact; IF NOT success THEN EXIT;
                    Emit1(multOpc);
                END;
                divSym: BEGIN
                    GetNextSymbol;
                    Fact; IF NOT success THEN EXIT;
                    Emit1(divOpc);
                END;
            END;
        END; 
    END; (* Term *)

    (* ---------------------------------------------------------------------- *)
    (* ---------------- Handles a MidiPascal expression --------------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Expr;
    BEGIN
        Term; IF NOT success THEN EXIT;
        WHILE(CurrentSymbol = plusSym) OR (CurrentSymbol = minusSym) DO BEGIN
            CASE CurrentSymbol OF 
                plusSym: BEGIN
                    GetNextSymbol;
                    Term; IF NOT success THEN EXIT;
                    Emit1(addOpc);
                END;
                minusSym: BEGIN
                    GetNextSymbol;
                    Term; IF NOT success THEN EXIT;
                    Emit1(subOpc);
                END;
            END;
        END;
    END; (* Expr *)

    (* ---------------------------------------------------------------------- *)
    (* ------------- Parses and validates a midi pascal program ------------- *)
    (* ---------------------------------------------------------------------- *)
    PROCEDURE Parse(VAR inputFile: TEXT; VAR outFile: FILE; VAR ok: BOOLEAN; VAR errorLine, errorColumn: INTEGER; VAR errorText: STRING);
    BEGIN
        success := TRUE;
        errText := '';

        InitScanner(inputFile);

        MP(outFile);

        ok := success;
        errorText := errText;
        GetCurrentSymbolPosition(errorLine, errorColumn);
    END; (* Parse *)
END. (* MPPars *)