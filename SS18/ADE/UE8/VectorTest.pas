PROGRAM VectorTest;

uses
VectorUnit;
VAR
	instance1 : Vector;
	value : Integer;
	ok : Boolean;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	WriteLn('Test Add : Value = 1'); instance1.Add(1);
	WriteLn('Test Add : Value = 2'); instance1.Add(2);
	WriteLn('Test Add : Value = 30'); instance1.Add(30);
	WriteLn('Test Add : Value = 44'); instance1.Add(44);
	WriteLn('Test Add : Value = 15');instance1.Add(15);
	WriteLn('Test Size : Expected = 5 - Result : ',instance1.Size);
	WriteLn('Test Capacity : Expected = 5 - Result : ',instance1.Capacity);
	instance1.RemoveElementAt(2,value,ok);
	WriteLn('Test Remove : Expected: 30 - TRUE - Result : ',value,' - ', ok);
	WriteLn('Test Size : Expected = 4 - Result : ',instance1.Size);
	WriteLn('Test Capacity : Expected = 4 - Result : ',instance1.Capacity);


END. (* VectorTest *)