#include <stdio.h>
#include "date.h"

int day, month, year;

void init_date(void){
	day = 15;
	month = 9;
	year = 2018;
}

void print_date(void){
	printf("%02d.%02d.%04d\n",day,month,year);
}