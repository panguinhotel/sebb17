PROGRAM MPI;

USES 
  MPInt;
  
VAR
  inputFile: TEXT;
  ok: BOOLEAN;
  errLine, errColumn: INTEGER;
  errMessage: STRING;
  
BEGIN
  IF ParamCount <> 1 THEN BEGIN 
    WriteLn('USAGE: mpi<srcfile>');
    Halt(1);
  END;
  
  Assign(inputFile, ParamStr(1));
  (*$I-*)
    Reset(inputFile);
  (*$I+*)
  IF IOResult <> 0 THEN BEGIN
    WriteLn('Error, opening source file.');
    Halt(2);
    END;
    
  Parse(inputFile, ok, errLine, errColumn, errMessage);
  Close(inputFile);
  IF NOT ok THEN BEGIN
    WriteLn('Error at line ', errLine, ', column', errColumn, ': ', errMessage);
    Halt(3);
  END;
END.