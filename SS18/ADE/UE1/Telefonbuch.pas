unit Telefonbuch;
	
interface
uses VKHash,MyExtensions,UnitList,OAHash;
(*Add Entry*)
FUNCTION AddEntry(vorname,nachname,nummer: string):Response;
(*DeleteEntry*)
FUNCTION DeleteEntry(vorname,nachname: string):Response;
(*GetNumberForName*)
FUNCTION GetNumberForName(vorname,nachname: string) : string;
(*Init*)
PROCEDURE Init(mode: Modus);
(*PrintOnScreen*)
PROCEDURE PrintOnScreen();

implementation
Var m_type:Modus;
Var m_list:TestResultList;
Var m_chaTable : THashTable;
VAR m_ohaTable : OTHashTable;

(*AddEntry*)
FUNCTION AddEntry(vorname,nachname,nummer: string):Response;
Var
	tmp : AddressBookEntry;
BEGIN
	tmp.vorname := vorname;
	tmp.nachname := nachname;
	tmp.nummer := nummer;
	AddEntry.status := Failure;
	AddEntry.message := 'Error';
	case m_type of
	Liste: BEGIN
				IF UnitList.AddValueToList(m_list,tmp)  THEN BEGIN
					AddEntry.status := Success;
					AddEntry.message := 'Added';
				END;
			END;
	CLHa:BEGIN
				IF VKHash.AddNodeToHashTable(m_chaTable,tmp) THEN BEGIN
					AddEntry.status := Success;
					AddEntry.message := 'Added';
				END;
			END;
	OHa: BEGIN
				IF OAHash.AddNodeToHashTable(m_ohaTable,tmp) THEN BEGIN
					AddEntry.status := Success;
					AddEntry.message := 'Added';
				END;
			END;
 	end;
	
END; (* AddEntry *)	

(*DeleteEntry*)
FUNCTION DeleteEntry(vorname,nachname: string):Response;
BEGIN
	DeleteEntry.status := Failure;
	DeleteEntry.message := 'Error';
	case m_type of
	Liste: BEGIN
				IF UnitList.DeleteValueFromList(m_list,vorname,nachname)  THEN BEGIN
					DeleteEntry.status := Success;
					DeleteEntry.message := 'REMOVE';
				END;
			END;
	CLHa: BEGIN
				IF VKHash.DisposeEntry(m_chaTable,vorname,nachname)  THEN BEGIN
					DeleteEntry.status := Success;
					DeleteEntry.message := 'REMOVE';
				END;
			END;
	OHa: BEGIN
				IF OAHash.DisposeEntry(m_ohaTable,vorname,nachname)  THEN BEGIN
					DeleteEntry.status := Success;
					DeleteEntry.message := 'REMOVE';
				END;
			END;
 	end;
END; (* DeleteEntry *)

(*GetNumberForName*)
FUNCTION GetNumberForName(vorname,nachname: string) : string;
Var
	tmp1 : TestResultList;
	tmp : AddressBookEntry;
BEGIN
	tmp.vorname := vorname;
	tmp.nachname := nachname;
	case m_type of
	Liste: BEGIN
			tmp1 := FindEntry(m_list,tmp);
				IF tmp1 <> NIL  THEN BEGIN
					GetNumberForName := tmp1^.Book.nummer;
				END;
			END;
	CLHa: BEGIN
				GetNumberForName := VKHash.getNumberForName(m_chaTable,tmp);
			END;
	OHa: BEGIN
				GetNumberForName := OAHash.getNumberForName(m_ohaTable,tmp);
			END;
 	end;
END; (* GetNumberForName *)

(*Describe me*)
PROCEDURE PrintOnScreen();
BEGIN
	case m_type of
	Liste: UnitList.DisplayList(m_list);
	CLHa: VKHash.DisplayHashTable(m_chaTable);
	OHa: OAHash.DisplayHashTable(m_ohaTable);
 	end;
END; (* PrintOnScreen *)
(*Init*)
PROCEDURE Init(mode: Modus);
BEGIN
	m_type := mode;
	case m_type of
	Liste:  Begin
			VKHash.DisposeHashTable(m_chaTable);
			OAHash.DisposeHashTable(m_ohaTable);
			UnitList.InitList(m_list);
			End;
	CLHa:  Begin
			UnitList.DisposeList(m_list);
			OAHash.DisposeHashTable(m_ohaTable);
			VKHash.InitHashTable(m_chaTable);
			End;
	OHa: Begin
			UnitList.DisposeList(m_list);
			VKHash.DisposeHashTable(m_chaTable);
			OAHash.InitHashTable(m_ohaTable);
			End;
 	end;
END; (* Init *)
end.