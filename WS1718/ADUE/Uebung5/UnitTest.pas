unit UnitTest;

interface
Uses UnitDate,UnitList;
TYPE  Response = RECORD
		status : (Failure,Success);
		message : string;
End;

TYPE   Testresult = RECORD    
		Name : string;
		DurationTime : Int64;
		Result: (passed,failed);
End;
TYPE   Testrun = RECORD    
		CreationDate: Date;
		Testresults: TestResultList;
End;

Var
	Testruns : array [0..19] of Testrun;
	idxLastTest : Integer;
(*Adds a TestRun with CrationDate to Testhistory max 20*)
PROCEDURE AddTestRun(d: Date; VAR res : Response);
(*Adds a Testresult with Name, DurationTime and Result to most recent Testrun (max 50)*)
PROCEDURE AddTestResultForMostRecentTestRun(param: Testresult; VAR res : Response);
(*Prints whole TestHistory on Console*)
PROCEDURE PrintTestHistory();
(*Calculates the average Test ExecutionTime for a specific name*)
FUNCTION AverageTestExecutionTime(name: string) : Int64;
(*Calculates the amount of times a Testresult occurs in the TestHistory*)
FUNCTION NumberOfTestExecutions(name: string) : INTEGER;
(*Calculates the number of failed Testruns*)
FUNCTION TotalNumberOfFailedTests() : INTEGER;
(*Clears the TestHistory*)
PROCEDURE ClearTestHistory();
(*Describe me*)
PROCEDURE PrintStatisticsForTest(creationDate: Date);
(*Describe me*)
PROCEDURE DeleteAllTestResultsForTest(creationDate: Date;Var res:Response);
(*Describe me*)
PROCEDURE DeleteTestRun(creationDate: Date;Var res:Response);
(*Describe me*)
FUNCTION NumberOfTestRuns () : INTEGER;
(*Describe me*)
FUNCTION TotalSuccessRatio () : Double;
implementation
(*Sorts TestHistory*)
PROCEDURE Sort();
VAR
	tmpTest : Testrun;
	i,k:Integer;
BEGIN
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		FOR k:= i+1 TO High(Testruns) DO BEGIN
			IF LiesBefore(Testruns[k].CreationDate,Testruns[i].CreationDate) THEN BEGIN
				tmpTest:= Testruns[i];
				Testruns[i] := Testruns[k];
				Testruns[k] := tmpTest;
				IF idxLastTest = i THEN idxLastTest := k;
				IF idxLastTest = k THEN idxLastTest := i;
			End;
		END;
	END;
END; (* Sort *)

(*Converts TestResult to Test for List*)
FUNCTION TestResultToTest(value: TestResult) : TestType;
BEGIN
	TestResultToTest.Name := value.Name;
	TestResultToTest.DurationTime := value.DurationTime;
	IF value.Result = passed  THEN BEGIN
		TestResultToTest.Result := UnitList.passed;
	END
	ELSE BEGIN
		TestResultToTest.Result := UnitList.failed;
	END;
	
END; (* TestResultToTest *)

PROCEDURE AddTestRun(d: Date ;VAR res : Response);
Var
i,k : Integer;
tmp,tmp1: Boolean;
BEGIN
	res.status := Success;
	res.message := 'Testrun added';
	tmp:= true;
	tmp1:= true;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF NOT IsValidDate(Testruns[i].CreationDate) Then begin
			if IsValidDate(d) then begin
				FOR k:=low(Testruns) TO (i-1) DO BEGIN
					IF (Testruns[k].CreationDate.day = d.day) and
					   (Testruns[k].CreationDate.month = d.month) and
					   (Testruns[k].CreationDate.year = d.year)  THEN BEGIN
						tmp1:= false;
					 	break;
					END;
				END;
				if tmp1 Then Begin
					Testruns[i].CreationDate := d;
					idxLastTest := i;
					tmp := false;
					Sort();
					EXIT;
				End;
			End
			else begin
				res.message := 'Error - Invalid Creationdate';
				res.status := Failure;
				EXIT;
			End; 
		End;
	End;
	IF not tmp1 THEN Begin
		res.message := 'Error - CreationDate already exits';
		res.status := Failure;
	End
	ELSE IF tmp THEN Begin
		res.message := 'Error - TestHistory is full';
		res.status := Failure;
	End;
END; (* AddTestRun *)
PROCEDURE AddTestResultForMostRecentTestRun(param: Testresult; VAR res : Response);
BEGIN
	res.status := Failure;
	IF idxLastTest < 0  THEN BEGIN
		res.message := 'Error - No Test found';
	END
	ELSE BEGIN
			 	AddValueToList(Testruns[idxLastTest].Testresults, TestResultToTest(param));
				res.message:='Testresult: '+param.Name+' added';
				res.status:=Success;
		END;
	IF res.status = Failure  THEN res.message := 'Error - No more Testresult aviable for this Testrun';
END; (* AddTestResultForMostRecentTestRun *)
PROCEDURE PrintTestHistory();
Var
i,k : Integer;
BEGIN
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF NOT IsValidDate(Testruns[i].CreationDate) Then EXIT;
		WriteLn('Testrun ',^i,i+1,^i,DateToString(Testruns[i].CreationDate));
		DisplayList(Testruns[i].Testresults);
		WriteLn('==========================');
	END;
END; (* PrintTestHistory *)
PROCEDURE ClearTestHistory();
Var
	d : Date;
	i,k : Integer;
BEGIN
	d.day := -1;
	d.month := -1;
	d.year := -1;
	idxLastTest:=-1;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		Testruns[i].CreationDate :=d;
		DisposeList(Testruns[i].Testresults);
		initList(Testruns[i].Testresults);
	END;
END; (* ClearTestHistory *)
FUNCTION AverageTestExecutionTime(name: string) : Int64;
Var
	i,k : Integer;
	anz,sum : Int64;
	n :UnitList.TestResult;
BEGIN
	anz := NumberOfTestExecutions(name);
	sum := 0;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF NOT IsValidDate(Testruns[i].CreationDate) Then break;
		n:=Testruns[i].Testresults;
		WHILE n <> NIL  DO BEGIN
			IF n^.Test.Name = name  THEN BEGIN
				sum := sum + n^.Test.DurationTime;
			END;
			n:= n^.next;
		END;
	END;
	IF anz = 0  THEN AverageTestExecutionTime:= 0
	ELSE AverageTestExecutionTime:= sum DIV anz;

END; (* AverageTestExecutionTime *)
FUNCTION NumberOfTestExecutions(name: string) : INTEGER;
Var
	i,k : Integer;
	n :UnitList.TestResult;
BEGIN
	NumberOfTestExecutions:= 0;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF NOT IsValidDate(Testruns[i].CreationDate) Then EXIT;
		n:=Testruns[i].Testresults;
		WHILE n <> NIL  DO BEGIN
			IF n^.Test.Name = name  THEN BEGIN
				NumberOfTestExecutions := NumberOfTestExecutions + 1;
			END;
			n:= n^.next;
		END;
	END;
END; (* NumberOfTestExecutions *)
FUNCTION TotalNumberOfFailedTests() : INTEGER;
Var
	i,k : Integer;
	n :UnitList.TestResult;
BEGIN
	TotalNumberOfFailedTests:= 0;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF NOT IsValidDate(Testruns[i].CreationDate) Then EXIT;
		n:=Testruns[i].Testresults;
		WHILE n <> NIL  DO BEGIN
			IF n^.Test.Result = UnitList.failed  THEN BEGIN
				TotalNumberOfFailedTests := TotalNumberOfFailedTests + 1;
				break;
			END;
			n:= n^.next;
		END;
	END;
END; (* TotalNumberOfFailedTests *)
(*Describe me*)
PROCEDURE DeleteAllTestResultsForTest(creationDate: Date;Var res:Response);
Var
	i:Integer;
	found :Boolean;
BEGIN
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF CompareDate(Testruns[i].CreationDate,creationDate) THEN BEGIN
			DisposeList(Testruns[i].Testresults);
			res.status := success;
			res.message := 'Testresults removed';
			found := true;
		END;
	END;
	IF not found  THEN BEGIN
			res.status := failure;
			res.message := 'Testrun not found';
	END;
END; (* DeleteAllTestResultsForTest  *)
(*Describe me*)
PROCEDURE DeleteTestRun(creationDate: Date;Var res:Response);
VAR
	i:Integer;
	d: Date;
BEGIN
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF  CompareDate(Testruns[i].CreationDate,creationDate) THEN BEGIN
			d.day := -1;
			d.month := -1;
			d.year := -1;
			idxLastTest:=-1;
			Testruns[i].CreationDate :=d;
			DeleteAllTestResultsForTest(creationDate,res);
		END;
	END;
END; (* DeleteTestRun *)
(*Describe me*)
PROCEDURE PrintStatisticsForTest(creationDate: Date);
Var
	i:Integer;
BEGIN
	WriteLn('==========================');
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF(CompareDate(Testruns[i].creationDate,creationDate)) Then Begin
		WriteLn('Testrun ',^i,i+1,^i,DateToString(Testruns[i].CreationDate));
		DisplayList(Testruns[i].Testresults);
		break;
		End;
	END;
	WriteLn('==========================');
END; (* PrintStatisticsForTest *)
(*Describe me*)
FUNCTION NumberOfTestRuns () : INTEGER;
Var
	i:Integer;
BEGIN
	NumberOfTestRuns:= 0;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF(IsValidDate(Testruns[i].CreationDate)) Then
			NumberOfTestRuns := NumberOfTestRuns +1;
	END;
END; (* NumberOfTestRuns  *)
(*Calculates the amout of Testruns containing Results*)
FUNCTION AmoutOfTestsWithResults() : INTEGER;
Var
	i,count : Integer;
BEGIN
	count := 0;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF (CountList(Testruns[i].Testresults) > 0) THEN count := count +1;
	END;
	AmoutOfTestsWithResults := count;
END; (* AmoutOfTestsWithResults *)
(*Describe me*)
FUNCTION TotalSuccessRatio () : Double;
BEGIN
	TotalSuccessRatio := 1- TotalNumberOfFailedTests() / AmoutOfTestsWithResults();
END; (* TotalSuccessRatio  *)
begin
	ClearTestHistory();
end.