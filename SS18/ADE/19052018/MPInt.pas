UNIT MPInt;

INTERFACE

(* parses minipascal program from given file inputFile *)
(* REF inputFile: File from which to read the Minipascal program *)
(* OUT ok: True if parsing was successful *)
(* OUT errorLine: Line in input file where errors has occurred *)
(* OUT errorColumn: ....*)
(* OUT errorText: ....*)
(* The parameters errorLine, errorColumn and errorText  must only be considered when parsing has failed (NOT ok)*)
  PROCEDURE Parse(VAR inputFile: TEXT; VAR ok: BOOLEAN; 
                  VAR errorLine, errorColumn: INTEGER; VAR errorText: STRING);
  

IMPLEMENTATION
  USES MPScan, SymTbl;
  
  VAR
    success: BOOLEAN;
    errText: STRING;
	
PROCEDURE SemErr(msg:String);
Begin
	errText := msg;
	success := FALSE;
END;

  PROCEDURE MP; FORWARD;
  PROCEDURE VarDecl; FORWARD;
  PROCEDURE StatSeq; FORWARD;
  PROCEDURE Stat; FORWARD;
    
  PROCEDURE Expr(Var e :Integer); FORWARD;
  PROCEDURE Term(Var e :Integer); FORWARD;
  PROCEDURE Fact(Var e :Integer); FORWARD;
  
  PROCEDURE MP;
  BEGIN
	{sem} ResetSymbolTable; {endsem}
    IF CurrentSymbol <> programSym THEN BEGIN success := FALSE; EXIT; END;
      GetNextSymbol;
    IF CurrentSymbol <> identSym THEN BEGIN success := FALSE; EXIT; END;
      GetNextSymbol;
    IF CurrentSymbol <> semicolonSym THEN BEGIN success := FALSE; EXIT; END;
      GetNextSymbol;
    IF CurrentSymbol = varSym THEN BEGIN
      VarDecl; IF NOT success THEN EXIT;
    END;
    IF CurrentSymbol <> beginSym THEN BEGIN success := FALSE; EXIT; END;
      GetNextSymbol;
    StatSeq; IF NOT success THEN EXIT;
    IF CurrentSymbol <> endSym THEN BEGIN success := FALSE; EXIT; END;
      GetNextSymbol;
    IF CurrentSymbol <> periodSym THEN BEGIN success := FALSE; EXIT; END;
    GetNextSymbol;
  END;
  
  
  PROCEDURE VarDecl;
  {LOCAL}
  VAR
	ok:Boolean;
  {ENDLOCAL}
  BEGIN
    IF CurrentSymbol <> varSym THEN BEGIN success := FALSE; EXIT; END;
      GetNextSymbol;
    IF CurrentSymbol <> identSym THEN BEGIN success := FALSE; EXIT; END;
	{SEM} DeclareVar(CurrentIdentName,ok); {ENDSEM}
      GetNextSymbol;
    WHILE CurrentSymbol = commaSym DO BEGIN
      GetNextSymbol;
      IF CurrentSymbol <> identSym THEN BEGIN success := FALSE; EXIT; END;
	  {SEM}
	  	DeclareVar(CurrentIdentName,ok);
		IF NOT ok THEN BEGIN
		  	SemErr('Variable already declared: ' + CurrentIdentName); Exit;
		END;
	   {ENDSEM}
      GetNextSymbol;
    END;
    IF CurrentSymbol <> colonSym THEN BEGIN success := FALSE; EXIT; END;
      GetNextSymbol;
    IF CurrentSymbol <> integerSym THEN BEGIN success := FALSE; EXIT; END;
      GetNextSymbol;
    IF CurrentSymbol <> semicolonSym THEN BEGIN success := FALSE; EXIT; END;
      GetNextSymbol;
  END;
  
  PROCEDURE StatSeq;
  BEGIN
    Stat; IF NOT success THEN EXIT;
    WHILE CurrentSymbol = semicolonSym DO BEGIN
      GetNextSymbol;
      Stat; IF NOT success THEN EXIT;
    END;
  END; (*StatSeq*)
  
  PROCEDURE Stat;
  {LOCAL}
  VAR
	dest: String;
	e:Integer;
  {ENDLOCAL}
  BEGIN
    CASE CurrentSymbol OF
      identSym: BEGIN 
        GetNextSymbol; 
        IF CurrentSymbol <> assignSym THEN BEGIN success := FALSE; EXIT; END;
          GetNextSymbol;
        Expr; IF NOT success THEN EXIT;
      END;
      readSym: BEGIN
        GetNextSymbol;
        IF CurrentSymbol <> leftParSym THEN BEGIN success := FALSE; EXIT; END;
        GetNextSymbol;
        IF CurrentSymbol <> identSym THEN BEGIN success := FALSE; EXIT; END;
        GetNextSymbol;
        IF CurrentSymbol <> rightParSym THEN BEGIN success := FALSE; EXIT; END;
        GetNextSymbol;
      END;
      writeSym: BEGIN
        GetNextSymbol;
        IF CurrentSymbol <> leftParSym THEN BEGIN success := FALSE; EXIT; END;
        GetNextSymbol;
        Expr; IF NOT success THEN EXIT;
        IF CurrentSymbol <> rightParSym THEN BEGIN success := FALSE; EXIT; END;
        GetNextSymbol;
      END;
    END; (*CASE*)
  END; (*Stat*)
  
  PROCEDURE Expr;
  BEGIN
    Term; IF NOT success THEN EXIT;
    WHILE (CurrentSymbol = plusSym) OR (CurrentSymbol = minusSym) DO BEGIN
      CASE CurrentSymbol OF
        plusSym: BEGIN
          GetNextSymbol;
          Term; IF NOT success THEN EXIT;
        END;
      minusSym: BEGIN
        GetNextSymbol;
        Term; IF NOT success THEN EXIT;
      END;
     END;
    END;
  END;
  
  PROCEDURE Term;
  BEGIN
    Fact; IF NOT success THEN EXIT;
    WHILE (CurrentSymbol = multSym) OR (CurrentSymbol = divSym) DO BEGIN
      CASE CurrentSymbol OF
        multSym: BEGIN
          GetNextSymbol;
          Fact; IF NOT success THEN EXIT;
        END;
        divSym: BEGIN
        GetNextSymbol;
        Fact; IF NOT success THEN EXIT;
        END;
       END;
    END;
  END;

  PROCEDURE Fact;
  BEGIN
    CASE CurrentSymbol OF 
      identSym: BEGIN
        GetNextSymbol;
      END;
      numberSym: BEGIN
        GetNextSymbol;
      END;
      leftParSym: BEGIN
        GetNextSymbol;
        Expr; IF NOT success THEN EXIT;
        IF CurrentSymbol <> rightParSym THEN BEGIN
          success := FALSE; EXIT;
        END;
        GetNextSymbol;
      END
      ELSE BEGIN
        success := FALSE; EXIT;
      END; (*CASE*)
    END;
  END;
  
  PROCEDURE Parse(VAR inputFile: TEXT; VAR ok: BOOLEAN; 
                  VAR errorLine, errorColumn: INTEGER; VAR errorText: STRING);
  BEGIN
    success := TRUE;
    errText := '';
    InitScanner(inputFile);
    MP;
    ok := success;
    GetCurrentSymbolPosition(errorLine, errorColumn);
    errorText := errText;
  END; (*Parse*)
  
 
  END.