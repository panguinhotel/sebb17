--1
SELECT f.title FROM film f
WHERE f.film_id NOT IN (SELECT i.film_id FROM inventory i);

--2
SELECT Count(*) AS Summe,c.first_name AS Vorname, c.last_name AS Nachname 
FROM customer c
INNER JOIN rental r ON c.customer_id = r.customer_id
GROUP BY c.first_name, c.last_name
ORDER BY Count(*) desc;

--3
SELECT title
FROM film
WHERE original_language_id IS NOT NULL;	

--4
SELECT Count(*)
FROM rental r
WHERE r.RENTAL_DATE between to_date('01.01.2014','dd.mm.yyyy') AND  to_date('31.12.2014','dd.mm.yyyy');

--5
SELECT i.inventory_id FROM inventory i
WHERE i.inventory_id NOT IN 
(SELECT Distinct t.inventory_id FROM rental t);

--6
SELECT first_name || ' ' || last_name AS Name
FROM (
    SELECT DISTINCT first_name, last_name
    FROM actor
    INNER JOIN film_actor USING (actor_id)
    GROUP BY first_name, last_name
    ORDER BY COUNT(*) desc)
WHERE rownum <= 10;

--7
SELECT title
FROM (
  SELECT title
  FROM film 
  INNER JOIN film_category USING (film_id)
  WHERE category_id IN (SELECT category_id
    FROM (SELECT category_id 
      FROM film
      INNER JOIN film_category USING (film_id)
      GROUP BY category_id
      ORDER BY MAX(length))
    WHERE rownum <= 1)
  ORDER BY length desc)
WHERE rownum = 1;

--8
SELECT c.first_name || ' ' || c.last_name AS Name, c.store_id, SUM(p.amount) AS Umsatz
FROM customer c 
INNER JOIN payment p ON p.customer_Id = c.customer_Id 
INNER JOIN (SELECT store_id, MAX(sumAmount) AS maximum             
FROM (SELECT customer_Id, SUM(amount) AS sumAmount, store_id                   
FROM payment p                   
INNER JOIN customer c USING(customer_Id)                   
GROUP BY customer_Id, store_id) s                 
GROUP BY s.store_id) s ON s.store_id = c.store_id 
GROUP BY c.customer_Id, c.store_id, c.first_name, c.last_name, s.maximum 
HAVING SUM(p.amount) = s.maximum;
 
 
