#pragma once

template<typename T>
class scoped_ptr final {
public:
  using value_type = T;

  scoped_ptr() = default;
  explicit scoped_ptr(T *ptr) : ptr(ptr) {}

  // prevent copying
  scoped_ptr(const scoped_ptr &) = delete;
  scoped_ptr& operator = (const scoped_ptr &) = delete;

  // allow only moving
  scoped_ptr(scoped_ptr &&other) {
    using std::swap;
    swap(ptr, other.ptr);
  }

  scoped_ptr& operator = (scoped_ptr &&other) {
    using std::swap;
    reset();
    swap(ptr, other.ptr);
    return *this;
  }

  ~scoped_ptr() { delete ptr; }

  void reset(T *ptr = nullptr) {
    if (this->ptr != ptr) {
      delete this->ptr;
      this->ptr = ptr;
    }
  }

  T* get() const { return ptr;}

  // NOTE: implement all pointer behaviors
  T& operator * () const { return *ptr; } // deref
  T* operator -> () const { return ptr; } // method call (deref)
  operator T* () const { return ptr; }    // cast to pointer

private:
  T* ptr { nullptr };
};

template <typename T, typename... Args>
scoped_ptr<T> make_scoped(Args&&... args) {
  return scoped_ptr<T>(new T(std::forward<Args>(args)...));
}
