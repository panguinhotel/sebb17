xquery version "1.0";
(: conditional und precedes op:)
declare default element namespace "http://www.fh-hagenberg.at/projectteam";
<result>
{
let $doc := fn:doc("person.xml")

for $pers in $doc//Person

return if ($pers >> $doc//Person[2]) then ("JA-") else ("NEIN-")

}
</result>