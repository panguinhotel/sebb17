PROGRAM ParserTest;

USES
	Parser;
VAR
	input: String;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	input := 'true+ (not false => t+ false and true <=>t eq(t+-f)    ) or t and f imp t';
	InitParser(input);IF NOT Parse THEN BEGIN WriteLn('ERROR at position :  ', ErrorPos);
	END ELSE BEGIN WriteLn('Success');END;

	input := 'true + false';
	InitParser(input);IF NOT Parse THEN BEGIN WriteLn('ERROR at position :  ', ErrorPos);
	END ELSE BEGIN WriteLn('Success');END;

	input := 'true imp t and (false <=> t)';
	InitParser(input);IF NOT Parse THEN BEGIN WriteLn('ERROR at position :  ', ErrorPos);
	END ELSE BEGIN WriteLn('Success');END;
END. (* ParserTest *)