package queues;

import java.util.ArrayList;

public class DHeapQueue
{

	private ArrayList<Integer> m_values;
	private int m_d;
	public DHeapQueue(int d)
	{
		m_values = new ArrayList<Integer>();
		m_d = d;
	}

	public boolean isEmpty() {
		return m_values.isEmpty();
	}

	public void enqueue(int x) {
		assert isHeap();
		m_values.add(x); // not insert()
		upHeap();
		assert isHeap();
	}
	
	public boolean isHeap() {
		int i = 1;
		while (i < m_values.size() && m_values.get(parent(i)) >= m_values.get(i))
			i++;
		return i >= m_values.size();
	}


	private int parent(int i) {
		return (i - 1) / m_d;
	}

	private int child(int i, int offset)
	{
		return i*m_d+offset;
	}

	public void upHeap() {
		int i = m_values.size() - 1;
		int x = m_values.get(i);
		while (i != 0 && m_values.get(parent(i)) < x) {
			m_values.set(i, m_values.get(parent(i)));
			i = parent(i);
		}
		m_values.set(i, x);
	}

	private void downHeap() {
		assert !m_values.isEmpty();
		int i = 0;
		int x = m_values.get(0);
		for(int k = 1; k < m_d; k++)
		{
			while (child(i,k) < m_values.size()) {
				int j = child(i,k); // j = larger child index
				for(int l = 1; l < m_d; l++)
				{
					if (child(i,l) < m_values.size() && m_values.get(j) < m_values.get(child(i,l))) {
						j = child(i,l);
					}
				}
				if (x >= m_values.get(j))
					break;
				m_values.set(i, m_values.get(j));
				i = j;
			}
			m_values.set(i, x);
		}
		
	}

	public int dequeue() {
		if(!isHeap())
			throw new IllegalStateException("Heap Eigenschaft ist nicht erf�llt");
		else if(m_values.isEmpty())
			throw new IllegalStateException("Warteschlange ist leer");
		else
		{
			int top = m_values.get(0);
			m_values.set(0, m_values.get(m_values.size() - 1));
			m_values.remove(m_values.size() - 1);
			if (!m_values.isEmpty())
				heapify();
			assert isHeap();
			return top;
		}
	}


	public void insertUnordered(int elem)
	{
		m_values.add(elem);
	}

	public void insertUnordered(int[] elemArray)
	{
		for(int i = 0; i < elemArray.length; i++)
		{
			insertUnordered(elemArray[i]);
		}
	}

	public void insert(int elem)
	{
		m_values.add(elem);
		upHeap();
	}

	public int max()
	{
		if(!isHeap())
			throw new IllegalStateException("Heap Eigenschaft ist nicht erf�llt");
		else if(m_values.isEmpty())
			throw new IllegalStateException("Warteschlange ist leer");
		else return m_values.get(0);

	}

	public int removeMax()
	{
		return dequeue();		
	}

	public int[] nLargest(int n)
	{
		if(!isHeap())
			throw new IllegalStateException("Heap Eigenschaft ist nicht erf�llt");
		else if(size() < n)
			throw new IllegalStateException("Warteschlange hat zu wenig elemente");
		
		int[] returnValue = new int[n];
		int count = 0;
		for(int i = 0; i < n; i++)
		{
			returnValue[count] = m_values.get(i);
			count ++;
		}
		
		return returnValue;
	}
	
	public int[] removeNLargest(int n)
	{
		if(!isHeap())
			throw new IllegalStateException("Heap Eigenschaft ist nicht erf�llt");

		int[] returnValue = new int[n];
		for(int i = 0; i < n && i < m_values.size();i++)
		{
			returnValue[i] = removeMax();
		}
		return returnValue;		
	}

	private int findMaxChildIdx(int idx)
	{
		int curMax = idx;		
		for(int i = 1; i <= m_d; i++)
		{
			if(child(idx,i)< size())
			{
				int tmpIdx = findMaxChildIdx(child(idx,i));
				if(m_values.get(curMax) < m_values.get(tmpIdx))
					curMax = tmpIdx;
			}
		}
		return curMax;
	}
	public void heapify()
	{
		if(!isHeap())
		{
			int start = size()/m_d;
			while(start >= 0)
			{
				heapifyIdx(start);	
				if(isHeap())
					break;				
				start--;
			}
		}
		
	}
	private void heapifyIdx(int idx)
	{
		int maxChildIdx = findMaxChildIdx(idx);
		if(maxChildIdx != idx)
		{
			int tmpVal = m_values.get(maxChildIdx);
			m_values.set(maxChildIdx, m_values.get(idx));
			m_values.set(idx, tmpVal);
			
		}
		
		for(int i = 1; i <= m_d; i++)
		{
			if(child(idx,i) < size())
				heapifyIdx(child(idx,i));
		}
	}
	public int size()
	{
		return m_values.size();
	}

	public void merge(DHeapQueue queue)
	{
		insertUnordered(queue.nLargest(queue.size()));
		heapify();
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(); // like stringstream
		sb.append("heap = [");
		for (int i = 0; i < m_values.size(); i++) {
			if (i > 0)
				sb.append(" ");
			sb.append(m_values.get(i));
		}
		sb.append("]");
		return sb.toString();
	}
}
