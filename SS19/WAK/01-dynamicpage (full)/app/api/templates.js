const router = require('express').Router();
const fs = require('fs');

router.get('/', function (req, res) {
    fs.readFile('./templates.json', 'utf8', function (err, data) {
        res.json(JSON.parse(data));
    });

});

module.exports = router;