/*
 * skipset.h
 *
 *  Created on: 22.03.2019
 *      Author: Lukas Schaubmayr
 */

#ifndef SKIPSET_H_
#define SKIPSET_H_

#include "skipnode.h"
#include "skipsetiterator.h"
#include <algorithm>
#include <limits>

template<typename T, const int MAXLEVEL=32>
class skip_set {
	friend class skip_set_iterator<T>;
public:
	typedef skip_set_iterator<T> iterator;
	skip_set(){
		m_probability = 0.5;
		T headKey = std::numeric_limits<T>::min();
		m_head = new skip_node<T>(headKey, MAXLEVEL);

		T tailKey = std::numeric_limits<T>::max();
		m_tail = new skip_node<T>(tailKey, MAXLEVEL);

		std::fill(m_head->forward.begin(),m_head->forward.end(), m_tail);
	}
	virtual ~skip_set(){
		delete m_head;
		delete m_tail;
	}

	int size() const{
		int count = 0;
		skip_node<T>* node = m_head;
		while(node->forward.at(0) != m_tail)
		{
			node = node->forward.at(0);
			count ++;
		}
		return count;
	}
	bool find(T value){
		skip_node<T>* result = nullptr;
		if (auto x = lower_bound(value))
		{
			if(x->key == value && x != m_tail)
				result = x;
		}
		return result != nullptr;
	}
	void insert(T value){
		if(!find(value))
		{
			std::vector<skip_node<T>*> pred = predecessors(value);
			const int nextLevel = random_level();
			auto * node = make_node(value,nextLevel);

			for(int i = 0; i < nextLevel; ++i)
			{
				node->forward[i] = pred[i]->forward[i];
				pred[i]->forward[i] = node;
			}
		}

	}
	bool erase(T value){
		auto pred = predecessors(value);
		auto *node = pred.at(0)->forward.at(0);
		if(node->key != value || node == m_tail)
			return false;

		for(auto i = 0; i < node->forward.size(); ++i)
		{
			pred[i]->forward[i] = node->forward[i];
		}
		delete node;

		return true;
	}

	iterator begin() {
		skip_set_iterator<T> * new_It = new skip_set_iterator<T>(m_head->forward[0]);

		return *new_It;
	}

	iterator end() {
		skip_set_iterator<T> * new_It = new skip_set_iterator<T>(m_tail);

		return *new_It;
	}
private:
	skip_node<T>* m_head;
	skip_node<T>* m_tail;
	float m_probability = 0.5;
	int m_max_level = MAXLEVEL;

	skip_node<T>* lower_bound(T key) const{
		auto *node = m_head;
		for(auto i = m_head->forward.size(); i-- > 0;)
		{
			while(node->forward.at(i)->key < key)
				node = node->forward.at(i);
		}
		return node->forward.at(0);
	}
	int random_level() const{
		int tmp = 1;
		while ((static_cast<double>(std::rand()) /RAND_MAX) < m_probability && tmp < m_max_level)
		{
			tmp++;
		}
		return tmp;
	}
	skip_node<T>* make_node(T key, int level){
		return new skip_node<T>(key,level);
	}

	std::vector<skip_node<T>*> predecessors(T key) const{
		std::vector<skip_node<T>*> returnVal(m_head->forward.size(),nullptr);
		auto *node = m_head;

		for(int i = m_head->forward.size(); i-- > 0;)
		{
			while(node->forward.at(i)->key < key)
				node = node->forward.at(i);

			returnVal[i] = node;
		}

		return returnVal;
	}
};

#endif /* SKIPSET_H_ */
