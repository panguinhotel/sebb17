-- PIVOT und UNPIVOT

-- Aufgabe 1
-- Erstellen Sie eine Pivot-Tabelle, die angibt, wie viele und welche Jobs es in welcher Abteilung gibt.
-- Listen Sie die Job-IDs zeilenweise auf und verwenden Sie die Abteilungen in den Spalten.
-- Sie k�nnen einschr�nken auf die Abteilungen Shipping, Marketing, Sales, Executive, Accounting.

SELECT *
FROM
(
  Select job_id,department_name from employees
  join departments using(department_id)
)
PIVOT
(
  Count(*) FOR department_name
  IN('Shipping', 'Marketing', 'Sales','Executive', 'Accounting')
);


      
-- Aufgabe 2
-- Erstellen Sie eine Unpivot-Tabelle f�r die vorherige Abfrage, nennen Sie die Spalten 
-- num_employees und department_name und geben Sie nur jene Zeilen an, deren
-- Anzahl an Angestellter gr��er als 0 sind.     

WITH job_pivot as(
SELECT *
FROM
(
  Select job_id,department_name from employees
  join departments using(department_id)
)
PIVOT
(
  Count(*) FOR department_name
  IN('Shipping'AS Shipping, 'Marketing'AS Marketing, 'Sales'AS Sales,'Executive' AS Executive, 'Accounting' AS Accounting)
)
)
SELECT *
FROM job_pivot
UNPIVOT
(
  value
  FOR num_employees
  IN(Shipping, Marketing, Sales, Executive, Accounting)
)
Where value > 0;


