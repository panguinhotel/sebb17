#include <stdio.h>
#include <string.h>

typedef enum bool {false,true};

int main(int argc, char *argv[]) {
    char password[11];
    char name[11];
    int level = 0;

    if (argc != 2) {
        printf("usage: buffer-overflow <username>\n");
        printf("e.g. \"buffer-overflow admin\"\n");
        printf("then enter your password\n");
        printf("NOTE: max 10 chars are allowed\n");
        return -1;
    }

    strcpy(name, argv[1]);
    printf("password: ");
    scanf("%s", password);
    if(strlen(password) > 10)
    {
        printf("Error: max 10 chars are allowed\n");
        return -1;
    }
        
    
    if (strcmp(name, "admin") == 0 &&
        strcmp(password, "pass") == 0) {
            level = 97;
    }

    printf("username = \"%s\", password = \"%s\", access level = %d (0x%x, '%c')\n", name, password, level, level, level);
    if (level == 97) {
      printf("FULL ACCESS GRANTED\nSource Code Password is \"bufovfl\"");
    } else {
      printf("You need access level = %d (0x%x, '%c') to access the ZIP-File password", 97, 97, 97);
    }

    return 0;
}
