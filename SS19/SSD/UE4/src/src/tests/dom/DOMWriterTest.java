package tests.dom;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

// Generates a valid "Course Catalog" XML file with one course 

public class DOMWriterTest {

	public static void main(String[] args) {
		StartMemoryProfiling();
		StartTimeProfiling();

		System.out.println("DOMWriterTest BEGIN");
		File inputFile = new File("xmlfiles/100Entries.xml");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(inputFile);

			int buchHasMoreThan1Author = 0;
			NodeList nList = doc.getElementsByTagName("Buch");
			for(int i = 0; i < nList.getLength(); ++i)
			{
				int autorCount = 0;
				Node nNode = nList.item(i);
				NodeList buchChildren = nNode.getChildNodes();
				for(int k = 0; k < buchChildren.getLength();++k)
				{   		 
					Node kNode = buchChildren.item(k);
					if(kNode.getNodeName().equals("Autor"))
					{
						autorCount += kNode.getTextContent().split(",").length;
					}
					else if(kNode.getNodeName().equals("ISBN"))
					{    			 
						kNode.setTextContent(kNode.getTextContent().replace("ISBN: ", ""));
					}
					else if(kNode.getNodeName().equals("Preis"))
					{
						NamedNodeMap attributes = kNode.getAttributes();

						for(int l = 0; l < attributes.getLength(); ++l)
						{
							if(attributes.item(l).getNodeName().equals("waehrung"))
							{
								if(attributes.item(l).getNodeValue().equals("Dollar"))
								{
									kNode.setTextContent(String.format("%.2f", Double.parseDouble(kNode.getTextContent())/1.12));
									attributes.item(l).setNodeValue("Euro");
								}
							}
						}
					}
				}

				if(autorCount > 1)
					buchHasMoreThan1Author ++; 
			}
			Element statistiks = doc.createElement("Statistik");
			Element autoren = doc.createElement("Autoren");     
			autoren.setAttribute("mehrAls", "1");
			autoren.setTextContent(String.valueOf(buchHasMoreThan1Author));
			statistiks.appendChild(autoren);

			doc.getDocumentElement().appendChild(statistiks);

			// Serialize the DOM to the given file
			String fileName = "100Entries_DOMWriter_1_" + new Date().toString().replaceAll("[ :]", "_") + ".xml";
			File file = new File("xmlfiles/" + fileName);
			TransformerFactory fact = TransformerFactory.newInstance();
			Transformer t = fact.newTransformer();
			t.setOutputProperty("doctype-system", "books.dtd");
			t.setOutputProperty("indent", "yes");
			t.transform(new DOMSource(doc), // source
					new StreamResult(new FileOutputStream(file)) // target
					);
			System.out.println("New file was created in " + file.getAbsolutePath());

		} catch (Throwable e) {
			System.out.println("Exception Type: " + e.getClass().toString());
			System.out.println("Exception Message: " + e.getMessage());
			e.printStackTrace();
		}


		StopMemoryProfiling();
		StopTimeProfiling();
		System.out.println("Time : " + Difference());
		System.out.println("Memory : " + MemoryUsageInBytes());
		
		System.out.println("DOMWriterTest END");

	}

	private static long startDate;
	private static long endDate;
	private static boolean timeProfilingStarted = false;

	public static void StartTimeProfiling() {
		if (!timeProfilingStarted) {
			startDate = System.currentTimeMillis();
			timeProfilingStarted = true;
		}
	}

	public static void StopTimeProfiling() {
		endDate = System.currentTimeMillis();
		timeProfilingStarted = false;
	}

	/**
	 * 
	 * @return the time difference in milliseconds
	 */
	public static long Difference() {
		return (endDate - startDate);
	}

	/**
	 * 
	 * @return the time difference in seconds
	 */
	public static long TimeDifferenceInSeconds() {
		return Difference() / 1000;
	}

	private static long startMemory;
	private static long endMemory;
	private static boolean memoryProfilingStarted = false;

	public static void StartMemoryProfiling() {
		if (!memoryProfilingStarted) {
			startMemory = GetSystemUsedMemory();
			memoryProfilingStarted = true;
		}
	}

	private static long GetSystemUsedMemory() {
		return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	}

	public static void StopMemoryProfiling() {
		endMemory = GetSystemUsedMemory();
		memoryProfilingStarted = false;
	}

	private static long MemoryUsageInBytes() {
		return (endMemory - startMemory);
	}

	public static long MemoryUsageInMB() {
		return MemoryUsageInBytes() / 1024 / 1024;
	}

	public static void Reset() {
		memoryProfilingStarted = false;
		timeProfilingStarted = false;
	}

}
