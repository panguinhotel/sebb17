--UE3-6.1 & 6.2
DROP TABLE top_salaries;                                        --SQL
CREATE TABLE top_salaries (salary NUMBER(8,2));                 --SQL

DELETE FROM top_salaries;                                       --SQL
DECLARE                                                         --PLSQL
  num NUMBER(3) := &p_num;                                      --PLSQL
  sal employees.salary%TYPE;                                    --PLSQL
  CURSOR  emp_cursor IS                                         --PLSQL
    SELECT DISTINCT salary                                      --SQL
    FROM employees                                              --SQL
      ORDER BY salary DESC;                                     --SQL
BEGIN                                                           --PLSQL
  OPEN emp_cursor;                                              --PLSQL
  FETCH emp_cursor INTO sal;                                    --PLSQL
  WHILE emp_cursor%ROWCOUNT <= num AND emp_cursor%FOUND LOOP    --PLSQL
    INSERT INTO top_salaries (salary)                           --SQL
    VALUES(sal);                                                --SQL
    FETCH emp_cursor INTO sal;                                  --PLSQL
  END LOOP;                                                     --PLSQL
  CLOSE emp_cursor;                                             --PLSQL
END;                                                            --PLSQL

Select * from top_salaries;                                     --SQL
--UE3-6.3
DROP TABLE top_salaries;                                    
CREATE TABLE top_salaries (salary NUMBER(8,2)); 

ALTER TABLE top_salaries
  ADD (emp_cnt number(3) NOT NULL CHECK(emp_cnt > 0))
  MODIFY (salary NUMBER(8,2) NOT NULL)
  ADD CONSTRAINT top_salariesPK PRIMARY KEY (salary);

--UE-6.4
DELETE FROM top_salaries;                                       
DECLARE                                                        
  num NUMBER(3) := &p_num;                                      
  sal employees.salary%TYPE;
  CNT top_salaries.emp_cnt%TYPE;
  CURSOR  emp_cursor IS                                      
    SELECT  salary, Count(employee_id) as CNT                                      
    FROM employees
    GROUP BY salary
    ORDER BY salary DESC;                                    
BEGIN                                                          
  OPEN emp_cursor;                                             
  FETCH emp_cursor INTO sal, CNT;                                    
  WHILE emp_cursor%ROWCOUNT <= num AND emp_cursor%FOUND LOOP    
    INSERT INTO top_salaries (salary,emp_cnt)                          
    VALUES(sal,CNT);                                               
    FETCH emp_cursor INTO sal,CNT;                                 
  END LOOP;                                                     
  CLOSE emp_cursor;                                            
END;                                                           

Select * from top_salaries;                                     