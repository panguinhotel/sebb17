Select * from dictionary;
select * from all_users;
Select * from user_all_tables;
Select * from user_tab_cols;
Select * from PRODUCT_COMPONENT_VERSION;

-- PIVOT und UNPIVOT

-- Z�hlt die Anzahl der Angestellten in den Abteilungen und gibt diese
-- in einer Zeile aus.
SELECT *
FROM (
  SELECT department_id
  FROM employees
)
PIVOT
( COUNT(*)
  FOR department_id
  IN (10,20,30,40,90) );
  
  
-- Minimum- und Maximum-Geh�lter verschiedener Job-IDs (grouped!) nach Abteilungen.
-- IN-Clause in Pivot ist nicht dynamisch, muss bei Bedarf im vorangehenden SQL ausgef�hrt werden,
-- gilt auch f�r zB Zusammenfassung von Altersgruppen).
SELECT *
FROM (
   SELECT department_id, salary,
      CASE 
        WHEN job_id LIKE 'SA%' THEN 'SA'
        WHEN job_id LIKE '%MAN' OR job_id LIKE '%MGR' THEN 'MAN'
        ELSE 'OTH' 
      END AS job_desc
   FROM employees
)
PIVOT
(
  MIN(salary) AS min_sal,
  MAX(salary) AS max_sal
  FOR job_desc
  IN ('SA' AS Sales,	-- Vergabe von Alias mit AS damit auf Spalten zugegriffen werden kann
      'MAN' AS Manager,
      'OTH' AS Others)
);


-- Erstellen Sie eine Tabelle, die die Kontaktdaten der Angestellten zusammenfasst.
-- Die Tabelle soll als Spalten den Nachnamen (last_name), die Kontakt-Art (contact_method = email, phone_number)
-- und den Wert (die E-Mail-Adresse, Telefonnummer) enthalten.
SELECT * 
FROM (
  SELECT last_name, email, phone_number
  FROM employees
)
UNPIVOT
( value							-- neuer Spaltenname (der Werte enth�lt)
  FOR contact_method			-- neuer Spaltenname (der Spalten-�berschriften enth�lt)
  IN (email, phone_number) );	-- Spalten-�berschriften, die (in contact_method) verwendet werden sollen


Select abflug, ziel, Flugnr, LEVEL, CONNECT_BY_ISCYCLE, CONNECT_BY_ISLEAF, 
SYS_CONNECT_BY_PATH(Abflug || '->' || Ziel, ', ')
FROM Flug
START WITH Abflug = 'Z�rich'
CONNECT BY NOCYCLE PRIOR Ziel = Abflug;

Insert into flug values('NY101','New York','Z�rich',1222);