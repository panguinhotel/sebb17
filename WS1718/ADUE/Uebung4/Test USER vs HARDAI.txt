TESTS: PLAYER VS HARD AI

Linking Uebung4.exe
256 lines compiled, 0.1 sec, 31152 bytes code, 1316 bytes data
Gamemodes :
0 = Player vs Player
1 = Player vs easy AI
2 = Player vs hard AI
Selected Mode = 2
User = Player1
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
==================
PlAYER 1 Turn
TIPP 1
Row-Index    : 5
Column-Index : 4
TIPP SET [ ROW : 5  COLUMN : 4 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       14
==================
TIPP 2
Row-Index    : 5
Column-Index : 3
TIPP SET [ ROW : 5  COLUMN : 3 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       2       14
==================
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
==================


PlAYER 2 Turn
TIPP SET [ ROW : 5  COLUMN : 2 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       4       0       0
==================
TIPP SET [ ROW : 5  COLUMN : 1 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       4       4       0       0
==================


PlAYER 2 Turn
TIPP SET [ ROW : 5  COLUMN : 0 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
3       4       4       0       0
==================
TIPP SET [ ROW : 4  COLUMN : 4 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       13
3       4       4       0       0
==================
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       4       4       0       0
==================


PlAYER 1 Turn
TIPP 1
Row-Index    : 4
Column-Index : 3
TIPP SET [ ROW : 4  COLUMN : 3 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       13      0
0       4       4       0       0
==================
TIPP 2
Row-Index    : 4
Column-Index : 4
TIPP SET [ ROW : 4  COLUMN : 4 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       13      13
0       4       4       0       0
==================


PlAYER 1 Turn
TIPP 1
Row-Index    : 4
Column-Index : 2
TIPP SET [ ROW : 4  COLUMN : 2 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       1       13      13
0       4       4       0       0
==================
TIPP 2
Row-Index    : 4
Column-Index : 1
TIPP SET [ ROW : 4  COLUMN : 1 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       8       1       13      13
0       4       4       0       0
==================
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       13      13
0       4       4       0       0
==================


PlAYER 2 Turn
TIPP SET [ ROW : 4  COLUMN : 0 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
8       0       0       13      13
0       4       4       0       0
==================
TIPP SET [ ROW : 4  COLUMN : 1 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
8       8       0       13      13
0       4       4       0       0
==================


PlAYER 2 Turn
TIPP SET [ ROW : 3  COLUMN : 4 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       14
8       8       0       13      13
0       4       4       0       0
==================
TIPP SET [ ROW : 5  COLUMN : 4 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       14
8       8       0       13      13
0       4       4       0       14
==================


PlAYER 2 Turn
TIPP SET [ ROW : 3  COLUMN : 3 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       10      14
8       8       0       13      13
0       4       4       0       14
==================
TIPP SET [ ROW : 3  COLUMN : 2 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       12      10      14
8       8       0       13      13
0       4       4       0       14
==================
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       14
8       8       0       13      13
0       4       4       0       14
==================


PlAYER 1 Turn
TIPP 1
Row-Index    : 3
Column-Index : 1
TIPP SET [ ROW : 3  COLUMN : 1 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       7       0       0       14
8       8       0       13      13
0       4       4       0       14
==================
TIPP 2
Row-Index    : 3
Column-Index : 0
TIPP SET [ ROW : 3  COLUMN : 0 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
2       7       0       0       14
8       8       0       13      13
0       4       4       0       14
==================
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       14
8       8       0       13      13
0       4       4       0       14
==================


PlAYER 2 Turn
TIPP SET [ ROW : 5  COLUMN : 3 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
0       0       0       0       14
8       8       0       13      13
0       4       4       2       14
==================
TIPP SET [ ROW : 3  COLUMN : 0 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
2       0       0       0       14
8       8       0       13      13
0       4       4       2       14
==================


PlAYER 2 Turn
TIPP SET [ ROW : 2  COLUMN : 4 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       11
2       0       0       0       14
8       8       0       13      13
0       4       4       2       14
==================
TIPP SET [ ROW : 2  COLUMN : 3 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       12      11
2       0       0       0       14
8       8       0       13      13
0       4       4       2       14
==================
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
2       0       0       0       14
8       8       0       13      13
0       4       4       2       14
==================


PlAYER 1 Turn
TIPP 1
Row-Index    : 3
Column-Index : 2
TIPP SET [ ROW : 3  COLUMN : 2 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       0       0
2       0       12      0       14
8       8       0       13      13
0       4       4       2       14
==================
TIPP 2
Row-Index    : 2
Column-Index : 3
TIPP SET [ ROW : 2  COLUMN : 3 ]
0       0       0       0       0
0       0       0       0       0
0       0       0       12      0
2       0       12      0       14
8       8       0       13      13
0       4       4       2       14
==================


PlAYER 1 Turn
TIPP 1
Row-Index    : 2
Column-Index : 2
TIPP SET [ ROW : 2  COLUMN : 2 ]
0       0       0       0       0
0       0       0       0       0
0       0       10      12      0
2       0       12      0       14
8       8       0       13      13
0       4       4       2       14
==================
TIPP 2
Row-Index    : 3
Column-Index : 3
TIPP SET [ ROW : 3  COLUMN : 3 ]
0       0       0       0       0
0       0       0       0       0
0       0       10      12      0
2       0       12      10      14
8       8       0       13      13
0       4       4       2       14
==================


PlAYER 1 Turn
TIPP 1
Row-Index    : 2
Column-Index : 1
TIPP SET [ ROW : 2  COLUMN : 1 ]
0       0       0       0       0
0       0       0       0       0
0       9       10      12      0
2       0       12      10      14
8       8       0       13      13
0       4       4       2       14
==================
TIPP 2
Row-Index    : 2
Column-Index : 0
TIPP SET [ ROW : 2  COLUMN : 0 ]
0       0       0       0       0
0       0       0       0       0
7       9       10      12      0
2       0       12      10      14
8       8       0       13      13
0       4       4       2       14
==================
0       0       0       0       0
0       0       0       0       0
0       0       10      12      0
2       0       12      10      14
8       8       0       13      13
0       4       4       2       14
==================


PlAYER 2 Turn
TIPP SET [ ROW : 3  COLUMN : 1 ]
0       0       0       0       0
0       0       0       0       0
0       0       10      12      0
2       7       12      10      14
8       8       0       13      13
0       4       4       2       14
==================
TIPP SET [ ROW : 2  COLUMN : 0 ]
0       0       0       0       0
0       0       0       0       0
7       0       10      12      0
2       7       12      10      14
8       8       0       13      13
0       4       4       2       14
==================


PlAYER 2 Turn
TIPP SET [ ROW : 1  COLUMN : 4 ]
0       0       0       0       0
0       0       0       0       3
7       0       10      12      0
2       7       12      10      14
8       8       0       13      13
0       4       4       2       14
==================
TIPP SET [ ROW : 5  COLUMN : 0 ]
0       0       0       0       0
0       0       0       0       3
7       0       10      12      0
2       7       12      10      14
8       8       0       13      13
3       4       4       2       14
==================


PlAYER 2 Turn
TIPP SET [ ROW : 1  COLUMN : 3 ]
0       0       0       0       0
0       0       0       15      3
7       0       10      12      0
2       7       12      10      14
8       8       0       13      13
3       4       4       2       14
==================
TIPP SET [ ROW : 1  COLUMN : 2 ]
0       0       0       0       0
0       0       6       15      3
7       0       10      12      0
2       7       12      10      14
8       8       0       13      13
3       4       4       2       14
==================
0       0       0       0       0
0       0       0       0       3
7       0       10      12      0
2       7       12      10      14
8       8       0       13      13
3       4       4       2       14
==================


PlAYER 1 Turn
TIPP 1
Row-Index    : 1
Column-Index : 1
TIPP SET [ ROW : 1  COLUMN : 1 ]
0       0       0       0       0
0       6       0       0       3
7       0       10      12      0
2       7       12      10      14
8       8       0       13      13
3       4       4       2       14
==================
TIPP 2
Row-Index    : 1
Column-Index : 2
TIPP SET [ ROW : 1  COLUMN : 2 ]
0       0       0       0       0
0       6       6       0       3
7       0       10      12      0
2       7       12      10      14
8       8       0       13      13
3       4       4       2       14
==================


PlAYER 1 Turn
TIPP 1
Row-Index    : 1
Column-Index : 0
TIPP SET [ ROW : 1  COLUMN : 0 ]
0       0       0       0       0
1       6       6       0       3
7       0       10      12      0
2       7       12      10      14
8       8       0       13      13
3       4       4       2       14
==================
TIPP 2
Row-Index    : 4
Column-Index : 2
TIPP SET [ ROW : 4  COLUMN : 2 ]
0       0       0       0       0
1       6       6       0       3
7       0       10      12      0
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================


PlAYER 1 Turn
TIPP 1
Row-Index    : 0
Column-Index : 4
TIPP SET [ ROW : 0  COLUMN : 4 ]
0       0       0       0       11
1       6       6       0       3
7       0       10      12      0
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================
TIPP 2
Row-Index    : 2
Column-Index : 4
TIPP SET [ ROW : 2  COLUMN : 4 ]
0       0       0       0       11
1       6       6       0       3
7       0       10      12      11
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================


PlAYER 1 Turn
TIPP 1
Row-Index    : 0
Column-Index : 3
TIPP SET [ ROW : 0  COLUMN : 3 ]
0       0       0       5       11
1       6       6       0       3
7       0       10      12      11
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================
TIPP 2
Row-Index    : 0
Column-Index : 2
TIPP SET [ ROW : 0  COLUMN : 2 ]
0       0       15      5       11
1       6       6       0       3
7       0       10      12      11
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================
0       0       0       0       11
1       6       6       0       3
7       0       10      12      11
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================


PlAYER 2 Turn
TIPP SET [ ROW : 1  COLUMN : 3 ]
0       0       0       0       11
1       6       6       15      3
7       0       10      12      11
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================
TIPP SET [ ROW : 0  COLUMN : 2 ]
0       0       15      0       11
1       6       6       15      3
7       0       10      12      11
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================


PlAYER 2 Turn
TIPP SET [ ROW : 0  COLUMN : 1 ]
0       9       15      0       11
1       6       6       15      3
7       0       10      12      11
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================
TIPP SET [ ROW : 2  COLUMN : 1 ]
0       9       15      0       11
1       6       6       15      3
7       9       10      12      11
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================


PlAYER 2 Turn
TIPP SET [ ROW : 0  COLUMN : 0 ]
5       9       15      0       11
1       6       6       15      3
7       9       10      12      11
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================
TIPP SET [ ROW : 0  COLUMN : 3 ]
5       9       15      5       11
1       6       6       15      3
7       9       10      12      11
2       7       12      10      14
8       8       1       13      13
3       4       4       2       14
==================
PLAYER: 1  POINTS: 6
PLAYER: 2  POINTS: 9
