PROGRAM MPC;

USES MPPCom;

VAR
    fIn: TEXT;
    fOut: FILE;
    ok: BOOLEAN;
    el, ec: INTEGER;
    et: STRING;

(* ---------------------------------------------------------------------- *)
(* ----------------------------- Main ----------------------------------- *)
(* ---------------------------------------------------------------------- *)
BEGIN
    Assign(fIn, ParamStr(1));
    Reset(fIn);
    Assign(fOut, ParamStr(1) + 'c');
    Rewrite(fOut);
    Parse(fIn, fOut, ok, el, ec, et);
    IF NOT ok THEN WriteLn('Error at line ', el, 'column ', ec, ': ', et);
    Close(fOut);
    Close(fIn);
END. (* MPC *)