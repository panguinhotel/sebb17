UNIT StackOOP1a;

INTERFACE

CONST
	max = 100;
TYPE
	Stack = OBJECT	// CLASS in fpc
		PUBLIC
			//inits stack
			PROCEDURE Init;

			// Pushes a value onto a stack
			// IN e: value will be pushed on the stack
			// OUT ok: FALSE in case of any erros
			PROCEDURE Push(e :INTEGER; VAR ok: BOOLEAN);

			// Pops the most recently pushed  value from the stack
			// OUT e: value popped from the stack
			// Out ok: FALSE in case of any errors
			PROCEDURE Pop(VAR e: INTEGER; VAR ok: BOOLEAN);

			// returns true is stack is empty
			FUNCTION Empty: BOOLEAN;

			PROCEDURE Done;

		PRIVATE
			// data components
			top: Integer;
			data : ARRAY [1..max] OF INTEGER;
	END;

IMPLEMENTATION



PROCEDURE Stack.Init;
BEGIN
	top := 0;
END;

PROCEDURE Stack.Done;
BEGIN
END;


PROCEDURE Stack.Push(e :INTEGER; VAR ok: BOOLEAN);
BEGIN
	IF top = max THEN BEGIN
		ok := FALSE;
	END ELSE BEGIN
		Inc(top);
		data[top] := e;
		ok := TRUE;
	END;
END;


PROCEDURE Stack.Pop(VAR e: INTEGER; VAR ok: BOOLEAN);
BEGIN
	IF empty THEN BEGIN
		ok := FALSE;
	END ELSE BEGIN
		e := data[top];
		Dec(top);
		ok := TRUE;
	END;
END;


FUNCTION Stack.Empty: BOOLEAN;
BEGIN
	Empty := top = 0;
END;

BEGIN
	// Init;
END.