PROGRAM PMBench1;
USES PMBib1;

TYPE 
	TPMProc = PROCEDURE(s, p: STRING; VAR pos: INTEGER; VAR ismatch : Boolean);

VAR 
	s, p: STRING;
	position: INTEGER;
	ismatch: Boolean;
	
PROCEDURE RunTest(pm: TPMProc);
VAR 
	stat: TPMStatistics;
BEGIN
	InitPmStatistics;
	pm(s, p, position,ismatch);
	GetPmStatistics(stat);
	if(ismatch) Then 
		WriteLn(ismatch,'   Position = ', position)
	ELSE WriteLn(ismatch)
END;

BEGIN
		WriteLn('TEST BRUTE FORCE');
		s:='Herr Berger kommt';
		p:='reger';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(BruteSearchLR);

		s:='Es herrscht reger Verkehr';
		p:='reger';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(BruteSearchLR);	

		s:='Es herrscht reger Verkehr';
		p:='regre';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(BruteSearchLR);	

		s:='Es herrscht reger Verkehr';
		p:='regee';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(BruteSearchLR);	

		s:='Obergrenze';
		p:='reger';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(BruteSearchLR);	

		s:='aabaabaaba';
		p:='aabba';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(BruteSearchLR);	

		s:='aabbbaaabaa';
		p:='aabba';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(BruteSearchLR);	

		WriteLn;
		WriteLn;
		WriteLn;
		WriteLn('TEST RABIN KARP');

		s:='Herr Berger kommt';
		p:='reger';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(RabinKarp);

		s:='Es herrscht reger Verkehr';
		p:='reger';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(RabinKarp);	

		s:='Es herrscht reger Verkehr';
		p:='regre';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(RabinKarp);	

		s:='Es herrscht reger Verkehr';
		p:='regee';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(RabinKarp);	

		s:='Obergrenze';
		p:='reger';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(RabinKarp);	

		s:='aabaabaaba';
		p:='aabba';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(RabinKarp);	

		s:='aabbbaaabaa';
		p:='aabba';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(RabinKarp);	
END.