package connect4;

import static org.junit.jupiter.api.Assumptions.assumingThat;

import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

import connect4.EngineInterface.FeldType;

public class Connect4Robot implements Robot {

	private class DecisionItem {

		public int value;
		public ArrayList<DecisionItem> children;
		public DecisionItem parent = null;
		public int row;
		public int column;
		public int player;

		void setParent(DecisionItem newParent)
		{
			if(parent != null)
				parent.children.remove(this);

			parent = newParent;
			parent.children.add(this);

		}
	}

	@Override
	public void connectToGame(String host, String port, int maxDepth, long timeLimit) throws RemoteException {
		m_stepsAhead = maxDepth;
		m_timeLimit = timeLimit;
		Registry registry = LocateRegistry.getRegistry(host);
		String url = String.format("rmi://%s/DateService", port);
		try {
			m_server = (Engine)registry.lookup(url);

		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void shutdown() throws RemoteException {
		// TODO Auto-generated method stub

	}

	private int calculateSetColumn() throws RemoteException
	{
		//Set Cells Value
		int width = m_server.getBoard().getWidth();
		int height = m_server.getBoard().getHeight();

		m_spielfeld = new int[height][width];
		for(int i = 0; i < height; ++i)
		{
			for(int k = 0; k < width; ++k)
			{
				m_spielfeld[i][k] = m_server.getBoard().getPlayer(k, i);		
			}
		}

		DecisionItem rootItem = new DecisionItem();
		rootItem.player = m_server.getCurrentPlayer()*-1;
		createTree(rootItem,0);
		DecisionItem setItem = evaluateTree(rootItem);

		return setItem.column;

	}
	public DecisionItem evaluateTree(DecisionItem parentItem)
	{
		if(parentItem.children.isEmpty())
			return parentItem;		
		else
		{
			DecisionItem curMaxItem = null;
			int maxVal = -10;
			for(int i = 0; i < parentItem.children.size(); ++i)
			{
				DecisionItem item = evaluateTree(parentItem.children.get(i));
				
				if(item.value == 3)
					return item;
				else
				{
					if(maxVal < item.value)
					{
						maxVal = item.value;
						curMaxItem = item;
					}
				}
			}
			return curMaxItem;
		}
	}
	private void createTree(DecisionItem current,int step) throws RemoteException
	{
		if(current.parent != null)
			m_spielfeld[current.row][current.column] = current.player;


		int width = m_server.getBoard().getWidth();
		int height = m_server.getBoard().getHeight();
		if(step <= m_stepsAhead*2)
		{
			for(int k = 0; k < width; ++k)
			{
				for(int i = 0; i < height; ++i)
				{
					if(m_spielfeld[i][k] == 0)
					{
						DecisionItem item = new DecisionItem();
						
						item.setParent(current);
						item.player = current.player*-1;
						item.value = Math.max(Math.max(checkHorizontal(m_spielfeld[i][k],i,k), checkVertical(m_spielfeld[i][k],i,k)), 
								checkDiagonal(m_spielfeld[i][k],i,k))*item.player;
						
						if(item.value != 3*item.player)
							createTree(item,step+=1);
						
						break;
					}
				}
			}
			
		}
		
		if(current.parent != null)
			m_spielfeld[current.row][current.column] = 0;
	}

	private int checkHorizontal(int Player, int row, int col) throws RemoteException
	{
		int width = m_server.getBoard().getWidth();

		int returnVal = 0;
		for(int i = col-1; i >= 0; ++i)
		{
			if(m_server.getBoard().getPlayer(i, row) == Player)
				returnVal++;
			else
				break;
		}

		for(int i= col+1; i < width; ++i)
		{
			if(m_server.getBoard().getPlayer(i, row) == Player)
				returnVal++;
			else
				break;
		}

		return Math.min(3, returnVal);
	}

	private int checkVertical(int Player, int row, int col) throws RemoteException
	{
		int returnVal = 0;
		for(int i = row-1; i >= 0; ++i)
		{
			if(m_server.getBoard().getPlayer(col, i) == Player)
				returnVal++;
			else
				break;
		}

		return returnVal;
	}

	private int checkDiagonal(int Player, int row, int col) throws RemoteException
	{
		int width = m_server.getBoard().getWidth();
		int height = m_server.getBoard().getHeight();

		int returnValFirst = 0;
		for(int i = row-1; i >= 0; ++i)
		{
			for(int k = col-1; k >= 0; ++k)
			{
				if(m_server.getBoard().getPlayer(k, i) == Player)
					returnValFirst++;
				else
					break;

			}
		}

		for(int i = row+1; i < height; ++i)
		{
			for(int k = col+1; k < width; ++k)
			{
				if(m_server.getBoard().getPlayer(k, i) == Player)
					returnValFirst++;
				else
					break;

			}
		}

		int returnValSecond = 0;
		for(int i = row+1; i < height; ++i)
		{
			for(int k = col-1; k >= 0; ++k)
			{
				if(m_server.getBoard().getPlayer(k, i) == Player)
					returnValSecond++;
				else
					break;

			}
		}

		for(int i = row-1; i >= 0; ++i)
		{
			for(int k = col+1; k < width; ++k)
			{
				if(m_server.getBoard().getPlayer(k, i) == Player)
					returnValSecond++;
				else
					break;

			}
		}

		return Math.max(Math.min(3, returnValFirst), Math.min(3, returnValSecond));
	}

	int[][] m_spielfeld;
	private int m_stepsAhead = 1;
	private long m_timeLimit = 1;
	private String m_hostPort;
	private String m_host;
	private Engine m_server;


}

