xquery version "1.0";
(:Comment:)
declare default element namespace "http://www.fh-hagenberg.at/projectteam";
<result>
{
let $doc := fn:doc("person.xml")

for $pers in $doc//Person

(:1. alle Personen, die 'Ma' enthalten:)

(:2. alle Personen, die ein 'i' enthalten :)

(:3. alle Personen, mit Textlänge < 120:)

return 
(:Nachname (Text) in Namen-Tag:)
 <name>{$pers/LastName/text()}</name>
}
</result>

