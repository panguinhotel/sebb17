xquery version "1.0";

declare default element namespace "http://www.fh-hagenberg.at/projectteam";
(:import schema default element namespace "http://www.fh-hagenberg.at/projectteam" at "person.xsd"; :)

(:
L�sung: keine Namespace-Deklaration sondern ein Schema-Import n�tig, um
benutzerdefinierte Datentypen von XML Schema verwenden zu k�nnen
 :) 
<result>
{
let $doc := fn:doc("person.xml") 

for $pers in $doc//Person 

(:nur ja ausgeben, wenn:)
(:1.born-Attribut enthalten:)


(:2.born-Attribut ein Datum ist:)


(:3.pers ein Element ist)


(:4.pers vom Typ PersonExtendedType ist:)
 return if($pers instance of element(*,PersonExtendedType)) 

	then <name tp="ja">{$pers/LastName/text()}</name>
	else <name tp="NEIN">{$pers/LastName/text()}</name>

}
</result>
