Create or replace view partners as
Select fa1.actor_id, fa2.actor_id AS partner_id,fa1.film_id
from film_actor fa1
inner join film_actor fa2 on (fa1.film_id = fa2.film_id)
where fa1.actor_id <> fa2.actor_id
and fa1.film_id <= 13
ORDER BY fa1.actor_id;


Select a.last_name|| ' ' || a.first_name as PartnerNames
from(
Select actor_id, partner_id, level as LV from partners
start with actor_id = 2
connect by nocycle prior partner_id = actor_id and level = 2
) b
inner join actor a on a.actor_id = b.partner_id
where b.partner_id != 2
and b.actor_id != 2
and b.partner_id NOT in 
(Select c.partner_id from partners c where c.actor_id = 2);
