
-- Teilweise ist es notwendig, den Check bestimmter Constraints zu verschieben;
-- bekannt unter "Henne-Ei"-Problem:
-- Achtung: Fehlermeldung
   CREATE TABLE chicken (cID INT PRIMARY KEY,
                      eID INT REFERENCES egg(eID));
   CREATE TABLE egg(eID INT PRIMARY KEY,
                 cID INT REFERENCES chicken(cID));
                 
-- Fehlermeldung: CREATE TABLE statement f�r chicken zeigt auf Tabelle egg (noch nicht erstellt)
-- Erstellung von Tabelle egg zeigt auf Tabelle chicken und liefert Fehler, 
-- da die Tabelle chicken noch nicht existiert.
-- -> Workaround: SQL Schema Modification Elements.

-- Zuerst werden Chicken und Egg Tabellen ohne FK Deklaration erstellt.

   CREATE TABLE chicken(cID INT PRIMARY KEY,
                        eID INT);
   CREATE TABLE egg(eID INT PRIMARY KEY,
                    cID INT);

-- Danach werden FK Constraints dazugef�gt:
   ALTER TABLE chicken ADD CONSTRAINT chickenREFegg
        FOREIGN KEY (eID) REFERENCES egg(eID)
        INITIALLY DEFERRED DEFERRABLE;
   ALTER TABLE egg ADD CONSTRAINT eggREFchicken
        FOREIGN KEY (cID) REFERENCES chicken(cID)
        INITIALLY DEFERRED DEFERRABLE;
   
-- INITIALLY DEFERRED DEFERRABLE: Oracle f�hrt Constraint-Checking verz�gert durch 
-- Beispiel: Einf�gen von (1, 2) in chicken einzuf�gen und (2, 1) in egg:

   INSERT INTO chicken VALUES(1, 2);
   INSERT INTO egg VALUES(2, 1);
   COMMIT;
   
-- Da die FK Constraints als "deferred" deklariert sind, werden sie beim Commit gepr�ft
-- ohne deferred Constraint Checking kann man nichts in chicken/egg einf�gen, 
-- da das erste Insert jeweils einen Constraint-Versto� darstellt.
   
-- Um die Tabellen zu l�schen, m�ssen zuerst die Constraints gel�scht werden,
-- da Oracle kein L�schen einer Tabelle erlaubt, die von einer anderen Tabelle
-- referenziert wird.

   ALTER TABLE egg DROP CONSTRAINT eggREFchicken;
   ALTER TABLE chicken DROP CONSTRAINT chickenREFegg;
   DROP TABLE egg;
   DROP TABLE chicken;
