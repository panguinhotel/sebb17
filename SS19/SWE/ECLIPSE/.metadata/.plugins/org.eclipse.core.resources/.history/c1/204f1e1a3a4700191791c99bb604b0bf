/*
 * skiplist.h
 *
 *  Created on: 15.03.2019
 *      Author: schau
 */

#ifndef SKIPLIST_H_
#define SKIPLIST_H_

#include <vector>

template<class T,int MAXSIZE>
class skip_list {

private:
	template<typename U>
	struct skip_node{
		long long key;
		U value{};
		std::vector<skip_node*> forward;
		skip_node<U> (long long k, U v, int level): key(k), value(v), forward(level,nullptr){}
	};

public:
	skip_list();
	virtual ~skip_list();
	void init(int probability = 0.5);

	skip_node<T>* find(long long key);
	void insert(long long key, T value);
	void erase(long long key);
private:

	skip_node<T>* m_head;
	skip_node<T>* m_tail;
	float m_probability = 0.5;
	int m_max_level = MAXSIZE;


	skip_node<T>* lower_bound(int searchKey) const ;
	int random_level();
	int node_level(const std::vector<skip_node<T>*> &value);
	skip_node<T>* make_node(long long key, T value, int level) const;
};
#endif /* SKIPLIST_H_ */
