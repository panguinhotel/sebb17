unit Parser;

interface
	PROCEDURE InitParser(VAR inp : TEXT);
	FUNCTION Parse : Boolean;
	PROCEDURE ErrorPos(Var line,column: INTEGER);

implementation
	USES
		Scanner;
	
	VAR
		success : Boolean;

	PROCEDURE JSONARRAY; FORWARD;
	PROCEDURE JSONOBJECT; FORWARD;
	PROCEDURE VALUE; FORWARD;

	PROCEDURE InitParser(VAR inp : TEXT);
	BEGIN
		InitScanner(inp);
		success := false;
	END;

	FUNCTION Parse : Boolean;
	BEGIN
		success := TRUE;
		VALUE;
		success := success and (CurrentSymbol = endSym);
		Parse :=success;
	END;

	PROCEDURE ErrorPos(Var line,column: INTEGER);
	VAR
		l,c : INTEGER;
	BEGIN
		GetCurrentSymbolPosition(l,c);
	END;
	PROCEDURE JSONOBJECT;
	begin
		IF CurrentSymbol = gkleftSym  THEN BEGIN
			GetNextSymbol;
			IF CurrentSymbol = stringSym  THEN BEGIN
				GetNextSymbol;
				IF CurrentSymbol = dpSym THEN BEGIN
					GetNextSymbol;
				END ELSE BEGIN success := FALSE; EXIT; END;
				VALUE;
				WHILE (CurrentSymbol <> gkrightSym) and success DO BEGIN
					If CurrentSymbol = commaSym THEN BEGIN
						GetNextSymbol;
						IF CurrentSymbol = stringSym THEN BEGIN
							GetNextSymbol;
							IF CurrentSymbol = dpSym THEN BEGIN
								GetNextSymbol;
							END ELSE BEGIN success := FALSE; EXIT; END;
							VALUE;
						END ELSE BEGIN success := FALSE; EXIT; END;
					END ELSE BEGIN success := FALSE; EXIT; END;
				END;
				IF CurrentSymbol = gkrightSym THEN BEGIN
					GetNextSymbol;
				END;
			END ELSE IF CurrentSymbol = gkrightSym THEN BEGIN
				GetNextSymbol;
			END ELSE BEGIN success := FALSE; EXIT; END;
		END ELSE BEGIN success := FALSE; EXIT; END;
	end;

	PROCEDURE JSONARRAY;
	BEGIN
		IF CurrentSymbol = ekleftSym THEN BEGIN
			GetNextSymbol;
			IF CurrentSymbol in [stringSym,numberSym,nullSym,trueSym,falseSym] THEN BEGIN
				GetNextSymbol;
				WHILE (CurrentSymbol <> ekrightSym) and success  DO BEGIN
					IF CurrentSymbol = commaSym THEN BEGIN
						GetNextSymbol;
						IF CurrentSymbol in [stringSym,numberSym,nullSym,trueSym,falseSym] THEN BEGIN
							GetNextSymbol;
						END ELSE IF CurrentSymbol <> ekrightSym THEN BEGIN success := FALSE; EXIT; END;
					END ELSE BEGIN success := FALSE; EXIT; END;
				END;
				IF CurrentSymbol = ekrightSym THEN BEGIN
					GetNextSymbol;
				END ELSE BEGIN success := FALSE; EXIT; END;
			END ELSE IF CurrentSymbol = gkleftSym THEN BEGIN
				JSONOBJECT;
				WHILE CurrentSymbol <> ekrightSym DO BEGIN
					IF CurrentSymbol = commaSym THEN BEGIN
						GetNextSymbol;
						IF CurrentSymbol = gkleftSym THEN BEGIN
							JSONOBJECT;
						END ELSE IF CurrentSymbol <> ekrightSym THEN BEGIN success := FALSE; EXIT; END;
					END ELSE BEGIN success := FALSE; EXIT; END;
				END;
				IF CurrentSymbol = ekrightSym THEN BEGIN
					GetNextSymbol;
				END ELSE BEGIN success := FALSE; EXIT; END;
			END ELSE IF CurrentSymbol = ekrightSym THEN BEGIN
				GetNextSymbol;
			END;
		END ELSE BEGIN success := FALSE; EXIT; END;
	END;
	
	PROCEDURE VALUE;
	begin
		CASE CurrentSymbol OF
		  ekleftSym : BEGIN
		  	JSONARRAY;
		  END;
		  gkleftSym : BEGIN
		  	JSONOBJECT;
		  END;
		  stringSym,numberSym,nullSym,trueSym,falseSym : BEGIN
			GetNextSymbol;
		  END ELSE BEGIN success := FALSE; EXIT; END;
		END;
	end;

end.