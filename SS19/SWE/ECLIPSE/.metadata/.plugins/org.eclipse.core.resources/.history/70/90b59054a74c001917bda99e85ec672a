/*
 * skipsetiterator.h
 *
 *  Created on: 22.03.2019
 *      Author: schau
 */

#ifndef SKIPSETITERATOR_H_
#define SKIPSETITERATOR_H_

#include "skipnode.h"

template<typename T>
class skip_set_iterator {
	friend class skip_node<T>;
public:
	skip_set_iterator() noexcept : m_currNode(nullptr){}
	skip_set_iterator(const skip_node<T>* node) noexcept : m_currNode(node){}
	skip_set_iterator& operator=(skip_node<T>* node){this->m_currNode = node; return *this;}
	skip_set_iterator& operator++(){
		if(m_currNode)
			m_currNode = m_currNode->forward[0];
		return *this;
	}
	skip_set_iterator operator++(int){
		skip_set_iterator iterate = *this;
		++*this;
		return iterate;
	}
	bool operator != (const skip_set_iterator& other){
		return m_currNode != other.m_currNode;
	}
	T operator*(){
		return m_currNode->key;
	}
	T operator->()
	{
		return operator*();
	}
private:
	skip_node<T>* m_currNode;
};

#endif /* SKIPSETITERATOR_H_ */
