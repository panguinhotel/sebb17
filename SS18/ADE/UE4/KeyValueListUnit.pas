unit KeyValueListUnit;
	
interface
TYPE	
	Data = ^DataRec;
	DataRec = Record
	Key : String;
	Value : Integer;
	isAnchor : Boolean;
	prev,next : Data; End;
	List = Data;

(*Init List*)
PROCEDURE Init(Var l: List);
(*Disposes List*)
PROCEDURE DisposeList(l: List);
(*Clears List*)
PROCEDURE Clear(Var l: List);
(*Appens New Value To List*)
PROCEDURE Append(VAR l : List; key : String; value: Integer);
implementation
(*Init List*)
PROCEDURE Init(Var l: List);
VAR
	n:Data;
BEGIN
	New(n);
	n^.isAnchor := true;
	n^.prev := n;
	n^.next := n;
	l := n;
END; (* Init *)
(*Disposes List*)
PROCEDURE DisposeList(l: List);
BEGIN
	Clear(l);
	Dispose(l);
END; (* Dispose *)
(*Clears List*)
PROCEDURE Clear(Var l: List);
VAR
	n, next : Data;
BEGIN
	n := l^.next; 
  WHILE  n <> l DO BEGIN
    next := n^.next;
    Dispose(n);
    n := next;
  END;
  l^.next := l; 
END; (* Clear *)
(*Appens New Value To List*)
PROCEDURE Append(VAR l : List;  key : String; value: Integer);
VAR
	n : Data;
BEGIN
	New(n);
	n^.value := value;
	n^.key := key;
	n^.isAnchor := false;
	n^.prev := l^.prev;
	l^.prev^.next := n;
	l^.prev := n;
	n^.next := l;
END; (* Append *)	
end.