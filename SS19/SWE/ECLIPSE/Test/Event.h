#ifndef EVENT_H
#define EVENT_H

using namespace std;

class Event {
protected:
	int    dateTime = 0;
	string type = "Event";
	bool   stopEvent = false;
public:
	Event(int eTime, string eType = "C", bool eStop = false)
			: dateTime{eTime}, type{eType}, stopEvent{eStop} {} ;
	~Event() = default;

	int getEventDateTime(){
		return dateTime;
	};

	string getEventType(){
		return type;
	};

	bool isStopEvent(){
		return stopEvent;
	};

	bool operator > (const Event &e) noexcept {
		return this->dateTime > e.dateTime;
	};
};

#endif
