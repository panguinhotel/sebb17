/*
 * ConsumerEvent.h
 *
 *  Created on: 08.03.2019
 *      Author: schau
 */

#ifndef CONSUMER_EVENT_H_
#define CONSUMER_EVENT_H_

#include "Base_Event.h"
#include <vector>
#include <iostream>
#include <thread>

class Consumer_Event : public Base_Event{
public:
	inline Consumer_Event(){}
	inline virtual ~Consumer_Event(){std::cout <<"C_E : DESTRUCTOR" << std::endl;}
	inline bool consume(std::vector<int> *data)
	{
		if(data->size() == 0)
		{
			std::cout<<"C_E : Container empty " << std::endl;
			return false;
		}
		else
		{
			std::this_thread::sleep_for(std::chrono::seconds(1));
			std::cout<<"C_E : consumed " << data->front() << std::endl;
			data->erase(data->begin(),data->begin()+1);
			return true;
		}
	}
};
#endif /* CONSUMER_EVENT_H_ */
