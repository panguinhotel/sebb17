PROCEDURE AddTestResultForMostRecentTestRun(param: Testresult; VAR res : Response);
BEGIN
	res.status := Failure;
	res.message := 'Error - No more Testresult aviable for this Testrun';
	IF idxLastTest < 0  THEN BEGIN
		res.message := 'Error - No Test found';
	END
	ELSE BEGIN
			 	IF AddValueToList(Testruns[idxLastTest].Testresults, TestResultToTest(param)) THEN BEGIN				 
					res.message:='Testresult: '+param.Name+' added';
					res.status:=Success;
				END
				ELSE BEGIN
					res.message:='No more space on Heap!';
					res.status:=Failure;
				END;

		END;
END; (* AddTestResultForMostRecentTestRun *)