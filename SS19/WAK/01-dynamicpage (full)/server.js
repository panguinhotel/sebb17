const express = require('express');
const exphbs = require('express-handlebars');
const moment = require('moment');

const app = express();

/**
 * Configure body-parser middleware for better working with http body values 
 */
const bodyParser = require('body-parser');
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '5mb'
}));

/**
 * Configure Handlebars JS template engine for express
 */
var hbs = exphbs.create({
    defaultLayout: 'main',
    helpers: {
        formatDate: function (datetime, format) {
            if (moment) {
                return moment(datetime).format('lll');
            }
            else {
                return datetime;
            }
        }
    },
    partialsDir: [
        'views/partials/'
    ]
});

app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

/**
 * Register controllers
 */
app.use('/', require('./app/controllers')(express));

/**
 * Register rest api
 */
app.use('/api', require('./app/routes')(express));

/**
 * Register static file access
 */
app.use(express.static(__dirname + '/public'));
app.use('/memes', express.static(__dirname + '/memes'));


/**
 * Start http server
 */
const port = process.env.PORT || 3000;
const server = app.listen(port, function () {
    console.log('Listening on port ' + server.address().port);
});