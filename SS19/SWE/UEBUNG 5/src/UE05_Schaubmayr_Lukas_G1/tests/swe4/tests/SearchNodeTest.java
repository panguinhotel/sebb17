package swe4.tests;

import static org.junit.Assert.*;
import org.junit.Test;
import swe4.Board;
import swe4.BoardException;
import swe4.IllegalMoveException;
import swe4.SearchNode;

public class SearchNodeTest {


	@Test
	public void boardTest()
	{

		try {
			Board board = new Board(3);
			SearchNode node = new SearchNode(board);
			assertTrue(node.getBoard().equals(board));
			board.moveLeft();

			SearchNode node1 = new SearchNode(board);

			assertFalse(node.getBoard().equals(board));
			assertTrue(node1.getBoard().equals(board));
			assertFalse(node.getBoard().equals(node1.getBoard()));
		} catch (IllegalMoveException e) {
			fail("Unexpeced BoardException.");
		}

	}


	@Test
	public void predecessorTest()
	{

		try {
			Board board = new Board(3);
			SearchNode node = new SearchNode(board);
			board.moveLeft();
			SearchNode node1 = new SearchNode(board);
			node1.setPredecessor(node);

			assertTrue(node1.getPredecessor().equals(node));
			assertEquals(node.getPredecessor(),null);
		} catch (IllegalMoveException e) {
			fail("Unexpeced BoardException.");
		}

	}
	
	@Test
	public void toMovesTest()
	{

		try {
			Board board = new Board(3);
			SearchNode node = new SearchNode(board);
			board.moveLeft();
			SearchNode node1 = new SearchNode(board);
			node1.setPredecessor(node);

			assertTrue(node1.getPredecessor().equals(node));
			assertEquals(1,node1.toMoves().size());
		} catch (IllegalMoveException e) {
			fail("Unexpeced BoardException.");
		}

	}

	@Test
	public void costsFromStartTest()
	{
		Board board = new Board(3);
		SearchNode node = new SearchNode(board);
		node.setCostsFromStart(5);
		SearchNode node1 = new SearchNode(board);
		node1.setCostsFromStart(10);

		assertEquals(5,node.costsFromStart());
		assertEquals(10,node1.costsFromStart());
		assertNotEquals(node1.costsFromStart(),node.costsFromStart());
		assertNotEquals(15,node.costsFromStart());
	}


	@Test
	public void simpleNodeTest() {
		try {
			Board board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 0);     
			SearchNode node = new SearchNode(board);      
			assertEquals(0, node.estimatedCostsToTarget());

			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 0);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 5);     
			node = new SearchNode(board);      
			assertEquals(2, node.estimatedCostsToTarget());

			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 0);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 2);     
			node = new SearchNode(board);      
			assertEquals(3, node.estimatedCostsToTarget());
		}
		catch (BoardException e) {
			fail("Unexpeced BoardException.");
		}
	}

	@Test
	public void simpleNodeTest1() {
		try {
			Board board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 0);     
			SearchNode node = new SearchNode(board);
			node.setCostsFromStart(4);
			assertEquals(4, node.estimatedTotalCosts());
			assertEquals(0, node.estimatedCostsToTarget());

			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 2);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 0);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 5);     
			node = new SearchNode(board);
			node.setCostsFromStart(4);
			assertEquals(6, node.estimatedTotalCosts());
			assertEquals(2, node.estimatedCostsToTarget());

			board = new Board(3);      
			board.setTile(1, 1, 1);
			board.setTile(1, 2, 0);
			board.setTile(1, 3, 3);
			board.setTile(2, 1, 4);
			board.setTile(2, 2, 5);
			board.setTile(2, 3, 6);
			board.setTile(3, 1, 7);
			board.setTile(3, 2, 8);
			board.setTile(3, 3, 2);     
			node = new SearchNode(board);
			node.setCostsFromStart(4);
			assertEquals(7, node.estimatedTotalCosts());
			assertEquals(3, node.estimatedCostsToTarget());
		}
		catch (BoardException e) {
			fail("Unexpeced BoardException.");
		}
	}
}
