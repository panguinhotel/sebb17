PROGRAM StackADTTest;

USES
	//StackADT1a;
	//StackADT1;
	//StackADT2;
	StackADT2a;
VAR
	i: Int64;
	ok,ok1:Boolean;
	val : Integer;
	s,s1 : Stack;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN	
	Init(s);
	Init(s1);

	i:=1;
	repeat
		Push(i*i,ok,s);
		Push(i*i,ok1,s1);
		i := i+1;
	until (i> 30 ) or Not ok or NOT ok1;

	IF Not ok or NOT ok1 Then begin
		WriteLn('Fehler!');
	end;
	
	repeat
		Pop(val, ok,s);
		WriteLn('popped #', val);
	until (IsEmpty(s) or Not ok);

	IF Not ok Then begin
		WriteLn('Fehler!');
	end;

	WriteLn('STACK 1');
	repeat
		Pop(val, ok1,s1);
		WriteLn('popped #', val);
	until (IsEmpty(s1)  or NOT ok1);

	IF Not ok1 Then begin
		WriteLn('Fehler!');
	end;
	Done(s);
	Done(s1);
END. (* StackADSTest *)