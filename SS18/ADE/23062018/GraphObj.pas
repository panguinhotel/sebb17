PROGRAM GraphObj;

CONST
	MAX=100;

TYPE
	ShapePtr = ^Shape;
	Shape = OBJECT
		CONSTRUCTOR Init;
		Destructor Done ; Virtual;
		
		PROCEDURE Describe(name: String; indent: Integer) VIRTUAl;
		PROCEDURE Draw; Virtual; Abstract;
		PROCEDURE MoveBy; Virtual; Abstract;
		PROCEDURE Erase; Virtual; Abstract;
	End;

	LinePtr = ^Line;
	Line = OBJECT(Shape)
		public
			CONSTRUCTOR Init(x1,y1,x2,y2 : Integer);
			PROCEDURE Describe(name: String; indent: Integer) VIRTUAl;
			Destructor Done ; Virtual;
			PROCEDURE Draw; Virtual;
			PROCEDURE MoveBy; Virtual;
			PROCEDURE Erase; Virtual;
		private
			x1,y1,x2,y2 : Integer;
	End;

	PicturePtr = ^Picture;
	Picture = Object(Shape)
		public
			CONSTRUCTOR Init;
			Destructor Done ; Virtual;
			
			PROCEDURE Describe(name: String; indent: Integer) VIRTUAl;
			PROCEDURE Draw; Virtual;
			PROCEDURE MoveBy; Virtual;
			PROCEDURE Erase; Virtual;
			PROCEDURE Add(s:ShapePtr);
		private
			n: Integer;
			shapes: Array [1..MAX] OF ShapePtr;
	END;
VAR
	
	objCount : Integer;

CONSTRUCTOR Shape.Init;
begin
	Inc(objCount);
end;

Destructor Shape.Done ;
begin
	Dec(objCount);
end;

PROCEDURE Shape.Describe(name: String; indent: Integer);
begin
	Write(' ': indent, name);
end;

CONSTRUCTOR Line.Init(x1,y1,x2,y2 : Integer);
begin
	INHERITED Init;
	self.x1 := x1;
	self.y1 := y1;
	self.x2 := x2;
	self.y2 := y2;
end;
PROCEDURE Line.Describe(name: String; indent: Integer);
begin
	INHERITED Describe(name,indent);
	Write('Line!');
end;
Destructor Line.Done;
Var
	i: Integer;
begin
	FOR i := 1 To n DO BEGIN
		Dispose(shapes[i],Done);
	End;
	INHERITED Done;
end;
PROCEDURE Line.Draw;
begin

end;
PROCEDURE Line.MoveBy;
begin

end;
PROCEDURE Line.Erase;
begin

end;

CONSTRUCTOR Picture.Init;
begin
	INHERITED Init;
end;

Destructor Picture.Done ;
begin
	
end;

			
PROCEDURE Picture.Describe(name: String; indent: Integer);
begin
	
end;

PROCEDURE Picture.Draw;
begin
	
end;

PROCEDURE Picture.MoveBy; 
begin
	
end;

PROCEDURE Picture.Erase;
begin
	
end;

PROCEDURE Picture.Add(s:ShapePtr);
begin
	
end;

BEGIN
	objCount := 0;

END.