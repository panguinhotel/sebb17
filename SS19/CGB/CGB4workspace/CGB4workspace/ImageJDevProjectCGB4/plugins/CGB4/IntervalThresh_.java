import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

public class IntervalThresh_ implements PlugInFilter{

	public int setup(String arg, ImagePlus imp) {
		if (arg.equals("about"))
			{showAbout(); return DONE;}
		return DOES_8G+DOES_STACKS+SUPPORTS_MASKING;
	} //setup

	
	public void run(ImageProcessor ip) {
		byte[] pixels = (byte[])ip.getPixels();
		int width = ip.getWidth();
		int height = ip.getHeight();
		
		int[][] inDataArrInt = ImageJUtility.convertFrom1DByteArr(pixels, width, height);
		
		//define the theshold limits
		int Tmin = 150;
		int Tmax = 240;
		
		//let the user input the range
		GenericDialog gd = new GenericDialog("interval border");
		gd.addNumericField("Tmin", Tmin, 0);
		gd.addNumericField("Tmax", Tmax, 0);
		gd.showDialog();
		
		if(!gd.wasCanceled())
		{
			Tmin = (int)gd.getNextNumber();
			Tmax = (int)gd.getNextNumber();
		}
		
		int[] intervalThreshTF = getIntervalThreshTF(Tmin,Tmax,255);
		          
        int[] invertTF = ImageTransformationFilter.getInversionTF(255);
        int[][] segmentedImg = ImageTransformationFilter.getTransformedImage(inDataArrInt, width, height, intervalThreshTF);        
        
        ImageJUtility.showNewImage(segmentedImg, width, height, "Segmented Image");
        
	} //run
	
	public static int[] getIntervalThreshTF(int Tmin,int Tmax, int maxVal)
	{
		int[] returnArr = new int[maxVal+1];
		for(int i = 0; i <=maxVal; i++)
		{
			if(i >= Tmin && i <=Tmax)
				returnArr[i] = 255; //FG
			else 
				returnArr[i] = 0; //BG
		}
		
		return returnArr;
	}

	void showAbout() {
		IJ.showMessage("About Template_...",
			"this is a PluginFilter template\n");
	} //showAbout
}
