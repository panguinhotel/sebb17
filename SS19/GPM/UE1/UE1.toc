\contentsline {chapter}{\numberline {1}Fragen}{2}
\contentsline {subsubsection}{Was ist Ansto\IeC {\ss } bzw. Ausl\IeC {\"o}ser des Prozesses?}{2}
\contentsline {subsubsection}{Womit endet der Prozess?}{2}
\contentsline {subsubsection}{Wer ist/sind Kunde(n)?}{2}
\contentsline {subsubsection}{Worin liegt der Nutzen des Prozesses f\IeC {\"u}r den Kunden?}{2}
\contentsline {subsubsection}{Woran kann man messen, wie gut dieser Prozess ist?}{2}
\contentsline {subsubsection}{Aus welchen T\IeC {\"a}tigkeiten besteht der Prozess?}{2}
\contentsline {subsubsection}{Was braucht man f\IeC {\"u}r diesen Prozess (Input)?}{3}
\contentsline {subsubsection}{Was wird entlang dieses Prozesses an Output erzeugt?}{4}
\contentsline {chapter}{\numberline {2}Istanalyse}{5}
\contentsline {section}{\numberline {2.1}Organigramm}{5}
\contentsline {section}{\numberline {2.2}Prozess}{5}
\contentsline {chapter}{\numberline {3}Schwachstellenanalyse}{7}
\contentsline {chapter}{\numberline {4}Sollkonzeption}{8}
