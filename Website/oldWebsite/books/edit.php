<?php
   include('../session.php');
   include('../config.php');

    $buchID = $_GET['id'];

   if($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['edit']))
    {
        $authorname = mysqli_real_escape_string($db,$_POST['b_autor']);
        $katname = mysqli_real_escape_string($db,$_POST['b_kat']);
        $typename = mysqli_real_escape_string($db,$_POST['b_type']);
        $verlagname = mysqli_real_escape_string($db,$_POST['b_verlag']);
        $title = mysqli_real_escape_string($db,$_POST['b_title']);
        $desc = mysqli_real_escape_string($db,$_POST['b_desc']);
        $gelesen = "NEIN";
        if(isset($_POST['b_gelesen']))
        {
            $gelesen = "JA";
        }
        $gekauft = "NEIN"; 
        if(isset($_POST['b_gekauft']))
        {
            $gekauft = "JA";
        }
        $date = date('Y-m-d');
    
        $sql1 = "SELECT id_autor from autor where name = '$authorname' and fk_id_user = $login_userID limit 1 ";
        $result1 = mysqli_query($db,$sql1);
        $value1 = mysqli_fetch_object($result1);
        $autorID = $value1->id_autor;
        
        $sql2 = "SELECT id_kategorie from kategorie where name = '$katname' and fk_id_user = $login_userID limit 1 ";
        $result2 = mysqli_query($db,$sql2);
        $value2 = mysqli_fetch_object($result2);
        $katID = $value2->id_kategorie;
    
        $sql3 = "SELECT id_buchtype from buch_type where name = '$typename' and fk_id_user = $login_userID limit 1 ";
        $result3 = mysqli_query($db,$sql3);
        $value3 = mysqli_fetch_object($result3);
        $typeID = $value3->id_buchtype;
    
        $sql4 = "SELECT id_verlag from verlag where name = '$verlagname' and fk_id_user = $login_userID limit 1 ";
        $result4 = mysqli_query($db,$sql4);
        $value4 = mysqli_fetch_object($result4);
        $verlagID = $value4->id_verlag;
    
        $sql = "Update buch SET fk_id_autor=$autorID,fk_id_kategorie=$katID,fk_id_verlag=$verlagID,fk_id_buchtype=$typeID,titel='$title',beschreibung='$desc',kauf='$gekauft',`read`='$gelesen',`timestamp`='$date' where id_buch=$buchID and fk_id_user=$login_userID";
        $result = mysqli_query($db,$sql);
    
        if($result) {
            echo '<script language="javascript">';
            echo 'alert("Ändern erfolgreich")';
            echo '</script>';
        }else {
            echo '<script language="javascript">';
            echo 'alert("Ändern fehlgeschlagen")';
            echo '</script>';
        }
    }
    else if(isset($_POST['delete']))
    {
        $sql = "Delete from buch where id_buch=$buchID and fk_id_user=$login_userID";
        $result = mysqli_query($db,$sql);

        if($result) {
            echo '<script language="javascript">';
            echo 'alert("Löschen erfolgreich")';
            echo '</script>';
        }else {
            echo '<script language="javascript">';
            echo 'alert("Löschen fehlgeschlagen")';
            echo '</script>';
        }
    }
}

?>
<html>  
   <head>
    <title>Buch ändern</title>
          <link rel="stylesheet" type="text/css" href="../style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
   </head>
   
   <body>
        <h1 class="headLine">Buch ändern</h1>
   		<div class="mainList">
                <div class="dropdown mainList">
                    <button name="autorname" id="autorNameBtn"  onclick="a_myFunction()" class="slimButton">
                        <?php 
                            $sql = "Select name from buch b join autor a on b.fk_id_autor = a.id_autor where b.id_buch = $buchID limit 1;";
                            $result = mysqli_query($db,$sql);
                            $value = mysqli_fetch_object($result);
                            echo (isset($value->name)?$value->name : '< Autor >'); 
                        ?>
                    </button>
                    <div id="a_myDropdown" class="dropdown-content">
                        <input type="text" class="searchInput" placeholder="Search.." id="a_myInput" class="myInput" onkeyup="a_filterFunction()">
                        <?php
                            $sql = "SELECT id_autor, name FROM autor where fk_id_user = $login_userID;";
                            $result = mysqli_query($db,$sql);
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                echo "<a class=\"a_ddItem\" href=\"#\" >".$row['name']."</a>";
                            }
                        ?> 
                    </div>
                    <button id="katNameBtn"  onclick="k_myFunction()" class="slimButton">
                        <?php 
                            $sql = "Select name from buch b join kategorie a on b.fk_id_kategorie = a.id_kategorie where b.id_buch = $buchID limit 1;";
                            $result = mysqli_query($db,$sql);
                            $value = mysqli_fetch_object($result);
                            echo (isset($value->name)?$value->name : '< Kategorie >'); 
                        ?>
                    </button>
                    <div id="k_myDropdown" class="dropdown-content">
                        <input type="text" class="searchInput" placeholder="Search.." id="k_myInput" class="myInput" onkeyup="k_filterFunction()">
                        <?php
                            $sql = "SELECT id_kategorie, name FROM kategorie where fk_id_user = $login_userID;";
                            $result = mysqli_query($db,$sql);
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                echo "<a class=\"k_ddItem\" href=\"#\" >".$row['name']."</a>";
                            }
                        ?> 
                    </div>
                    <button id="typeNameBtn"  onclick="t_myFunction()" class="slimButton">
                        <?php 
                            $sql = "Select name from buch b join buch_type a on b.fk_id_buchtype = a.id_buchtype where b.id_buch = $buchID limit 1;";
                            $result = mysqli_query($db,$sql);
                            $value = mysqli_fetch_object($result);
                            echo (isset($value->name)?$value->name : '< Buchtyp >'); 
                        ?>
                    </button>
                    <div id="t_myDropdown" class="dropdown-content">
                        <input type="text" class="searchInput" placeholder="Search.." id="t_myInput" class="myInput" onkeyup="t_filterFunction()">
                        <?php
                            $sql = "SELECT id_buchtype, name FROM buch_type where fk_id_user = $login_userID;";
                            $result = mysqli_query($db,$sql);
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                echo "<a class=\"t_ddItem\" href=\"#\" >".$row['name']."</a>";
                            }
                        ?> 
                    </div>
                    <button id="verlagNameBtn"  onclick="v_myFunction()" class="slimButton">
                        <?php 
                                $sql = "Select name from buch b join verlag a on b.fk_id_verlag = a.id_verlag where b.id_buch = $buchID limit 1;";
                                $result = mysqli_query($db,$sql);
                                $value = mysqli_fetch_object($result);
                                echo (isset($value->name)?$value->name : '< Verlag >'); 
                        ?>
                    </button>
                    <div id="v_myDropdown" class="dropdown-content">
                        <input type="text" class="searchInput" placeholder="Search.." id="v_myInput" onkeyup="v_filterFunction()">
                        <?php
                            $sql = "SELECT id_verlag, name FROM verlag where fk_id_user = $login_userID;";
                            $result = mysqli_query($db,$sql);
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                               echo "<a class=\"v_ddItem\" href=\"#\" >".$row['name']."</a>";
                            }
                        ?> 
                    </div>
                </div>

            <form action = "" method = "post" >
                <input type="hidden" id="b_autor" name="b_autor" 
                    <?php 
                        $sql = "Select name from buch b join autor a on b.fk_id_autor = a.id_autor where b.id_buch = $buchID limit 1;";
                        $result = mysqli_query($db,$sql);
                        $value = mysqli_fetch_object($result);
                        echo ('value="'.(isset($value->name)?$value->name : '0').'"'); 
                    ?>
                />
                <input type="hidden" id="b_kat" name="b_kat"
                    <?php 
                        $sql = "Select name from buch b join kategorie a on b.fk_id_kategorie = a.id_kategorie where b.id_buch = $buchID limit 1;";
                        $result = mysqli_query($db,$sql);
                        $value = mysqli_fetch_object($result);
                        echo ('value="'.(isset($value->name)?$value->name : '0').'"'); 
                    ?>
                />
                <input type="hidden" id="b_type" name="b_type"
                    <?php 
                        $sql = "Select name from buch b join buch_type a on b.fk_id_buchtype = a.id_buchtype where b.id_buch = $buchID limit 1;";
                        $result = mysqli_query($db,$sql);
                        $value = mysqli_fetch_object($result);
                        echo ('value="'.(isset($value->name)?$value->name : '0').'"'); 
                    ?>
                />
                <input type="hidden" id="b_verlag" name="b_verlag"
                    <?php 
                        $sql = "Select name from buch b join verlag a on b.fk_id_verlag = a.id_verlag where b.id_buch = $buchID limit 1;";
                        $result = mysqli_query($db,$sql);
                        $value = mysqli_fetch_object($result);
                        echo ('value="'.(isset($value->name)?$value->name : '0').'"'); 
                    ?>
                />              
                <input class="loginInput"  type="text" placeholder="Titel" name="b_title" required
                    <?php 
                        $sql = "Select titel from buch b where b.id_buch = $buchID limit 1;";
                        $result = mysqli_query($db,$sql);
                        $value = mysqli_fetch_object($result);
                        echo (isset($value->titel)?('value="'.$value->titel.'"') : ''); 
                    ?>
                ><br>
                <input class="loginInput" type="text" placeholder="Beschreibung" name="b_desc"
                    <?php 
                        $sql = "Select beschreibung from buch b where b.id_buch = $buchID limit 1;";
                        $result = mysqli_query($db,$sql);
                        $value = mysqli_fetch_object($result);
                        echo (isset($value->beschreibung)?('value="'.$value->beschreibung.'"') : ''); 
                    ?>
                ><br>
                <label class="slimButton loginInput">Gekauft
                    <input type="checkbox" name="b_gekauft"
                        <?php 
                            $sql = "Select kauf as status from buch where id_buch = $buchID limit 1;";
                            $result = mysqli_query($db,$sql);
                            $value = mysqli_fetch_object($result);
                            echo (isset($value->status)?($value->status=='JA'?'checked="checked"':'') : ''); 
                        ?>
                    >
                    <span class="checkmark"></span>
                </label>
                <label class="slimButton loginInput">Gelesen
                    <input type="checkbox" name="b_gelesen"
                        <?php 
                            $sql = "Select `read` as status from buch where id_buch = $buchID limit 1;";
                            $result = mysqli_query($db,$sql);
                            $value = mysqli_fetch_object($result);
                            echo (isset($value->status)?($value->status=='JA'?'checked="checked"':'') : ''); 
                        ?>
                    >
                    <span class="checkmark"></span>
                </label>
                <br>   
                <input class="mainButton" type="submit" name="edit" value="Ändern">
                <input class="deleteButton" type="submit" name="delete" value="Löschen">
            </form>		
			<button class="backbutton" onclick="window.location.href='../books.php'">Zurück</button>
        </div>

    <script>
        function a_myFunction() {
            document.getElementById("a_myDropdown").classList.toggle("show");
            var x = document.getElementsByClassName("a_ddItem");

            for(var i = 0; i < x.length; i++)
            {
                x[i].addEventListener("click", a_setName);
            }
        }

        function a_filterFunction() {
            var input, filter, ul, li, a, i;
            input = document.getElementById("a_myInput");
            filter = input.value.toUpperCase();
            div = document.getElementById("a_myDropdown");
            a = div.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                txtValue = a[i].textContent || a[i].innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                a[i].style.display = "";
                } else {
                a[i].style.display = "none";
                }
            }
        }
        function a_setName(event){
            var clickedElement = event.target;
            document.getElementById("autorNameBtn").innerHTML = clickedElement.innerHTML;
            document.getElementById("b_autor").value = clickedElement.innerHTML;
            document.getElementById("a_myDropdown").classList.toggle("show");
        }
        


        function k_myFunction() {
            document.getElementById("k_myDropdown").classList.toggle("show");
            var x = document.getElementsByClassName("k_ddItem");

            for(var i = 0; i < x.length; i++)
            {
                x[i].addEventListener("click", k_setName);
            }
        }

        function k_filterFunction() {
            var input, filter, ul, li, a, i;
            input = document.getElementById("k_myInput");
            filter = input.value.toUpperCase();
            div = document.getElementById("k_myDropdown");
            a = div.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                txtValue = a[i].textContent || a[i].innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                a[i].style.display = "";
                } else {
                a[i].style.display = "none";
                }
            }
        }

        function k_setName(event){
            var clickedElement = event.target;
            document.getElementById("katNameBtn").innerHTML = clickedElement.innerHTML;
            document.getElementById("b_kat").value = clickedElement.innerHTML;
            document.getElementById("k_myDropdown").classList.toggle("show");
        }




        function t_myFunction() {
            document.getElementById("t_myDropdown").classList.toggle("show");
            var x = document.getElementsByClassName("t_ddItem");

            for(var i = 0; i < x.length; i++)
            {
                x[i].addEventListener("click", t_setName);
            }
        }

        function t_filterFunction() {
            var input, filter, ul, li, a, i;
            input = document.getElementById("t_myInput");
            filter = input.value.toUpperCase();
            div = document.getElementById("t_myDropdown");
            a = div.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                txtValue = a[i].textContent || a[i].innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                a[i].style.display = "";
                } else {
                a[i].style.display = "none";
                }
            }
        }

        function t_setName(event){
            var clickedElement = event.target;
            document.getElementById("typeNameBtn").innerHTML = clickedElement.innerHTML;
            document.getElementById("b_type").value = clickedElement.innerHTML;
            document.getElementById("t_myDropdown").classList.toggle("show");
        }



         function v_myFunction() {
            document.getElementById("v_myDropdown").classList.toggle("show");
            var x = document.getElementsByClassName("v_ddItem");

            for(var i = 0; i < x.length; i++)
            {
                x[i].addEventListener("click", v_setName);
            }
        }

        function v_filterFunction() {
            var input, filter, ul, li, a, i;
            input = document.getElementById("v_myInput");
            filter = input.value.toUpperCase();
            div = document.getElementById("v_myDropdown");
            a = div.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                txtValue = a[i].textContent || a[i].innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                a[i].style.display = "";
                } else {
                a[i].style.display = "none";
                }
            }
        }

        function v_setName(event){
            var clickedElement = event.target;
            document.getElementById("verlagNameBtn").innerHTML = clickedElement.innerHTML;
            document.getElementById("b_verlag").value = clickedElement.innerHTML;
            document.getElementById("v_myDropdown").classList.toggle("show");
        }


	</script>      
   </body> 
</html>