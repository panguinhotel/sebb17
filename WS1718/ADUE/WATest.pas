(* Test program for WinApp unit. *)
(* (C) Gabriel Horn 31.01.2017 *)
PROGRAM WATest;

USES WinApp, Windows;

PROCEDURE Paint(wnd: HWnd; dc: HDC);
BEGIN
	WriteLn('Paint');
END;

PROCEDURE MouseDown(wnd: HWnd; x, y: INTEGER);
BEGIN
	WriteLn('MouseDown: ', x, ', ', y);
END;

BEGIN
	OnPaint := @Paint;
	OnMouseDown := @MouseDown;
	Run;
END.