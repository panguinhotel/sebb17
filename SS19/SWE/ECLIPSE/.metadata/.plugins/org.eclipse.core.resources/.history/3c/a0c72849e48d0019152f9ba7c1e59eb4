package connect4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import java.util.List;


public class Connect4Database implements Database {
	  private Connection connection;
	  private String connectionString;

	  public Connect4Database(String connectionString) {
	    this.connectionString = connectionString;
	  }

	  @Override
	  public void openConnection() throws SQLException {
	      connection = DriverManager.getConnection(connectionString);

	  }

	  @Override
	  public Connection getConnection() throws SQLException {
	    if (connection == null)
	      openConnection();
	    return connection;
	  }

	  @Override
	  public void disposeConnection() throws SQLException {
	      if (connection != null)
	        connection.close();
	      connection = null;
	  }
	  
	  @Override
	  public boolean insertScore(String player1, String player2, int winner)
	  {
		  try (PreparedStatement statement = getConnection()
			        .prepareStatement(
			            "INSERT INTO scores " +
			            "(player1, player1, winner) " +
			            "values (?, ?, ?)",
			            Statement.RETURN_GENERATED_KEYS)) {
			      statement.setString(1, player1);
			      statement.setString(2, player2);
			      statement.setInt(3, winner < 0 ? 2 : winner);
			      
			      return statement.executeUpdate() != 0;
			      
		  } catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	  }
	  
	  @Override
	  public List<Score> getScores() throws SQLException
	  {
	  List<Score> c = new ArrayList<Score>();
		    PreparedStatement statement = getConnection().prepareStatement(
		        "Select DISTINCT IF(player1 < player2,player1, player2) as player1, IF(player1 < player2,player2, player1) as player2, " + 
		        "(Select Count(winner) from scores s where (s.player1 = p.player1 or s.player1 = p.player2) and (s.player2 = p.player1 or s.player2 = p.player2) and winner = 1) as winner1, " + 
		        "(Select Count(winner) from scores s where (s.player1 = p.player1 or s.player1 = p.player2) and (s.player2 = p.player1 or s.player2 = p.player2) and winner = 2) as winner2 " + 
		        "from scores p;");
		      ResultSet resultSet = statement.executeQuery();
		        while (resultSet.next()) {
		          c.add(new Connect4Score(
		              resultSet.getString("player1"),
		              resultSet.getString("player2"),
		              resultSet.getInt("winner1"),
		              resultSet.getInt("winner2")));
		      } // includes finally resultSet.close();
		    return c;
		  }
}
