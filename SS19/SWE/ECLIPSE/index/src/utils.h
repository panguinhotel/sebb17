#pragma once

#include <string>
#include <fstream>

bool isPunctt(char ch);
bool isDigit(char ch);
bool isAlpha(char ch);

char toLower(char ch);

std::string normalize(std::string word);
void open_stream(int argc, char *argv[], std::ifstream &fin);
