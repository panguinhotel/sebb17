set serveroutput on;
-- 1
CREATE OR REPLACE PROCEDURE CHECK_SALARY(jobID employees.job_id%Type, newSal employees.salary%Type)
IS
  maxSal employees.salary%Type;
  minSal employees.salary%Type;
  toHigh_exep EXCEPTION;
  PRAGMA EXCEPTION_INIT(toHigh_exep,-01400);
BEGIN
  Select MAX_SALARY INTO maxSal FROM jobs
  where job_id = jobID;
  Select MIN_SALARY INTO minSal FROM jobs
  where job_id = jobID;
  
  IF newSal > maxSal THEN
    RAISE toHigh_exep;
  END IF;
  IF newSal < minSal THEN
    Update jobs set MIN_SALARY = newSal
    Where job_id = jobID;
  END IF;
EXCEPTION
  WHEN toHigh_exep THEN
    dbms_output.put_line('Invalid salary '||newSal||', Salary too high for job ' ||jobID);
END;
/

--TEST 1
BEGIN
  CHECK_SALARY('AD_PRES',50000);
END;

-- 2
CREATE OR REPLACE TRIGGER CHECK_SALARY_TRG
BEFORE INSERT OR UPDATE
ON employees
FOR EACH ROW
BEGIN
  CHECK_SALARY(:NEW.job_id, :NEW.salary);
END;

DROP trigger check_salary_trg;

--TEST 3
UPDATE employees SET salary = 50000 Where employee_id = 100;
UPDATE employees SET salary = 10000 Where employee_id = 100;


-- 5
ALTER TABLE employees add(
user_modified VARCHAR2(256) DEFAULT NULL,
date_modified Date Default Null);

CREATE OR REPLACE TRIGGER LOG_EMPLOYEES
BEFORE INSERT OR UPDATE
ON employees
FOR EACH ROW
BEGIN
  :NEW.user_modified := USER;
  :NEW.date_modified := SYSDATE;
END;

-- TESTS
Select employee_ID, USER_MODIFIED, DATE_MODIFIED, salary
from employees
where employee_id = 100;
Select * from jobs;


Rollback;