--UE02-4a
CREATE view UE02_04a AS
WITH revenue (store_id,name,amount) AS (
Select i.store_id,k.name,Sum(p.amount)
from payment p
join rental r on p.rental_id = r.rental_id
join inventory i on i.inventory_id = r.inventory_id
join film f on f.film_id = i.film_id
join film_category c on c.film_id = f.film_id
join category k on k.category_id = c.category_id
group by i.store_id,k.name
order by i.store_id,k.name),
revSum (store_id,endsum) AS (
Select i.store_id,Sum(p.amount)
from payment p
join rental r on p.rental_id = r.rental_id
join inventory i on i.inventory_id = r.inventory_id
join film f on f.film_id = i.film_id
join film_category c on c.film_id = f.film_id
join category k on k.category_id = c.category_id
group by i.store_id
order by i.store_id)
Select r.store_id,r.name,r.amount,Round(r.amount/s.endsum*100,2) as Anteil
from revenue r
join revSum s on r.store_id = s.store_id;

--UE02-4b
CREATE MATERIALIZED VIEW UE02_04b
REFRESH COMPLETE ON DEMAND
AS SELECT store_id,name,amount,anteil FROM UE02_04a;

--UE02-4c
CREATE MATERIALIZED VIEW UE02_04c
REFRESH NEXT SYSDATE + 22/24 
AS SELECT store_id,name,amount,anteil FROM UE02_04a;

DROP MATERIALIZED VIEW UE02_04c;