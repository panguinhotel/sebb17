<?php
   include('../session.php');
   include('../config.php');
?>
<html>  
   <head>
    <title>Eier anzeigen</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta http-equiv="content-type" content="text/html; charset=utf-8"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
   </head>
   
   <body>
   <h1 class="headLine">Eier</h1>
   <div class="mainList">
            <button class="backbutton" onclick="window.location.href='../eier.php'">Zurück</button>
			<input type="text" id="myInput" class="searchInput" onkeyup="myFunction()" placeholder="Search..." title="Type in a name">
					<button id="BTEGGDAY" class="slimButton" onClick="onDayClick()" >Tag</button>
					<button id="BTEGGMONTH" class="slimButton" onClick="onMonthClick()" >Monat</button>
					<button id="BTEGGYEAR" class="slimButton" onClick="onYearClick()" >Jahr</button>
					<button id="BTEGGALL" class="slimButton" onClick="onAllClick()" >Alles</button>
			<table id="myTable">
                <thead>
                    <tr>
                        <th>Datum</th>
                        <th>Weiß</th>
                        <th>Braun</th>
                        <th>Grün</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						$sql = "Select e.timestamp as Datum, e.color_white as white,e.color_green as green, e.color_brown as brown from eier e 
						group by YEAR(e.timestamp),MONTH(e.timestamp),DAY(e.timestamp) order by e.timestamp desc;";
                        $result = mysqli_query($db,$sql);
                        while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                            echo "<tr name=\"trDay\">";
                            echo "<td data-column=\"Titel\">" . $row['Datum'] . "</td>";
                            echo "<td data-column=\"Kategorie\">" . $row['white'] . "</td>";
                            echo "<td data-column=\"Autor\">" . $row['green'] . "</td>";
                            echo "<td data-column=\"Status\">" . $row['brown'] . "</td>";
                            echo "</tr>";
						}
						$sql1 = "Select YEAR(e.timestamp) as Datum, Sum(e.color_white) as white ,Sum(e.color_green) as green, Sum(e.color_brown) as brown 
						from eier e group by YEAR(e.timestamp) order by e.timestamp desc;";
                        $result1 = mysqli_query($db,$sql1);
                        while($row1 = mysqli_fetch_array($result1,MYSQLI_ASSOC)){
                            echo "<tr name=\"trYear\">";
                            echo "<td data-column=\"Titel\">" . $row1['Datum'] . "</td>";
                            echo "<td data-column=\"Kategorie\">" . $row1['white'] . "</td>";
                            echo "<td data-column=\"Autor\">" . $row1['green'] . "</td>";
                            echo "<td data-column=\"Status\">" . $row1['brown'] . "</td>";
                            echo "</tr>";
						}
                        $sql = "Select CONCAT(YEAR(e.timestamp), '-', LPAD(MONTH(e.timestamp), 2, '0')) as Datum, Sum(e.color_white) as white ,Sum(e.color_green) as green, 
						Sum(e.color_brown) as brown from eier e group by YEAR(e.timestamp),MONTH(e.timestamp) order by e.timestamp desc;";
                        $result = mysqli_query($db,$sql);
                        while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                            echo "<tr name=\"trMonth\">";
                            echo "<td data-column=\"Titel\">" . $row['Datum'] . "</td>";
                            echo "<td data-column=\"Kategorie\">" . $row['white'] . "</td>";
                            echo "<td data-column=\"Autor\">" . $row['green'] . "</td>";
                            echo "<td data-column=\"Status\">" . $row['brown'] . "</td>";
                            echo "</tr>";
						}
						$sql1 = "Select 'Summe' as Datum, Sum(e.color_white) as white ,Sum(e.color_green) as green, Sum(e.color_brown) as brown from eier e;";
                        $result1 = mysqli_query($db,$sql1);
						while($row1 = mysqli_fetch_array($result1,MYSQLI_ASSOC)){
                            echo "<tr name=\"trSum\">";
                            echo "<td>" . $row1['Datum'] . "</td>";
                            echo "<td>" . $row1['white'] . "</td>";
                            echo "<td>" . $row1['green'] . "</td>";
                            echo "<td>" . $row1['brown'] . "</td>";
                            echo "</tr>";
						}
                    ?>
                </tbody> 
            </table>
            <input type="hidden" id="activeButton" value="BTEGGDAY">
            <button class="backbutton" onclick="window.location.href='../eier.php'">Zurück</button>
            </div>
    <script>
    function myFunction() {       
        var input, filter, table, tr, a, i, txtValue,btx;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");

        btx=document.getElementById("activeButton").value;
        for (i = 1; i < tr.length; i++) {
            if(tr[i].getAttribute("name")==btx)
            {
                if(btx=="trSum")
                    tr[i].style.display = "";
                else
                {
                    a = tr[i].getElementsByTagName("td")[0];
                    txtValue = a.textContent || a.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
                
            }      
        }
    }
    function onDayClick(){
        var table,tr;
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for(i = 1; i < tr.length; i++){
            if(tr[i].getAttribute("name")=="trDay")
            {
                tr[i].style.display = "";
            }
            else{
                tr[i].style.display = "none";
            }
        }
        document.getElementById("BTEGGDAY").style.background="#255bcf";
        document.getElementById("BTEGGDAY").style.color="white";

        document.getElementById("BTEGGMONTH").style.background="white";
        document.getElementById("BTEGGMONTH").style.color="#255bcf";

        document.getElementById("BTEGGYEAR").style.background="white";
        document.getElementById("BTEGGYEAR").style.color="#255bcf";

        document.getElementById("BTEGGALL").style.background="white";
        document.getElementById("BTEGGALL").style.color="#255bcf";  

        document.getElementById("activeButton").value="trDay";

        myFunction();
    }
    function onMonthClick(){
        var table,tr;
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for(i = 1; i < tr.length; i++){
            if(tr[i].getAttribute("name")=="trMonth")
            {
                tr[i].style.display = "";
            }
            else{
                tr[i].style.display = "none";
            }
        }
        document.getElementById("BTEGGDAY").style.background="white";
        document.getElementById("BTEGGDAY").style.color="#255bcf";

        document.getElementById("BTEGGMONTH").style.background="#255bcf";
        document.getElementById("BTEGGMONTH").style.color="white";


        document.getElementById("BTEGGYEAR").style.background="white";
        document.getElementById("BTEGGYEAR").style.color="#255bcf";

        document.getElementById("BTEGGALL").style.background="white";
        document.getElementById("BTEGGALL").style.color="#255bcf";

        document.getElementById("activeButton").value="trMonth";
        myFunction();
          
    }
    function onYearClick(){
        var table,tr;
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for(i = 1; i < tr.length; i++){
            if(tr[i].getAttribute("name")=="trYear")
            {
                tr[i].style.display = "";
            }
            else{
                tr[i].style.display = "none";
            }
        }
        document.getElementById("BTEGGDAY").style.background="white";
        document.getElementById("BTEGGDAY").style.color="#255bcf";

        document.getElementById("BTEGGMONTH").style.background="white";
        document.getElementById("BTEGGMONTH").style.color="#255bcf";

        document.getElementById("BTEGGYEAR").style.background="#255bcf";
        document.getElementById("BTEGGYEAR").style.color="white";

        document.getElementById("BTEGGALL").style.background="white";
        document.getElementById("BTEGGALL").style.color="#255bcf";

        document.getElementById("activeButton").value="trYear";

        myFunction();
    }
    function onAllClick(){
        var table,tr;
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for(i = 1; i < tr.length; i++){
            if(tr[i].getAttribute("name")=="trSum")
            {
                tr[i].style.display = "";
            }
            else{
                tr[i].style.display = "none";
            }
        }
        document.getElementById("BTEGGDAY").style.background="white";
        document.getElementById("BTEGGDAY").style.color="#255bcf";

        document.getElementById("BTEGGMONTH").style.background="white";
        document.getElementById("BTEGGMONTH").style.color="#255bcf";

        document.getElementById("BTEGGYEAR").style.background="white";
        document.getElementById("BTEGGYEAR").style.color="#255bcf";

        document.getElementById("BTEGGALL").style.background="#255bcf";
        document.getElementById("BTEGGALL").style.color="white";

        document.getElementById("activeButton").value="trSum";

        myFunction();
    }

    onDayClick();
    
    </script>
   </body> 
</html>