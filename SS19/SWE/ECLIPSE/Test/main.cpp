#include <iostream>
#include <ctime>

#include "Event.h"
#include "EventQueue.h"
#include "TheSimulator.h"

using namespace std;

int main() {
	EventQueue eq = EventQueue();
	theSimulator ts = theSimulator();

	cout << "create 100 random events incl. 1 stopEvent with random dateTime" << endl;
	ts.createEvents(eq, 100);
	cout << "*** eventqueue size: " << eq.getSize() << endl;

	cout << "---------------------------------------" << endl;
	cout << "simulation with 1 step" << endl;
	ts.step(eq, 1);
	cout << "*** eventqueue size: " << eq.getSize() << endl;

	cout << "---------------------------------------" << endl;
	cout << "simulation with 5 steps" << endl;
	ts.step(eq, 5);
	cout << "*** eventqueue size: " << eq.getSize() << endl;

	cout << "---------------------------------------" << endl;
	cout << "simulation with run till empty or stop events" << endl;
	ts.run(eq);
	cout << "* ** eventqueue size: " << eq.getSize() << endl;

	return 0;
}
