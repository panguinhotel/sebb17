PROGRAM TreeParserTest;

USES
	TreeParser;
VAR
	tree : Node;
	input : String;
	ok : Boolean;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN

	input := '(true) d eq (false eq true) eq (false eq true)';
    Parse(input, ok, tree);
	WriteLn(input);
	if NOT ok THEN WriteLn('Error') ELSE BEGIN
		Write('POSTFIX : '); WriteTreePostfix(tree);
		WriteLn;
		Write('INFIX : ');WriteTreeInfix(tree);
		WriteLn;
		Write('PREFIX : ');WriteTreePrefix(tree);
	END;

	WriteLn;WriteLn;
	input := 'true+ (not false => t+ false and true <=>t eq(t+-f)    ) or t and f imp t';
    Parse(input, ok, tree);
	WriteLn(input);
	IF not ok THEN WriteLn('Error') ELSE BEGIN
		Write('POSTFIX : '); WriteTreePostfix(tree);
		WriteLn;
		Write('INFIX : ');WriteTreeInfix(tree);
		WriteLn;
		Write('PREFIX : ');WriteTreePrefix(tree);
	END;

	WriteLn;WriteLn;
	input := ' true imp   t and (false <=> t';
    Parse(input, ok, tree);
	WriteLn(input);
	if NOT ok THEN WriteLn('Error') ELSE BEGIN
		Write('POSTFIX : '); WriteTreePostfix(tree);
		WriteLn;
		Write('INFIX : ');WriteTreeInfix(tree);
		WriteLn;
		Write('PREFIX : ');WriteTreePrefix(tree);
	END;
END.