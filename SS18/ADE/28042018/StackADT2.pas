unit StackADT2;
	
interface

	TYPE
		Node = ^NodeRec;
		NodeRec = Record
			value : Integer;
			next : Node;
		END;
		Stack = Record
			top:Node;
		END;

	(*Initializes the stack *)
	(*In/Out s: the Stack that should be initialized*)
	Procedure Init(VAR s: Stack);

	(* resets the stack. Stack can be reused afterwards *)
	Procedure Done(VAR s: Stack);

	(* Pushes a value onto the stack*)
	(* In value : value that will be pushed on the stack*)
	(* In/Out s: the Stack that should be used*)
	(* Out ok : False in case of any errors; TRUE otherwise
		IF false, the value will has not been pushed onto the stack *)
	PROCEDURE Push(value:Integer; Var ok : Boolean;VAR s: Stack);

	(* Pops the most recently pushed value from the stack *)
	(* Out value : value popped from the stack. *)
	(* In/Out s: the Stack that should be used*)
	(* Out ok : False in case of any errors (e.g. stack is empty)
		In this case, the returned value mustn't be used *)
	PROCEDURE Pop(Var value:Integer; Var ok : Boolean;VAR s: Stack);

	(* Checks if stack is currently empty*)
	(* In/Out s: the Stack that should be used*)
	(* Returns : True if stack is empty; False otherwise*)
	Function IsEmpty(Var s: Stack): Boolean;

implementation
	Procedure Init(VAR s: Stack);
	BEGIN
		s.top:= NIL;
	END; (* Init *)

	Procedure Done(VAR s: Stack);
	Var
		next : Node;
	BEGIN
		While s.top <> NIL Do Begin
			next := s.top^.next;
			Dispose(s.top);
			s.top := next;
		End;
		s.top := NIL;
	END; (* Done *)

	PROCEDURE Push(value:Integer; Var ok : Boolean;VAR s: Stack);
	Var
		n : Node;
	BEGIN
		New(n);
		ok := n <> NIL;
		IF ok Then Begin
			n^.value := value;
			n^.next := s.top;
			s.top := n;
		END;
	END; (* Push *)

	PROCEDURE Pop(Var value:Integer; Var ok : Boolean;VAR s: Stack);
	Var
		n: Node;
	BEGIN
		IF not IsEmpty(s) Then Begin
			value := s.top^.value;
			n := s.top^.next;
			Dispose(s.top);
			s.top := n;
			ok := true;
		End ELSE BEGIN
			ok := False;
		END;
	END; (* Pop *)

	Function IsEmpty(Var s: Stack): Boolean;
	Begin
		IsEmpty := s.top = NIL;
	END;
end.