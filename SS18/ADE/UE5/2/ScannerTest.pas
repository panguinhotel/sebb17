PROGRAM ScannerTest;

USES
	Scanner;

VAR
  inputFile: TEXT;
  ok: BOOLEAN;
BEGIN
 IF ParamCount <> 1 THEN BEGIN 
    WriteLn('USAGE: mpi<srcfile>');
    Halt(1);
  END;
   Assign(inputFile, ParamStr(1));
  (*$I-*)
    Reset(inputFile);
  (*$I+*)
  IF IOResult <> 0 THEN BEGIN
    WriteLn('Error, opening source file.');
    Halt(2);
  END;
  		InitScanner(inputFile);
		WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN
			GetNextSymbol;
		END;

		IF CurrentSymbol = noSym THEN begin
			WriteLn('ERROR');
		end ELSE BEGIN
			WriteLn('Success');
		END;
		Write('-->');
    
END. (* ScannerTest *)