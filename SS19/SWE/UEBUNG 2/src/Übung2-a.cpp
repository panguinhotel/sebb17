//============================================================================
// Name        : �bung2-a.cpp
// Author      : Lukas Schaubmayr
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "skipset.h"
#include <set>
#include <windows.h>

using namespace std;

int main() {
	skip_set<int> s1;
	set<int> s2;

	{
		std::cout<< "Insert 0...1000000 -> skip_set \t: ";
		long long c1 = GetTickCount();
		for(int i = 0; i < 1000000; ++i)
		{
			s1.insert(i);
		}
		std::cout << "Elapsed Ticks " << GetTickCount() - c1 << endl;


		std::cout<< "Insert 0...1000000 -> set \t: ";
		c1 = GetTickCount();
		for(int i = 0; i < 1000000; ++i)
		{
			s2.insert(i);
		}
		std::cout << "Elapsed Ticks " << GetTickCount() - c1<< endl;
	}

	{
		std::cout<< "Erase 0...1000000 -> skip_set \t: ";
		long long c1 = GetTickCount();
		for(int i = 0; i < 1000000; ++i)
		{
			s1.erase(i);
		}
		std::cout << "Elapsed Ticks " << GetTickCount() - c1 << endl;


		std::cout<< "Erase 0...1000000 -> set \t: ";
		c1 = GetTickCount();
		for(int i = 0; i < 1000000; ++i)
		{
			s2.erase(i);
		}
		std::cout << "Elapsed Ticks " << GetTickCount() - c1<< endl;
	}

	{
		std::cout<< "Insert 1000000...0 -> skip_set \t: ";
		long long c1 = GetTickCount();
		for(int i = 1000000; i > 0; --i)
		{
			s1.insert(i);
		}
		std::cout << "Elapsed Ticks " << GetTickCount() - c1 << endl;


		std::cout<< "Insert 1000000...0 -> set \t: ";
		c1 = GetTickCount();
		for(int i = 1000000; i > 0; --i)
		{
			s2.insert(i);
		}
		std::cout << "Elapsed Ticks " << GetTickCount() - c1<< endl;
	}
	{
		std::cout<< "Find 999999 -> skip_set \t: ";
		long long c1 = GetTickCount();
		s1.find(999999);
		std::cout << "Elapsed Ticks " << GetTickCount() - c1 << endl;


		std::cout<< "Find 999999 -> set \t\t: ";
		c1 = GetTickCount();
		s2.find(999999);
		std::cout << "Elapsed Ticks " << GetTickCount() - c1<< endl;
	}

	{
		std::cout<< "Find 1000001 -> skip_set \t: ";
		long long c1 = GetTickCount();
		s1.find(1000001);
		std::cout << "Elapsed Ticks " << GetTickCount() - c1 << endl;


		std::cout<< "Find 100001 -> set \t\t: ";
		c1 = GetTickCount();
		s2.find(1000001);
		std::cout << "Elapsed Ticks " << GetTickCount() - c1<< endl;
	}

	{
		std::cout<< "Insert 1000000 -> skip_set \t: ";
		long long c1 = GetTickCount();
		s1.insert(1000000);
		std::cout << "Elapsed Ticks " << GetTickCount() - c1 << endl;


		std::cout<< "Insert 1000000 -> set \t\t: ";
		c1 = GetTickCount();
		s2.insert(1000000);
		std::cout << "Elapsed Ticks " << GetTickCount() - c1<< endl;
	}


	std::cout<<"ITERATOR TESTS" << endl;
	{
		std::cout<< "Iterator ++ -> skip_set \t: ";
		long long c1 = GetTickCount();
		for(auto it = s1.begin(); it != end(s1); ++it)
		{
		}
		std::cout << "Elapsed Ticks " << GetTickCount() - c1 << endl;


		std::cout<< "Iterator ++ -> set \t\t: ";
		c1 = GetTickCount();
		for(auto it = s2.begin(); it != end(s2); ++it)
		{
		}
		std::cout << "Elapsed Ticks " << GetTickCount() - c1<< endl;

		std::cout<< "Iterator -- -> skip_set \t: ";

		c1 = GetTickCount();
		for(auto it = s1.end(); it != begin(s1); ++it)
		{
		}
		std::cout << "Elapsed Ticks " << GetTickCount() - c1 << endl;


		std::cout<< "Iterator -- -> set \t\t: ";
		c1 = GetTickCount();
		for(auto it = s2.end(); it != begin(s2); ++it)
		{
		}
		std::cout << "Elapsed Ticks " << GetTickCount() - c1<< endl;
	}


	{
		std::cout <<"COMPLEXITY TESTS skip_set" << endl;

		skip_set<int> s3;
		for(int i = 0; i < 100000; ++i)
		{
			s3.insert(i);
			if(i % 1000 == 0)
			{
				int one = i /4;
				int two = one *2;
				int three = one *3;

				long long c0 = GetTickCount();
				s3.find(one);
				long long c1 = GetTickCount() - c0;

				c0 = GetTickCount();
				s3.find(one);
				long long c2 = GetTickCount()- c0;

				c0 = GetTickCount();
				s3.find(one);
				long long c3 = GetTickCount() - c0;

				std::cout << one << " - " << c1;
				std::cout << two << " - " << c2;
				std::cout << three << " - " << c3;
			}
		}

		std::cout <<"COMPLEXITY TESTS set" << endl;
		set<int> s4;
		for(int i = 0; i < 100000; ++i)
		{
			s4.insert(i);
			if(i % 1000 == 0)
			{
				int one = i /4;
				int two = one *2;
				int three = one *3;

				long long c0 = GetTickCount();
				s4.find(one);
				long long c1 = GetTickCount() - c0;

				c0 = GetTickCount();
				s4.find(one);
				long long c2 = GetTickCount()- c0;

				c0 = GetTickCount();
				s4.find(one);
				long long c3 = GetTickCount() - c0;

				std::cout << one << " - " << c1;
				std::cout << two << " - " << c2;
				std::cout << three << " - " << c3;
			}
		}
	}
	return 0;
}
