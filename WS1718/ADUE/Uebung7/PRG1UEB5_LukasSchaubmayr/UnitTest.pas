unit UnitTest;

interface
Uses UnitDate;
TYPE  Response = RECORD
		status : (Failure,Success);
		message : string;
End;

TYPE   Testresult = RECORD    
		Name : string;
		DurationTime : Int64;
		Result: (passed,failed);
End;
TYPE   Testrun = RECORD    
		CreationDate: Date;
		Testresults: array [0..49] of Testresult;
End;

Var
	Testruns : array [0..19] of Testrun;
	idxLastTest : Integer;
(*Adds a TestRun with CrationDate to Testhistory max 20*)
PROCEDURE AddTestRun(d: Date; VAR res : Response);
(*Adds a Testresult with Name, DurationTime and Result to most recent Testrun (max 50)*)
PROCEDURE AddTestResultForMostRecentTestRun(param: Testresult; VAR res : Response);
(*Prints whole TestHistory on Console*)
PROCEDURE PrintTestHistory();
(*Calculates the average Test ExecutionTime for a specific name*)
FUNCTION AverageTestExecutionTime(name: string) : Int64;
(*Calculates the amount of times a Testresult occurs in the TestHistory*)
FUNCTION NumberOfTestExecutions(name: string) : INTEGER;
(*Calculates the number of failed Testruns*)
FUNCTION TotalNumberOfFailedTests() : INTEGER;
(*Clears the TestHistory*)
PROCEDURE ClearTestHistory();
implementation
(*Sorts TestHistory*)
PROCEDURE Sort();
VAR
	tmpTest : Testrun;
	i,k:Integer;
BEGIN
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		FOR k:= i+1 TO High(Testruns) DO BEGIN
			IF LiesBefore(Testruns[k].CreationDate,Testruns[i].CreationDate) THEN BEGIN
				tmpTest:= Testruns[i];
				Testruns[i] := Testruns[k];
				Testruns[k] := tmpTest;
				IF idxLastTest = i THEN idxLastTest := k;
				IF idxLastTest = k THEN idxLastTest := i;
			End;
		END;
	END;
END; (* Sort *)

PROCEDURE AddTestRun(d: Date ;VAR res : Response);
Var
i,k : Integer;
tmp,tmp1: Boolean;
BEGIN
	res.status := Success;
	res.message := 'Testrun added';
	tmp:= true;
	tmp1:= true;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF NOT IsValidDate(Testruns[i].CreationDate) Then begin
			if IsValidDate(d) then begin
				FOR k:=low(Testruns) TO (i-1) DO BEGIN
					IF (Testruns[k].CreationDate.day = d.day) and
					   (Testruns[k].CreationDate.month = d.month) and
					   (Testruns[k].CreationDate.year = d.year)  THEN BEGIN
						tmp1:= false;
					 	break;
					END;
				END;
				if tmp1 Then Begin
					Testruns[i].CreationDate := d;
					idxLastTest := i;
					tmp := false;
					Sort();
					EXIT;
				End;
			End
			else begin
				res.message := 'Error - Invalid Creationdate';
				res.status := Failure;
				EXIT;
			End; 
		End;
	End;
	IF not tmp1 THEN Begin
		res.message := 'Error - CreationDate already exits';
		res.status := Failure;
	End
	ELSE IF tmp THEN Begin
		res.message := 'Error - TestHistory is full';
		res.status := Failure;
	End;
END; (* AddTestRun *)
PROCEDURE AddTestResultForMostRecentTestRun(param: Testresult; VAR res : Response);
Var
	k:Integer;
BEGIN
	res.status := Failure;
	IF idxLastTest < 0  THEN BEGIN
		res.message := 'Error - No Test found';
	END
	ELSE BEGIN
		FOR k:= low(Testruns[idxLastTest].Testresults) TO High(Testruns[idxLastTest].Testresults) DO BEGIN
			IF Testruns[idxLastTest].Testresults[k].DurationTime = -1 Then Begin
				Testruns[idxLastTest].Testresults[k].Name := param.Name;
				Testruns[idxLastTest].Testresults[k].DurationTime := param.DurationTime;
				Testruns[idxLastTest].Testresults[k].Result := param.Result;
				res.message:='Testresult: '+param.Name+' added';
				res.status:=Success;
				EXIT;
			END;
		END;
		IF res.status = Failure  THEN res.message := 'Error - No more Testresult aviable for this Testrun';
	END;
END; (* AddTestResultForMostRecentTestRun *)
PROCEDURE PrintTestHistory();
Var
i,k : Integer;
BEGIN
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF NOT IsValidDate(Testruns[i].CreationDate) Then EXIT;
		WriteLn('Testrun ',^i,i+1,^i,DateToString(Testruns[i].CreationDate));
		FOR k:= low(Testruns[i].Testresults) TO High(Testruns[i].Testresults) DO BEGIN
			IF Testruns[i].Testresults[k].DurationTime = -1 Then break;
			WriteLn(Testruns[i].Testresults[k].Name,^i,Testruns[i].Testresults[k].DurationTime,^i,Testruns[i].Testresults[k].Result);
		END;
		WriteLn('==========================');
	END;
END; (* PrintTestHistory *)
PROCEDURE ClearTestHistory();
Var
	d : Date;
	i,k : Integer;
BEGIN
	d.day := -1;
	d.month := -1;
	d.year := -1;
	idxLastTest:=-1;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		Testruns[i].CreationDate :=d;
		FOR k:= low(Testruns[i].Testresults) TO High(Testruns[i].Testresults) DO BEGIN
			Testruns[i].Testresults[k].Name := 'NOT TESTED';
			Testruns[i].Testresults[k].DurationTime := -1;
			Testruns[i].Testresults[k].Result := failed;
		END;
	END;
END; (* ClearTestHistory *)
FUNCTION AverageTestExecutionTime(name: string) : Int64;
Var
	i,k : Integer;
	anz,sum : Int64;
BEGIN
	anz := NumberOfTestExecutions(name);
	sum := 0;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF NOT IsValidDate(Testruns[i].CreationDate) Then break;
		FOR k:= low(Testruns[i].Testresults) TO High(Testruns[i].Testresults) DO BEGIN
			IF Testruns[i].Testresults[k].DurationTime = -1 Then break;
			IF Testruns[i].Testresults[k].Name = name Then
				sum := sum + Testruns[i].Testresults[k].DurationTime;
		END;
	END;
	IF anz = 0  THEN AverageTestExecutionTime:= 0
	ELSE AverageTestExecutionTime:= sum DIV anz;

END; (* AverageTestExecutionTime *)
FUNCTION NumberOfTestExecutions(name: string) : INTEGER;
Var
	i,k : Integer;
BEGIN
	NumberOfTestExecutions:= 0;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF NOT IsValidDate(Testruns[i].CreationDate) Then EXIT;
		FOR k:= low(Testruns[i].Testresults) TO High(Testruns[i].Testresults) DO BEGIN
			IF Testruns[i].Testresults[k].DurationTime = -1 Then break;
			IF Testruns[i].Testresults[k].Name = name Then
				NumberOfTestExecutions := NumberOfTestExecutions +1;
		END;
	END;
END; (* NumberOfTestExecutions *)
FUNCTION TotalNumberOfFailedTests() : INTEGER;
Var
	i,k : Integer;
BEGIN
	TotalNumberOfFailedTests:= 0;
	FOR i:=low(Testruns) TO High(Testruns) DO BEGIN
		IF NOT IsValidDate(Testruns[i].CreationDate) Then EXIT;
		FOR k:= low(Testruns[i].Testresults) TO High(Testruns[i].Testresults) DO BEGIN
			IF Testruns[i].Testresults[k].DurationTime = -1 Then break;;
			IF Testruns[i].Testresults[k].Result = failed Then Begin
				TotalNumberOfFailedTests:=TotalNumberOfFailedTests +1;
				break;
			End;
		END;
	END;
END; (* TotalNumberOfFailedTests *)
begin
	ClearTestHistory();
end.