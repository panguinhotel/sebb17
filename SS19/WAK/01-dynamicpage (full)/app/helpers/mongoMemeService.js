const fs = require('fs');
const uuid = require('uuid/v1');
const async = require('async');
const MongoClient = require('mongodb').MongoClient;

const memeDirectory = './memes'

const url = 'mongodb://localhost:27017';
const dbName = 'memeDB';
const collection = 'memes';

class MemeService {

    constructor() {
        MongoClient.connect(url, (err, client) => {
            this.client = client;
            this.db = client.db(dbName);
        });
    }

    loadMemes(callback) {
        const col = this.db.collection(collection);
        col.find({}).toArray(function (err, docs) {
            callback(err, docs.sort((o1, o2) => o2.createdAt > o1.createdAt));
        });
    }

    storeMeme(meme, callback) {
        const id = uuid();

        fs.writeFile(`${memeDirectory}/${id}.png`, meme.data, {
            encoding: 'base64'
        });

        const col = this.db.collection(collection);
        col.insert({
            id: id,
            imageUrl: `./memes/${id}.png`,
            createdAt: new Date().getTime(),
            creator: meme.creator
        }, (err, result) => callback());
    }
}

module.exports = new MemeService();