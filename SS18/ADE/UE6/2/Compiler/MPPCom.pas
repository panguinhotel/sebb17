Unit MPPCom;
INTERFACE

PROCEDURE Parse(Var inputFile: Text; Var outFile: FILE; Var ok : Boolean; Var errorLine, errorColumn : Integer; Var errorText: String);
IMPLEMENTATION
USES MPScan, SymTblC, CodeDef,CodeGen;

Var
	success : Boolean;
	errText : String;

PROCEDURE SemErr(msg: String);
begin
	success := FALSE;
	errText := msg;
end;

PROCEDURE MP(Var outFile: File); FORWARD;
PROCEDURE VarDecl; FORWARD;
PROCEDURE StatSeq;FORWARD;
PROCEDURE Stat; FORWARD;
PROCEDURE Expr;FORWARD;
PROCEDURE Term;FORWARD;
PROCEDURE Fact;FORWARD;

PROCEDURE MP(Var outFile: FILE);
begin
	IF CurrentSymbol <> programSym THEN Begin success := false; EXIT; End;
	{sem}
	ResetSymbolTable;
	ResetCodeGenerator;
	{endSem}
	GetNextSymbol;
	IF CurrentSymbol <> identSym THEN  BEGIN success := FALSE; Exit; END;
	GetNextSymbol;
	IF CurrentSymbol <> semicolonSym THEN  BEGIN success := FALSE; Exit; END;
	GetNextSymbol;
	If CurrentSymbol = varSym THEN
	BEGIN
		VarDecl; IF not success THEN EXIT;
	END;

	IF CurrentSymbol <> beginSym THEN  BEGIN success := FALSE; Exit; END;
	GetNextSymbol;
	StatSeq; IF not success THEN Exit;

	IF CurrentSymbol <> endSym THEN  BEGIN success := FALSE; Exit; END;
	GetNextSymbol;
	If CurrentSymbol <> periodSym THEN  BEGIN success := FALSE; Exit; END;
	{sem}
	Emit1(endOpc);
	WriteCodeToFile(outFile);
	{endSem}
	GetNextSymbol;
end;

Procedure VarDecl;
Var ok : Boolean;
Begin
	IF CurrentSymbol <> varSym THEN  BEGIN success := FALSE; Exit; END;
	GetNextSymbol;
	IF CurrentSymbol <> identSym THEN  BEGIN success := FALSE; Exit; END;
	{sem}
	DeclareVar(CurrentIdentName, ok);
	{endSem}
	GetNextSymbol;

	While CurrentSymbol = commaSym DO BEGIN
		GetNextSymbol;
		IF CurrentSymbol <> identSym THEN BEGIN success := FALSE; Exit; END;
		{sem}
		DeclareVar(CurrentIdentName,ok);
		IF not ok THEN BEGIN SemErr('Variable already declared ' + CurrentIdentName); Exit; END;
		{endSem}
		GetNextSymbol;
	END;

	IF CurrentSymbol <> colonSym THEN  BEGIN success := FALSE; Exit; END;
	GetNextSymbol;
	IF CurrentSymbol <> integerSym THEN  BEGIN success := FALSE; Exit; END;
	GetNextSymbol;
	IF CurrentSymbol <> semicolonSym THEN  BEGIN success := FALSE; Exit; END;
	GetNextSymbol;
END;

PROCEDURE StatSeq;
BEGIN
	Stat; IF not success THEN EXIT;
	While CurrentSymbol = semicolonSym DO BEGIN
		GetNextSymbol;
		Stat; IF not success THEN Exit;
	END;
END;

PROCEDURE Stat;
{local}
Var
	dest: STring;
	addr, addr2: Integer;
{endLocal}
BEGIN
	CASE CurrentSymbol of 
		identSym : BEGIN
			dest:= CurrentIdentName;
			IF not isDeclared(dest) THEN BEGIN
				SemErr('Variable not declared: ' + dest); Exit;
			END;
			GetNextSymbol;
			IF CurrentSymbol <> assignSym THEN BEGIN success := FALSE; Exit; END;
			GetNextSymbol;
			Expr; IF not success THEN EXIT;
			Emit2(storeOpc, AddressOf(dest));
		END;
		readSym: BEGIN
			GetNextSymbol;
			IF CurrentSymbol <> leftParSym THEN BEGIN success := FALSE; Exit; END;
			GetNextSymbol;
			IF CurrentSymbol <> identSym THEN BEGIN success := FALSE; Exit; END;
			GetNextSymbol;

			IF not isDeclared(CurrentIdentName) THEN BEGIN
				SemErr('Variable not declared: ' + CurrentIdentName); Exit;
			END;

			Emit2(readOpc,AddressOf(CurrentIdentName));
			IF CurrentSymbol <> rightParSym THEN BEGIN success := FALSE; Exit; END;
			GetNextSymbol;
		END;
		writeSym: BEGIN
			GetNextSymbol;
			IF CurrentSymbol <> leftParSym THEN BEGIN success := FALSE; Exit; END;
			GetNextSymbol;
			Expr; IF not success THEN Exit;
			Emit1(writeOpc);
			IF CurrentSymbol <> rightParSym THEN BEGIN success := FALSE; Exit; END;
			GetNextSymbol;
		END;
		beginSym: BEGIN
			GetNextSymbol;
			StatSeq; IF not success THEN Exit;
			If CurrentSymbol <> endSym THEN BEGIN success := FALSE; Exit; END;
			GetNextSymbol;
		END;
		ifSym: BEGIN
			GetNextSymbol;
			Expr; IF not success THEN Exit;
			{sem}
			Emit2(jumpZeroOpc,0);
			addr := CurrentAddress -2;
			{endSem}
			IF CurrentSymbol <> thenSym THEN BEGIN success := FALSE; Exit; END;
			GetNextSymbol;
			Stat; IF not success Then Exit;


			IF CurrentSymbol = elseSym THEN BEGIN
				{sem}
				Emit2(jumpOpc,0);
				FixUpJumpTarget(addr,CurrentAddress);
				addr := CurrentAddress -2;
				{endSem}

				GetNextSymbol;
				Stat; IF not success THEN EXIT;
				{sem}
				FixUpJumpTarget(addr,CurrentAddress);
				{endSem}
			END;
		END;
		whileSym: Begin
			{sem}
			addr := CurrentAddress;
			{endSem}
			GetNextSymbol;
			Expr; IF NOT success THEN Exit;
			{sem}
			Emit2(jumpZeroOpc,0);
			addr2 := CurrentAddress -2;
			{endSem}
			IF CurrentSymbol <> doSym THEN BEGIN success := FALSE; Exit; END;
			GetNextSymbol;
			Stat; IF  not success THEN EXIT;
			{sem}
			Emit2(jumpOpc,addr);
			FixUpJumpTarget(addr2,CurrentAddress);
			{endSem}
		END
		ELSE BEGIN success := FALSE; Exit; END;
	END;
END;

PROCEDURE Fact;
BEGIN
	CASE CurrentSymbol OF
		identSym : BEGIN
			{sem}
			IF not isDeclared(CurrentIdentName) THEN BEGIN
				SemErr('Variable not declared: ' + CurrentIdentName); Exit;
			END;
			Emit2(loadValOpc,AddressOf(CurrentIdentName));
			{endSem}
			GetNextSymbol;
		End;
		numberSym : BEGIN
			Emit2(loadConstOpc, CurrentNumberValue);
			GetNextSymbol;
		END;
		leftParSym: BEGIN
			GetNextSymbol;
			Expr; IF not success THEN EXIT;
			IF CurrentSymbol <> rightParSym THEN BEGIN success := FALSE; Exit; END;
			GetNextSymbol;
		END
		ELSE BEGIN success := FALSE; Exit; END;
	END;
END;

PROCEDURE Term;
    BEGIN
        Fact; IF NOT success THEN EXIT;
        WHILE(CurrentSymbol = multSym) OR (CurrentSymbol = divSym) DO BEGIN
            CASE CurrentSymbol OF 
                multSym: BEGIN
                    GetNextSymbol;
                    Fact; IF NOT success THEN EXIT;
                    Emit1(multOpc);
                END;
                divSym: BEGIN
                    GetNextSymbol;
                    Fact; IF NOT success THEN EXIT;
                    Emit1(divOpc);
                END;
            END;
        END; 
    END;
    PROCEDURE Expr;
    BEGIN
        Term; IF NOT success THEN EXIT;
        WHILE(CurrentSymbol = plusSym) OR (CurrentSymbol = minusSym) DO BEGIN
            CASE CurrentSymbol OF 
                plusSym: BEGIN
                    GetNextSymbol;
                    Term; IF NOT success THEN EXIT;
                    Emit1(addOpc);
                END;
                minusSym: BEGIN
                    GetNextSymbol;
                    Term; IF NOT success THEN EXIT;
                    Emit1(subOpc);
                END;
            END;
        END;
    END;
	PROCEDURE Parse(Var inputFile: Text; Var outFile: FILE; Var ok : Boolean; Var errorLine, errorColumn : Integer; Var errorText: String);
	BEGIN
		success := True;
		errText :=  '';
		InitScanner(inputFile);
		MP(outFile);
		ok := success;
		errorText := errText;
		GetCurrentSymbolPosition(errorLine,errorColumn);
	END;
	End.