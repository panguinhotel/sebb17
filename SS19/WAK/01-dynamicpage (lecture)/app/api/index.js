var router = require('express').Router();

router.use('/memes', require('./memes'));
router.use('/templates', require('./templates'));

module.exports = router;