PROGRAM PMBench;
USES PMBib;

TYPE 
	TPMProc = PROCEDURE(s, p: STRING; VAR pos: INTEGER);

VAR 
	s, p: STRING;
	position: INTEGER;
	
PROCEDURE RunTest(name: STRING; pm: TPMProc);
VAR 
	stat: TPMStatistics;
BEGIN
	Write(name, ' ');
	InitPmStatistics;
	pm(s, p, position);
	GetPmStatistics(stat);
	Write('pos = ', position:3, ', char comps = ', stat.comps);
END;

BEGIN


	WHILE TRUE DO BEGIN
		WriteLn;
		WriteLn('Enter END for [STRING] OR [PATTERN] to end');
		WriteLn;
		
		Write('[STRING] = ');
		Readln(s);
		
		Write('[PATTERN] = ');
		Readln(p);
		
		WriteLn('FreePascal pos()');
		position := Pos(p, s);
		WriteLn('pos = ', position:3);
		
		WriteLn;
		
		IF (s = 'END') or (p = 'END')  THEN EXIT;
		RunTest('BruteSearchLR', BruteSearchLR);
		WriteLn;
		RunTest('KMP', KMP);
		WriteLn;
		RunTest('KMP2', KMP2);
		WriteLn;
		RunTest('BruteSearchRL', BruteSearchRL);
		WriteLn;
		RunTest('BoyerMoore', BoyerMoore);
		WriteLn;
		RunTest('RabinKarp', RabinKarp);
		WriteLn;	
	END;
	
	position := 0;
	BruteSearchLR('AABBBCDEFGHIJKLMNOPUausdujoAAAugeibfbdsfhjasdodifn564924698asdfi', 'AAA',  position);
	WriteLn;
	KMP('AABBBCDEFGHIJKLMNOPUausdujoAAAugeibfbdsfhjasdodifn564924698asdfi', 'AAA',  position);
	
END.