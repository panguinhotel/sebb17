unit Scanner;
	
interface
	TYPE Symbol = 
		(noSym,
		endSym,
		negSym,
		konjSym,
		disSym,
		kondSym,
		bikoSym,
		leftParSym,
		rightParSym,
		exprSym);
	PROCEDURE InitScanner(inp: String);
	PROCEDURE GetNextSymbol;
	FUNCTION CurrentSymbol : Symbol;
	FUNCTION CurrentSymbolPos : BYTE;
	FUNCTION CurrentNumberValue : LONGINT;

implementation
	CONST
		ENDOFINPUT = CHR(0);
		BLANK = ' ';
		TAB = Chr(9);
	VAR
		input : String;
		curPos : BYTE;
		curChar : Char;
		curNumberValue : LONGINT;
		curSymbol : Symbol;

	PROCEDURE GetNextChar;
	BEGIN
		IF curPos < Length(input) Then begin
			Inc(curPos);
			curChar := input[curPos];
		END ELSE BEGIN
			curChar := ENDOFINPUT;			
		END;
	END;
	PROCEDURE InitScanner(inp: String);
	begin
		input := inp;
		curPos := 0;
		curNumberValue := 0;
		GetNextChar;
		GetNextSymbol;
	end;
	PROCEDURE GetNextSymbol;
	VAR
		numberStr : String;
		code : LongInt;
	begin
		WHILE (curChar = BLANK) OR (curChar = TAB) DO
    	GetNextChar;

		CASE curChar of
			ENDOFINPUT : BEGIN curSymbol := endSym; END;
			'+' : BEGIN curSymbol := disSym; GetNextChar; End;
			'o' : BEGIN
				GetNextChar;
				IF curChar = 'r' THEN BEGIN curSymbol := disSym; GetNextChar; END
				ELSE BEGIN curSymbol := noSym; END;
			END;
			'-' : BEGIN curSymbol := negSym; GetNextChar; END;
			'n' : BEGIN
				GetNextChar;
				IF curChar = 'o' THEN BEGIN
					GetNextChar;
					IF curChar = 't' THEN BEGIN curSymbol := negSym; GetNextChar; END 
					ELSE BEGIN curSymbol := noSym; END;
				END ELSE BEGIN curSymbol := noSym; END;
			END;
			'*' : BEGIN curSymbol := konjSym; GetNextChar; END;
			'a' : BEGIN
				GetNextChar;
				IF curChar = 'n' THEN BEGIN
					GetNextChar;
					IF curChar = 'd' THEN BEGIN curSymbol := konjSym; GetNextChar; END 
					ELSE BEGIN curSymbol := noSym; END;
				END ELSE BEGIN curSymbol := noSym; END;
			END;
			'=' : BEGIN
				GetNextChar;
				IF curChar = '>' THEN BEGIN curSymbol := kondSym; GetNextChar;
				END ELSE BEGIN curSymbol := noSym; END;
			END;
			'i' : BEGIN
				GetNextChar;
				IF curChar = 'm' THEN BEGIN
					GetNextChar;
					IF curChar = 'p' THEN BEGIN curSymbol := kondSym; GetNextChar; END 
					ELSE BEGIN curSymbol := noSym; END;
				END ELSE BEGIN curSymbol := noSym; END;
			END;
			'<' : BEGIN
				GetNextChar;
				IF curChar = '=' THEN BEGIN
					GetNextChar;
					IF curChar = '>' THEN BEGIN curSymbol := bikoSym; GetNextChar; END 
					ELSE BEGIN curSymbol := noSym; END;
				END ELSE BEGIN curSymbol := noSym; END;
			END;
			'e' : BEGIN
				GetNextChar;
				IF curChar = 'q' THEN BEGIN curSymbol := bikoSym; GetNextChar;
				END ELSE BEGIN curSymbol := noSym; END;
			END;
			'(' : BEGIN curSymbol := leftParSym; GetNextChar; END;
			')' : BEGIN curSymbol := rightParSym; GetNextChar; END;
			't' : BEGIN
				GetNextChar;
				IF curChar = 'r' THEN BEGIN
					GetNextChar;
					IF curChar = 'u' THEN BEGIN
					GetNextChar;
						IF curChar = 'e' THEN BEGIN
						curSymbol := exprSym; GetNextChar;
						END ELSE BEGIN curSymbol := noSym; END;
					END ELSE BEGIN curSymbol := noSym; END;
				END ELSE BEGIN curSymbol := exprSym; GetNextChar; END;
			END;
			'f' : BEGIN
				GetNextChar;
				IF curChar = 'a' THEN BEGIN
					GetNextChar;
					IF curChar = 'l' THEN BEGIN
						GetNextChar;
						IF curChar = 's' THEN BEGIN
							GetNextChar;
							IF curChar = 'e' THEN BEGIN
								curSymbol := exprSym; GetNextChar;
							END ELSE BEGIN curSymbol := noSym; END;
						END ELSE BEGIN curSymbol := noSym; END;
					END ELSE BEGIN curSymbol := noSym; END;
				END ELSE BEGIN curSymbol := exprSym; GetNextChar; END;
			END
			ELSE begin
				curSymbol := noSym; END;
			end;
	end;
	FUNCTION CurrentSymbol : Symbol;
	begin
		CurrentSymbol := curSymbol;
	end;
	FUNCTION CurrentSymbolPos : BYTE;
	begin
		CurrentSymbolPos := curPos;
	end;
	FUNCTION CurrentNumberValue : LONGINT;
	begin
		CurrentNumberValue := curNumberValue
	end;

BEGIN
	InitScanner('');
end.