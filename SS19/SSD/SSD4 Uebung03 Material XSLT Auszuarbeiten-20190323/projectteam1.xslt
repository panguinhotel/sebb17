<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:fn="http://www.w3.org/2005/xpath-functions"
xpath-default-namespace="http://www.fh-hagenberg.at/projectteam"

>

	<xsl:output method="html" version="5.0" encoding="ISO-8859-1" indent="yes"/>
	<xsl:preserve-space elements="*"></xsl:preserve-space>
	
	<xsl:template match="/">
		<html>
			<head>s
				<title>Project Team</title>
			</head>
			<body>
				<h1>PROJECT TEAM</h1>
				<xsl:apply-templates/>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="Person">
		<xsl:text>****************************</xsl:text>
		<br/>
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="FirstName">
		<xsl:text>Vorname: </xsl:text>
		<xsl:value-of select="fn:upper-case(.)"></xsl:value-of>
	</xsl:template>

	<xsl:template match="MiddleName">
		<xsl:if test="not(nilled(.))">
			<xsl:text>Zweiter Vorname:</xsl:text>
			<xsl:value-of select="."></xsl:value-of>
			<br/>
		</xsl:if>
	</xsl:template>	
	
	<xsl:template match="LastName">
		<xsl:text>Nachname: </xsl:text>
		<xsl:value-of select="fn:upper-case(.)"></xsl:value-of>
	</xsl:template>
	
	<xsl:template match="BirthPlace">
		<xsl:text>Geburtsort: (</xsl:text>
		<xsl:value-of select="."></xsl:value-of>
		<xsl:text>)</xsl:text>
	</xsl:template>
	
	<xsl:template match="ProjectTeam">
		<xsl:apply-templates select="Person">
			<xsl:sort select="FirstName" order="descending"></xsl:sort>
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match="comment()">
		<xsl:value-of select="upper-case(.)"></xsl:value-of>
	</xsl:template>
</xsl:stylesheet>
