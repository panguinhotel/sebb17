unit DictionaryADS;
interface
	uses
		KeyValueListUnit;
	(*Initializes Tree*)
	Procedure Init;
	(*Insert or Replace(when existing) Node*)
	Procedure Put(key:String; value : Integer);
	(*Gets the value for a given Key*)
	Procedure Get(key:String; var value : Integer; Var found : Boolean);
	(*Clears Tree*)
	Procedure Clear;
	(*Returns the number of nodes in the tree*)
	Function Size : Integer;
	(*Returns true when tree contains key*)
	Function Contains(key : String) : Boolean;
	(*Removes single Node with given Key*)
	Procedure Remove(key:String);
implementation
	TYPE
		Pair = ^PairRec;
		PairRec = Record
			key : String;
			value : Integer;
			left, right : Pair;
		END;		
	Var
		m_rootItem : Pair;

	Function CalcOrdValue(value : String) : Integer;
	Var
		i,returnVal: Integer;
	begin
		returnVal := 0;
		FOR i:= 1 TO Length(value) DO BEGIN
			returnVal := returnVal + ORD(value[i]);
		END;

		CalcOrdValue := returnVal;
	End;

	Function NewPair(key:String; value :Integer) : Pair;
	Var
		n : Pair;
	begin		
		New(n);
		n^.key := key;
		n^.value := value;
		n^.left := NIL;
		n^.right := NIL;
		NewPair := n;
		
	End;

	Procedure AddItemToTree(itemToInsert: Pair;VAR currentItem: Pair);
	begin
		IF currentItem = NIL  THEN
			currentItem:= itemToInsert
		ELSE IF CalcOrdValue(currentItem^.key) <= CalcOrdValue(itemToInsert^.key)  THEN 
			 AddItemToTree(itemToInsert,currentItem^.left)
		ELSE AddItemToTree(itemToInsert,currentItem^.right);

	End;
	procedure UpdateValue(key : STring; value : Integer; VAR currentItem: Pair);
	begin
		IF currentItem <> NIL  THEN BEGIN
			IF currentItem^.key = key Then
				currentItem^.value := value
			ELSE IF CalcOrdValue(currentItem^.key) < CalcOrdValue(key)  THEN 
			 	UpdateValue(key,value,currentItem^.left)
			ELSE UpdateValue(key,value,currentItem^.right);
		END
		
	end;
	Procedure GetValueForKey(key: string; VAR value : Integer; currentItem: Pair);
	begin
		IF currentItem <> NIL  THEN BEGIN
			IF(currentItem^.key = key) THEN
				value := currentItem^.value
			ELSE IF CalcOrdValue(currentItem^.key) < CalcOrdValue(key)  THEN 
			 	GetValueForKey(key,value,currentItem^.left)
			ELSE GetValueForKey(key,value,currentItem^.right);
		END
		ELSE value := 0;
	End;
	Function NrOfPairs(currentItem :Pair) : Integer;
	begin
		IF currentItem = NIL  THEN Begin
			NrOfPairs := 0;
		END
		ELSE NrOfPairs := 1+NrOfPairs(currentItem^.left)+ NrOfPairs(currentItem^.right);
	END;
	Function TreeContainsItem(key: String; currentItem : Pair) : Boolean;
	begin
		IF currentItem = NIL Then Begin
			TreeContainsItem := false;
		END
		ELSE IF currentItem^.key = key Then Begin
			TreeContainsItem := true;
		END
		ELSE IF CalcOrdValue(currentItem^.key) < CalcOrdValue(key) Then
			TreeContainsItem := TreeContainsItem(key,currentItem^.left)
		ELSE TreeContainsItem := TreeContainsItem(key,currentItem^.right);
	END;

	Procedure DisposeTree(VAR currentItem: Pair);
	begin
		IF currentItem <> NIL THEN BEGIN
			DisposeTree(currentItem^.left);
			DisposeTree(currentItem^.right);
			Dispose(currentItem);
			currentItem := NIL;
		END;
	End;

	Procedure Init;
	begin
		m_rootItem := NIL;
	end;

	Procedure Put(key:String; value : Integer);
	begin
		IF not Contains(key)  THEN BEGIN
			AddItemToTree(NewPair(key,value), m_rootItem);
		END
		ELSE UpdateValue(key,value,m_rootItem);
	end;

	Procedure Get(key:String; var value : Integer; Var found : Boolean);
	begin
		found := Contains(key);
		value := 0;
		IF found  THEN BEGIN
			GetValueForKey(key,value,m_rootItem);
		END;

	end;

	Procedure Clear;
	begin
		DisposeTree(m_rootItem);
	end;

	Function Size : Integer;
	begin
		Size := NrOfPairs(m_rootItem);
	end;

	Function Contains(key:String) : Boolean;
	VAR
		n: Pair;
	begin
		Contains := TreeContainsItem(key,m_rootItem);
	end;

	Procedure AddItemsToList(Var l : List; currentItem : Pair);
	begin
		IF currentItem <> NIL  THEN BEGIN
			KeyValueListUnit.Append(l,currentItem^.key,currentItem^.value);
			AddItemsToList(l,currentItem^.left);
			AddItemsToList(l,currentItem^.right);			
		END;
	End;

	Procedure Remove(key:String);
	VAR
		l,li : List;
		n : Pair;
		i : Integer;
	Begin
		IF TreeContainsItem(key,m_rootItem)  THEN BEGIN
			KeyValueListUnit.Init(l);
			AddItemsToList(l,m_rootItem);
			Clear();
			li := l^.next;
			WHILE li <> l  DO BEGIN
				IF li^.key <> key  THEN BEGIN
					Put(li^.key,li^.value)
				END;
				li := li^.next;
			END;
			KeyValueListUnit.DisposeList(l);
		END;
	End;



	
end.