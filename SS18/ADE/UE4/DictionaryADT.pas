unit DictionaryADT;
	
interface
	uses
		KeyValueListUnit;
	TYPE
		Item = Pointer;

	(*Initializes Tree*)
	Procedure Init(VAR it : Item);
	(*Insert or Replace(when existing) Node*)
	Procedure Put(key:String; value : Integer;VAR it : Item);
	(*Gets the value for a given Key*)
	Procedure Get(key:String; var value : Integer; Var found : Boolean;VAR it : Item);
	(*Clears Tree*)
	Procedure Clear(VAR it : Item);
	(*Returns the number of nodes in the tree*)
	Function Size(VAR it : Item) : Integer;
	(*Returns true when tree contains key*)
	Function Contains(key : String;VAR it : Item) : Boolean;
	(*Removes single Node with given Key*)
	Procedure Remove(key:String; VAR it : Item);

implementation
	TYPE
		Pair = ^PairRec;
		PairRec = Record
			key : String;
			value : Integer;
			left, right : Pair;
		END;		

	Function CalcOrdValue(value : String) : Integer;
	Var
		i,returnVal: Integer;
	begin
		returnVal := 0;
		FOR i:= 1 TO Length(value) DO BEGIN
			returnVal := returnVal + ORD(value[i]);
		END;

		CalcOrdValue := returnVal;
	End;

	Function NewPair(key:String; value :Integer) : Pair;
	Var
		n : Pair;
	begin		
		New(n);
		n^.key := key;
		n^.value := value;
		n^.left := NIL;
		n^.right := NIL;
		NewPair := n;
		
	End;

	Procedure AddItemToTree(itemToInsert: Pair;VAR currentItem: Pair);
	begin
		IF currentItem = NIL  THEN
			currentItem:= itemToInsert
		ELSE IF CalcOrdValue(currentItem^.key) <= CalcOrdValue(itemToInsert^.key)  THEN 
			 AddItemToTree(itemToInsert,currentItem^.left)
		ELSE AddItemToTree(itemToInsert,currentItem^.right);

	End;
	procedure UpdateValue(key : STring; value : Integer; VAR currentItem: Pair);
	begin
		IF currentItem <> NIL  THEN BEGIN
			IF currentItem^.key = key Then
				currentItem^.value := value
			ELSE IF CalcOrdValue(currentItem^.key) < CalcOrdValue(key)  THEN 
			 	UpdateValue(key,value,currentItem^.left)
			ELSE UpdateValue(key,value,currentItem^.right);
		END
		
	end;
	Procedure GetValueForKey(key: string; VAR value : Integer; currentItem: Pair);
	begin
		IF currentItem <> NIL  THEN BEGIN
			IF(currentItem^.key = key) THEN
				value := currentItem^.value
			ELSE IF CalcOrdValue(currentItem^.key) < CalcOrdValue(key)  THEN 
			 	GetValueForKey(key,value,currentItem^.left)
			ELSE GetValueForKey(key,value,currentItem^.right);
		END
		ELSE value := 0;
	End;
	Function NrOfPairs(currentItem :Pair) : Integer;
	begin
		IF currentItem = NIL  THEN Begin
			NrOfPairs := 0;
		END
		ELSE NrOfPairs := 1+NrOfPairs(currentItem^.left)+ NrOfPairs(currentItem^.right);
	END;
	Function TreeContainsItem(key: String; currentItem : Pair) : Boolean;
	begin
		IF currentItem = NIL Then
			TreeContainsItem := false
		ELSE IF currentItem^.key = key Then
			TreeContainsItem := true
		ELSE IF CalcOrdValue(currentItem^.key) < CalcOrdValue(key) Then
			TreeContainsItem := TreeContainsItem(key,currentItem^.left)
		ELSE TreeContainsItem := TreeContainsItem(key,currentItem^.right);
	END;

	Procedure DisposeTree(VAR currentItem: Pair);
	begin
		IF currentItem <> NIL THEN BEGIN	
			DisposeTree(currentItem^.left);
			DisposeTree(currentItem^.right);
			Dispose(currentItem);
			currentItem := NIL;
		END;
	End;

	Procedure Init(VAR it : Item);
	begin
		it := NIL;
	end;

	Procedure Put(key:String; value : Integer;VAR it : Item);
	begin
		IF not Contains(key,Pair(it))  THEN BEGIN
			AddItemToTree(NewPair(key,value), Pair(it));
		END
		ELSE UpdateValue(key,value,Pair(it));
	end;

	Procedure Get(key:String; var value : Integer; Var found : Boolean;VAR it : Item);
	begin
		found := Contains(key,Pair(it));
		value := 0;
		IF found  THEN BEGIN
			GetValueForKey(key,value,Pair(it));
		END;

	end;

	Procedure Clear(VAR it : Item);
	begin
		DisposeTree(Pair(it));
	end;

	Function Size(VAR it : Item) : Integer;
	begin
		Size := NrOfPairs(Pair(it));
	end;

	Function Contains(key:String;VAR it : Item) : Boolean;
	begin
		Contains := TreeContainsItem(key,Pair(it));
	end;

	Procedure AddItemsToList(Var l : List; currentItem : Pair);
	begin
		IF currentItem <> NIL  THEN BEGIN
			KeyValueListUnit.Append(l,currentItem^.key,currentItem^.value);
			AddItemsToList(l,currentItem^.left);
			AddItemsToList(l,currentItem^.right);			
		END;
	End;
	Procedure Remove(key:String; VAR it : Item);
	VAR
		l,li : List;
		i : Integer;
	Begin
		IF TreeContainsItem(key,it)  THEN BEGIN
			KeyValueListUnit.Init(l);
			AddItemsToList(l,it);
			Clear(it);
			li := l^.next;
			WHILE li <> l  DO BEGIN
				IF li^.key <> key  THEN BEGIN
					Put(li^.key,li^.value,it)
				END;
				li := li^.next;
			END;
			KeyValueListUnit.DisposeList(l);
		END;
	End;


	
end.