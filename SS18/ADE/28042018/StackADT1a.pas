unit StackADT1a;
	
interface
	
	TYPE Stack = Pointer;

	(*Initializes the stack *)
	(*In/Out s: the Stack that should be initialized*)
	Procedure Init(VAR s: Stack);

	(* resets the stack. Stack can be reused afterwards *)
	Procedure Done(VAR s: Stack);

	(* Pushes a value onto the stack*)
	(* In value : value that will be pushed on the stack*)
	(* In/Out s: the Stack that should be used*)
	(* Out ok : False in case of any errors; TRUE otherwise
		IF false, the value will has not been pushed onto the stack *)
	PROCEDURE Push(value:Integer; Var ok : Boolean;VAR s: Stack);

	(* Pops the most recently pushed value from the stack *)
	(* Out value : value popped from the stack. *)
	(* In/Out s: the Stack that should be used*)
	(* Out ok : False in case of any errors (e.g. stack is empty)
		In this case, the returned value mustn't be used *)
	PROCEDURE Pop(Var value:Integer; Var ok : Boolean;VAR s: Stack);

	(* Checks if stack is currently empty*)
	(* In/Out s: the Stack that should be used*)
	(* Returns : True if stack is empty; False otherwise*)
	Function IsEmpty(Var s: Stack): Boolean;

implementation
	CONST
		MaxSize = 10;
	TYPE
		State = ^StateRec;
		StateRec = Record
			top: Integer;
			data: Array[1..MaxSize] Of Integer;
		END;

	Procedure Init(VAR s: Stack);
	VAR
		st : State;
	begin
		new(st);
		st^.top := 0;
		s := st;
	end;

	Procedure Done(VAR s: Stack);
	begin
		Dispose(State(s));
	end;

	PROCEDURE Push(value:Integer; Var ok : Boolean;VAR s: Stack);
	begin
		IF State(s)^.top = MaxSize  THEN BEGIN
			ok := false;
		END ELSE BEGIN
			Inc(State(s)^.top);
			State(s)^.data[State(s)^.top] := value;
			ok := true;
		END;
	end;

	PROCEDURE Pop(Var value:Integer; Var ok : Boolean;VAR s: Stack);
	begin
		IF IsEmpty(s)  THEN BEGIN
			ok := False;
		END ELSE BEGIN
			value := State(s)^.data[State(s)^.top];
			Dec(State(s)^.top);
			ok := TRUE;
		END;
	end;

	Function IsEmpty(Var s: Stack): Boolean;
	begin
		IsEmpty := State(s)^.top = 0;
	end;

	
end.