unit Scanner;


interface
	TYPE Symbol = (noSym, trueSym,falseSym,
	nullSym,numberSym,stringSym,
	gkleftSym, gkrightSym, ekleftSym, 
	ekrightSym, dpSym, commaSym,endSym);
CONST
    TAB = Chr(9);
    LF = Chr(10);
    CR = Chr(13);
    BLANK = ' ';
PROCEDURE InitScanner(VAR inputFile:TEXT);
PROCEDURE GetNextSymbol;
FUNCTION CurrentSymbol:Symbol;
PROCEDURE GetCurrentSymbolPosition(Var line,column: INTEGER);
FUNCTION CurrentText : STRING;
implementation

VAR
    inpFile: Text;
    curChar: CHAR;
    charLine, charColumn: INTEGER;
    curSymbol: Symbol;
    symbolLine, symbolColumn: INTEGER;
	currTxt : String;

PROCEDURE GetNextChar;
 BEGIN
	IF not EOF(inpFile) THEN BEGIN
		Read(inpFile, curChar);
			IF (curChar = CR) OR (curChar = LF) THEN BEGIN
			IF (charColumn > 0) THEN
				Inc(charLine);
			curChar := BLANK;
			charColumn := 0;
			END
		ELSE
		Inc(charColumn);
	END ELSE 
	BEGIN
		curChar := Chr(0);
	END;
   
END;
    
PROCEDURE InitScanner(VAR inputFile : TEXT);
BEGIN
    inpFile := inputFile;
    charLine := 1;
    charColumn := 0;
    GetNextChar;
    GetNextSymbol;
END;

PROCEDURE GetNextSymbol;
VAR
	isEscapeBefore : Boolean;
BEGIN
	WHILE ((curChar = BLANK) OR (curChar = TAB)) and (curChar <> Chr(0)) DO
    	GetNextChar;
    
    symbolLine := charLine;
    symbolColumn := charColumn;
	currTxt := curChar;
    
    CASE curChar OF
		Chr(0) : BEGIN curSymbol := endSym; END;
		':' : BEGIN curSymbol := dpSym; GetNextChar; END;
		',' : BEGIN curSymbol := commaSym; GetNextChar; END;
		'[' : BEGIN curSymbol := ekleftSym; GetNextChar; END;
		']' : BEGIN curSymbol := ekrightSym; GetNextChar; END;
		'{' : BEGIN curSymbol := gkleftSym; GetNextChar; END;
		'}' : BEGIN curSymbol := gkrightSym; GetNextChar; END;
		'n' : BEGIN
				GetNextChar;
				currTxt := currTxt + curChar;
				IF curChar = 'u'  THEN BEGIN
					GetNextChar;
					currTxt := currTxt + curChar;
					IF curChar = 'l' THEN BEGIN
						GetNextChar;
						currTxt := currTxt + curChar;
						IF curChar = 'l' THEN BEGIN
							curSymbol := nullSym;
							GetNextChar;							
						END ELSE curSymbol := noSym; 
					END ELSE curSymbol := noSym; 
				END ELSE curSymbol := noSym; 
			END;
		't' : BEGIN
				GetNextChar;
				currTxt := currTxt + curChar;
				IF curChar = 'r'  THEN BEGIN
					GetNextChar;
					currTxt := currTxt + curChar;
					IF curChar = 'u' THEN BEGIN
						GetNextChar;
						currTxt := currTxt + curChar;
						IF curChar = 'e' THEN BEGIN
							curSymbol := trueSym;
							GetNextChar;
						END ELSE curSymbol := noSym; 
					END ELSE curSymbol := noSym; 
				END ELSE curSymbol := noSym; 
			END;
		'f' : BEGIN
				GetNextChar;
				currTxt := currTxt + curChar;
				IF curChar = 'a'  THEN BEGIN
					GetNextChar;
					currTxt := currTxt + curChar;
					IF curChar = 'l' THEN BEGIN
						GetNextChar;
						currTxt := currTxt + curChar;
						IF curChar = 's' THEN BEGIN
							GetNextChar;
							currTxt := currTxt + curChar;
							IF curChar = 'e' THEN BEGIN
								curSymbol := falseSym;
								GetNextChar;
							END ELSE curSymbol := noSym;
						END ELSE curSymbol := noSym; 
					END ELSE curSymbol := noSym; 
				END ELSE curSymbol := noSym; 
			END;		
		'"' : BEGIN 
				isEscapeBefore := curChar = '\';
				GetNextChar;
				currTxt := currTxt + curChar;
				WHILE (curChar <> '"') or ((curChar = '"') and isEscapeBefore) DO BEGIN //TODO
					isEscapeBefore := curChar = '\';
					GetNextChar;
					currTxt := currTxt + curChar;
				END;
				curSymbol := stringSym;
				GetNextChar;
			END;
		'-','0'..'9' : BEGIN
				GetNextChar;
				
				WHILE curChar in ['0'..'9'] DO BEGIN
					currTxt := currTxt + curChar;
					GetNextChar;	
				END;
				IF curChar = '.' THEN BEGIN
					currTxt := currTxt + curChar;
					GetNextChar;	
					IF curChar in ['0'..'9'] THEN BEGIN
						currTxt := currTxt + curChar;
						WHILE curChar in ['0'..'9']  DO BEGIN
							GetNextChar;
							currTxt := currTxt + curChar;
						END;
						IF curChar = 'E'  THEN BEGIN
							GetNextChar;
							IF curChar in ['-','+']  THEN BEGIN
								currTxt := currTxt + curChar;
								GetNextChar;
								IF curChar in ['0'..'9'] THEN BEGIN
									currTxt := currTxt + curChar;
									 WHILE curChar in ['0'..'9'] DO BEGIN
									 	GetNextChar;
										currTxt := currTxt + curChar;
									 END;
									 curSymbol := numberSym;
								END ELSE curSymbol := noSym;
							END ELSE curSymbol := noSym;
						END ELSE curSymbol := numberSym;
					END ELSE curSymbol := noSym;
				END ELSE curSymbol := numberSym;		
			END
			ELSE curSymbol := noSym;
			END;

		{sem}
		IF curSymbol <> endSym THEN BEGIN
			Write(CurrentText);
			Flush(Output);
		END;{endSem}
END;

FUNCTION CurrentSymbol: Symbol;
BEGIN
	CurrentSymbol := curSymbol;
END;
PROCEDURE GetCurrentSymbolPosition(VAR line, column: INTEGER);
BEGIN
	line := symbolLine;
	column := symbolColumn;
END;

FUNCTION CurrentText : STRING;
BEGIN
    CurrentText := currTxt;
END;
end.