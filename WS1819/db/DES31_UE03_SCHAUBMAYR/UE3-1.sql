--UE3-1a
Select f.release_year, f.Title as Filmtitel,
  LISTAGG((substr(a.first_name,0,1)||'. '||a.last_name),', ') WITHIN GROUP (ORDER BY a.last_name, a.first_name) as actors
from film f
join film_actor fa on f.film_id = fa.film_id
join actor a on a.actor_id = fa.actor_id
group by f.Title,f.release_year
order by f.release_year;

--UE3-1b (in den letzten 4 Jahren wurden keine Filme ausgeliehen)
SELECT (c.first_name||' '||c.last_name) as "CUSTOMER",
  LISTAGG((f.Title||' ('||f.release_year||')'),', ') WITHIN GROUP (ORDER BY f.release_year desc) as FILMS
from customer c
join rental r on r.customer_id = c.customer_id
join inventory i on i.inventory_id = r.inventory_id
join film f on f.film_id = i.film_id
where (Extract(year from sysdate)-f.release_year) < 4
group by (c.first_name||' '||c.last_name);
