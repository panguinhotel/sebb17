/*
 * Producer_Event.h
 *
 *  Created on: 07.03.2019
 *      Author: Lukas Schaubmayr
 */

#ifndef PRODUCER_EVENT_H_
#define PRODUCER_EVENT_H_

#include "Base_Event.h"
#include <vector>
#include <iostream>
#include <thread>

class Producer_Event : public Base_Event{
public:
	inline Producer_Event(int produce_value){m_produce_value = produce_value;}
	inline virtual ~Producer_Event(){std::cout <<"P_E : DESTRUCTOR" << std::endl;}

	inline bool produce(std::vector<int> *data, int max_size = -1)
	{
		if(max_size != -1 && data->size()+1 > max_size)
		{
			std::cout<<"P_E : Container full!"<< std::endl;
			return false;
		}
		else
		{
			std::this_thread::sleep_for(std::chrono::seconds(2));
			data->push_back(m_produce_value);
			std::cout<<"P_E : produced " << m_produce_value << std::endl;
			return true;
		}
	}
private:
	int m_produce_value = 0;
};

#endif /* PRODUCER_EVENT_H_ */
