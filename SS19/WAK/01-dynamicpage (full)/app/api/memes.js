const router = require('express').Router();
const memeService = require("../helpers/mongoMemeService");
// const memeService = require("../helpers/localMemeService");

router.get('/', function (req, res) {
    memeService.loadMemes(function (err, memes) {
        if (err) {
            res.sendStatus(500);
        } else {
            res.json(memes);
        }
    });
});

router.post('/', function (req, res) {
    memeService.storeMeme(req.body, () => res.sendStatus(200));
});

module.exports = router;