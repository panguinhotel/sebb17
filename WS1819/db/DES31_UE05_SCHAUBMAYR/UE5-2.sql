DROP TABLE USER_LOGGING;
CREATE TABLE USER_LOGGING (
  session_id int not null,
  login_time Timestamp not null,
  db_user VARCHAR2(50) not null,
  os_user VARCHAR2(50) not null,
  ip VARCHAR2(50) not null,
  host_name VARCHAR2(50) not null
);

CREATE OR REPLACE TRIGGER MY_SYS_TRIGGER
  AFTER LOGON ON SCHEMA
BEGIN
  INSERT INTO USER_LOGGING (session_id,login_time, db_user,os_user,ip,host_name)
  VALUES(SYS_CONTEXT('USERENV','SESSIONID'),
        SYSTIMESTAMP,
        USER,
        SYS_CONTEXT('USERENV','OS_USER'),
        SYS_CONTEXT('USERENV','IP_ADDRESS'),
        SYS_CONTEXT('USERENV','HOST'));
END;

Select * from USER_LOGGING;

rollback;
