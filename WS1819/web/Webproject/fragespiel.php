<?php
   include('session.php');
   include('config.php');
   
   $currentgameID=-1;
   if($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['bt_set']))
    {
        $sql = "INSERT into game (id_hostUser) VALUES($login_userID)";   
        if(mysqli_query($db,$sql))
        {
            $sql4 = "SELECT Max(id_game) as ID from game where id_hostUser = $login_userID ";
            $result4 = mysqli_query($db,$sql4);
            $value4 = mysqli_fetch_object($result4);
            $currentGameID = $value4->ID;
        }
        else
        {
            echo '<script language="javascript">';
            echo 'alert("Cannot create new game!")';
            echo '</script>';
        }
    }
  }
?>
<html>
   
   <head>
    <title>Fragespiel</title>
          <link rel="stylesheet" type="text/css" href="style.css">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">    
   </head>
   
   <body>
     <h1 class="headLine">Fragespiel</h1> 
        <div class="mainList">
            <input id="myInput" <?php echo('value="'.$currentGameID.'"') ?> type="number" class="loginInput" placeholder="Game ID"> 
            <button id="myButton" class="mainButton" >Join</button>
            <button id="myHostButton" class="mainButton" >Host</button>
            <br>
            <form class="container" action = "" method = "post" >
                <button name="bt_set" class="mainButton" type="submit">Create Game</button>
            </form>

            <button class="backbutton" onclick="window.location.href='./welcome.php'">Zurück</button>
        </div>
    <script>
        var ele = document.getElementById("myButton");
        ele.addEventListener("click", redirectFunction);

        var ele2 = document.getElementById("myHostButton");
        ele2.addEventListener("click", redirectFunctionHost);

        function redirectFunction(event){
            var ele1 = document.getElementById("myInput");
            var newPage = "./fragespiel/client.php?id="+ele1.value;
            window.location.href = newPage;
        }

        function redirectFunctionHost(event){
            var ele1 = document.getElementById("myInput");
            var newPage = "./fragespiel/host.php?id="+ele1.value;
            window.location.href = newPage;
        }
    </script>    
   </body>
   
</html>