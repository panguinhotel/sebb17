PROGRAM ScannerTest;


USES Scanner;
VAR
	inputFile: TEXT;
	ok: BOOLEAN;
(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	Assign(inputFile,'./Shapes.txt');
	(*$I-*)
    Reset(inputFile);
    (*$I+*)
    IF IOResult <> 0 THEN BEGIN
      WriteLn('Error, opening source file.');
      Halt(1);
    END;

	InitScanner(inputFile);
	WHILE (CurrentSymbol <> endSym) AND (CurrentSymbol <> noSym)  DO BEGIN
			GetNextSymbol;
		END;

		IF CurrentSymbol = noSym THEN begin
			WriteLn('ERROR');
		end ELSE BEGIN
			WriteLn('Success');
		END;

END. (* ScannerTest *)