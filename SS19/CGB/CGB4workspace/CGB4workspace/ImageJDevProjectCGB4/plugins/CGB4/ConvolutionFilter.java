import ij.IJ;

public class ConvolutionFilter {

	
	/**
	 * convolution of input image with kernel, normalization to kernel sum 1.0
	 * only use for low-pass filters
	 * 
	 * @param inputImg
	 * @param width
	 * @param height
	 * @param kernel double[][] kernel image
	 * @param radius kernel radius
	 * @return
	 */
	public static double[][] convolveDoubleNorm(double[][] inputImg, int width, int height, double[][] kernel, int radius) {
		double[][] returnImg = new double[width][height];
		
		//TODO: implementation required
		
		return returnImg;
	}
	
	/**
	 * convolution of input image with kernel
	 * 
	 * @param inputImg
	 * @param width
	 * @param height
	 * @param kernel double[][] kernel image
	 * @param radius kernel radius
	 * @return
	 */
	public static double[][] convolveDouble(double[][] inputImg, int width, int height, double[][] kernel, int radius) {
		double[][] returnImg = new double[width][height];
		
		//iterate over all pixels (place yellow kernel with hot-spot at (x,y))
		for(int x = 0; x < width; ++x)
		{
			for(int y = 0; y < height; ++y)
			{
				double newVal = 0.0;
				double kernelSum = 0.0;
				
				//calculate new value from all neighbours in the yellow kernel
				for(int xOffset = -radius; xOffset <= radius; ++xOffset)
				{
					for(int yOffset = -radius; yOffset <= radius; ++yOffset)
					{
						//current neighbour (white original image)
						int nbX = x+xOffset;
						int nbY = y+yOffset;
						
						//ATTENTION: in case of border we need to check indices
						if(nbX >= 0 && nbX < width && nbY >= 0 && nbY < height)
						{
							double origNBval = inputImg[nbX][nbY];
							newVal += origNBval * kernel[xOffset+radius][yOffset+radius];
							kernelSum +=kernel[xOffset+radius][yOffset+radius];
						}
					}
				}
				
				//now inside the image, kernelSum should hold a value approx. 1.0;
				//but close to the borders maybe ~0.5
				
				//now normalize for correction
				double normFactor = 1.0 / kernelSum;
				
				//finally assign normalized value
				returnImg[x][y] = newVal * normFactor;
			}
		}
		
		return returnImg;
	}
	
	/**
	 * returns kernel image according to specified radius for mean low-pass filtering
	 * 
	 * @param tgtRadius
	 * @return
	 */
	public static double[][] getMeanMask(int tgtRadius) {
		int size = 2 * tgtRadius + 1;
		double[][] kernelImg = new double[size][size];
		
		double coefficient = 1.0 / ((double) size*size);
		IJ.log("mean coefficient = " + coefficient);
		
		for(int x = 0; x < size; ++x)
		{
			for(int y = 0; y < size; ++y)
			{
				kernelImg[x][y] = coefficient;
			}
		}
		
		return kernelImg;
	}
		
}
