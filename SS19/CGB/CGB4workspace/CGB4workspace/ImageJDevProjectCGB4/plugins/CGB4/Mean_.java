import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

public class Mean_ implements PlugInFilter {  
	 
	public int setup(String arg, ImagePlus imp) {
		if (arg.equals("about"))
			{showAbout(); return DONE;}
		return DOES_8G+DOES_STACKS+SUPPORTS_MASKING;
	} //setup

	
	public void run(ImageProcessor ip) {
		byte[] pixels = (byte[])ip.getPixels();
		int width = ip.getWidth();
		int height = ip.getHeight();
	    int[][] inDataArrInt = ImageJUtility.convertFrom1DByteArr(pixels, width, height);
       
	    double[][] inDataArrDbl = ImageJUtility.convertToDoubleArr2D(inDataArrInt, width, height);//i
	    
        int radius = 3; //7x7
        //TODO Let user input target radius
        
        GenericDialog gd = new GenericDialog("user input");
        gd.addNumericField("radius",radius,0);
        gd.showDialog();
        
        if(!gd.wasCanceled()) {
        	radius = (int)gd.getNextNumber();
        }
        
        double[][] kernelImg = ConvolutionFilter.getMeanMask(radius);//k
        
        //now apply the convolution: i�k == IxK
        double[][] resultImg = ConvolutionFilter.convolveDouble(inDataArrDbl, width, height, kernelImg, radius);
        
        ImageJUtility.showNewImage(resultImg,width,height,"result of convolution, mean= "+radius);
        
	} //run
	
	

	void showAbout() {
		IJ.showMessage("About Template_...",
			"this is a Mean\n");
	} //showAbout
	
} //class FilterTemplate_
