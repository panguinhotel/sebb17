PROGRAM ErreichbarkeitvonFelder;
VAR
	feld : Array [0..49] of Array [0..19] of Char;
	tmp : Array[0..49] of Array[0..19] of Boolean;
	tmp1 : Array[0..49] of Array[0..19] of Integer;
	TEMP:boolean;
	
(*Initialize Field*)
PROCEDURE InitField();
VAR
 i,k : Integer;
BEGIN
	FOR i:= Low(feld) TO High(feld) DO BEGIN
		FOR k:=Low(feld[i]) TO High(feld[i]) DO BEGIN
			feld[i,k] := '.';
			tmp[i,k] := false;
			tmp1[i,k]:=10000;
		END;
	END;
	feld[2,3] := '#';feld[2,4] := '#';feld[2,5] := '#';feld[2,13] := '#';
	feld[2,14] := '#';feld[2,15] := '#';feld[2,16] := '#';feld[3,3] := '#';
	feld[3,17] := '#';feld[4,0] := '#';feld[4,1] := '#';feld[4,2] := '#';
	feld[4,3] := '#';feld[4,17] := '#';feld[5,7] := '#';feld[5,8] := '#';
	feld[5,9] := '#';feld[5,10] := '#';feld[6,19] := '#';feld[9,5] := '#';
	feld[9,6] := '#';feld[9,7] := '#';feld[9,8] := '#';feld[9,9] := '#';
	feld[9,16] := '#';feld[10,5] := '#';feld[10,9] := '#';feld[10,16] := '#';
	feld[11,5] := '#';feld[11,9] := '#';feld[11,16] := '#';feld[12,5] := '#';
	feld[12,9] := '#';feld[12,16] := '#';feld[9,5] := '#';feld[13,6] := '#';
	feld[13,7] := '#';feld[13,8] := '#';feld[13,9] := '#';feld[13,16] := '#';
	feld[14,16] := '#';feld[14,17] := '#';feld[14,18] := '#';feld[14,19] := '#';

	TEMP:=true;

END; (* Initfield *)
(*Clear Memory*)
PROCEDURE ClearMemory();
VAR
	i,k : Integer;
BEGIN
	TEMP:=true;
		FOR i:= Low(feld) TO High(feld) DO BEGIN
			FOR k:=Low(feld[i]) TO High(feld[i]) DO BEGIN
				tmp[i,k] := false;
				tmp1[i,k]:=10000;
			END;
		END;
END; (* ClearMemory *)
(*Checks if Path Exits*)
FUNCTION PathExists(ax,ay,bx,by: INTEGER) : Boolean;
BEGIN
	tmp[ax,ay] := true;
	PathExists:=false;
	if(ax=bx) and (ay= by) Then begin
		PathExists := true;
	END
	ELSE BEGIN
		//up
		IF (ay-1 >= Low(feld[ax])) and (not tmp[ax,ay-1]) and (feld[ax,ay-1] = '.') THEN Begin	
			PathExists := PathExists or PathExists(ax,ay-1,bx,by)
		END;
		//down
		IF (ay+1 <= High(feld[ax])) and (not tmp[ax,ay+1]) and (feld[ax,ay+1] = '.') THEN begin
			PathExists := PathExists or PathExists(ax,ay+1,bx,by)
		END;
		//left
		IF (ax-1 >= Low(feld)) and (not tmp[ax-1,ay]) and (feld[ax-1,ay] = '.') THEN BEGIN
			PathExists := PathExists or PathExists(ax-1,ay,bx,by)
		END;
		//right
		IF(ax+1 <= High(feld)) and (not tmp[ax+1,ay]) and (feld[ax+1,ay] = '.') THEN BEGIN
			PathExists := PathExists or PathExists(ax+1,ay,bx,by)
		END;
	END;
END; (* PathExists *)
(*Calculates Minimum *)
FUNCTION Min(a,b: INTEGER) : INTEGER;
BEGIN
	If a > b Then Min := b Else Min := a;
END; (* Min *)
(*Calculates Shortest Path Between to Positions*)
FUNCTION LenghtOfShortestPath(ax,ay,bx,by: INTEGER) : INTEGER;
BEGIN
	LenghtOfShortestPath := 10000;
	if(TEMP) Then begin
		tmp1[ax,ay] := 0;
		TEMP :=false;
		if(not PathExists(ax,ay,bx,by))Then
			LenghtOfShortestPath:= -1;
	end;

	if(ax=bx) and (ay= by) Then begin
		LenghtOfShortestPath := tmp1[ax,ay];
	END
	ELSE BEGIN
		//up
		IF (ay-1 >= Low(feld[ax])) and (feld[ax,ay-1] = '.') and (tmp1[ax,ay-1] > (tmp1[ax,ay]+1)) THEN Begin
			tmp1[ax,ay-1]:=tmp1[ax,ay]+1;	
			LenghtOfShortestPath:= Min(LenghtOfShortestPath,LenghtOfShortestPath(ax,ay-1,bx,by));
		END;
		//down
		IF (ay+1 <= High(feld[ax])) and (feld[ax,ay+1] = '.') and (tmp1[ax,ay+1] > (tmp1[ax,ay]+1)) THEN begin
			tmp1[ax,ay+1]:=tmp1[ax,ay]+1;	
			LenghtOfShortestPath:= Min(LenghtOfShortestPath,LenghtOfShortestPath(ax,ay+1,bx,by));
		END;
		//left
		IF (ax-1 >= Low(feld)) and (feld[ax-1,ay] = '.') and (tmp1[ax-1,ay] > (tmp1[ax,ay]+1)) THEN BEGIN
			tmp1[ax-1,ay]:=tmp1[ax,ay]+1;	
			LenghtOfShortestPath:= Min(LenghtOfShortestPath,LenghtOfShortestPath(ax-1,ay,bx,by));
		END;
		//right
		IF(ax+1 <= High(feld)) and (feld[ax+1,ay] = '.') and (tmp1[ax+1,ay] > (tmp1[ax,ay]+1)) THEN BEGIN
			tmp1[ax+1,ay]:=tmp1[ax,ay]+1;	
			LenghtOfShortestPath:= Min(LenghtOfShortestPath,LenghtOfShortestPath(ax+1,ay,bx,by));
		END;
	END;
END; (* LenghtOfShortestPath *)

(*Print Field*)
PROCEDURE PrintField();
VAR
	i,k:Integer;
BEGIN
	FOR i:= Low(feld) TO High(feld) DO BEGIN
			FOR k:=Low(feld[i]) TO High(feld[i]) DO BEGIN
				Write(feld[i,k]);
			END;
			WriteLn();
		END;
END; (* PrintField *)

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	InitField();	
	PrintField();
	WriteLn('TEST Path Exists');
	ClearMemory();WriteLn('PathExists [0,0] -> [0,0] : Exprected = TRUE - Result = ',PathExists(0,0,0,0));
	ClearMemory();WriteLn('PathExists [0,0] -> [1,1] : Exprected = TRUE - Result = ',PathExists(0,0,1,1));
	ClearMemory();WriteLn('PathExists [0,0] -> [10,7] : Exprected = FALSE - Result = ',PathExists(0,0,10,7));
	ClearMemory();WriteLn('PathExists [0,0] -> [2,3] : Exprected = FALSE - Result = ',PathExists(0,0,2,3));
	ClearMemory();WriteLn('PathExists [0,0] -> [15,19] : Exprected = TRUE - Result = ',PathExists(0,0,15,19));

	WriteLn('TEST Lenght of Shortest Path');
	ClearMemory();WriteLn('LenghtOfShortestPath [0,0] -> [0,0] : Exprected = 0 - Result = ',LenghtOfShortestPath(0,0,0,0));
	ClearMemory();WriteLn('LenghtOfShortestPath [0,0] -> [1,1] : Exprected = 2 - Result = ',LenghtOfShortestPath(0,0,1,1));
	ClearMemory();WriteLn('LenghtOfShortestPath [0,0] -> [10,7] : Exprected = -1 - Result = ',LenghtOfShortestPath(0,0,10,7));
	ClearMemory();WriteLn('LenghtOfShortestPath [0,0] -> [2,3] : Exprected = -1 - Result = ',LenghtOfShortestPath(0,0,2,3));
	ClearMemory();WriteLn('LenghtOfShortestPath [0,0] -> [15,19] : Exprected = 34 - Result = ',LenghtOfShortestPath(0,0,15,19));

END. (* ErreichbarkeitvonFelder *)