﻿#include "GL/glew.h"
#include "GL/freeglut.h"
#include <iostream>
#include <array>
#include <math.h>
using namespace std;

void setY(int value);
void getRequestedIndex(int& returnX, int &returnZ, float x, float z);	//Gibt Index im ItemArray zurück wo sich x,z befindet
void checkPosition(float x, float z);	//Position überprüfen ob Bewegung in Richtung möglich ist
void checkForItem();	//Überprüfen ob Item an Position ist
void drawShape_1();		//Shape 1 zeichnen
void drawShape_2();		//Shape 2 zeichnen
void drawShape_3();		//Shape 3 zeichnen
void drawShape_4();		//Shape 4 Zeichnen -> Grundfläche Sechseck
void drawLabyrinth();	//Labyrinth zeichnen

// GLUT Window ID
int windowid;
float step = 0.05;
float m_itemSize = 0.5f;
float m_currX, m_currY, m_currZ = 0.0f;
bool doInitialize = true;	//In drawLabyrinth wird das ItemData Array gefüllt und dass darf logischerweise nur 1 mal passieren 
							//-> wird nach 1 aufruf von drawLabyrinth auf false gesetzt
float m_rotateAngle = 0.0;		// Winkel in Grad zum Rotieren 
float m_currentMouseX = 0.0;	// Mausposition zur Berechnung des Winkels
bool mouseDown = false;			// Winkel darf sich nur ändern wenn Mouse Down
float offsetZ = 3.0f;			// Offset in z Richtung zum Zeichnen des Labyrinths (damit es etwas weiter hinten gezeichnet wird)

/*-[Keyboard Callback]-------------------------------------------------------*/
void keyboard(unsigned char key, int x, int y) {
	
	//Winkel berechnen für Bewegungen
	//*100 um hunderdstel Grad Bewegungen zu berücksichtigen
	// / 57.3 um in Radianten umzurechnen
	float useAngle = ((int)(m_rotateAngle * 100 + 36000)) % 36000 / 100/57.3; 
	switch (key) {
	case 'a': // lowercase character 'a'
		checkPosition(m_currX+ cos(useAngle)*step,m_currZ + sin(useAngle)*step);
		break;
	case 'd': // lowercase character 'd'
		checkPosition(m_currX - cos(useAngle)*step, m_currZ - sin(useAngle)*step);
		break;
	case 'w': // lowercase character 'w' 
		checkPosition(m_currX - sin(useAngle)*step, m_currZ + cos(useAngle)*step);
		break;
	case 's': // lowercase character 's'
		checkPosition(m_currX + sin(useAngle)*step, m_currZ- cos(useAngle)*step);
		break;
	case 'f': // take item
		checkForItem();
		break;
	case 27: // Escape key
		glutDestroyWindow(windowid);
		exit(0);
		break;
	case 32: // Space key -- jump	
		m_currY -= 3 * step;
		glutTimerFunc(200, setY, 0);
		break;
	}
	glutPostRedisplay();
}

/*-[MouseClick Callback]-----------------------------------------------------*/
void onMouseMove(int x, int y) {

	//Winkel ändern um 2 Grad
	if (m_currentMouseX > x)
		m_rotateAngle -= 2;
	else
		m_rotateAngle+= 2;

	m_currentMouseX = x;
	glutPostRedisplay();

}
/*-[MouseClick Callback]-----------------------------------------------------*/
void onMouseClick(int button, int state, int x, int y) 
{
	//Winkel darf sich nur ändern wenn Maus gedrückt ist
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		if(mouseDown == false)
			m_currentMouseX = x;
		mouseDown = true;
	}
	else
		mouseDown = false;
}

void setY(int value) // jump reset function
{
	m_currY = 0;
	glutPostRedisplay();
}

//Typen für Items
enum ItemType {
	Free,
	Wall,
	Item
};

//Typ für Itemdata um
struct Itemdata {
	float x;
	float y;
	float z;
};
ItemType m_data[5][5] = {
	{Wall,Item,Wall,Wall,Wall},
	{ Wall,Free,Item,Free,Wall},
	{ Wall,Wall,Wall,Free,Wall},
	{ Wall,Item,Item,Free,Wall},
	{ Wall,Wall,Wall,Wall,Wall}
};

Itemdata m_itemdata[5][5];

void renderScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	drawShape_1();
	drawShape_2();
	drawShape_3();
	drawShape_4();
	
	glutSwapBuffers();
}

void drawShape_1() 
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(m_rotateAngle, 0, 1, 0);
	glTranslated(m_currX, m_currY, m_currZ);
	glPushMatrix();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glTranslatef(0, 0, -2.4);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_POLYGON);
	glVertex3f(0.25f, 0.1f, 0.20f);
	glVertex3f(0.0f, 0.0f, 0.28f);
	glVertex3f(-0.25f, 0.3f, 0.3f);
	glVertex3f(0.0f, 0.4f, 0.28f);
	glEnd();
	glColor3f(0.0f, 1.0f, 1.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(0.25f, 0.1f, 0.20f);
	glVertex3f(-0.25f, 0.3f, 0.3f);
	glVertex3f(0.0f, 0.0f, 0.28f);
	glFlush();
	glEnd();
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(0.25f, 0.1f, 0.20f);
	glVertex3f(-0.25f, 0.3f, 0.3f);
	glVertex3f(0.0f, 0.4f, 0.28f);
	glEnd();
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glPopMatrix();
	glPushMatrix();
	glRotatef(m_rotateAngle, 0, 1, 0);
	glTranslated(m_currX, m_currY, m_currZ);
	glPushMatrix();
}

void drawShape_2()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(m_rotateAngle, 0, 1, 0);
	glTranslated(m_currX, m_currY, m_currZ);
	glPushMatrix();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glTranslatef(2, -0.4, -2.4);
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(-0.25f, 0.0f, 0.0f);
	glVertex3f(0.25f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, -0.5f);
	glEnd();
	glColor3f(0.4f, 0.3f, 0.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(-0.25f, 0.0f, 0.0f);
	glVertex3f(0.25f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.5f, -0.25f);
	glEnd();
	glColor3f(1.0f, 0.0f, 1.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0f, 0.5f, -0.25f);
	glVertex3f(0.25f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, -0.5f);
	glEnd();
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(-0.25f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.5f, -0.25f);
	glVertex3f(0.0f, 0.0f, -0.5f);
	glEnd();
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glPopMatrix();
	glPushMatrix();
}

void drawShape_3()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(m_rotateAngle, 0, 1, 0);
	glTranslated(m_currX, m_currY, m_currZ);
	glPushMatrix();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glTranslatef(0.0, -0.8, -2);
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_POLYGON);
	glVertex3f(-0.25f, 0.0f, 0.0f);
	glVertex3f(0.25f, 0.0f, 0.0f);
	glVertex3f(0.25f, 0.0f, -0.5f);
	glVertex3f(-0.25f, 0.0f, -0.5f);
	glEnd();
	glColor3f(0.4f, 0.3f, 0.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(-0.25f, 0.0f, 0.0f);
	glVertex3f(0.25f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.5f, -0.25f);
	glEnd();
	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(-0.25f, 0.0f, -0.5f);
	glVertex3f(0.25f, 0.0f, -0.5f);
	glVertex3f(0.0f, 0.5f, -0.25f);
	glEnd();
	glColor3f(1.0f, 0.0f, 1.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0f, 0.5f, -0.25f);
	glVertex3f(0.25f, 0.0f, 0.0f);
	glVertex3f(0.25f, 0.0f, -0.5f);
	glEnd();
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(-0.25f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.5f, -0.25f);
	glVertex3f(-0.25f, 0.0f, -0.5f);
	glEnd();
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glPopMatrix();
	glPushMatrix();
}

void drawShape_4()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(m_rotateAngle, 0, 1, 0);
	glTranslated(m_currX, m_currY, m_currZ);
	glPushMatrix();
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glTranslatef(0.0, -0.3, -2);
	glColor3f(0.0f, 0.0f, 1.0f);

	glBegin(GL_POLYGON);
	glVertex3f(-0.15f, 0.0f, 0.26f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(-0.15f, 0.0f, -0.26f);
	glVertex3f(0.15f, 0.0f, -0.26f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.26f);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(-0.15f, 0.25f, 0.26f);
	glVertex3f(-0.3f, 0.25f, 0.0f);
	glVertex3f(-0.15f, 0.25f, -0.26f);
	glVertex3f(0.15f, 0.25f, -0.26f);
	glVertex3f(0.3f, 0.25f, 0.0f);
	glVertex3f(0.15f, 0.25f, 0.26f);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(-0.15f, 0.0f, -0.26f);
	glVertex3f(0.15f, 0.0f, -0.26f);
	glVertex3f(0.15f, 0.25f, -0.26f);
	glVertex3f(-0.15f, 0.25f, -0.26f);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(-0.15f, 0.0f, 0.26f);
	glVertex3f(0.15f, 0.0f, 0.26f);
	glVertex3f(0.15f, 0.25f, 0.26f);
	glVertex3f(-0.15f, 0.25f, 0.26f);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(-0.15f, 0.0f, 0.26f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(-0.3f, 0.25f, 0.0f);
	glVertex3f(-0.15f, 0.25f, 0.26f);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(0.15f, 0.0f, 0.26f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.25f, 0.0f);
	glVertex3f(0.15f, 0.25f, 0.26f);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(0.15f, 0.0f, -0.26f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.25f, 0.0f);
	glVertex3f(0.15f, 0.25f, -0.26f);
	glEnd();
	glBegin(GL_POLYGON);
	glVertex3f(-0.15f, 0.0f, -0.26f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(-0.3f, 0.25f, 0.0f);
	glVertex3f(-0.15f, 0.25f, -0.26f);
	glEnd();
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glPopMatrix();
	glPushMatrix();
}

void drawLabyrinth()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(m_rotateAngle, 0, 1, 0);
	glTranslated(m_currX, m_currY, m_currZ);
	glPushMatrix();
	for (int i = 0; i < 5; ++i) {
		for (int j = 0; j < 5; ++j) {

			glTranslatef(j * m_itemSize, 0.0f, -i * m_itemSize - offsetZ);
			if (doInitialize)
			{
				m_itemdata[i][j].x = m_currX + j * m_itemSize;
				m_itemdata[i][j].y = m_currY + 0;
				m_itemdata[i][j].z = m_currZ + -i * m_itemSize - offsetZ;

				if (i == 4 && j == 4)
					doInitialize = false;
			}
			switch (m_data[i][j])
			{
			case Free:
				//Do nothing
				break;
			case Wall:
				glColor3f(0.0f, 1.0f, 0.0f);
				glutSolidCube(m_itemSize);
				break;
			case Item:
				glColor3f(0.0f, 0.0f, 1.0f);
				glutSolidSphere(m_itemSize / 8, 50, 50);
				break;
			default:
				break;
			}
			glPopMatrix();
			glPushMatrix();
			glRotatef(m_rotateAngle, 0, 1, 0);
			glTranslated(m_currX, m_currY, m_currZ);
			glPushMatrix();
		}
	}
}

void checkForItem() //Überprüfung ob Item an Position ist
{
	int foundXIDx = -1;							//X Index der überprüfenden Position im Array
	int foundZIDx = -1;							//Z Index der überprüfenden Position im Array

	getRequestedIndex(*&foundXIDx, *&foundZIDx, m_currX, m_currZ);
	if (foundXIDx != -1 && foundZIDx != -1 && m_data[foundZIDx][foundXIDx] == Item)
	{
		m_data[foundZIDx][foundXIDx] = Free;
	}
}

void checkPosition(float x, float z)//Position überprüfen ob Bewegung in Richtung möglich ist
{
	int foundXIDx = -1;							//X Index der überprüfenden Position im Array
	int foundZIDx = -1;							//Z Index der überprüfenden Position im Array

	getRequestedIndex(*&foundXIDx, *&foundZIDx, x, z);

	if (!(foundXIDx != -1 && foundZIDx != -1 && m_data[foundZIDx][foundXIDx] == Wall)) //Wenn Bewegung in X und Z Richtung möglich ist bewegen
	{
		m_currX = x;
		m_currZ = z;
	}

}

void getRequestedIndex(int& returnX, int &returnZ, float x, float z) //Gibt Index im ItemArray zurück wo sich x,z befindet
{
	int foundXIDx = -1;							//X Index der überprüfenden Position im Array
	int foundZIDx = -1;							//Z Index der überprüfenden Position im Array
	float useX = (x - m_itemSize / 2)* -1.0;	//zu überprüfende X Position
	float useZ = z* -1.0;						//zu überprüfende Z Position
	for (int i = 0; i < 5; ++i)
	{
		for (int k = 0; k < 5; ++k)
		{
			if (useX >= m_itemdata[i][k].x && useX < m_itemdata[i][k].x + m_itemSize) //X Position überprüfen
			{
				returnX = k;
			}

			if (useZ <= m_itemdata[i][k].z + m_itemSize && useZ > m_itemdata[i][k].z) //Z Position überprüfen
			{
				returnZ = i;
			}
		}

	}
}

/*-[Reshape Callback]--------------------------------------------------------*/
void reshapeFunc(int x, int y) {
	if (y == 0 || x == 0) return;  //Nothing is visible then, so return

	glMatrixMode(GL_PROJECTION); //Set a new projection matrix
	glLoadIdentity();
	//Angle of view: 40 degrees
	//Near clipping plane distance: 0.5
	//Far clipping plane distance: 20.0

	gluPerspective(40.0, (GLdouble)x / (GLdouble)y, 0.5, 20.0);
	glViewport(0, 0, x, y);  //Use the whole window for rendering
							 //glTranslated(currx, curry, currz);
							 //glViewport(0, 0, x/2, y/2);  //Use the whole window for rendering
							 //glOrtho(-1.0 * (GLdouble)x / (GLdouble)y, (GLdouble)x / (GLdouble)y, -1.0, 1.0, 0.5, 20.0);
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowPosition(50, 50); //determines the initial position of the window
	glutInitWindowSize(1000, 600); //determines the size of the window
	windowid = glutCreateWindow("Our Second OpenGL Window"); // create and name window

															 // register callbacks
	glutKeyboardFunc(keyboard);
	glutMouseFunc(onMouseClick);
	glutMotionFunc(onMouseMove);
	glutReshapeFunc(reshapeFunc);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glutDisplayFunc(renderScene);
	glutMainLoop(); // start the main loop of GLUT
	return 0;
}