unit TreeParser;

INTERFACE
	TYPE
	 Node = ^NodeRec;
        NodeRec = RECORD
            value: STRING; (* operator or operand in textual representation *)
            left, right: Node;
        END;
(*PROCEDURE InitParser(inp: String);
FUNCTION Parse(Var t: Node) : BOOLEAN;*)
PROCEDURE Parse(inp: STRING; VAR ok: BOOLEAN; VAR tree: Node);

PROCEDURE WriteTreePrefix(t: Node);
PROCEDURE WriteTreePostfix(t: Node);
PROCEDURE WriteTreeInfix(t: Node);
IMPLEMENTATION
USES Scanner;

VAR
	success : Boolean;

PROCEDURE Expr(Var t: Node); FORWARD;
PROCEDURE KONJ(Var t: Node); FORWARD;
PROCEDURE KOND(Var t: Node); FORWARD;
PROCEDURE NEG(Var t: Node); Forward;
PROCEDURE DIS(Var t: Node); FORWARD;


PROCEDURE Expr(Var t: Node);
VAR
	left,right : Node;
begin
	KOND(t); IF NOT success THEN BEGIN Exit; END;
	While CurrentSymbol = bikoSym DO
	BEGIN	
		GetNextSymbol;
		{sem}left := t;{endSem}
		KOND(t); IF NOT success THEN BEGIN Exit; END;
		{sem}right := t;
		New(t);
		t^.right := right;
		t^.left := left;
		t^.value := 'eq';{endSem}
	END;
END;

PROCEDURE  KOND(Var t: Node);
VAR
	left,right: Node;
BEGIN
	DIS(t); IF NOT success THEN BEGIN Exit; END;
	While CurrentSymbol = kondSym DO
	BEGIN
		GetNextSymbol;
		{sem}left := t;{endSem}
		DIS(t); IF NOT success THEN BEGIN Exit; END;
		{sem}right := t;
		New(t);
		t^.right := right;
		t^.left := left;
		t^.value := 'imp';{endSem}
	END;
END;

PROCEDURE DIS(Var t: Node);
VAR
	left,right : Node;
BEGIN
	KONJ(t); IF NOT success THEN BEGIN Exit; END;
	WHILE CurrentSymbol = disSym  DO 
	BEGIN
		GetNextSymbol;
		{sem}left := t;{endSem}
		KONJ(t); IF NOT success THEN BEGIN Exit; END;
		{sem}right := t;
		New(t);
		t^.right := right;
		t^.left := left;
		t^.value := 'or';{endSem}
	END;
END;

PROCEDURE KONJ(Var t : Node);
VAR
	left,right : Node;
BEGIN
	NEG(t); IF NOT success THEN BEGIN Exit; END;
	While CurrentSymbol = konjSym DO
	BEGIN
		GetNextSymbol;
		{sem}left := t;{endSem}
		NEG(t); IF NOT success THEN BEGIN Exit; END;
		{sem}right := t;
		New(t);
		t^.right := right;
		t^.left := left;
		t^.value := 'and';{endSem}	
	END;
END;

PROCEDURE NEG(Var t: Node);
VAR left : Node;
BEGIN
	left := NIL;
	IF CurrentSymbol = negSym THEN
	BEGIN
		{sem}New(t);
		t^.right := NIL;
		t^.left := NIL;
		t^.value := 'not';
		left := t;{endSem}
		GetNextSymbol;
	END;

	IF CurrentSymbol = exprSym THEN
	BEGIN
		{sem}New(t);
		t^.right := NIL;
		t^.left := NIL;
		t^.value := ValueForCurrentBoolSym;

		IF left <> NIL THEN
		BEGIN
			left^.left := t;
			t := left;
		END;{endSem}
		GetNextSymbol;
	END ELSE
	BEGIN
		IF CurrentSymbol = leftParSym THEN
		BEGIN
			GetNextSymbol;
			Expr(t); IF NOT success THEN BEGIN Exit; END;
			IF CurrentSymbol = rightParSym THEN
			BEGIN
				GetNextSymbol;
			END ELSE BEGIN  EXIT; END;
		END ELSE BEGIN EXIT; END;
	END;
END;

PROCEDURE Parse(inp: STRING; VAR ok: BOOLEAN; VAR tree: Node);
    BEGIN
        success := TRUE;
        InitScanner(inp);
        Expr(tree);
        ok := success and (CurrentSymbol = endSym);
    END; 

PROCEDURE WriteTreePrefix(t: Node);
BEGIN
	IF(t <> NIL) THEN
	BEGIN
		Write(t^.value);
		Write(' ');
		WriteTreePrefix(t^.left);
		WriteTreePrefix(t^.right);
	END;
END;

PROCEDURE WriteTreePostfix(t: Node);
BEGIN
	IF(t <> NIL) THEN
	BEGIN
		WriteTreePrefix(t^.left);
		WriteTreePrefix(t^.right);
		Write(t^.value);
		Write(' ');		
	END;
END;

PROCEDURE WriteTreeInfix(t: Node);
BEGIN
	IF(t <> NIL) THEN
	BEGIN
		WriteTreePrefix(t^.left);
		Write(t^.value);
		Write(' ');	
		WriteTreePrefix(t^.right);
	END;
END;
end.