/*
 * Extended_Simulator.h
 *
 *  Created on: 07.03.2019
 *      Author: Lukas Schaubmayr
 */

#ifndef EXTENDED_SIMULATOR_H_
#define EXTENDED_SIMULATOR_H_

#include <stdlib.h>
#include "Base_Simulator.h"

class Extended_Simulator : public Base_Simulator {
public:
	Extended_Simulator(){}
	virtual ~Extended_Simulator(){}
	void step() override;

private:
	std::vector<int> m_data;
};

#endif /* EXTENDED_SIMULATOR_H_ */
