
--Erstellen Sie eine Liste aller Schauspieler, Verketten Sie Vor- und Nachname (getrennt durch ein Leerzeichen) und nennen Sie die neue Spalte "Name". Sortieren Sie das Ergebnis nach dem Nachnamen.


SELECT first_name || ' ' || last_name AS Name
FROM actor
ORDER BY last_name;

--Geben Sie eine Liste mit Titel und L�nge all jener Filme aus, die l�nger als 120 Minuten dauern.

SELECT title, length
FROM film
WHERE length > 120;



--Geben Sie eine Liste mit Filmtitel aus, deren Namen an vierter Stelle ein 'A' enth�lt, geben Sie die Titel so aus, dass jeweils der erste Buchstabe eines Wortes mit einem Gro�buchstaben beginnt (zB Atlantis Cause).
SELECT INITCAP(title)
FROM film
WHERE title LIKE '___A%';



--Geben Sie den Titel jener Filme aus, bei denen eine Original-Sprache eingetragen ist.

SELECT title
FROM film
WHERE original_language_id IS NOT NULL;	
-- alternativ:
SELECT title
FROM film f 
   INNER JOIN language l ON (f.original_language_id = l.language_id);

--Geben Sie alle verschiedenen Ratings der Filme aus, ohne numerischen Teil und Bindestrich; geben Sie jedes Rating nur einmal aus.

SELECT DISTINCT REGEXP_SUBSTR(rating, '[^0-9-]+')
FROM film;

--Geben Sie die Anzahl der verliehenen Filme zwischen 1.1.2014 und 31.12.2014 aus.

SELECT COUNT(*) AS AnzahlFilmeVerliehen
FROM rental
WHERE rental_date BETWEEN to_date('01.01.2014', 'dd.mm.yyyy') AND to_date('31.12.2014', 'dd.mm.yyyy');
-- oder
SELECT COUNT(*) AS AnzahlFilmeVerliehen
FROM rental
WHERE EXTRACT(year FROM rental_date) = '2014';

--Geben Sie alle Inventar-Ids aus, die noch nie verliehen wurden.

SELECT inventory_id
FROM inventory
MINUS 
SELECT inventory_id
FROM rental;

--Erstellen Sie eine Liste mit Kunden (Vor- und Nachname), die in Memphis, Aurora und London wohnen. 

SELECT first_name, last_name, city
FROM customer
  INNER JOIN address USING (address_id)
  INNER JOIN city USING (city_id)
WHERE city IN ('Memphis', 'Aurora', 'London');


--Geben Sie den durchschnittlichen Leihpreis pro Filmkategorie aus (sollte ein Film zu mehreren Kategorien geh�ren, z�hlt er zu allen). Runden Sie auf zwei Nachkommastellen.

SELECT name, ROUND(AVG(rental_rate),2)
FROM category
   INNER JOIN film_category USING (category_id)
   INNER JOIN film USING (film_id)
GROUP BY name;
   
 
--Geben Sie die Titel aller Filme aus, die l�nger dauern als der Film mit der ID 21 und deren Ersetzungskosten gr��er sind als der Film mit der ID 30.

SELECT title
FROM film
WHERE length > (SELECT length
                FROM film
                WHERE film_id = 21)
  AND replacement_cost > (SELECT replacement_cost
                          FROM film
                          WHERE film_id = 30);

--Geben Sie die Titel aller Filme aus, die l�nger als 180 Minuten dauern und in den gleichen Kategorien spielen als die Filme mit den Ids 10, 20 oder 30.

SELECT title
FROM film f
   INNER JOIN film_category USING (film_id)
WHERE length > 180
  AND category_id IN (SELECT category_id
                      FROM film_category
                      WHERE film_id IN (10,20,30));

--Erstellen Sie eine Liste aus Schauspieler (Vor- und Nachname) und Anzahl der Filme in denen sie mitspielen, die Liste soll nur jene Schauspieler enthalten, die in mehr als 20 Filmen mitspielen.

SELECT first_name, last_name, COUNT(film_id)
FROM actor
   INNER JOIN film_actor USING (actor_id)
GROUP BY first_name, last_name
HAVING COUNT(film_id) > 20;

--Erstellen Sie eine Liste mit den Titeln und der L�nge jener Filme, die l�nger als der Durchschnitt sind.

SELECT title, length
FROM film
WHERE length > (SELECT AVG(length)
                FROM film);

--Ermitteln Sie die Namen jener Filmkategorien zu denen weniger als 60 Filme geh�ren.

SELECT name
FROM category
   INNER JOIN film_category USING (category_id)
GROUP BY name
HAVING COUNT(*) < 60;

--Ermitteln Sie alle Filme, die die l�ngsten in ihrem Erscheinungsjahr sind, geben Sie Titel, Dauer und Erscheinungsjahr aus.

SELECT title, length, release_year
FROM film f1
WHERE length = (SELECT MAX(length)
                FROM film f2
                WHERE f1.release_year = f2.release_year);		

 
--Geben Sie die zehn zuletzt verliehenen Filme und das Verleihdatum im Format YYYY-MM-DD aus.

SELECT title, to_char(rental_date, 'yyyy-mm-dd')
FROM (
	SELECT title, rental_date
	FROM rental
	   INNER JOIN inventory USING (inventory_id)
	   INNER JOIN film USING (film_id)
	ORDER BY rental_date DESC
)
WHERE ROWNUM <= 20;	


--Erstellen Sie eine neue Tabelle "new_film", diese soll den gleichen Aufbau wie die Tabelle "film" haben, jedoch nur die neusten Filme (Erscheinungsjahr) enthalten.

CREATE TABLE new_film AS
SELECT *
FROM film
WHERE release_year = (SELECT MAX(release_year)
                      FROM film);
					  
--F�gen Sie den Film "Jason Bourne" (2016) in Englisch mit 5 Tagen Verleihdauer (zum Preis von 1,79) mit Ersetzungskosten von 16,99 in die Tabelle ein.

INSERT INTO new_film (film_id, title, language_id, rental_duration, rental_rate, replacement_cost, last_update)
VALUES (1001, 'Jason Bourne', 1, 5, 1.79, 16.99, SYSDATE);

--Erh�hen Sie den Leihpreis der Filme in der Tabelle �new_film� um 10%, wenn der Leihpreis kleiner als 2 ist.
UPDATE new_film					  
   SET rental_rate = rental_rate * 1.1
WHERE rental_rate < 2;

--Erstellen Sie eine View, die alle Filme der Tabelle "new_film" enth�lt, deren Leihpreis maximal 2 betr�gt, vergeben Sie die Check-Option. Die View soll nur den Filmtitel, die Beschreibung, den Leihpreis und die L�nge enthalten.

CREATE VIEW cheap_film AS
SELECT title, description, rental_rate, length
FROM new_film
WHERE rental_rate <= 2
WITH CHECK OPTION;

--Welche Auswirkungen haben COMMIT und ROLLBACK an dieser Stelle (nachdem Sie die View erstellt haben).

-- keine Auswirkung, da DDL-Statement implizites COMMIT enth�lt

--K�nnen Sie die Datens�tze Ihrer neuen View ver�ndern? Wenn ja, f�hren Sie die Erh�hung der Filme ein weiteres Mal, diesmal auf Ihre View durch (10%). Wenn nein, warum nicht? 

UPDATE cheap_film					  
   SET rental_rate = rental_rate * 1.1
WHERE rental_rate < 2;
-- ja, die Datens�tze sind generell ver�nderbar, jedoch zieht bei diesem Update die Check-Option, dh. Filme w�rden aus der View rausfallen, Statmenent schl�gt fehl.

--L�schen Sie alle Eintr�ge aus der Tabelle new_film, deren Leihpreis �ber 1.79 liegt. 

DELETE FROM new_film
WHERE rental_rate > 1.79;

--K�nnen Sie den Leihpreis der Filme in der View nun erh�hen? Erkl�ren Sie dieses Verhalten.

UPDATE cheap_film					  
   SET rental_rate = rental_rate * 1.1
WHERE rental_rate < 2;
-- es gibt durch das Update keinen Datensatz mehr, der aus der View f�llt
	
--	L�schen Sie die Tabelle "new_film" und Ihre erstellte View wieder.

DROP VIEW cheap_film;
DROP TABLE new_film;
