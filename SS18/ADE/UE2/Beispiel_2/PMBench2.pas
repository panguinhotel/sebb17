PROGRAM PMBench2;
USES PMBib2;

TYPE 
	TPMProc = PROCEDURE(s, p: STRING; VAR pos: INTEGER; VAR ismatch : Boolean);

VAR 
	s, p: STRING;
	position: INTEGER;
	ismatch: Boolean;
	
PROCEDURE RunTest(pm: TPMProc);
VAR 
	stat: TPMStatistics;
BEGIN
	InitPmStatistics;
	pm(s, p, position,ismatch);
	GetPmStatistics(stat);
	WriteLn(ismatch);
END;

BEGIN
		s:='ABC$';
		p:='ABC$';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(BruteSearchLR);

		s:='AB$';
		p:='ABC$';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '       p und s passen zusammen?  ');
		RunTest(BruteSearchLR);

		s:='ABCD$';
		p:='ABC$';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '     p und s passen zusammen?  ');
		RunTest(BruteSearchLR);

		s:='AXC$';
		p:='A?C$';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(BruteSearchLR);

		s:='$';
		p:='*$';
		Write('Musterkette: ',p,'       Zeichenkette: ',s, '         p und s passen zusammen?  ');
		RunTest(BruteSearchLR);

		s:='AC$';
		p:='A*C$';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '       p und s passen zusammen?  ');
		RunTest(BruteSearchLR);

		s:='AXYZC$';
		p:='A*C$';
		Write('Musterkette: ',p,'     Zeichenkette: ',s, '    p und s passen zusammen?  ');
		RunTest(BruteSearchLR);

		s:='AXCB$';
		p:='A?C?$';
		Write('Musterkette: ',p,'    Zeichenkette: ',s, '     p und s passen zusammen?  ');
		RunTest(BruteSearchLR);

		s:='AXC$';
		p:='A??C$';
		Write('Musterkette: ',p,'    Zeichenkette: ',s, '      p und s passen zusammen?  ');
		RunTest(BruteSearchLR);


		
END.