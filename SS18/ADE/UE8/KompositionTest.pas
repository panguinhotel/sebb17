PROGRAM KompositionTest;

uses
	KompositionUnit;
VAR
	instance1 : EvenQueue;
	instance2 : LimitedStack;
	instance3 : RandomPickBox;
	value : Integer;
	ok : Boolean;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	WriteLn('EvenQueue TESTS');
	WriteLn('Test IsEmpty : Expected = TRUE - Result : ',instance1.IsEmpty);
	instance1.Enqueue(1,ok);
	WriteLn('Test Enqueue : Value = 1 : Expected = FALSE - Result :',ok ); 
	WriteLn('Test IsEmpty : Expected = TRUE - Result : ',instance1.IsEmpty);
	instance1.Enqueue(2,ok);
	WriteLn('Test Enqueue : Value = 2 : Expected = TRUE - Result :',ok ); 
	instance1.Enqueue(30,ok);
	WriteLn('Test Enqueue : Value = 30 : Expected = TRUE - Result :',ok ); 
	instance1.Enqueue(44,ok);
	WriteLn('Test Enqueue : Value = 44 : Expected = TRUE - Result :',ok ); 
	instance1.Enqueue(15,ok);
	WriteLn('Test Enqueue : Value = 15 : Expected = FALSE - Result :',ok ); 
	instance1.Dequeue(30,value,ok);
	WriteLn('Test Dequeue : Expected: 30 - TRUE - Result : ',value,' - ', ok);
	instance1.Dequeue(15,value,ok);
	WriteLn('Test Dequeue : Expected: 15 - FALSE - Result : ',value,' - ', ok);
	WriteLn('Test IsEmpty : Expected = FALSE - Result : ',instance1.IsEmpty);
	WriteLn();
	WriteLn();
	WriteLn('LimitedStack TESTS');
	WriteLn('Init LimitedStack with min = 10 and max = 30 ');instance2.Init(10,30);
	WriteLn('Test IsEmpty : Expected = TRUE - Result : ',instance2.IsEmpty);
	instance2.Push(1,ok);
	WriteLn('Test Push : Value = 1 : Expected = FALSE - Result :',ok ); 
	WriteLn('Test IsEmpty : Expected = TRUE - Result : ',instance2.IsEmpty);
	instance2.Push(2,ok);
	WriteLn('Test Push : Value = 2 : Expected = FALSE - Result :',ok ); 
	instance2.Push(30,ok);
	WriteLn('Test Push : Value = 30 : Expected = TRUE - Result :',ok ); 
	instance2.Push(44,ok);
	WriteLn('Test Push : Value = 44 : Expected = FALSE - Result :',ok ); 
	instance2.Push(15,ok);
	WriteLn('Test Push : Value = 15 : Expected = TRUE - Result :',ok ); 
	instance2.Pop(value,ok);
	WriteLn('Test Pop : Expected: 15 - TRUE - Result : ',value,' - ', ok);
	instance2.Pop(value,ok);
	WriteLn('Test Pop : Expected: 30 - TRUE - Result : ',value,' - ', ok);
	instance2.Pop(value,ok);
	WriteLn('Test Pop : Expected: 0 - FALSE - Result : ',value,' - ', ok);
	WriteLn('Test IsEmpty : Expected = TRUE - Result : ',instance2.IsEmpty);
	WriteLn();
	WriteLn();
	WriteLn('RandomPickBox TESTS');
	instance3.Add(1,ok);
	WriteLn('Test Add : Value = 1 : Expected = TRUE - Result :',ok ); 
	instance3.Add(2,ok);
	WriteLn('Test Add : Value = 2 : Expected = TRUE - Result :',ok );
	instance3.Add(2,ok);
	WriteLn('Test Add : Value = 2 : Expected = FALSE - Result :',ok ); 
	instance3.Add(30,ok);
	WriteLn('Test Add : Value = 30 : Expected = TRUE - Result :',ok ); 
	instance3.Add(44,ok);
	WriteLn('Test Add : Value = 44 : Expected = TRUE - Result :',ok ); 
	instance3.Add(15,ok);
	WriteLn('Test Add : Value = 15 : Expected = TRUE - Result :',ok ); 	
	WriteLn('Test RandomPick : Result : ',instance3.RandomPick);
	WriteLn('Test RandomPick : Result : ',instance3.RandomPick);
	WriteLn('Test RandomPick : Result : ',instance3.RandomPick);


END. (* KompositionTest *)