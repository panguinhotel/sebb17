#include "utils.h"
#include <fstream>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <iostream>
#include <set>
#include <sstream>
#include <map>
#include <iomanip>

#include "entry.h"

using namespace std;

//=============================================================================
// VERSION 1
//=============================================================================
static void version1(int argc, char *argv[]) {
	ifstream fin;
	open_stream(argc,argv,fin);

	using cont_t = vector<string>;
	cont_t words;

	transform(istream_iterator<string>(fin),istream_iterator<string>(),back_inserter(words),normalize);
	fin.close();

	sort(begin(words),end(words));
	cont_t::iterator new_end;
	new_end = unique(begin(words),end(words));
	words.erase(new_end,end(words)); //actually earase eements
	copy(begin(words),end(words),ostream_iterator<string>(cout,", "));
	cout<<endl<< words.size()<<"words" << endl;
}

//=============================================================================
// VERSION 2
//=============================================================================
// == Version 1 mit back_inserter
//=============================================================================
// VERSION 3
//=============================================================================
using entry_t = entry<string, int>;
using entry_set_t = set<entry_t>;

static entry_set_t words;
static int cur_line_nr = 0;

static void add_word(string word) {
	entry_t e(normalize(word));
	// pair<entry_set_type::const_iterator, bool> result_pair
	auto rp = words.insert(e);
	// returns old entry if it exists -> see set::insert() documentation
	// remove const'ness, dereference iterator
	const_cast<entry_t&>(*rp.first).add_value(cur_line_nr);
	// very careful when changing "constants"
	// in this case the sort order is not affected
}

static void version3(int argc, char *argv[]) {
	ifstream fin;
	open_stream(argc, argv, fin);
	string line;
	while(fin.good())
	{
		cur_line_nr++;
		getline(fin,line);
		istringstream line_stream(line);
		for_each(istream_iterator<string>(line_stream),)
			istream_iterator<string>(),
			add_word);
	}


	fin.close(); // or automatic at return

	copy(begin(words),
			end(words),
			ostream_iterator<entry_t>(cout, "\n"));

	cout << endl << words.size() << " words" << endl;
}

//=============================================================================
// VERSION 4
//=============================================================================
using entry_set_type = map<string, set<int>>;
static entry_set_type words_v4;

static void add_word_v4(string word) {
	words_v4[normalize(word)].insert(cur_line_nr);
}

static void version4(int argc, char *argv[]) {
	ifstream fin;
	open_stream(argc, argv, fin);

	string line;
	while (fin.good()) {
		cur_line_nr++;
		getline(fin, line);
		istringstream line_stream(line);
		for_each(
				istream_iterator<string>(line_stream),
				istream_iterator<string>(),
				add_word_v4); // call function with one argument
	}

	fin.close(); // or automatic at return

	for(const auto &entry: words_v4) {
		cout << setw(15) << entry.first
				<< " (" << entry.second.size() << "): ";

		copy(begin(entry.second),
				end(entry.second),
				ostream_iterator<int>(cout, ","));
		cout << endl;
	}

	cout << endl << words_v4.size() << " words" << endl;
}

//=============================================================================
// VERSION 5
//=============================================================================
class word_adder { // ADD
private:
	entry_set_type &words; // obtain external reference!
	int line_nr;
public:
	word_adder(entry_set_type &words, int line_nr) // called directly
: words(words), line_nr(line_nr) {}

	void operator () (const string &word) { // called by for_each internally
		words[normalize(word)].insert(line_nr);
	}
};

static void version5(int argc, char *argv[]) {
	ifstream fin;
	open_stream(argc, argv, fin);

	string line;
	entry_set_type words;
	int cur_line_nr = 0;


	while (fin.good()) {
		cur_line_nr++;
		getline(fin, line);
		istringstream line_iss(line);

		//v1: functor
//		for_each(istream_iterator<string>(line_iss),
//				istream_iterator<string>(),
//				word_adder(words,cur_line_nr));

		//v2: lamda
//		for_each(istream_iterator<string>(line_iss),
//						istream_iterator<string>(),
//						[&words,cur_line_nr](const string &word) {words[normalize(word)].insert(cur_line_nr);});

		//v3: lamda short
			for_each(istream_iterator<string>(line_iss),
							istream_iterator<string>(),
							[&](const string &word) {words[normalize(word)].insert(cur_line_nr);});
	}


	fin.close();


	for(const auto &entry: words) {
		cout << setw(15) << entry.first
				<< " (" << entry.second.size() << "): ";

		copy(begin(entry.second),
				end(entry.second),
				ostream_iterator<int>(cout, ","));
		cout << endl;
	}

	cout << endl << words.size() << " words" << endl;
}

int main(int argc, char *argv[]) {
	//version1(argc, argv);
	//version2(argc, argv);
	//version3(argc, argv);
	//version4(argc, argv);
	version5(argc, argv);

	return 0;
}
