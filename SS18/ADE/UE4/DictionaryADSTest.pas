PROGRAM DictionaryADSTest;

USES
	DictionaryADS;
VAR
	i : Integer;
	value :Integer;
	found :Boolean;

(* -----------------------------------------------------------------------*)
(* ----------------------------- Main ------------------------------------*)
(* -----------------------------------------------------------------------*)
BEGIN
	Init;
	WriteLn('Test : Size : Expected = 0 -> Result = ',Size());
	Init();
	WriteLn('Test : Put  : key: Test , value : 0');Put('Test',0);	
	WriteLn('Test : Put  : key: Hallo , value : 1');Put('Hallo',1);
	WriteLn('Test : Put  : key: Fred , value : 2');Put('Fred',2);
	WriteLn('Test : Put  : key: Affe , value : 3');Put('Affe',3);
	WriteLn('Test : Put  : key: Banane , value : 4');Put('Banane',4);
	WriteLn('Test : Put  : key: Haus , value : 5');Put('Haus',5);
	WriteLn('Test : Put  : key: Tor , value : 6');Put('Tor',6);
	WriteLn('Test : Put  : key: Papier , value : 7');Put('Papier',7);
	WriteLn('Test : Put  : key: Stein , value : 8');Put('Stein',8);
	WriteLn('Test : Put  : key: Schere , value : 9');Put('Schere',9);
	WriteLn('Test : Size : Expected = 10 -> Result = ',Size());
	WriteLn('Test : Put  : key: Fred , value : 11');Put('Fred',11);
	WriteLn('Test : Size : Expected = 10 -> Result = ',Size());
	WriteLn('Test : Put  : key: Erdbeere , value : 11');Put('Erdbeere',11);
	WriteLn('Test : Size : Expected = 11 -> Result = ',Size());
	Get('Banane',value,found);
	WriteLn('Test : Get  : key: Banane  : Expected : value = 4 , found = TRUE -> Result : value = ',value,' found = ',found);
	Get('Erdbeere',value,found);
	WriteLn('Test : Get  : key: Erdbeere  : Expected : value = 11 , found = TRUE -> Result : value = ',value,' found = ',found);
	WriteLn('Test : Put  : key: Erdbeere , value : -5');Put('Erdbeere',-5);
	Get('Erdbeere',value,found);
	WriteLn('Test : Get  : key: Erdbeere  : Expected : value = -5 , found = TRUE -> Result : value = ',value,' found = ',found);
	Get('Schwein',value,found);
	WriteLn('Test : Get  : key: Schwein  : Expected : value = 0 , found = FALSE -> Result : value = ',value,' found = ',found);
	WriteLn('Test : Contains : key: Affe  : Expected : TRUE -> Result = ',Contains('Affe'));
	WriteLn('Test : Contains : key: Stein  : Expected : TRUE -> Result = ',Contains('Stein'));
	WriteLn('Test : Size : Expected = 11 -> Result = ',Size());
	WriteLn('Test : Remove : key : Stein ');
	Remove('Stein');
	WriteLn('Test : Size : Expected = 10 -> Result = ',Size());
	WriteLn('Test : Contains : key: Stein  : Expected : FALSE -> Result = ',Contains('Stein'));
	WriteLn('Test : Contains : key: Hermann  : Expected : FALSE -> Result = ',Contains('Hermann'));
	Clear();
	WriteLN('Test : Clear ');
	WriteLn('Test : Size : Expected = 0 -> Result = ',Size());
	WriteLn('Test : Put  : key: Banane , value : 4');Put('Banane',4);
	WriteLn('Test : Put  : key: Haus , value : 5');Put('Haus',5);
	WriteLn('Test : Size : Expected = 2 -> Result = ',Size());
	Clear();

END. (* DictionaryADSTest *)