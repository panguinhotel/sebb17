const express = require('express');
const exphbs = require('express-handlebars');
const moment = require('moment');

const app = express();

/**
 * Configure body-parser middleware for better working with http body values 
 */
const bodyParser = require('body-parser');
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '5mb'
}));

/**
 * Configure Handlebars JS template engine for express
 */

/** TODO 1 */

/**
 * Register controllers
 */

/** TODO 4 */

/**
 * Register rest api
 */
/** TODO 8 */

/**
 * Register static file access
 */
app.use(express.static(__dirname + '/public'));
app.use('/memes', express.static(__dirname + '/memes'));


/**
 * Start http server
 */
const port = process.env.PORT || 3000;
const server = app.listen(port, function () {
    console.log('Listening on port ' + server.address().port);
});