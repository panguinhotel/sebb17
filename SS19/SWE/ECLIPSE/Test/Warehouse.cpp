#include "Warehouse.h"

Warehouse::Warehouse() {
};

void  Warehouse::addProduct() {
	products++;
};

void Warehouse::buyProduct() {
	products--;
};

bool Warehouse::anythingToBuy() {
	if (products > 0) {
		return true;
	} else {
		return false;
	}
};

bool Warehouse::isFull() {
	if (products >= maxSize) {
		return true;
	} else {
		return false;
	}
}

int Warehouse::countProducts()   {
	return products;
}
