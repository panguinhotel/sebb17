PROGRAM LogExp;

USES LogExpPars;

VAR
    expression : STRING;
    ok: BOOLEAN;
    errorText: STRING;
    errorColumn : INTEGER;
    structureOfExpr : Node;
    

(* ---------------------------------------------------------------------- *)
(* ----------- Writes the tree structure in prefix notation ------------- *)
(* ---------------------------------------------------------------------- *)
PROCEDURE WriteTreeStructurePrefix(treeStructure: Node);
BEGIN
    IF(treeStructure <> NIL) THEN BEGIN
        Write(treeStructure^.value);
        Write('  ');

        WriteTreeStructurePrefix(treeStructure^.left);
        WriteTreeStructurePrefix(treeStructure^.right);
    END;
END;


(* ---------------------------------------------------------------------- *)
(* ----------- Writes the tree structure in infix notation -------------- *)
(* ---------------------------------------------------------------------- *)
PROCEDURE WriteTreeStructureInfix(treeStructure: Node);
BEGIN
    IF(treeStructure <> NIL) THEN BEGIN
        WriteTreeStructureInfix(treeStructure^.left);

        Write(treeStructure^.value);
        Write('  ');

        WriteTreeStructureInfix(treeStructure^.right);
    END;
END;

(* ---------------------------------------------------------------------- *)
(* ----------- Writes the tree structure in postfix notation ------------ *)
(* ---------------------------------------------------------------------- *)
PROCEDURE WriteTreeStructurePostfix(treeStructure: Node);
BEGIN
    IF(treeStructure <> NIL) THEN BEGIN
        WriteTreeStructurePostfix(treeStructure^.left);
        WriteTreeStructurePostfix(treeStructure^.right);

        Write(treeStructure^.value);
        Write('  ');
    END;
END;

(* ---------------------------------------------------------------------- *)
(* ----------- Main program for a logical expression validator ---------- *)
(* ---------------------------------------------------------------------- *)
BEGIN
    WriteLn('Please enter your logic expression:');
    //ReadLn(expression);
    expression := 'true+ (not false => t+ false and true <=>t eq(t+-f)    ) or t and f imp t';
    expression := '(true) eq (false eq true) eq (false eq true)';
    Parse(expression, ok, errorColumn, errorText, structureOfExpr);

    IF ok THEN BEGIN
        WriteLn('The requested logic expression is valid!');

        WriteLn;
        WriteLn;
        WriteLn('Prefix: ');
        WriteTreeStructurePrefix(structureOfExpr);

        WriteLn;
        WriteLn;
        Writeln('Infix: ');
        WriteTreeStructureInfix(structureOfExpr);

        WriteLn;
        WriteLn;
        Writeln('Postfix: ');
        WriteTreeStructurePostfix(structureOfExpr);    
        WriteLn;
        WriteLn;
    END
    ELSE
        WriteLn('Error on validating the logic expression at column ', errorColumn, '. ', errorText);

    WriteLn('Press "Enter" to exit the application.');
    ReadLn();
END. (* LogExp *)