unit VectorUnit;
	
interface
TYPE
	Vector = Object
		PUBLIC
			PROCEDURE Add(value: INTEGER);
			PROCEDURE GetElementAt(position: INTEGER; VAR value: INTEGER; VAR ok: BOOLEAN);
			PROCEDURE RemoveElementAt(position: INTEGER; VAR value: INTEGER; VAR ok: BOOLEAN);
			FUNCTION Size: INTEGER;
			FUNCTION Capacity: INTEGER;
			FUNCTION HasElement(value : Integer) : Boolean;
		PRIVATE
			var myArr: array of Integer;	
END;	
implementation

PROCEDURE Vector.Add(value: INTEGER);
begin
	IF Size = Capacity THEN BEGIN
		SetLength(myArr, Size+1);
	END;
	myArr[Length(myArr)-1] := value;
end;

PROCEDURE Vector.GetElementAt(position: INTEGER; VAR value: INTEGER; VAR ok: BOOLEAN);
begin
	ok := (position >= Low(myArr)) and (position <= High(myArr));
	if ok THEN
		value := myArr[position];
end;

PROCEDURE Vector.RemoveElementAt(position: INTEGER; VAR value: INTEGER; VAR ok: BOOLEAN);
VAR
	i: Integer;
begin
	ok := (position >= Low(myArr)) and (position <= High(myArr));
	if ok THEN BEGIN
		value := myArr[position];
		FOR i:=position TO High(myArr)-1 DO BEGIN
			myArr[i] := myArr[i+1];
		END;
		SetLength(myArr,Length(myArr)-1);
	END;
end;

FUNCTION Vector.Size: INTEGER;
begin
	IF High(myArr) < 0 THEN
		Size := 0
	ELSE Size := High(myArr)+1;
end;

FUNCTION Vector.Capacity: INTEGER;
begin
	IF High(myArr) < 0 THEN
		Capacity := 0
	ELSE Capacity := High(myArr)+1;
end;

FUNCTION Vector.HasElement(value : Integer) : Boolean;
VAR
	i : Integer;
	ok : Boolean;
begin
	ok := FALSE;
	i := Low(myArr);
	While (i <= High(myArr)) and  not ok DO BEGIN
		IF myArr[i] = value THEN
			ok := true;

		i := i+1;
	END;
	HasElement := ok;
end;
	
end.