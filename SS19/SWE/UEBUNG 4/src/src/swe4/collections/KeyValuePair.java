package swe4.collections;

public interface KeyValuePair<K,V> {
	K getKey();
	V getValue();
}
