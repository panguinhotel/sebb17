UNIT PMBib;

INTERFACE 

TYPE 
	TPmStatistics = RECORD
		comps: LONGINT;
	END;

PROCEDURE BruteSearchLR(s, p: STRING; VAR pos: INTEGER);
PROCEDURE BruteSearchRL(s,p : String; VAR pos : Integer);
PROCEDURE GetPMStatistics(VAR stat: TPmStatistics);
PROCEDURE InitPMStatistics;
PROCEDURE KMP(s, p: STRING; VAR pos: INTEGER);
PROCEDURE KMP2(s, p: STRING; VAR pos: INTEGER);
PROCEDURE BoyerMoore(s,p : String; VAR pos : Integer);
PROCEDURE RabinKarp(s,p : String; VAR pos : Integer);

IMPLEMENTATION
CONST 
	MAX_STR_LENGTH = 255;
VAR 
	ps: TPmStatistics;

PROCEDURE InitPMStatistics;
BEGIN
	WITH ps DO BEGIN
		comps := 0;
	END;
END;

PROCEDURE GetPMStatistics(VAR stat: TPmStatistics);
BEGIN
	stat := ps;
END;

FUNCTION EQ(ch1, ch2: CHAR): BOOLEAN;
BEGIN
	EQ := (ch1 = ch2);
	Inc(ps.comps);
END;

PROCEDURE KMP(s, p: STRING; VAR pos: INTEGER);
VAR 
	next: ARRAY[1..MAX_STR_LENGTH] OF INTEGER;
	sLen, pLen, i, j: INTEGER;
	
	PROCEDURE InitNext;
		VAR i, j: INTEGER;
	BEGIN
		i := 1;
		j := 0;
		next[1] := 0;
		WHILE i < pLen DO BEGIN
			IF (j=0) OR EQ(p[i], p[j]) THEN BEGIN
				Inc(i);
				Inc(j);
				next[i] := j;
			END ELSE 
				j := next[j];
			END;
		END;
BEGIN

	sLen := Length(s);
	pLen := Length(p);
	InitNext;
	
	i := 1;
	j := 1;
	REPEAT
		IF (j=0) OR EQ(s[i], p[j]) THEN BEGIN
			Inc(j);
			Inc(i);
		END ELSE
			j := next[j];
	UNTIL (i > sLen) OR (j > pLen);
	
	IF j > pLen THEN BEGIN
		pos := i - pLen;
	END 
	ELSE BEGIN
		pos := 0;
	END;
END;

PROCEDURE KMP2(s, p: STRING; VAR pos: INTEGER);
VAR 
	next: ARRAY[1..MAX_STR_LENGTH] OF INTEGER;
	sLen, pLen, i, j: INTEGER;
	
PROCEDURE InitNextImproved;
	VAR i, j: INTEGER;
BEGIN
	i := 1;
	j := 0;
	next[1] := 0;
	WHILE i < pLen DO BEGIN
		IF (j=0) OR EQ(p[i], p[j]) THEN BEGIN
			Inc(i);
			Inc(j);
			
			IF NOT EQ(p[i], p[j]) THEN
				next[i] := j
			ELSE
				next[i] := next[j]
			END ELSE 
				j := next[j];
	END;
END;
BEGIN

	sLen := Length(s);
	pLen := Length(p);
	InitNextImproved;
	
	i := 1;
	j := 1;
	REPEAT
		IF (j=0) OR EQ(s[i], p[j]) THEN BEGIN
			Inc(j);
			Inc(i);
		END ELSE
			j := next[j];
	UNTIL (i > sLen) OR (j > pLen);
	
	IF j > pLen THEN BEGIN
		pos := i - pLen;
	END 
	ELSE BEGIN
		pos := 0;
	END;
END;

PROCEDURE BruteSearchLR(s, p: STRING; VAR pos: INTEGER);
VAR 
	sLen, pLen, i, j: INTEGER;
BEGIN
	sLen := Length(s);
	pLen := Length(p);
	
	pos := 0;
	i := 1;
	
	WHILE (pos = 0) AND (i + pLen-1 <= sLen) DO BEGIN
		j := 1;
		WHILE (j <= pLen) AND (EQ(s[i+j-1], p[j])) DO BEGIN
			Inc(j);
		END;
		IF j > pLen THEN BEGIN
			pos := i;
		END
		ELSE BEGIN
			Inc(i);
		END;
	END;
END;

(*BoyerMoore*)
PROCEDURE BoyerMoore(s,p : String; VAR pos : Integer);
VAR
	skip : Array[char] of Byte;
	sLen,pLen, i,j : Integer;
	procedure InitSkip;
	VAR
		ch : Char;
		j : Integer;
	begin
		FOR ch:= Low(Char) TO High(Char) DO BEGIN
			skip[ch] := pLen;
		END;
		FOR j := 1 TO pLen DO BEGIN
			skip[p[j]] := pLen -j;
		END;	
	end;
BEGIN
	sLen := Length(s);
	pLen := Length(p);

	InitSkip;
	i:= pLen;
	j:= pLen;

	repeat
		IF EQ(s[i], p[j])  THEN BEGIN
			Dec(i);
			Dec(j);
		END ELSE BEGIN
			i := i + skip[s[i]];
			j := pLen;		
		END;
	until (i > sLen) or (j < 1);

	IF (j < 1)  THEN BEGIN
		pos := i+1;
	END ELSE BEGIN
		pos := 0;
	END;
END; (* BoyerMoore *)

(*Describe me*)
PROCEDURE RabinKarp(s,p : String; VAR pos : Integer);
CONST
	q = 8355967; (*Long Random prime*)
	r = 256;
VAR
	hp : LongInt;
	hs : LongInt;
	rm : LongInt;

	sLen,pLen, i,j : Integer;
	iMax : Integer;
	sPos : Integer;
BEGIN
	sLen := Length(s);
	pLen := Length(p);
	rm := 1;
	pos := 0;

	FOR i := 1 TO pLen -1 DO BEGIN
		rm:=(r*rm) MOD q;
	END;
	 hp := 0;
	 hs := 0;

	 FOR i := 1 TO pLen DO BEGIN
	 	hp := (r*hp + ORD(p[i])) MOD q;
		hs := (r*hs + ORD(s[i])) MOD q;
	 END;
	
	 i := 1;
	 j := 1;
	 iMax := sLen - pLen + 1;

	 
	 WHILE (i <= iMax) AND (j <= pLen)  DO BEGIN
	 	IF hp = hs  THEN BEGIN
			j := 1;
			sPos := i;
			WHILE (j <= pLen) and EQ(p[j], s[sPos])  DO BEGIN
				Inc(j);
				Inc(sPos);
			END;
		END;
		 hs := (hs +q -rm * ORD(s[i]) MOD q) MOD q;
	 	 hs := (hs * r + Ord(s[i+pLen])) MOD q;
		Inc(i);
	 END;
	 IF (i > iMax) AND (j <= pLen)  THEN BEGIN
	 	pos := 0;
	 END
	 ELSE BEGIN
	 	pos := i-1;
	 END;
END; (* RabinKarp *)
(*Describe me*)
PROCEDURE BruteSearchRL(s,p : String; VAR pos : Integer);
VAR 
	sLen, pLen, i, j: INTEGER;
BEGIN
	sLen := Length(s);
	pLen := Length(p);
	
	i := pLen;
	j := pLen;
	repeat
		IF (EQ(s[i],p[j]))  THEN BEGIN
			Dec(i);
			Dec(j);
		END ELSE begin
			i := i + pLen -j +1;
			J := pLen;
		end;
	until (i > sLen) or (j < 1);

	IF (j < 1)  THEN 
		pos := i+1
	ELSE
		pos := 0;
END; (* BruteSearchRL *)

BEGIN
END.