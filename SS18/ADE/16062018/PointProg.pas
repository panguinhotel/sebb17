PROGRAM PointProg;

TYPE
	Point2DPtr = ^Point2D;
	Point2D = OBJECT
		PUBLIC
		PROCEDURE Init(x, y: INTEGER);
		FUNCTION ToString : String;
		// PROCEDURE SetX(x_: INTEGER);
		// PROCEDURE SetY(y_: INTEGER);

		// PROCEDURE GetX : INTEGER;
		// PROCEDURE GetY : INTEGER;

		PRIVATE
			x,y: INTEGER;
	END;

	Point3DPtr = ^Point3D;
	Point3D = OBJECT(Point2D)
		PUBLIC
		PROCEDURE Init(x,y,z: INTEGER);
		FUNCTION ToString : String;

		PRIVATE
			z: INTEGER;
	END;

	PROCEDURE Point2D.Init(x, y: INTEGER);
	BEGIN
		self.x := x;
		self.y := y;
	END;

	FUNCTION Point2D.ToString : String;
	VAR
		xstr, ystr: String;
	BEGIN
		Str(x, xstr);
		Str(y, ystr);
		ToString := '(' + xstr + ',' + ystr + ')';
	END;


	PROCEDURE Point3D.Init(x,y,z: INTEGER);
	BEGIN
		INHERITED Init(x,y);
		self.z := z;
	END;

	FUNCTION Point3D.ToString : String;
	VAR
		xstr, ystr, zStr: String;
	BEGIN
		Str(x, xstr);
		Str(y, ystr);
		Str(z, zStr);
		ToString := '(' + xstr + ',' + ystr + ',' + zstr + ')';

	END;
VAR
	p1,p3 : Point2DPtr;
	p2,p4 : Point3DPtr;
BEGIN
	New(p1);
	p1^.Init(1,2);
	New(p2);
	p2^.Init(66,77,88);

	WriteLn(p1^.ToString);
	WriteLn(p2^.ToString);

	p3 := p2;
	p4 := Point3DPtr(p3);

	WriteLn(p3^.ToString);
	WriteLn(p4^.ToString);
END.
{VAR
	p2d1: Point2D;
	p2d2: Point2D;
	p3d1: Point3D;
	arr : Array [0..1] OF Point2D;
BEGIN
	p2d1.Init(1,2);
	p2d2.Init(4,5);
	p3d1.Init(66,77,88);
	WriteLn(p2d1.ToString);
	WriteLn(p2d2.ToString);
	WriteLn(p3d1.ToString);
	arr[0] := p2d1;
	arr[1] := p3d1;

	WriteLn(arr[0].ToString);
	WriteLn(arr[1].ToString);

END.}