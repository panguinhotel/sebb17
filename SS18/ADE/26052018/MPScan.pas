UNIT MPScan;

INTERFACE
  TYPE Symbol = (noSym,
                 beginSym, endSym,
                 integerSym,
                 programSym, readSym, writeSym, varSym,
                 plusSym,(* + *) minusSym,(* - *) multSym,(* * *)divSym,(* / *)
                 leftParSym, (* ( *) rightParSym, (* ) *)
                 commaSym, colonSym, assignSym, semicolonSym, periodSym,
                 numberSym, identSym);
    
  (*initializes scanner for a given input file *)
  (*REF inputFile: Source file *)
  (*First symbol is automatically scanned from input when InitScanner is called*)
  PROCEDURE InitScanner(VAR inputFile: TEXT);
  
  (* Advances to next symbol *)
  PROCEDURE GetNextSymbol;
  
  (*Gets last scanned symbol *)
  FUNCTION CurrentSymbol: Symbol;
  
  (* Gets start position of last scanned symbol *)
  (* OUT line: line of last scanned symbol*)
  (* OUT column: column of last scanned symbol*)
  PROCEDURE GetCurrentSymbolPosition(VAR line, column: INTEGER);
  
  (* Gets actual value of last scanned number terminal *)
  (*value is only valid when current symbol is numberSym*)
  FUNCTION CurrentNumberValue: LONGINT;
  
  (*Gets actual value of last scanned ident terminal *)
  (*value is only valid when current symbol is identSym *)
  FUNCTION CurrentIdentName: STRING;
  
IMPLEMENTATION
  CONST
    TAB = Chr(9);
    LF = Chr(10);
    CR = Chr(13);
    BLANK = ' ';
  
  VAR
    inpFile: Text;
    curChar: CHAR;
    charLine, charColumn: INTEGER;
    curSymbol: Symbol;
    symbolLine, symbolColumn: INTEGER;
    curNumberValue: LONGINT;
    curIdentName: STRING;
    
  PROCEDURE GetNextChar;
  BEGIN
    Read(inpFile, curChar);
    IF (curChar = CR) OR (curChar = LF) THEN BEGIN
      IF (charColumn > 0) THEN
        Inc(charLine);
      curChar := BLANK;
      charColumn := 0;
    END ELSE
      Inc(charColumn);
  END;
    
  PROCEDURE InitScanner(VAR inputFile: TEXT);
  BEGIN
    inpFile := inputFile;
    charLine := 1;
    charColumn := 0;
    GetNextChar;
    GetNextSymbol;
  END;
    
    
  
 
  
 
  
  PROCEDURE GetNextSymbol;
  BEGIN
    WHILE (curChar = BLANK) OR (curChar = TAB) DO
      GetNextChar;
      
    symbolLine := charLine;
    symbolColumn := charColumn;
    
    CASE curChar OF
      (*ENDOFINPUT: BEGIN curSymbol := endSym; END;*)
      '+': BEGIN curSymbol := plusSym; GetNextChar; END;
      '-': BEGIN curSymbol := minusSym; GetNextChar; END;
      '*': BEGIN curSymbol := multSym; GetNextChar; END;
      '/': BEGIN curSymbol := divSym; GetNextChar; END;
      '(': BEGIN curSymbol := leftParSym; GetNextChar; END;
      ')': BEGIN curSymbol := rightParSym; GetNextChar; END;
      '.': BEGIN curSymbol := periodSym; GetNextChar; END;
      ',': BEGIN curSymbol := commaSym; GetNextChar; END;
      ';': BEGIN curSymbol := semicolonSym; GetNextChar; END;
      ':': BEGIN 
              GetNextChar;
              IF  curChar = '=' THEN BEGIN
                curSymbol := assignSym;
                GetNextChar;
              END ELSE
                curSymbol := colonSym;
           END;
      'a'..'z', 'A'..'Z', '_': BEGIN;
              curIdentName := '';
              WHILE curChar IN [ 'a'..'z', 'A'..'Z', '0'..'9', '_'] DO BEGIN
                curIdentName := curIdentName + UpCase(curChar); 
                GetNextChar;
              END;
              IF curIdentName = 'BEGIN' THEN
                curSymbol := beginSym
              ELSE IF curIdentName = 'END' THEN
                curSymbol := endSym
              ELSE IF curIdentName = 'INTEGER' THEN
                curSymbol := integerSym
              ELSE IF curIdentName = 'PROGRAM' THEN
                curSymbol := programSym
              ELSE IF curIdentName = 'READ' THEN
                curSymbol := readSym
              ELSE IF curIdentName = 'VAR' THEN
                curSymbol := varSym
              ELSE IF curIdentName = 'WRITE' THEN
                curSymbol := writeSym
              ELSE  
                curSymbol := identSym;
            END;
      '0'..'9': BEGIN 
        curSymbol := numberSym;
        curNumberValue := 0;
        WHILE curChar IN ['0'..'9'] DO BEGIN
          curNumberValue := (curNumberValue * 10) + (Ord(curChar) - Ord('0'));
          GetNextChar;
        END; (*WHILE*)
      END
      ELSE curSymbol := noSym;
    END; (*CASE*)
  END; (* GetNextSymbol *)
  
  FUNCTION CurrentSymbol: Symbol;
  BEGIN
    CurrentSymbol := curSymbol;
  END;
  
  PROCEDURE GetCurrentSymbolPosition(VAR line, column: INTEGER);
  BEGIN
    line := symbolLine;
    column := symbolColumn;
  END;
  
  FUNCTION CurrentNumberValue: LONGINT;
  BEGIN
    CurrentNumberValue := curNumberValue;
  END;
  
  FUNCTION CurrentIdentName: STRING;
  BEGIN
    CurrentIdentName := curIdentName;
  END;
  
END.
    